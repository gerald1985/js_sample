var cursor = $(".cursor"),
	follower = $(".follow");

var posX = 0,
	posY = 0;

var mouseX = 0,
	mouseY = 0;

TweenMax.to({}, 0.016, {
	repeat: -1,
	onRepeat: function() {
		posX += (mouseX - posX) / 5;
		posY += (mouseY - posY) / 5;

		TweenMax.set(follower, {
			css: {    
				left: posX - 12,
				top: posY - 12
			}
		});

		TweenMax.set(cursor, {
			css: {    
				left: mouseX,
				top: mouseY
			}
		});
	}
});

$(document).on("mousemove", function(e) {
	mouseX = e.pageX;
	mouseY = e.pageY;
});

$("a, button, input, textarea").on("mouseenter", function() {
	cursor.addClass("active");
	follower.addClass("active");
});
$("a, button, input, textarea").on("mouseleave", function() {
	cursor.removeClass("active");
	follower.removeClass("active");
});

$(".touch").on("mouseenter", function() {
	cursor.addClass("active");
	follower.addClass("active");
});
$(".touch").on("mouseleave", function() {
	cursor.removeClass("active");
	follower.removeClass("active");
});
