﻿var chartConfig = {
    type: 'line',
    data: {
        labels: [],
        datasets: []
    },
    options: {
        legend: {
            display: true
        },
        tooltips: {
            mode: 'index',
            intersect: false
        }
    }
};

function colorize(opaque, ctx) {
    var v = ctx.dataset.data[ctx.dataIndex];
    var c = v < -50 ? '#D60000'
        : v < 0 ? '#F46300'
            : v < 50 ? '#0358B6'
                : '#44DE28';

    return opaque ? c : utils.transparentize(c, 1 - Math.abs(v / 150));
}

var transactionChartConfig = JSON.parse(JSON.stringify(chartConfig));
var servicesChartConfig = JSON.parse(JSON.stringify(chartConfig));

var successfulDataSetConfig = {
    label: 'Exitosas',
    data: [],
    "fill": false,
    "borderColor": "#3399ff",
    "backgroundColor": "#3399ff"
};

var failedDataSetConfig = {
    label: 'Fallidas',
    data: [],
    "fill": false,
    "borderColor": "red",
    "backgroundColor": "red"
};

var transactionCtx = document.getElementById('transaction').getContext('2d');
var servicesCtx = document.getElementById('services').getContext('2d');

var transactionLineChart = new Chart(transactionCtx, transactionChartConfig);
var serviceChart = new Chart(servicesCtx, servicesChartConfig);

function changeChartType(newType, showLeyend) {
    showLeyend = showLeyend || false;
    let temp = JSON.parse(JSON.stringify(chartConfig));
    temp.options.legend.display = showLeyend;
    temp.type = newType;
    if (serviceChart) {
        serviceChart.destroy();
    }
    servicesChartConfig = temp;
    serviceChart = new Chart(servicesCtx, temp);
}
