﻿/**
 * El metodo permite realizar consultas ajax a los controladores
 * @param {any} url Se requiere para realizar la consulta
 * @param {any} data Parametros que puede recibir cada servicio
 * @param {any} type Los diferentes tipos de peticiones GET, POST, PUT, etc
 * @param {any} processData En caso de enviar un FormData este debe establecerse en false, sin embargo por defecto su valor es true
 * @param {any} isAsync En caso de requerir una consulta asincrona esta variable por defecto es true
 * @returns {JQueryStatic} Retorna una consulta con ajax
 */
function execute_ajax(url, data, type, processData = true, isAsync = true) {
    if (processData) return $.ajax({ url: url, type: type, async: isAsync, data: data });
    return $.ajax({ url: url, type: type, async: isAsync, contentType: false, processData: false, cache: false, data: data });
}

/**
 * Presenta una alerta con la cual se consulta al usuario si realmente desea realizar la operacion y evitar acciones accidentales
 * @param {any} title Titulo de el modal
 * @param {any} type Los diferentes tipos son: warning, error, success, info y question. Estas opciones controlan el icono del modal
 * @param {any} confirmBtnTxt El texto que tendra el boton de confirmacion
 * @param {any} html El cuerpo del modal (se acepta codigo html)
 * @param {any} showClose Para mostrar la el boton de cierre del modal en la parte superior derecha
 * @param {any} showCancel Para mostrar o no el boton de cancelar accion
 * @param {any} focusConfirm En caso que se desee establecer el focus sobre el boton de aceptar la accion
 * @param {any} cancelButtonText Texto para el boton cancelar
 * @param {any} callback Funcion para cuando se acepta la accion
 * @param {any} then Funcion para cuando ya se ejecuto o cancelo la accion
 */
function messageLoader(title, type, confirmBtnTxt, html, showClose, showCancel, focusConfirm, cancelButtonText, callback, then) {
    swal.queue([{
        title: '<i>' + title + '</i>', type: type, html: html, showLoaderOnConfirm: true, showCloseButton: showClose, showCancelButton: showCancel,
        focusConfirm: focusConfirm, confirmButtonText: '<i class="fa fa-thumbs-up"></i> ' + confirmBtnTxt, confirmButtonAriaLabel: 'Thumbs up, great!', cancelButtonText: '<i class="fa fa-thumbs-down"> ' + cancelButtonText + '</i>',
        cancelButtonAriaLabel: 'Thumbs down', preConfirm: callback
    }]).then(then === undefined ? function () { } : then).catch(swal.noop);
}

function message(title, text, type, callback) {
    swal(title, text, type).then(callback === undefined ? function () { } : callback);
}

function messageTimer(title, text, type, timer) {
    swal({ title: title, text: text, type: type, timer: timer });
}

function fail_request(error) {
    hideLoader();
    console.warn(error);
    if (error.status === 401) {
        messageLoader('Administrador de transacciones', "info", "Iniciar sesión", "No estas autorizado para realizar esta operación, es posible que tu sesión haya expirado.", true, true, true, "Aceptar", function () {
            window.location.href = "/Admin";
        });
    } else
        message('Administrador de transacciones', `<p class='c_red'>Ocurrió un error inesperado</p>`, 'error');
}

let profile_hover = document.getElementById("profile_hover");
let hover_container = document.getElementById("hover_container");
let profile_image_remove = document.getElementById("profile_image_remove");
let profile_img_content = document.getElementById("profile_img_content");

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if (profile_img_content)
                profile_img_content.setAttribute("src", e.target.result);
            if (profile_hover)
                profile_hover.setAttribute("src", e.target.result);
            if (profile_image_remove)
                profile_image_remove.classList.remove("hide");
        };
        reader.readAsDataURL(input.files[0]);
    }
}

let profile_file_content = document.getElementById("profile_file_content");
if (profile_file_content) {
    profile_file_content.onchange = function () {
        readURL(this);
    };
}

if (profile_image_remove) {
    profile_image_remove.onclick = function () {
        var label = document.getElementById("file_url");
        if (label !== null)
            label.value = "Seleccione una imagen";
        if (profile_img_content !== null)
            profile_img_content.setAttribute('src', `${document.location.origin}/images/pic_default.png`);
        var file = document.getElementById("profile_file_content");
        if (file !== null)
            file.value = null;
        if (profile_hover)
            profile_hover.setAttribute('src', `${document.location.origin}/images/pic_default.png`);
        profile_image_remove.classList.add("hide");
    };
}

let generateBusinessKey = document.getElementById("generateBusinessKey");
if (generateBusinessKey) {
    generateBusinessKey.onclick = function () {
        execute_ajax("/Admin/Business/AccesKeySuggestion", null, "GET").done(function (response) {
            if (response.ResponseCode === 200) {
                document.getElementById("AccessKey").focus();
                document.getElementById("AccessKey").value = response.ResponseText;
            }
        }).fail(fail_request);
    };
}

function cc_format(value) {
    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    var matches = v.match(/\d{4,20}/g);
    var match = matches && matches[0] || '';
    var parts = [];
    for (i = 0, len = match.length; i < len; i += 4) {
        parts.push(match.substring(i, i + 4));
    }
    if (parts.length) {
        return parts.join(' ');
    } else {
        return value;
    }
}

function showLoader() {
    var loader = document.getElementById("loader");
    if (loader) {
        loader.classList.add("visibility_show");
    }
}

function hideLoader() {
    var loader = document.getElementById("loader");
    if (loader) {
        loader.classList.remove("visibility_show");
        loader.classList.add("visibility_hide");
    }
}


function copyToClipboard(str) {
    let textarea;
    let result;

    try {
        textarea = document.createElement('textarea');
        textarea.setAttribute('readonly', true);
        textarea.setAttribute('contenteditable', true);
        textarea.style.position = 'fixed'; // prevent scroll from jumping to the bottom when focus is set.
        textarea.value = str;

        document.body.appendChild(textarea);

        textarea.focus();
        textarea.select();

        const range = document.createRange();
        range.selectNodeContents(textarea);

        const sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);

        textarea.setSelectionRange(0, textarea.value.length);
        result = document.execCommand('copy');
    } catch (err) {
        console.error(err);
        result = null;
    } finally {
        document.body.removeChild(textarea);
    }

    // manual copy fallback using prompt
    if (!result) {
        const isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
        const copyHotkey = isMac ? '⌘C' : 'CTRL+C';
        result = prompt(`Press ${copyHotkey}`, str); // eslint-disable-line no-alert
        if (!result) {
            return false;
        }
    }
    return true;
}

/**
 * 
 * @param {any} merchantID MerchantId CBS
 * @param {any} environment application mode
 * @returns {number} Returns FingerPringId
 */
function cybs_dfprofiler(merchantID, environment = "test") {
    var org_id;
    if (environment.toLowerCase() === 'live') {
        org_id = 'k8vif92e';
    } else {
        org_id = '1snn5n9w';
    }

    var sessionID = new Date().getTime();

    //One-Pixel Image Code
    var paragraphTM = document.createElement("p");
    str = "";
    str = "background:url(https://h.online-metrix.net/fp/clear.png?org_id=" + org_id + "&session_id=" + merchantID + sessionID + "&m=1)";

    paragraphTM.styleSheets = str;

    document.body.appendChild(paragraphTM);

    var img = document.createElement("img");

    str = "https://h.online-metrix.net/fp/clear.png?org_id=" + org_id + "&session_id=" + merchantID + sessionID + "&m=2";

    img.src = str;
    img.alt = "";

    document.body.appendChild(img);

    //Flash Code
    var objectTM = document.createElement("object");

    objectTM.data = "https://h.online-metrix.net/fp/fp.swf?org_id=" + org_id + "&session_id=" + merchantID + sessionID;

    objectTM.type = "application/x-shockwave-flash";
    objectTM.width = "1";
    objectTM.height = "1";
    objectTM.id = "thm_fp";

    var param = document.createElement("param");
    param.name = "movie";

    param.value = "https://h.online-metrix.net/fp/fp.swf?org_id=" + org_id + "&session_id=" + merchantID + sessionID;

    objectTM.appendChild(param);

    document.body.appendChild(objectTM);

    //JavaScript Code
    var tmscript = document.createElement("script");

    tmscript.src = "https://h.online-metrix.net/fp/tags.js?org_id=" + org_id + "&session_id=" + merchantID + sessionID;

    tmscript.type = "text/javascript";

    document.body.appendChild(tmscript);

    return sessionID;
}

function hasNumber(currentValue) {
    return isNaN(currentValue) ? false : /\d/.test(currentValue);
}

function doubleChangeEvent(element, defaultValue = 1.00, toFixed = 2) {
    let value = element.target.value;
    element.target.value = !hasNumber(value) ? Number.parseFloat(defaultValue).toFixed(toFixed) : Number.parseFloat(value).toFixed(toFixed);
}

function getRadioValue(elementName) {
    let radios = document.getElementsByName(elementName);
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return radios[i].value;
        }
    }
}

function getRandomColor(isArray, limit) {
    isArray = isArray || false;
    limit = limit || 0;
    let letters = '0123456789ABCDEF';
    let color = '#';
    let colors = [];
    if (isArray) {
        for (let i = 0; i < limit; i++) {
            for (let c = 0; c < 6; c++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            colors.push(color);
            color = '#';
        }
        return colors;
    }
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}