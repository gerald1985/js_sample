﻿let quotas = document.getElementById("quotas");
let totalAmount = document.getElementById("totalAmount");
let quantity = document.getElementById("Quantity");
let UseSameInfoReceipt = document.getElementById("UseSameInfoReceipt");
let serviceTabs = document.getElementById("tabs-swipe-service");
let City = document.getElementById("City");
var instanceTabs;
$(document).ready(function () {
    instanceTabs = M.Tabs.getInstance(serviceTabs);
    instanceTabs.options.onShow = function () {
        if (instanceTabs.index !== 0)
            validateForm(true);
    };
    UseSameInfoReceiptEvent();
    grecaptcha.ready(reCAPTCHA_execute);
    setInterval(reCAPTCHA_execute, 120000);
    getCountries();
    countrySelection();
    $('.chocolat-parent').Chocolat();
    if (quotas)
        setInterval(updateQuotas, 180000);
});

$("#CVVNumber").keyup(fill_CardInfo);
$("#AccountNumber").keyup(fill_CardInfo);
$("#CardHolder").keyup(fill_CardInfo);
$("#ExpirationMonth").change(fill_CardInfo);
$("#ExpirationYear").change(fill_CardInfo);
let card_type = document.getElementById('card_type');
let card_type_preview = document.getElementById('CardTypePreview');
let card_info = document.getElementById("card_info");
let deviceFingerprintID = document.getElementById("DeviceFingerprintID");
let ipAddress = document.getElementById("IpAddress");
$("#CVVNumber").focusin(function () {
    $(card_info).addClass("flip_active");
});
$("#CVVNumber").focusout(function () {
    $(card_info).removeClass("flip_active");
});
function fill_CardInfo() {
    $("#AccountNumber").val(cc_format($("#AccountNumber").val()));
    let currentAccountNumber = $("#AccountNumber").val();
    if (!currentAccountNumber)
        currentAccountNumber = '';
    if (card_type) {
        switch (currentAccountNumber.charAt(0)) {
            case '5':
            case '2': card_type.setAttribute('src', `/images/card_mastercard.png`);
                card_type_preview.setAttribute('src', `/images/card_mastercard.png`);
                card_type_preview.classList.add("max_w_4");
                break;
            default: card_type.setAttribute('src', `/images/card_visa.png`);
                card_type_preview.setAttribute('src', '/images/card_visa.png');
                card_type_preview.classList.remove("max_w_4");
                break;
        }
    }
    $("#CardHolderPreview").val($("#CardHolder").val());
    $("#AccountNumberPreview").val($("#AccountNumber").val());
    $("#CVVNumberPreview").val($("#CVVNumber").val());
    let expirationMonth = $("#ExpirationMonth").val();
    let expirationYear = $("#ExpirationYear").val();
    $("#ExpirationPreview").val(`${expirationMonth}/${expirationYear}`);
    M.updateTextFields();
}

fill_CardInfo();

let form = document.getElementById("service_PayForm");
if (form) {
    if (deviceFingerprintID) {
        $(deviceFingerprintID).val(cybs_dfprofiler("tc_ni_001339308", "test"));
    }
    if (ipAddress) {
        $.getJSON("https://api.ipify.org?format=json",
            function (json) {
                $(ipAddress).val(json.ip);
            }
        );
    }
    form.onsubmit = function (evt) {
        evt.preventDefault();

        let url = form.action;
        let method = form.method;
        var formData = new FormData();
        let token = document.getElementsByName("__RequestVerificationToken")[0];

        formData.append("RequestToken", hashBase64StringAndReturnBase64String($(this).serialize()));
        formData.append("__RequestVerificationToken", token.value);

        let questionAlert = document.getElementById("questionAlert");
        let successAlert = document.getElementById("successAlert");

        messageLoader("BICheckOut", "question", questionAlert.value, true, true, true, function () {
            return new Promise(function () {
                execute_ajax(url, formData, method, false, true).done(function (res) {
                    if (res.ResponseCode === 200) {
                        window.location.href = "/Home/ThankYouPage/" + res.OrderNumber;
                    } else {
                        message('BICheckOut', `<p class='c_red'>${res.ResponseText}</p>`, 'error');
                    }
                }).fail(fail_request);
            });
        });
    };
}

let btnNext = document.getElementById("btnNext");
if (btnNext) {
    btnNext.onclick = function () {
        if (UseSameInfoReceipt.checked) {
            instanceTabs.select('test-swipe-2');
        } else if (validateForm()) {
            instanceTabs.select('test-swipe-3');
        }
    };
}

let btnPreviousReceipt = document.getElementById("btnPreviousReceipt");
if (btnPreviousReceipt) {
    btnPreviousReceipt.onclick = function () {
        instanceTabs.select('test-swipe-1');
    };
}

let btnNextReceipt = document.getElementById("btnNextReceipt");
if (btnNextReceipt) {
    btnNextReceipt.onclick = function () {
        if (validateForm(true))
            instanceTabs.select('test-swipe-2');
    };
}

let btnPrevious = document.getElementById("btnPrevious");
if (btnPrevious) {
    btnPrevious.onclick = function () {
        instanceTabs.select('test-swipe-1');
    };
}

function reCAPTCHA_execute() {
    grecaptcha.execute('6LcZgJ0UAAAAAAjDAiwso4WnDHqncvJfCfnWjznP', { action: 'e_commerce' }).then(function (token) {
        document.getElementById('Captcha').value = token;
    });
}

function hashBase64StringAndReturnBase64String(str) {
    var key = CryptoJS.enc.Utf8.parse('9c479dca09d54a2f999b102c60ef0405');
    var iv = CryptoJS.enc.Utf8.parse('734982a68db34f82');
    var encrypted = CryptoJS.AES.encrypt(CryptoJS.enc.Utf8.parse(str), key,
        {
            keySize: 256,
            iv: iv,
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7
        });
    return encrypted;
}

let country = document.getElementById("Country");
let postalCodeSection = document.getElementById("postalCodeSection");
let stateSection = document.getElementById("stateSection");
let postalCode = document.getElementById("PostalCode");
let state = document.getElementById("State");
function countrySelection() {
    if (country) {
        let currentCountry = country.value;
        if (currentCountry !== "US" && currentCountry !== "CA") {
            postalCodeSection.classList.add("hide");
            stateSection.classList.add("hide");
        }
        changeCountryEvent();
    }
}
let stateInstance = initSelect2(state, []);
function changeCountryEvent() {
    if (country) {
        country.onchange = function () {
            let currentCountry = country.value;
            if (currentCountry === "US" || currentCountry === "CA") {
                postalCodeSection.classList.remove("hide");
                stateSection.classList.remove("hide");
                showLoader();
                execute_ajax("/Service/GetStates", { CurrentCountry: currentCountry }, "GET").done(function (res) {
                    hideLoader();
                    stateInstance.select2('destroy');
                    state.options.length = 0;
                    initSelect2(state, res);
                }).fail(fail_request);
            }
            else {
                postalCodeSection.classList.add("hide");
                stateSection.classList.add("hide");
                postalCode.value = null;
                state.value = null;
            }
        };
    }
}

function updateQuotas() {
    execute_ajax("/Service/UpdateQuotas", { ServiceId: form.querySelector("#ServiceId").value, BusinessId: form.querySelector("#BusinessId").value }, "GET").done(function (res) {
        quotas.innerHTML = res.ResponseCode === 200 ? res.Quotas : 0;
    }).fail(fail_request);
}

if (quantity) {
    quantity.onchange = function (evt) {
        showLoader();
        execute_ajax("/Service/UpdateAmount", { ServiceId: form.querySelector("#ServiceId").value, BusinessId: form.querySelector("#BusinessId").value, Quantity: quantity.value }, "GET").done(function (res) {
            hideLoader();
            if (res.ResponseCode === 200)
                totalAmount.innerHTML = res.TotalAmount;
        }).fail(fail_request);
    };
}

if (UseSameInfoReceipt)
    UseSameInfoReceipt.onchange = UseSameInfoReceiptEvent;

function UseSameInfoReceiptEvent() {
    let invoice_li = document.getElementById("invoice-data-li");
    if (invoice_li) {
        if (UseSameInfoReceipt.checked) {
            invoice_li.classList.add("disabled");
            instanceTabs.select("test-swipe-1");
        }
        else {
            invoice_li.classList.remove("disabled");
        }
    }
}

function initSelect2(element, data) {
    return $(element).select2({
        data: data
    });
}

let countryData = [];
function getCountries() {
    showLoader();
    execute_ajax("/Service/GetCountries", {}, "GET").done(function (res) {
        hideLoader();
        countryData = res;
        initSelect2(country, countryData);
    }).fail(fail_request);
}

function validateForm(redirect = false) {
    let title = document.getElementById("AlertTitle").value;
    let FirstName = document.getElementById("FirstName");
    let LastName = document.getElementById("LastName");
    let Email = document.getElementById("Email");
    let Street1 = document.getElementById("Street1");
    let InvoiceName = document.getElementById("InvoiceName");
    let InvoiceRUC = document.getElementById("InvoiceRUC");
    let InvoiceAddress = document.getElementById("InvoiceAddress");
    let InvoicePhoneNumber = document.getElementById("InvoicePhoneNumber");

    if (FirstName.value.length === 0) {
        message(title, FirstName.dataset.valRequired, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (FirstName.value.length > FirstName.dataset.valMaxlengthMax) {
        message(title, FirstName.dataset.valMaxlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    }

    if (LastName.value.length === 0) {
        message(title, LastName.dataset.valRequired, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (LastName.value.length > LastName.dataset.valMaxlengthMax) {
        message(title, LastName.dataset.valMaxlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    }

    if (Email.value.length === 0) {
        message(title, Email.dataset.valRequired, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (Email.value.length > Email.dataset.valMaxlengthMax) {
        message(title, Email.dataset.valMaxlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (!validateEmail(Email.value)) {
        message(title, Email.dataset.valEmail, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    }

    if (Street1.value.length === 0) {
        message(title, Street1.dataset.valRequired, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (Street1.value.length < Street1.dataset.valMinlengthMin) {
        message(title, Street1.dataset.valMinlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (Street1.value.length > Street1.dataset.valMaxlengthMax) {
        message(title, Street1.dataset.valMaxlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    }

    if (City.value.length === 0) {
        message(title, City.dataset.valRequired, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    } else if (City.value.length > City.dataset.valMaxlengthMax) {
        message(title, City.dataset.valMaxlength, 'warning');
        if (redirect) instanceTabs.select('test-swipe-1');
        return false;
    }

    if (!UseSameInfoReceipt.checked && instanceTabs.index === 2) {
        if (InvoiceName.value.length === 0) {
            message(title, document.getElementById("InvoiceNameRequired").value, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoiceName.value.length > InvoiceName.dataset.valMaxlengthMax) {
            message(title, InvoiceName.dataset.valMaxlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        }

        if (InvoiceRUC.value.length === 0) {
            message(title, document.getElementById("RUCRequired").value, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoiceRUC.value.length > InvoiceRUC.dataset.valMaxlengthMax) {
            message(title, InvoiceRUC.dataset.valMaxlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        }

        if (InvoiceEmail.value.length === 0) {
            message(title, document.getElementById("InvoiceEmailRequired").value, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoiceEmail.value.length > InvoiceEmail.dataset.valMaxlengthMax) {
            message(title, InvoiceEmail.dataset.valMaxlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (!validateEmail(InvoiceEmail.value)) {
            message(title, InvoiceEmail.dataset.valEmail, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        }

        if (InvoiceAddress.value.length === 0) {
            message(title, document.getElementById("InvoiceAddressRequired").value, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoiceAddress.value.length < InvoiceAddress.dataset.valMinlengthMin) {
            message(title, InvoiceAddress.dataset.valMinlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoiceAddress.value.length > InvoiceAddress.dataset.valMaxlengthMax) {
            message(title, InvoiceAddress.dataset.valMaxlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        }

        if (InvoicePhoneNumber.value.length === 0) {
            message(title, document.getElementById("InvoicePhoneNumberRequired").value, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        } else if (InvoicePhoneNumber.value.length > InvoicePhoneNumber.dataset.valMaxlengthMax) {
            message(title, InvoicePhoneNumber.dataset.valMaxlength, 'warning');
            if (redirect) instanceTabs.select('test-swipe-3');
            return false;
        }
    }

    return true;
}