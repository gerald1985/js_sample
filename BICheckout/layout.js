// swiper
var swiper = new Swiper('.swiper_corp', {
    loop: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    simulateTouch: true,
    allowTouchMove: true,
    // freeMode: true,
});

// swiper
var swiper = new Swiper('.swiper_testimonial', {
    loop: true,
    centeredSlides: true,
    slidesPerView: '1',
    simulateTouch: true,
    allowTouchMove: true,
    autoHeight: true,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.next_testimonial',
        prevEl: '.prev_testimonial',
    },
});

var swiper = new Swiper('.swiper_intro', {
    autoHeight: true,
    loop: true,
    autoplay: {
        delay: 5000,
    },
    centeredSlides: true,
    slidesPerView: 'auto',
    simulateTouch: true,
    allowTouchMove: true,
    pagination: {
        el: '.swiper-pagination',
    },
    navigation: {
        nextEl: '.next_intro',
        prevEl: '.prev_intro',
    },
});

var swiper = new Swiper('.sipper_freemode', {
    loop: true,
    centeredSlides: true,
    slidesPerView: 'auto',
    simulateTouch: true,
    allowTouchMove: true,
    freeMode: true,
    centeredSlides: true,
});





// code
(function () {
    //filter IE8 and earlier which don't support the generated content
    if (typeof (window.getComputedStyle) == 'undefined') {
        return;
    }

    //get the collection of PRE elements
    var pre = document.getElementsByTagName('pre');

    //now iterate through the collection
    for (var len = pre.length, i = 0; i < len; i++) {
        //get the CODE or SAMP element inside it, 
        //or just in case there isn't one, continue to the next PRE
        var code = pre[i].getElementsByTagName('code').item(0);
        if (!code) {
            code = pre[i].getElementsByTagName('samp').item(0);
            if (!code) {
                continue;
            }
        }

        //create a containing DIV column (but don't append it yet)
        //including aria-hidden so that ATs don't read the numbers
        var column = document.createElement('div');
        column.setAttribute('aria-hidden', 'true');

        //split the code by line-breaks to count the number of lines
        //then for each line, add an empty span inside the column
        for (var n = 0; n < code.innerHTML.split(/[\n\r]/g).length; n++) {
            column.appendChild(document.createElement('span'));
        }

        //now append the populated column before the code element
        pre[i].insertBefore(column, code);

        //finally add an identifying class to the PRE to trigger the extra CSS
        pre[i].className = 'line-numbers';
    }

})();


// object fit css
// Detect objectFit support
if ('objectFit' in document.documentElement.style === false) {
    // assign HTMLCollection with parents of images with objectFit to variable
    var container = document.getElementsByClassName('img_cover_container');
    // Loop through HTMLCollection
    for (var i = 0; i < container.length; i++) {
        // Asign image source to variable
        var imageSource = container[i].querySelector('img').src;
        // Hide image
        container[i].querySelector('img').style.display = 'none';
        // Add background-size: cover
        container[i].style.backgroundSize = 'cover';
        // Add background-image: and put image source here
        container[i].style.backgroundImage = 'url(' + imageSource + ')';
        // Add background-position: center center
        container[i].style.backgroundPosition = 'center bottom';
    }
}

window.onload = function () {
    hideLoader();
};

//target links
let mainNavLinks = document.querySelectorAll(".nav");
let mainSections = document.querySelectorAll(".section_nav");

let lastId;
let cur = [];

// This should probably be throttled.
// Especially because it triggers during smooth scrolling.
// https://lodash.com/docs/4.17.10#throttle
// You could do like...
// window.addEventListener("scroll", () => {
//    _.throttle(doThatStuff, 100);
// });
// Only not doing it here to keep this Pen dependency-free.

window.addEventListener("scroll", event => {
    let fromTop = window.scrollY;

    mainNavLinks.forEach(link => {
        let section = document.querySelector(link.hash);

        if (
            section.offsetTop <= fromTop &&
            section.offsetTop + section.offsetHeight > fromTop
        ) {
            link.classList.add("active");
        } else {
            link.classList.remove("active");
        }
    });
});



//form buttom
function zforms_open_window(url, height, width) { var leftPos = 0; var topPos = 0; if (screen) { leftPos = (screen.width - width) / 2; topPos = (screen.height - height) / 2; window.open(url, null, 'width=' + width + ',height=' + height + ',left=' + leftPos + ',top=' + topPos + ', toolbar=0, location=0, status=1, scrollbars=1, resizable=1'); } }


//section account business
let select_captions = document.getElementsByClassName("select_caption");
Array.prototype.forEach.call(select_captions, function (select_caption) {
    select_caption.onclick = function () {
        let info_selection = document.getElementsByClassName("info_selection")[0];
        Array.prototype.forEach.call(info_selection.querySelectorAll("div.active"), function (selection) {
            selection.classList.remove('active');
        });
        Array.prototype.forEach.call(select_captions, function (selection) {
            selection.classList.remove('active');
        });
        let caption = document.getElementById(select_caption.dataset.id);
        if (caption)
            caption.classList.add('active');
        select_caption.classList.add('active');
    }
});