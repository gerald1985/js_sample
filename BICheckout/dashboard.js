﻿let flatOptions = {
    enableTime: false,
    dateFormat: "Y-m-d",
    altInput: true,
    altFormat: "d/m/Y",
    disableMobile: true
};

document.getElementsByClassName("flatpickr").flatpickr(flatOptions);

let totalTransactions = document.getElementsByClassName("totalTransactions");
Array.prototype.forEach.call(totalTransactions, function (totalTransaction) {
    totalTransaction.onclick = function (transaction) {
        let transactions = document.getElementsByClassName("totalTransactions");
        Array.prototype.forEach.call(transactions, function (el) {
            el.classList.remove("active");
        });
        transaction.currentTarget.classList.add("active");
        let labels = document.getElementsByClassName("totalTransactionsLabel");
        Array.prototype.forEach.call(labels, function (label) {
            if (label.classList.contains("hide")) {
                label.classList.remove("hide");
            }
            else label.classList.add("hide");
        });
    };
});

function radioOptionsClear(containerId) {
    let container = document.getElementById(containerId);
    let options = container.querySelectorAll("input[type=radio]");
    Array.prototype.forEach.call(options, function (option) {
        option.removeAttribute("checked");
    });
}

function ulOptionsIndexActive(ulID) {
    let ul = document.getElementById(ulID);
    let options = ul.querySelectorAll("li");
    for (var i = 0; i < options.length; i++) {
        if (options[i].classList.contains("active")) {
            return i;
        }
    }
}

function changeActive(ulId, evt) {
    let container = document.getElementById(ulId);
    let options = container.getElementsByTagName("li");
    Array.prototype.forEach.call(options, function (option) {
        option.classList.remove("active");
        let hoverActive = option.querySelector("a");
        hoverActive.classList.remove("c_blue_active");
    });
    evt.target.parentNode.classList.add("active");
    evt.target.classList.add("c_blue_active");
}

function removeDataSet(src, dataSetName) {
    for (let i = 0; i < src.data.datasets.length; i++) {
        if (dataSetName) {
            if (src.data.datasets[i].label === dataSetName) {
                src.data.datasets.splice(i, 1);
                break;
            }
        } else {
            src.data.datasets.length = 0;
            break;
        }
    }
}

function containsDataSet(src, dataSetName) {
    for (var i = 0; i < src.data.datasets.length; i++) {
        if (src.data.datasets[i].label === dataSetName) {
            return true;
        }
    }
    return false;
}

window.addEventListener('load', getSuccessfulTransaction);
window.addEventListener('load', getAllServices);

let startDateTransaction = document.getElementById("startDateTransaction");
let endDateTransaction = document.getElementById("endDateTransaction");
let startDateService = document.getElementById("startDateService");
let endDateService = document.getElementById("endDateService");
function getTransactionParameters() {
    return {
        StartDate: startDateTransaction.value,
        EndDate: endDateTransaction.value,
        Quantities: getRadioValue("transactionGroup")
    };
}

function getServiceParameters() {
    return {
        StartDate: startDateService.value,
        EndDate: endDateService.value,
        Quantities: getRadioValue("serviceGroup")
    };
}

function getSuccessfulTransaction() {
    showLoader();

    execute_ajax("/Admin/Dashboard/GetSuccessfulTransactions", getTransactionParameters(), "GET").done(function (res) {
        if (res.ResponseCode === 200) {
            transactionChartConfig.data.labels = res.Labels;
            transactionChartConfig.data.datasets.push(successfulDataSetConfig);
            successfulDataSetConfig.data = res.Data;
        }
        hideLoader();
        transactionLineChart.update();
    }).fail(fail_request);
}

function getFailedTransactions() {
    showLoader();

    execute_ajax("/Admin/Dashboard/GetFailedTransactions", getTransactionParameters(), "GET").done(function (res) {
        if (res.ResponseCode === 200) {
            transactionChartConfig.data.labels = res.Labels;
            transactionChartConfig.data.datasets.push(failedDataSetConfig);
            failedDataSetConfig.data = res.Data;
        }
        hideLoader();
        transactionLineChart.update();
    }).fail(fail_request);
}

function getAllTransactions() {
    showLoader();

    execute_ajax("/Admin/Dashboard/GetAllTransactions", getTransactionParameters(), "GET").done(function (res) {
        if (res.ResponseCode === 200) {
            transactionChartConfig.data.labels = res.Labels;
            successfulDataSetConfig.data = res.SuccessData;
            failedDataSetConfig.data = res.FailedData;
            transactionChartConfig.data.datasets.push(successfulDataSetConfig);
            transactionChartConfig.data.datasets.push(failedDataSetConfig);
        }
        hideLoader();
        transactionLineChart.update();
    }).fail(fail_request);
}

let radioOptions = document.getElementsByName("transactionGroup");
let serviceGroup = document.getElementsByName("serviceGroup");
Array.prototype.forEach.call(radioOptions, function (radio) {
    radio.onclick = function (evt) {
        transactionChartConfig.data.labels.length = 0;
        if (ulOptionsIndexActive("trans_selection") === 0) {
            removeDataSet(transactionChartConfig);
            getSuccessfulTransaction();
        }
    };
});

Array.prototype.forEach.call(serviceGroup, function (radio) {
    radio.onclick = function (evt) {
        reSearchService();
    };
});

let moneyOptionRadio = document.getElementById("moneyOptionRadio");
let quantityOptionRadio = document.getElementById("quantityOptionRadio");

let successfulTransactionsBtn = document.getElementById("successfulTransactions");
if (successfulTransactionsBtn) {
    successfulTransactionsBtn.onclick = function (evt) {
        changeActive("trans_selection", evt);
        reSearchTransaction("success");
    };
}

let failedTransactionsBtn = document.getElementById("failedTransactions");
if (failedTransactionsBtn) {
    failedTransactionsBtn.onclick = function (evt) {
        changeActive("trans_selection", evt);
        reSearchTransaction("failed");
    };
}

let allTransactionsBtn = document.getElementById("allTransactions");
if (allTransactionsBtn) {
    allTransactionsBtn.onclick = function (evt) {
        changeActive("trans_selection", evt);
        reSearchTransaction("all");
    };
}

function reSearchTransaction(action) {
    if (action === "failed" || action === "all") {
        moneyOptionRadio.classList.add("hide");
        radioOptionsClear("radioOptions");
        quantityOptionRadio.checked = true;
    } else
        moneyOptionRadio.classList.remove("hide");
    switch (action) {
        case "success":
            removeDataSet(transactionChartConfig);
            getSuccessfulTransaction();
            break;
        case "failed":
            removeDataSet(transactionChartConfig);
            getFailedTransactions();
            break;
        case "all":
            removeDataSet(transactionChartConfig);
            getAllTransactions();
            break;
        default:
    }
}

function transactionDatesOnChange(evt) {
    let position = ulOptionsIndexActive("trans_selection");
    switch (position) {
        case 0: removeDataSet(transactionChartConfig);
            getSuccessfulTransaction();
            break;
        case 1: removeDataSet(transactionChartConfig);
            getFailedTransactions();
            break;
        case 2: removeDataSet(transactionChartConfig);
            getAllTransactions();
            break;
        default: break;
    }
}

startDateTransaction.onchange = transactionDatesOnChange;
endDateTransaction.onchange = transactionDatesOnChange;


let autocomplete = document.getElementById("autocomplete");
$(autocomplete).select2({
    ajax: {
        delay: 1000,
        url: '/Admin/Dashboard/GetServicesNames',
        data: function (params) {
            return { ServiceName: params.term };
        },
        processResults: function (result) {
            var filterData = [];

            Array.prototype.forEach.call(result.Data, function (el) {
                filterData.push({ 'id': el.ServiceId, 'text': el.ServiceName });
            });

            return { results: filterData };
        },
        transport: function (params, success, failure) {
            let $request = $.ajax(params);

            $request.then(success);
            $request.fail(fail_request);

            return $request;
        }
    },
    placeholder: 'Servicio...',
    allowClear: true,
    language: {
        noResults: function (params) {
            return 'Sin resultados';
        }
    }
});

autocomplete.onchange = function (evt) {
    reSearchService();
};

function getAllServices() {
    showLoader();
    execute_ajax('/Admin/Dashboard/GetServicesOrders', getServiceParameters(), 'GET').done(function (res) {
        if (res.ResponseCode === 200) {
            changeChartType('bar');
            servicesChartConfig.data.labels = res.Labels;
            servicesChartConfig.data.datasets.length = 0;
            servicesChartConfig.data.datasets.push({
                backgroundColor: getRandomColor(true, res.Data.length),
                data: res.Data
            });
        }
        hideLoader();
        serviceChart.update();
    }).fail(fail_request);
}

function getService() {
    showLoader();
    let data = getServiceParameters();
    data.ServiceId = autocomplete.value;
    execute_ajax('/Admin/Dashboard/GetServiceOrders', data, 'GET').done(function (res) {
        if (res.ResponseCode === 200) {
            changeChartType('line', true);
            servicesChartConfig.data.labels = res.Labels;
            servicesChartConfig.data.datasets.push({
                label: $(autocomplete).select2('data')[0]['text'],
                fill: false,
                borderColor: "#3399ff",
                backgroundColor: "#3399ff",
                data: res.Data
            });
        }
        hideLoader();
        serviceChart.update();
    }).fail(fail_request);
}

function reSearchService() {
    let selectedService = autocomplete.value;
    let action = selectedService ? 'specific' : 'all';
    switch (action) {
        case "specific": getService();
            break;
        case "all": default: getAllServices();
            break;
    }
}

startDateService.onchange = function (evt) {
    reSearchService();
};

endDateService.onchange = function (evt) {
    reSearchService();
};