
// object fit css
// Detect objectFit support
if('objectFit' in document.documentElement.style === false) {
  // assign HTMLCollection with parents of images with objectFit to variable
  var container = document.getElementsByClassName('img_cover_container');
  // Loop through HTMLCollection
  for(var i = 0; i < container.length; i++) {
    // Asign image source to variable
    var imageSource = container[i].querySelector('img').src;
    // Hide image
    container[i].querySelector('img').style.display = 'none';
    // Add background-size: cover
    container[i].style.backgroundSize = 'cover';
    // Add background-image: and put image source here
    container[i].style.backgroundImage = 'url(' + imageSource + ')';
    // Add background-position: center center
    container[i].style.backgroundPosition = 'center bottom';
  }
}

window.onload = function () {
    hideLoader();
};

//target links
let mainNavLinks = document.querySelectorAll(".nav");
let mainSections = document.querySelectorAll(".section_nav");

let lastId;
let cur = [];

// This should probably be throttled.
// Especially because it triggers during smooth scrolling.
// https://lodash.com/docs/4.17.10#throttle
// You could do like...
// window.addEventListener("scroll", () => {
//    _.throttle(doThatStuff, 100);
// });
// Only not doing it here to keep this Pen dependency-free.

window.addEventListener("scroll", event => {
    let fromTop = window.scrollY;

    mainNavLinks.forEach(link => {
        let section = document.querySelector(link.hash);

        if (
            section.offsetTop <= fromTop &&
            section.offsetTop + section.offsetHeight > fromTop
        ) {
            link.classList.add("active");
        } else {
            link.classList.remove("active");
        }
    });
});



//button form
function zforms_open_window(url, height, width) { var leftPos = 0; var topPos = 0; if (screen) { leftPos = (screen.width - width) / 2; topPos = (screen.height - height) / 2; window.open(url, null, 'width=' + width + ',height=' + height + ',left=' + leftPos + ',top=' + topPos + ', toolbar=0, location=0, status=1, scrollbars=1, resizable=1'); } }