﻿let saleForm = document.getElementsByClassName("saleForm");
Array.prototype.forEach.call(saleForm, function (form) {
    form.onsubmit = function (evt) {
        evt.preventDefault();

        let url = form.action;
        let method = form.method;
        let formData = new FormData($(this)[0]);

        messageLoader("Liquidar transacción", "question", "Si, acepto", "¿Estas seguro de continuar con la operación?", true, true, true, "Cancelar", function () {
            return new Promise(function () {
                execute_ajax(url, formData, method, false, true).done(function (res) {
                    if (res.ResponseCode === 200) {
                        message('Liquidar transacción', "Operación realizada con éxito", 'success', function () {
                            window.location.reload();
                        });
                    } else {
                        message('Liquidar transacción', `<p class='c_red'>${res.ResponseText}</p>`, 'error');
                    }
                }).fail(fail_request);
            });
        });
    };
});

let reverseForm = document.getElementsByClassName("reverseForm");
Array.prototype.forEach.call(reverseForm, function (form) {
    form.onsubmit = function (evt) {
        evt.preventDefault();

        let url = form.action;
        let method = form.method;
        let formData = new FormData($(this)[0]);

        messageLoader("Revertir transacción", "question", "Si, acepto", "¿Estas seguro de continuar con la operación?", true, true, true, "Cancelar", function () {
            return new Promise(function () {
                execute_ajax(url, formData, method, false, true).done(function (res) {
                    if (res.ResponseCode === 200) {
                        message('Revertir transacción', "Operación realizada con éxito", 'success', function () {
                            window.location.reload();
                        });
                    } else {
                        message('Revertir transacción', `<p class='c_red'>${res.ResponseText}</p>`, 'error');
                    }
                }).fail(fail_request);
            });
        });
    };
});

let orderDetails = document.getElementsByClassName("orderDetailBtn");
Array.prototype.forEach.call(orderDetails, function (orderDetail) {
    orderDetail.onclick = function () {
        showLoader();
        execute_ajax("/Admin/Order/GetOrderDetail", { OrderNumber: orderDetail.dataset.order }, "GET").done(function (res) {
            hideLoader();
            document.getElementById("orderDetail").innerHTML = res;
        }).fail(fail_request);
    };
});

let notifications = document.getElementsByClassName("order_notification");
Array.prototype.forEach.call(notifications, function (notification) {
    notification.onclick = function () {
        let instance = M.Modal.init(document.getElementById("resend_receipt"));
        instance.open();
        showLoader();
        execute_ajax("/Admin/Order/GetResendReceipt", { OrderId: notification.dataset.order }, "GET").done(function (res) {
            hideLoader();
            document.getElementById("resend_receipt").innerHTML = res;
            resendEvent();
        }).fail(fail_request);
    };
});

function resendEvent() {
    let resend_receipt_form = document.getElementById("resend_receipt_form");
    if (resend_receipt_form) {
        resend_receipt_form.onsubmit = function (evt) {
            evt.preventDefault();

            let url = resend_receipt_form.action;
            let method = resend_receipt_form.method;
            let formData = new FormData($(this)[0]);

            execute_ajax(url, formData, method, false, true).done(function (res) {
                if (res.ResponseCode === 200) {
                    message('Reenviar notificación', "Operación realizada con éxito", 'success');
                } else {
                    message('Reenviar notificación', `<p class='c_red'>${res.ResponseText}</p>`, 'error');
                }
            }).fail(fail_request);
        };
    }
}