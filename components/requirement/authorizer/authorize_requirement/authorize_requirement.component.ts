import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Util } from "../../../util/util";
import { AlertComponent } from '../../../alerts/alert.component';
import { BreadCrumbService } from '../../../util/breadcrumbService';
import { RequirementSup } from '../../../util/entities/requirement_sup';
import { DataService } from '../../../util/data_service';
import { Supervisor } from '../../../util/entities/supervisor';
import { SessionInfo } from '../../../util/entities/session_info';
import { SessionInfoService } from '../../../util/SessionInfoService';
import { RequirementAuto } from '../../../util/entities/requirement_auto';
import { ClosureReturnComponent } from '../../../shared/closure_return/closure_return.component';

@Component({
    selector: 'authorize-requirement',
    templateUrl: './authorize_requirement.component.html',
    providers: [AlertComponent]
})

export class AuthorizeRequirementComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public source: RequirementAuto[] = [];
    public requirements: RequirementAuto[] = [];
    public statusList: string[] = [];
    public filterEquipment: string = "";
    public filterStatus: string = "-1";
    public authUrl: string = "";
    public havePermission: boolean = false;

    @Input()
    workOrderNumber: string;

    //initializing p to one
    pageReqAuthorize: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, public sessionInfoService: SessionInfoService,
        public router: Router, ) {
        this.workOrderNumber = this.dataService.getWorkOrderNumber();
        this.authUrl = authUrl;
    }

    toRequirement(aufnr: any, nrequisa: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(aufnr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setRenderDeleteColumnMat(false);
        this.dataService.setRenderMaterialsFromAuthorizer(true);
        this.dataService.setWorkOrderQuantity(this.requirements.length);
        this.router.navigateByUrl('/orden-de-trabajo-autorizador');
    }

    requirementURL() {
        return this.http.get(this.baseUrl + 'api/Requirement/EnumRequirementAuto');
    }

    supervisorsURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }

    ngOnInit() {
        this.loadRequirements();
    }

    loadStatusList() {
        this.statusList = this.requirements.map(function (e) {
            return new RequirementSup(e)._Requirement.staT_EQ;
        });
        this.statusList = Util.getUniqueValuesFromArray(this.statusList);
    }

    loadRequirements() {
        if (Util.getPermissions(['63'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.requirementURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.elemRendered = true;
                this.source = Util.loadJsonWithToast<RequirementAuto[]>(this.source, result, this.alertComponent);
                this.changeFilter();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }


    changeFilter() {
        let context = this;
        context.requirements.length = 0;
        if (this.filterEquipment.trim().length != 0) {

            Array.prototype.forEach.call(this.source, function (el: any) {
                if (el.equnr == context.filterEquipment.trim()) {
                    context.requirements.push(el);
                }
            });
        }
        if (this.filterStatus != '-1') {
            Array.prototype.forEach.call(this.requirements.length > 0 ? this.requirements : this.source, function (el: any) {
                if (el.staT_EQ == context.filterStatus) {
                    context.requirements.push(el);
                }
            });
        }
        this.requirements = Util.getUniqueValuesFromArray(this.requirements);
        if (this.filterEquipment.trim().length == 0 && this.filterStatus == '-1') {
            this.requirements = Object.assign([], this.source);
        }
        this.loadStatusList();
    }

    ngAfterViewChecked() {
        if (this.elemRendered) {
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Bandeja Entrada");
            BreadCrumbService.changeBreadCrumbChild("Autorizar requisas");
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }

    returnReq() {
        let references: string[] = [];
        let toSave = this.requirements.filter(function (f) {
            return new RequirementSup(f)._Requirement.checked;
        }).map(function (m) {
            let req = new RequirementSup(m)._Requirement;
            return {
                NREQUISA: req.nrequisa,
                AUFNR: req.aufnr
            };
        });
        Array.prototype.forEach.call(toSave, function (el: any) {
            references.push(el.AUFNR + "-REQ-" + el.NREQUISA);
        });
        let context = this;
        ClosureReturnComponent.init("", "Regresar requisa", this.alertComponent, "WORK ORDER", "Retorno", function () { context.saveRequirementIngr(toSave); }, references, true);
    }

    saveRequirementIngr(toSave: any) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + 'api/Requirement/ReturnRequirements', toSave, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.loadRequirements();
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    havePermissionToAuthorize(): boolean {
        if (Math.max.apply(Math, Util.getPermissions(['63', '69'], this.dataService.getMenuList())) != 5) {
            return false;
        }
        return true;
    }

    autoReq() {
        let toSave = this.requirements.filter(function (f) {
            return new RequirementSup(f)._Requirement.checked;
        }).map(function (m) {
            let req = new RequirementSup(m)._Requirement;
            return {
                NREQUISA: req.nrequisa,
                AUFNR: req.aufnr
            };
        });

        this.http.put(this.baseUrl + 'api/Requirement/AuthorizeRequirements', toSave, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadRequirements();
                } else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }
}