import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import { Util } from "../../../util/util";
import { AlertComponent } from '../../../alerts/alert.component';
import { BreadCrumbService } from '../../../util/breadcrumbService';
import { Requirement } from '../../../util/entities/requirement';
import { DataService } from '../../../util/data_service';
import { Provider } from '../../../util/entities/provider';

@Component({
    selector: 'wo-requirement-technician',
    templateUrl: './technician_requirement.component.html',
    providers: [AlertComponent]
})
export class WOTechnicianRequirementComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public requirements: Requirement[] = [];
    public enableOptions: boolean = false;
    public providers: Provider[] = [];
    @Input()
    workOrderNumber: string;

    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //initializing p to one
    pageTechnicianRequirement: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef, private dataService: DataService) {
        this.workOrderNumber = this.dataService.getWorkOrderNumber();
    }

    requirementURL() {
        return this.http.get(this.baseUrl + 'api/Requirement/EnumRequirement?' + 'order=' + this.workOrderNumber);
    }

    providerUrl() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumProvider");
    }

    ngOnInit() {
        this.loadRequirements();
    }

    //evalPermission(permissionId: number, description: string) {
    //    return Util.havePermission(permissionId, description, this.dataService.getMenuList());
    //}

    peticionBorradoClick(e: any) {
        e.preventDefault();
    }

    loadRequirements() {
        Util.ShowLoader();
        this.requirementURL().subscribe(result => {
            if (result != undefined) {
                this.requirements = Util.loadJsonWithToast<Requirement[]>(this.requirements, result, this.alertComponent);
                if (this.requirements.length > 0) {
                    let req = new Requirement(this.requirements[0])._Requirement;
                    this.enableOptions = req.status == 'CREA';
                    this.dataService.setNREQUISA(req.nrequisa);
                    let temp = new Requirement(this.requirements[0]);
                    Array.prototype.forEach.call(temp._Requirement.lst_ZADF_REQUOPR, function (el: any) {
                        if (el.peticionborrado == "1") {
                            el.peticionborrado = true;
                        }
                        else {
                            el.peticionborrado = false;
                        }
                    });
                    this.requirements.length = 0;
                    this.requirements.push(temp._Requirement as any);
                } else {
                    this.enableOptions = true;
                }
                this.providerUrl().subscribe(resultProvider => {
                    this.providers = Util.loadJsonWithToast<Provider[]>(this.providers, resultProvider, this.alertComponent);
                }, error => {
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
            }
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    withoutSend() {
        Util.ShowLoader();
        this.http.post(this.baseUrl + 'api/Requirement/WithoutSend', { NREQUISA: this.dataService.getNREQUISA() }, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.loadRequirements();
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMaterials(vornr: string, nrequisa: string, ktsch: string, status: string, cm_Description: string) {
        this.dataService.setVORNR(vornr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setModelKey(ktsch);
        this.dataService.setCMDescription(cm_Description);
        this.dataService.setLoadMaterialsFromOrder(false);
        this.dataService.setLoadMaterials(true);
        this.dataService.setRequirementStatus(status);
        this.dataService.setShowOperationsMaterial(true);
    }

    deleteVORNR(req: any, status: string) {
        if (status == 'CREA') {
            req = req as Ilst_ZADF_REQUOPR;
            if (req.lst_ZADF_REQUMAT.length > 0) {
                this.alertComponent.toastMessage = "No es permitido eliminar una operaci&oacute;n que contiene materiales";
                this.alertComponent.warningMessage();
            }
            else {
                req.activo = "0";
                this.updtSRVEX(req);
            }
        }
        else {
            this.alertComponent.toastMessage = "No es permitido eliminar una operaci&oacute;n con un estado distinto a CREA";
            this.alertComponent.warningMessage();
        }
    }

    preventUpdate(event: any, status: string) {
        if (status != "CREA") {
            event.preventDefault();
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado CREA";
            this.alertComponent.warningMessage();
        }
    }

    updtSRVEX(req: any) {
        Util.ShowLoader();
        req = req as Ilst_ZADF_REQUOPR;
        let toUpdate = {
            NREQUISA: this.dataService.getNREQUISA(),
            VORNR: req.vornr,
            KTSCH: req.ktsch,
            SRVEX: req.srvex,
            TEXTO: req.texto,
            ARBPL: req.arbpl,
            TXTCLAVE: req.txtclave,
            PETICIONBORRADO: req.peticionborrado,
            ACTIVO: req.activo,
            AUFNR: this.dataService.getWorkOrderNumber()
        };
        this.http.put(this.baseUrl + 'api/Requirement/UpdateSRVEX', toUpdate, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadRequirements();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    createRequirement() {
        if (this.dataService.getReceptionStatus() != "PROCESO") {
            this.alertComponent.toastMessage = "Unicamente se permite modificar en estado PROCESO";
            this.alertComponent.errorMessage();
            return;
        }
        Util.ShowLoader();
        let req = { AUFNR: this.dataService.getWorkOrderNumber() };
        this.http.post(this.baseUrl + 'api/Requirement/CreateRequirement', req, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadRequirements();
                }
                else if (result.json()["responseCode"] != 409) {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.warningMessage();
                }
                if (result.json()["responseCode"] == 409 || result.json()["responseCode"] == 200) {
                    this.dataService.setLoadModelKeys(true);
                    Util.openModal(".model_key");
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    sendRequirement() {
        if (this.requirements.length > 0) {
            let req = new Requirement(this.requirements[0])._Requirement;
            if (req.lst_ZADF_REQUOPR.filter(function (f) { return f.peticionborrado; }).length > 0) {
                this.alertComponent.toastMessage = "No es permitido enviar una requisa con operaciones que tengan solicitud de borrado";
                this.alertComponent.warningMessage();
            } else {
                Util.ShowLoader();
                this.http.post(this.baseUrl + 'api/Requirement/SendRequirement', { NREQUISA: this.dataService.getNREQUISA() }, {}).subscribe(result => {
                    Util.HideLoader();
                    if (result != undefined) {
                        if (result.json()["responseCode"] == 200) {
                            this.loadRequirements();
                            this.alertComponent.toastMessage = "Operacion exitosa";
                            this.alertComponent.successMessage();
                        } else if (result.json()["responseCode"] == 409) {
                            Util.openModal(".model_key");
                        }
                        else {
                            this.alertComponent.toastMessage = result.json()["responseText"];
                            this.alertComponent.errorMessage();
                        }
                    }
                }, error => {
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
            }
        }
    }

    loadMessengerRequirement(requirement: any, keyModel: any) {
        BreadCrumbService.setTypeReference("Requisa materiales o servicios");
        let reference = new Requirement(requirement)._Requirement;
        if (!keyModel) {
            BreadCrumbService.setReference(this.workOrderNumber.concat("-REQ-").concat(reference.nrequisa));
        } else {
            BreadCrumbService.setReference(this.workOrderNumber.concat("-").concat(reference.nrequisa).concat("-").concat(keyModel));
        }

        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    ngAfterViewChecked() {
        if (!this.elemRendered) {
            Util.loadMaterialElements();
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Orden de Trabajo");
            BreadCrumbService.changeBreadCrumbChild("Requisa de Materiales");
        }
        if (this.dataService.getRenderRequirement()) {
            this.loadRequirements();
            this.dataService.setRenderRequirement(false);
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}