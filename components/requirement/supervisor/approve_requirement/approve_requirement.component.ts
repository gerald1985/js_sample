import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Util } from "../../../util/util";
import { AlertComponent } from '../../../alerts/alert.component';
import { BreadCrumbService } from '../../../util/breadcrumbService';
import { RequirementSup } from '../../../util/entities/requirement_sup';
import { DataService } from '../../../util/data_service';
import { Supervisor } from '../../../util/entities/supervisor';
import { SessionInfo } from '../../../util/entities/session_info';
import { SessionInfoService } from '../../../util/SessionInfoService';
import { ClosureReturnComponent } from '../../../shared/closure_return/closure_return.component';

@Component({
    selector: 'approve-requirement-supervisor',
    templateUrl: './approve_requirement.component.html',
    providers: [AlertComponent]
})
export class ApproveRequirementSupComponent {
    public navigation: string = "";
    public elemRendered = false;
    public requirements: RequirementSup[] = [];
    public enumSpecialty: string[] = [];
    public filterSupervisor: string = "-1";
    public filterEquipmentSup: string = "";
    public filterSpecialtySup: string = "-1";
    public enumSupervisor: Supervisor[] = [];
    public specialties: string[] = [];
    public filterApproved: boolean = false;
    public refreshSelect: boolean = false;
    public authUrl: string = "";
    public havePermission: boolean = false;

    @Input()
    workOrderNumber: string;

    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //initializing p to one
    pageApproveRequirement: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService,
        public router: Router, ) {
        this.workOrderNumber = this.dataService.getWorkOrderNumber();
        this.authUrl = authUrl;
        
    }

    toRequirement(aufnr: any, nrequisa: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(aufnr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setRenderDeleteColumnMat(false);
        this.dataService.setShowOperationsMaterial(false);
        this.dataService.setRenderMaterialsFromAuthorizer(false);
        this.router.navigateByUrl('/orden-de-trabajo-supervisor');
    }

    requirementURL() {
        if (this.filterSupervisor === '-1') {
            return this.http.get(this.baseUrl + 'api/Requirement/EnumRequirementSup?status=' + this.filterApproved);
        }
        return this.http.get(this.baseUrl + 'api/Requirement/EnumRequirementSup?status=' + this.filterApproved + '&sup=' + this.filterSupervisor);
    }

    supervisorsURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }

    loadSpecialty() {
        this.enumSpecialty = Util.getUniqueValuesFromArray(this.requirements.map(function (e) { return new RequirementSup(e)._Requirement.espec.toUpperCase(); }));
    }

    loadSupervisors() {
        if (Util.getPermissions(['52'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.supervisorsURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.enumSupervisor = Util.loadJsonWithToast<Supervisor[]>(this.enumSupervisor, result, this.alertComponent);
                var user = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
                var found = this.enumSupervisor.find(function (elem) { return new Supervisor(elem)._Supervisor.usuario.toUpperCase() == user; });
                if (found != undefined) {
                    this.filterSupervisor = new Supervisor(found)._Supervisor.usuario;
                }
                else {
                    this.filterSupervisor = new Supervisor(this.enumSupervisor[0])._Supervisor.usuario;
                }
                this.loadRequirements();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadRequirements() {
        Util.ShowLoader();
        this.requirementURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.requirements = Util.loadJsonWithToast<RequirementSup[]>(this.requirements, result, this.alertComponent);
                if (this.filterEquipmentSup) {
                    this.requirements = this.requirements.filter(f => new RequirementSup(f)._Requirement.nrequisa === this.filterEquipmentSup);
                }
                if (this.filterSpecialtySup != '-1') {
                    this.requirements = this.requirements.filter(f => new RequirementSup(f)._Requirement.espec === this.filterSpecialtySup);
                }
                this.loadSpecialty();
                this.refreshSelect = true;
                if (this.requirements.length > 0) {
                    this.dataService.setNREQUISA(new RequirementSup(this.requirements[0])._Requirement.nrequisa);
                }
                //Array.prototype.forEach.call(this.requirements, function (element: any) {
                //    let time = Math.floor(parseInt(element.tiempo));
                //    element.tiempo = Util.secondsToTime(time * 60, false);
                //});
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    createRequirement() {
        Util.ShowLoader();
        let req = { AUFNR: this.dataService.getWorkOrderNumber() };
        this.http.post(this.baseUrl + 'api/Requirement/CreateRequirement', req, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeFilter() {
        this.loadRequirements();
    }

    changeEquipment() {
        let temp: RequirementSup[] = [];
        let filter = this.filterEquipmentSup.trim();
        if (filter) {
            Util.ShowLoader();
            this.requirements = this.requirements.filter(f => new RequirementSup(f)._Requirement.equnr == filter);
            Util.HideLoader();
        } else
            this.changeFilter();
    }

    updateSup(aufnr: string, nrequisa: string) {
        this.dataService.setWorkOrderNumber(aufnr);
        this.dataService.setNREQUISA(nrequisa);
        BreadCrumbService.setReference(aufnr.concat("-").concat("REQ-").concat(nrequisa))
        this.dataService.setLoadReasonToReturnReqFromSupervisor(true);
    }

    loadMessengerRequirementSup(requirement: any) {
        BreadCrumbService.setTypeReference("Requisa materiales o servicios");
        let reference = new RequirementSup(requirement)._Requirement;

        BreadCrumbService.setReference(reference.aufnr.concat("-").concat("REQ-").concat(reference.nrequisa));
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    ngAfterViewChecked() {
        //if (!this.elemRendered && this.enumSupervisor.length > 0) {
        //    this.elemRendered = true;
        //    //BreadCrumbService.newBreadCrumbRoot("Orden de Trabajo");
        //    //BreadCrumbService.changeBreadCrumbChild("Requisa de Materiales");
        //}
        if (this.dataService.getLoadRequirementFromSupervisor()) {
            this.dataService.setLoadRequirementFromSupervisor(false);
            this.loadRequirements();
        }

        if (this.refreshSelect) {
            Util.loadMaterialBySelector("select.filter_sup", {}, "FormSelect");
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
            this.refreshSelect = false;
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}