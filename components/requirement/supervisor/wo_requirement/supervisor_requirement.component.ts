import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Util } from "../../../util/util";
import { Security } from "../../../util/security";
import { AlertComponent } from '../../../alerts/alert.component';
import { BreadCrumbService } from '../../../util/breadcrumbService';
import { Requirement } from '../../../util/entities/requirement';
import { DataService } from '../../../util/data_service';
import { ReqOper } from '../../../util/entities/requirement_operation';
import { Provider } from '../../../util/entities/provider';
import { SessionInfoService } from '../../../util/SessionInfoService';
import { SessionInfo } from '../../../util/entities/session_info';

@Component({
    selector: 'wo-requirement-supervisor',
    templateUrl: './supervisor_requirement.component.html',
    providers: [AlertComponent]
})
export class WORequirementSupComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public requirements: Requirement[] = [];
    public providers: Provider[] = [];
    public renderFromAuthorizer: boolean = false;
    @Input()
    workOrderNumber: string;

    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //initializing p to one
    pageSupervisorRequirement: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent, private sessionInfoService: SessionInfoService,
        private cdRef: ChangeDetectorRef, private dataService: DataService) {
        this.workOrderNumber = this.dataService.getWorkOrderNumber();
    }

    existPermission(menuId: string) {
        return this.dataService.havePermission(menuId);
    }

    providerUrl() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumProvider");
    }

    requirementURL() {
        return this.http.get(this.baseUrl + 'api/Requirement/EnumRequirement?' + 'order=' + this.workOrderNumber);
    }

    ngOnInit() {
        this.loadRequirements();
    }

    returnRequirement() {
        if (this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR") {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            return;
        }
        BreadCrumbService.setReference(this.dataService.getWorkOrderNumber().concat("-REQ-").concat(this.dataService.getNREQUISA()))
        this.dataService.setLoadReasonToReturnReqFromSupervisor(true);
    }

    srvexClick(e: any) {
        e.preventDefault();
        this.alertComponent.toastMessage = "La operaci&oacute;n solicitada no es permitida";
        this.alertComponent.warningMessage();
    }

    cmPeticionBorrado(cm: any) {

    }

    loadRequirements() {
        Util.ShowLoader();
        this.requirementURL().subscribe(result => {
            if (result != undefined) {
                this.requirements = Util.loadJsonWithToast<Requirement[]>(this.requirements, result, this.alertComponent);
                if (this.requirements.length > 0) {
                    let temp = new Requirement(this.requirements[0]);
                    Array.prototype.forEach.call(temp._Requirement.lst_ZADF_REQUOPR, function (el: any) {
                        if (el.peticionborrado == "1") {
                            el.peticionborrado = true;
                        }
                        else {
                            el.peticionborrado = false;
                        }
                    });
                    this.requirements.length = 0;
                    this.requirements.push(temp._Requirement as any);
                    this.dataService.setNREQUISA(new Requirement(this.requirements[0])._Requirement.nrequisa);
                    this.dataService.setRequirementStatus(new Requirement(this.requirements[0])._Requirement.status);
                }
                this.providerUrl().subscribe(resultProvider => {
                    this.providers = Util.loadJsonWithToast<Provider[]>(this.providers, resultProvider, this.alertComponent);
                }, error => {
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
            }
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    isSupervisor(): boolean {
        return new SessionInfo(this.sessionInfoService.sessionInfo)._SessionInfo.RoleName == "SupervisorADF"
    }

    preventProviderChange(e: any, srvex: string) {
        if (srvex != "1") {
            e.preventDefault();
        }
        if (!this.isSupervisor()) {
            e.preventDefault();
        }
        if (this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR") {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            e.preventDefault();
        }
    }

    providerChange(req: any) {
        this.updtSRVEX(req);
    }

    loadMaterials(vornr: string, nrequisa: string, ktsch: string, status: string, cm_Description: string) {
        this.dataService.setVORNR(vornr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setModelKey(ktsch);
        this.dataService.setCMDescription(cm_Description);
        this.dataService.setLoadMaterialsFromOrder(false);
        this.dataService.setRequirementStatus(status);
        this.dataService.setLoadMaterials(true);
        this.dataService.setShowOperationsMaterial(true);
    }

    preventBorrado(event: any) {
        if (this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR") {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            event.preventDefault();
        }
    }

    approveMaterials() {
        if (this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR") {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            return;
        }
        let modelKeys: ReqOper[] = [];
        Array.prototype.forEach.call(this.requirements, function (m: any) {
            let req = new Requirement(m)._Requirement;
            Array.prototype.forEach.call(req.lst_ZADF_REQUOPR, function (lst: any) {
                if ((lst as Ilst_ZADF_REQUMAT).lst_ZADF_REQUMAT.length > 0) {
                    modelKeys.push(lst);
                }
            });
        });
        if (modelKeys.length > 0) {
            this.dataService.setModelKeys(modelKeys);
            this.dataService.setLoadMaterialsFromOrder(false);
            this.dataService.setShowOperationsMaterial(true);
            this.dataService.setApproveMaterialsRequirement(true);
        }
        else {
            this.alertComponent.toastMessage = "Las claves modelo no contienen materiales";
            this.alertComponent.warningMessage();
        }
    }

    updtSRVEX(req: any) {
        Util.ShowLoader();
        req = req as Ilst_ZADF_REQUOPR;
        this.http.put(this.baseUrl + 'api/Requirement/UpdateSRVEX', req, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadRequirements();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    approveRequirement() {
        if (this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR") {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            return;
        }
        Util.ShowLoader();
        let req = { AUFNR: this.dataService.getWorkOrderNumber(), NREQUISA: this.dataService.getNREQUISA(), PNDAUT: "1", STATUS: "APRO" };
        this.http.put(this.baseUrl + 'api/Requirement/ApproveRequirement', req, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadRequirements();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    sendRequirement() {
        Util.ShowLoader();
        this.http.post(this.baseUrl + 'api/Requirement/WithoutSend', { NREQUISA: this.dataService.getNREQUISA(), MODIFNR: 1 }, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.loadRequirements();
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMessengerRequirement(requirement: any, keyModel: any) {
        BreadCrumbService.setTypeReference("Requisa materiales o servicios");
        let reference = new Requirement(requirement)._Requirement;
        if (!keyModel) {
            BreadCrumbService.setReference(this.workOrderNumber.concat("-REQ-").concat(reference.nrequisa));
        } else {
            BreadCrumbService.setReference(this.workOrderNumber.concat("-").concat(reference.nrequisa).concat("-").concat(keyModel));
        }

        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    ngAfterViewChecked() {
        if (!this.elemRendered) {
            Util.loadMaterialElements();
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Orden de Trabajo");
            BreadCrumbService.changeBreadCrumbChild("Requisa de Materiales");
        }
        if (this.dataService.getRenderRequirement()) {
            this.loadRequirements();
            this.dataService.setRenderRequirement(false);
        }
        this.renderFromAuthorizer = this.dataService.getRenderMaterialsFromAuthorizer();
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}