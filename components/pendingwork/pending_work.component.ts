import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../util/entities/session_info';
import { AlertComponent } from '../alerts/alert.component';
import { Util } from '../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../util/data_service';
import { ModelFailure } from '../util/entities/model_failure';
import { KeyDamage } from '../util/entities/key_damage';
import { PendingWorkDetail } from '../util/entities/pending_work_detail';
import { PendingWO } from '../util/entities/pending_wo';
import { NoticeType } from '../util/entities/notice_type';
import { Router } from '@angular/router';

@Component({
    selector: 'pending-work',
    templateUrl: './pending_work.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class PendingWorkComponent {
    public modelFailures: ModelFailure[] = [];
    public modelKeyDamages: KeyDamage[] = [];
    public noticesTypes: NoticeType[] = []
    filterSelectedModelFailure: string = "-1";
    filterSelectedKeyDamage: string = "-1";
    public filterNoticeType: string = "-1";
    public description: string = "";
    public refreshSelect: boolean = false;
    public refreshSelectModelFailure: boolean = false;
    public pendingWorkDetails: PendingWorkDetail[] = [];
    public oRDENID: string = "";
    public eQUIPO: string = "";
    public showFromFinishWO: boolean = false;
    public kText: string = "";
    public WorkStation: string = "";

    //initializing p to one
    pagePendingWorks: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {
    }

    ngOnInit() {

    }

    filterModelFailure() {
        return this.http.get(this.baseUrl + 'api/pendingwork/GetModelFailure?TIPOAVISO=' + this.filterNoticeType);
    }

    filterModelKeyDamage() {
        return this.http.get(this.baseUrl + 'api/pendingwork/GetKeyDamage');
    }

    noticeTypeUrl() {
        return this.http.get(this.baseUrl + 'api/pendingwork/NoticeType');
    }

    loadModelFailure() {
        Util.ShowLoader();
        this.filterModelFailure().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.modelFailures = Util.loadJsonWithToast<ModelFailure[]>(this.modelFailures, result, this.alertComponent);
            }
        })
    }

    loadKeyDamges() {
        Util.ShowLoader();
        let modelKeyDamagesBase: KeyDamage[] = [];
        this.filterModelKeyDamage().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                modelKeyDamagesBase = Util.loadJsonWithToast<KeyDamage[]>(modelKeyDamagesBase, result, this.alertComponent);

                if (this.filterSelectedModelFailure != '-1') {
                    this.modelKeyDamages = [];
                    this.modelKeyDamages.length = 0;
                    let context = this;
                    modelKeyDamagesBase = modelKeyDamagesBase.filter(function (f) { let kd = new KeyDamage(f)._KeyDamage; return kd.fegrp === context.filterSelectedModelFailure && kd.txt.trim().length > 0; });
                }

                this.modelKeyDamages = modelKeyDamagesBase;
                this.refreshSelect = true;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadNoticeTypes() {
        Util.ShowLoader();
        this.noticeTypeUrl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.noticesTypes = Util.loadJsonWithToast<NoticeType[]>(this.noticesTypes, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadPendingWorkOrderDetail() {
        let pendingWorkDetailsBase: PendingWorkDetail[] = [];
        let params: URLSearchParams = new URLSearchParams();
        params.set('ORDENID', this.oRDENID);
        params.set('EQUIPO', this.eQUIPO);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.get(this.baseUrl + "api/pendingwork/GetPendingWorkDetails", requestOptions).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                pendingWorkDetailsBase = Util.loadJsonWithToast<PendingWorkDetail[]>(pendingWorkDetailsBase, result, this.alertComponent);

                Array.prototype.forEach.call(pendingWorkDetailsBase, function (el: PendingWorkDetail) {
                    let detail: PendingWorkDetail = new PendingWorkDetail(el);//Set Status Eliminado Boolean
                });

                this.pendingWorkDetails = pendingWorkDetailsBase;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeFilterModelFailure() {
        if (this.filterSelectedModelFailure != "-1") {
            this.loadKeyDamges();
        }
        else {
            this.modelKeyDamages.length = 0;
            this.filterSelectedKeyDamage = "-1";
        }
    }

    changeNoticeType() {
        if (this.filterNoticeType != "-1") {
            this.loadModelFailure();
        } else {
            this.filterSelectedModelFailure = "-1";
            this.modelFailures.length = 0;
            this.changeFilterModelFailure();
        }
    }

    add() {
        let hasErrror: boolean = false;

        if (this.filterSelectedModelFailure == '-1') {
            this.alertComponent.toastMessage = "Debe seleccionar el tipo de falla."
            this.alertComponent.errorMessage();
            hasErrror = true;
            return;
        }

        if (this.filterSelectedKeyDamage == '-1') {
            this.alertComponent.toastMessage = "Debe seleccionar la falla."
            this.alertComponent.errorMessage();
            hasErrror = true;
            return;
        }

        if (this.filterNoticeType == '-1') {
            this.alertComponent.toastMessage = "Debe seleccionar el tipo de aviso."
            this.alertComponent.errorMessage();
            hasErrror = true;
            return;
        }

        if (this.description.trim() == "") {
            this.alertComponent.toastMessage = "Debe ingresar el texto largo."
            this.alertComponent.errorMessage();
            hasErrror = true;
            return;
        }

        if (!hasErrror) {
            let newPendingWO: PendingWO = new PendingWO();
            newPendingWO.aufnr = this.oRDENID;
            newPendingWO.id_trab_pend = "";//Get in the controller
            newPendingWO.ktsch = this.filterSelectedKeyDamage;
            newPendingWO.status = "PENDIENTE APROBACION";//Set in the controller too
            newPendingWO.descripcion = this.description;
            if (this.filterSelectedKeyDamage != '-1') {
                let selected = this.filterSelectedKeyDamage;
                let keyDamageSelected = new KeyDamage(this.modelKeyDamages.filter(function (kd) { return new KeyDamage(kd)._KeyDamage.ktsch == selected; })[0])._KeyDamage;
                newPendingWO.fegrp = keyDamageSelected.fegrp;
                newPendingWO.fecod = keyDamageSelected.fecod;
            }
            let noticeSelected = this.noticesTypes.filter(f => new NoticeType(f)._NoticeType.qmart == this.filterNoticeType)[0] as any;
            newPendingWO.qmart = noticeSelected.qmart;
            newPendingWO.qmartx = noticeSelected.qmartx;

            let params: URLSearchParams = new URLSearchParams();
            params.set('EQUIPO', this.eQUIPO);

            Util.ShowLoader();

            this.http.post(this.baseUrl + 'api/pendingwork/CreatePendingWO', newPendingWO, { params: params })
                .subscribe(result => {
                    Util.HideLoader();
                    if (result != undefined) {
                        if (result.json()["responseCode"] == 200) {
                            this.alertComponent.toastMessage = "Operacion Exitosa";
                            this.alertComponent.successMessage();
                            this.loadPendingWorkOrderDetail();
                            this.filterSelectedModelFailure = '-1';
                            this.filterSelectedKeyDamage = '-1';
                            this.filterNoticeType = "-1";
                            this.description = "";
                        }
                        else {
                            this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                            this.alertComponent.errorMessage();
                        }
                    }
                }, error => {
                    Util.HideLoader();
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
        }
    }

    pendingWOClick(order: any, e: any) {
        let pendingWO = order as IPendingWorkDetail;

        if (pendingWO.status == "ELIMINADO") {
            e.preventDefault();
            this.alertComponent.toastMessage = "No se puede eliminar en estado ELIMINADO";
            this.alertComponent.warningMessage();
        }
    }

    deleteWO(order: any) {
        Util.ShowLoader();
        let updatePendingWO = order as IPendingWorkDetail;
        updatePendingWO.status = "ELIMINADO";

        this.http.put(this.baseUrl + 'api/pendingwork/UpdatePendingWO', updatePendingWO, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.loadPendingWorkOrderDetail();
                    }
                    else {
                        this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    approvePendingWorks() {
        Util.ShowLoader();
        let params: URLSearchParams = new URLSearchParams();
        params.set('ORDENID', this.oRDENID);
        params.set('EQUIPO', this.eQUIPO);
        this.http.put(this.baseUrl + 'api/pendingwork/ApprovePendingWorks', {}, { params: params })
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.loadPendingWorkOrderDetail();
                        Util.closeModal(".pending_work_modal");
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    havePermissionToApprove() {
        if (Math.max.apply(Math, Util.getPermissions(['124'], this.dataService.getMenuList())) < 4) {
            return false;
        }
        return true;
    }

    savePendingWorks() {
        Util.ShowLoader();
        if (this.dataService.getSaveNotificationNEJE()) {
            Util.ShowLoader();
            let notify = {
                AUFNR: this.dataService.getWorkOrderNumber(),
                VORNR: this.dataService.getVORNR(),
                MOTIVO: "NEJE"
            };
            this.http.put(this.baseUrl + 'api/WO/OrderNotificationEnd', notify, {}).subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.dataService.setReloadWODetail(true);
                        Util.closeModal('.notification_modal');
                        Util.closeModal('.pending_work_modal');
                        Util.closeModal('.closure_reason_modal');
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        } else {
            this.http.put(this.baseUrl + 'api/OrderFlow/GetOperByOrder', { AUFNR: this.oRDENID, PC_STATUS: this.dataService.getwoPC_Status(), RENTNR: this.dataService.getRentnr() }, {}).subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {

                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion exitosa";
                        this.alertComponent.successMessage();
                        this.router.navigateByUrl('/bandeja-de-entrada-tecnico');
                    } else if (result.json()["responseCode"] == 409) {
                        Util.openModal(".model_key");
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
    }

    cancel() {
        Util.closeModal('.pending_work_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadPendingWork()) {
            this.dataService.setLoadPendingWork(false);
            this.showFromFinishWO = this.dataService.getLoadPendingWorkFromFinishWO();
            if (Util.getPermissions(['99', '113', '138', '152', '166'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.oRDENID = this.dataService.getWorkOrderNumber();
                this.eQUIPO = this.dataService.getEquipoModel();
                this.loadNoticeTypes();
                this.loadPendingWorkOrderDetail();
                this.kText = this.dataService.getKTEXT();
                this.WorkStation = this.dataService.getArbplr();
            }
        }
        this.cdRef.detectChanges();
    }
}
