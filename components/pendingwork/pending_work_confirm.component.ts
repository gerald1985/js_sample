import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../util/entities/session_info';
import { AlertComponent } from '../alerts/alert.component';
import { Util } from '../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../util/data_service';
import { Router } from '@angular/router';

@Component({
    selector: 'pending-work-confirm',
    templateUrl: './pending_work_confirm.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class PendingWorkConfirmComponent {
    public pendingWorkConfirmWO: string = "";
    public pc_status: string = "";

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {
    }

    ngOnInit() {
    }

    confirm() {
        this.dataService.setLoadPendingWork(true);
        Util.closeModal('.pending_work_confirm_modal');
        Util.openModal('.pending_work_modal');
    }

    cancel() {
        Util.ShowLoader();
        this.http.put(this.baseUrl + 'api/OrderFlow/GetOperByOrder', { AUFNR: this.pendingWorkConfirmWO, PC_STATUS: this.pc_status, RENTNR: this.dataService.getRentnr() }, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.router.navigateByUrl('/bandeja-de-entrada-tecnico');
                } else if (result.json()["responseCode"] == 409) {
                    Util.openModal(".model_key");
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });

        Util.closeModal('.pending_work_confirm_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadPendingWorkConfirm()) {
            this.dataService.setLoadPendingWorkConfirm(false);
            this.pendingWorkConfirmWO = this.dataService.getPendingWorkConfirmWO();
            this.pc_status = this.dataService.getwoPC_Status();
        }
        this.cdRef.detectChanges();
    }
}
