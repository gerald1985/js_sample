import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { AlertComponent } from '../alerts/alert.component';
import { Http } from '@angular/http';
import { BreadCrumbService } from '../util/breadcrumbService';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
    providers: []
})
export class NavMenuComponent {
    public sessionInfo: any;


    constructor(private cdRef: ChangeDetectorRef, private sessionInfoService: SessionInfoService) {

    }

    public setBreadcrumbs(breadcrumbRoot: string, breadcrumbChild: string) {
        BreadCrumbService.newBreadCrumbRoot(breadcrumbRoot);
        if (breadcrumbChild && breadcrumbChild.trim().length > 0) {
            BreadCrumbService.changeBreadCrumbChild(breadcrumbChild);
        }
    }

    ngAfterViewChecked() {
        this.sessionInfo = this.sessionInfoService.getSessionInfo();
        this.cdRef.detectChanges();
    }
}
