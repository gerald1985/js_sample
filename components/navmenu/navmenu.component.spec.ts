﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { NavMenuComponent } from './navmenu.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../util/util';
import { MockResponse } from '../../../test/mock/mock_reponse';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { BreadCrumbService } from '../util/breadcrumbService';
import { TopMenuComponent } from '../topmenu/topmenu.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertComponent } from '../alerts/alert.component';


let fixture: ComponentFixture<NavMenuComponent>;
let fixtureTop: ComponentFixture<TopMenuComponent>;
let comp: NavMenuComponent;
let compTop:TopMenuComponent;

describe('Testing NavMenu component', () => {
    console.log('Testing NavMenu component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [NavMenuComponent, TopMenuComponent],
            imports: [HttpModule, RouterTestingModule],
            providers: [MockResponse, AlertComponent,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" }, SessionInfoService]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(NavMenuComponent);
            fixtureTop = TestBed.createComponent(TopMenuComponent);
            fixture.detectChanges();
            fixtureTop.detectChanges();
            comp = fixture.componentInstance;
            compTop = fixtureTop.componentInstance;
            });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should validate breadcrumb on TopMenu from NavMenu Method', inject([XHRBackend], (mockBackend: any) => {
        
        comp.setBreadcrumbs("parent-text", 'child-text')
        fixture.detectChanges();
        fixtureTop.detectChanges();
        const dom = fixtureTop.nativeElement.querySelector('.bread-crumb');
        console.log(dom);
        expect(dom.textContent).toEqual('parent-text / child-text');

    }));
});