import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { AlertComponent } from '../../../alerts/alert.component';
import { Util } from '../../../util/util';
import { Http, URLSearchParams } from '@angular/http';
import { DataService } from '../../../util/data_service';
import { WorkOrderOperation } from '../../../util/entities/work_order_operation';

@Component({
    selector: 'external-service-blocker',
    templateUrl: './external_service.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class BlockerExternalServiceComponent {
    public baseUrl: string = '';
    public externalServices: WorkOrderOperation[] = [];

    pageBExternalService: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadExternalServicesUrl() {
        return this.http.get(this.baseUrl + "api/KeyDamage/EnumExternalService?aufnr=" + this.dataService.getWorkOrderNumber() + '&ktsch=' + this.dataService.getModelKey());
    }

    loadExternalServices() {
        Util.ShowLoader();
        Util.openModal('.external_service_blocker_modal');
        this.loadExternalServicesUrl().subscribe(result => {
            Util.HideLoader();
            this.externalServices = Util.loadJsonWithToast(this.externalServices, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    saveExternalServices() {
        if (Math.max.apply(Math, Util.getPermissions(['94', '108', '117', '133', '147', '161'], this.dataService.getMenuList())) <= 1) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }
        let toSave = this.externalServices.filter(function (f) {
            let oper = new WorkOrderOperation(f)._WorkOrderOperation;
            if (oper.checked != undefined && oper.checked) {
                return true;
            }
            return false;
        }).map(function (m) {
            let oper = new WorkOrderOperation(m)._WorkOrderOperation;
            return {
                AUFNR: oper.aufnr,
                VORNR: oper.vornr
            };
        });
        let params: URLSearchParams = new URLSearchParams();
        params.set('OperVorn', this.dataService.getVORNR());
        Util.ShowLoader();
        this.http.post("api/KeyDamage/UnemploymentService", toSave, { params: params }).subscribe(result => {
            Util.HideLoader();
            if (result.json()["responseCode"] == 200) {
                this.dataService.setReloadWODetail(true);
                Util.closeModal('.external_service_blocker_modal');
                Util.closeModal('.closure_reason_modal');
                Util.closeModal('.notification_modal');
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.external_service_blocker_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadBlockerExternalServices()) {
            this.dataService.setLoadBlockerExternalServices(false);
            if (Util.getPermissions(['94', '108', '117', '133', '147', '161'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadExternalServices();
            }
        }

        this.cdRef.detectChanges();
    }
}
