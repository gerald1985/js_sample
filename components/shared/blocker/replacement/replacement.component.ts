import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { AlertComponent } from '../../../alerts/alert.component';
import { Util } from '../../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../../util/data_service';
import { MaterialOper } from '../../../util/entities/material_oper';

@Component({
    selector: 'replacement-blocker',
    templateUrl: './replacement.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class BlockerReplacementComponent {
    public baseUrl: string = '';
    public kdMaterials: MaterialOper[] = [];

    pageReplacement: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadKDMaterialUrl() {
        return this.http.get(this.baseUrl + "api/KeyDamage/EnumMaterial?order=" + this.dataService.getWorkOrderNumber() + "&vornr=" + this.dataService.getVORNR());
    }

    loadKDMaterial() {
        Util.ShowLoader();
        Util.openModal('.replacement_blocker_modal');
        this.loadKDMaterialUrl().subscribe(result => {
            Util.HideLoader();
            this.kdMaterials = Util.loadJsonWithToast<MaterialOper[]>(this.kdMaterials, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.replacement_blocker_modal');
    }

    saveReplacement() {
        if (Math.max.apply(Math, Util.getPermissions(['93', '107', '116', '132', '146', '160'], this.dataService.getMenuList())) <= 1) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }

        let aufnr = this.dataService.getWorkOrderNumber();
        let vornr = this.dataService.getVORNR();
        let toSave = this.kdMaterials.filter(function (f: any) {
            let mat = new MaterialOper(f)._Material;
            if (mat.selected != undefined && mat.selected) {
                return true;
            }
            return false;
        }).map(function (m) {
            let mat = new MaterialOper(m)._Material;
            return {
                AUFNR: aufnr,
                VORNR: vornr,
                MATNR: mat.matnr,
                CNTFALT: mat.canT_FALT
            };
        });
        Util.ShowLoader();
        this.http.post("api/KeyDamage/UnemploymentMaterial", toSave, {}).subscribe(result => {
            Util.HideLoader();
            if (result.json()["responseCode"] == 200) {
                this.dataService.setReloadWODetail(true);
                Util.closeModal('.replacement_blocker_modal');
                Util.closeModal('.closure_reason_modal');
                Util.closeModal('.notification_modal');
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadReplacement()) {
            this.dataService.setLoadReplacement(false);
            if (Util.getPermissions(['93', '107', '116', '132', '146', '160'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadKDMaterial();
            }
        }
        this.cdRef.detectChanges();
    }
}
