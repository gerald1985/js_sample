import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { Util } from '../../util/util';
import { Http, URLSearchParams, RequestOptions } from '@angular/http';
import { DataService } from '../../util/data_service';
import { BreadCrumbService } from '../../util/breadcrumbService';

@Component({
    selector: 'notification',
    templateUrl: './notification.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class NotificationComponent {
    public baseUrl: string = '';
    public renderNotificationStart: boolean = true;
    public reasonTime: string = "";
    public cmDescription: string = "";
    public isNegativeTime: boolean = false;

    //Bar Chart
    single: any[] = [];

    view: any[] = [700, 250];

    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Country';
    showYAxisLabel = true;
    yAxisLabel = 'Population';
    title = "";
    colorScheme = {
        domain: ['#3b9053', '#00a9ff', '#d9a500', '#AAAAAA']
    };
    //Finish Bar Chart

    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    onSelect(event: any) {
        //console.log(event);
    }

    notificationStart() {
        if (Math.max.apply(Math, Util.getPermissions(['114', '91', '105', '130', '144', '158'], this.dataService.getMenuList())) <= 1) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }

        if (!this.dataService.getTimeFromExtenarlWorkControl()) {
            Util.ShowLoader();
            let order = { AUFNR: this.dataService.getWorkOrderNumber(), RENTNR: this.dataService.getRentnr(), VORNR: this.dataService.getVORNR() };

            let params: URLSearchParams = new URLSearchParams();
            params.set('ReceptionStatus', this.dataService.getReceptionStatus());

            let requestOptions = new RequestOptions();
            requestOptions.search = params;

            this.http.put(this.baseUrl + 'api/WO/OrderNotification', order, requestOptions).subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.dataService.setReceptionStatus("");//Limpiar para evitar cualquier envio de la variable cuando esta ya fue seteada
                        this.dataService.setRenderNotificationStart(false);
                        this.dataService.setReloadWODetail(true);
                        this.alertComponent.toastMessage = "Operacion exitosa";
                        this.alertComponent.successMessage();
                    } else {
                        var errors = result.json()["responseText"].split(',');
                        errors.forEach((err: string) => { Util.showMessage(err, "warning-toast c_yellow z-depth-3"); });
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
        else {
            let externalWorkControl = this.dataService.getExtenarlWorkControl();
            //this.dataService.setExtenarlWorkControl(externalWorkControl);
            this.dataService.setLoadServicesFromExternalWC(true);
            Util.openModal(".service_request_modal");
        }
    }

    loadClosureReason() {
        if (Math.max.apply(Math, Util.getPermissions(['114', '91', '105', '130', '144', '158'], this.dataService.getMenuList())) <= 1) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }
        if (!this.dataService.getTimeFromExtenarlWorkControl()) {
            if (document.querySelector(".closure_reason_modal") != undefined && document.querySelector(".closure_reason_modal") != null) {
                this.dataService.setLoadClosureReason(true);
                Util.openModal(".closure_reason_modal");
            }
        }
        else {
            this.dataService.setLoadExternalWorkControlReception(true);
            this.dataService.setLoadServicesFromExternalWC(true);
            Util.openModal(".service_reception_modal");
        }
    }

    loadInstructions() {
        this.dataService.setLoadCheckList(true);
        Util.openModal('.checklist_modal');
    }

    loadMessengerNofication() {
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    cancel() {
        Util.closeModal(".notification_modal");
    }

    ngAfterViewChecked() {
        this.xAxisLabel = this.dataService.getModelKey();
        this.renderNotificationStart = this.dataService.getRenderNotificationStart();
        this.cmDescription = this.dataService.getCMDescription();

        if (!this.dataService.getTimeFromExtenarlWorkControl()) {
            this.dataService.setTimeFromExtenarlWorkControl(false);
            this.reasonTime = this.dataService.getReasonTime();
        }

        if (this.dataService.getLoadDataToBarChart()) {
            this.dataService.setLoadDataToBarChart(false);
            this.title = this.dataService.getModelKey();
            this.single = this.dataService.getSingleToBarChart();
        }
        this.isNegativeTime = this.dataService.getIsNegativeTime();
        this.cdRef.detectChanges();
    }
}