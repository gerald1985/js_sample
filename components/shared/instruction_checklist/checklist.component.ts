import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../util/data_service';
import { Reason } from '../../util/entities/reason';
import { Instruction } from '../../util/entities/instruction';

@Component({
    selector: 'instruction-checklist',
    templateUrl: './checklist.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class CheckListComponent {
    public baseUrl: string = '';
    public loadReason: boolean = false;
    public instructions: Instruction[] = [];
    public modelKey: string = "";
    public isModifiable: boolean = true;

    pageModelInstructions: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadInstructionURL() {
        return this.http.get(this.baseUrl + "api/WO/EnumInstruction?order=" + this.dataService.getWorkOrderNumber() + "&vornr=" + this.dataService.getVORNR());
    }

    loadInstructions() {
        Util.ShowLoader();
        this.loadInstructionURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.instructions = Util.loadJsonWithToast<Instruction[]>(this.instructions, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    clickInstruction(event: any) {
        if (!this.isModifiable) {
            event.preventDefault();
            console.log("instruction");
        }
    }

    changeInstruction(ins: any) {
        Util.ShowLoader();
        let instruction = new Instruction(ins)._Instruction;
        this.http.put(this.baseUrl + 'api/WO/CheckInstruction', { ORDENID: instruction.aufnr, OPERID: instruction.vornr, CHECK: instruction.verif ? '1' : '', CODIGO: instruction.codinst }, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                Util.transactionWithToast(result, this.alertComponent);
                if (result.json()["responseCode"] == 200) {
                    this.dataService.setReloadWODetail(true);
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal(".checklist_modal");
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadCheckList()) {
            this.dataService.setLoadCheckList(false);
            if (Util.getPermissions(['92', '106', '115', '131', '145', '159'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadInstructions();
                this.isModifiable = this.dataService.getCheckListIsModifiable();
                this.modelKey = this.dataService.getModelKey();
            }
        }
        this.cdRef.detectChanges();
    }
}
