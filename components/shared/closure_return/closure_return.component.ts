import { Component, Inject, OnInit, Injectable, Output, EventEmitter } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { Comment } from '../../util/entities/comment'
import { DatePipe } from '@angular/common';
import { SessionInfoService } from '../../util/SessionInfoService';
import { BreadCrumbService } from '../../util/breadcrumbService';

@Component({
    selector: 'closure-return',
    templateUrl: './closure_return.component.html',
    providers: [AlertComponent],
})

@Injectable()
export class ClosureReturnComponent implements OnInit {
    public reasonToReturnComponent: string = "";
    private static commentsQueue: Comment[] = [];
    public static reference: string = '';
    public static typeReference: string = "";
    public static display: string = "";
    public static prefix: string = '';
    public static alertComponent: AlertComponent;
    public static multipleReferences: boolean = false;
    public static references: string[] = [];
    datePipe: DatePipe = new DatePipe("es-NI");
    commentDate: any = Date.now();
    private static callback: any;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public sessionInfoService: SessionInfoService) {
    }

    ngOnInit() {

    }

    public static init(reference: string, typeReference: string, alertComponent: AlertComponent, display: string, prefix: string, call: () => void, references: string[] = [], multipleReferences: boolean = false) {
        this.reference = reference;
        this.typeReference = typeReference;
        this.alertComponent = alertComponent;
        this.display = display;
        this.prefix = prefix;
        this.callback = call;
        this.references = references;
        this.multipleReferences = multipleReferences;
        Util.loadMaterialBySelector('.closure_return_component_modal', { dismissible: false }, 'Modal');
        Util.openModal('.closure_return_component_modal');
    }

    cancel() {
        this.reasonToReturnComponent = '';
        Util.closeModal('.closure_return_component_modal');
    }

    postComment(comment: any) {
        return this.http.post(this.baseUrl + 'api/WO/CreateComment', comment, {});
    }

    addClosureReturn() {
        if (this.reasonToReturnComponent.trim() == "") {
            ClosureReturnComponent.alertComponent.toastMessage = "Por favor ingresa un mensaje en el campo de texto";
            ClosureReturnComponent.alertComponent.warningMessage();
        }
        else {
            Util.ShowLoader();
            if (ClosureReturnComponent.multipleReferences) {
                let context = this;
                let comments: any[] = [];
                Array.prototype.forEach.call(ClosureReturnComponent.references, function (commentReference: string) {
                    comments.push({
                        USUARIO: new SessionInfo(context.sessionInfoService.getSessionInfo())._SessionInfo.UserName,
                        REFERENCIA: commentReference,
                        FECHA: context.datePipe.transform(context.commentDate, 'dd/MM/yyyy'),
                        HORA: context.datePipe.transform(context.commentDate, "HH:mm:ss"),
                        MODULO: "ORDERS",
                        PANTALLA: ClosureReturnComponent.display,
                        STATUS: "ACTIVE",
                        COMENTARIO: ClosureReturnComponent.prefix + ": " + context.reasonToReturnComponent,
                    });
                });
                this.postComment(comments).subscribe(result => {
                    Util.HideLoader();
                    if (result.json()["responseCode"] == 200) {
                        ClosureReturnComponent.callback();
                        BreadCrumbService.setRefreshComments(true);
                        this.cancel();
                    } else {
                        var errors = result.json()["responseText"].split(',');
                        errors.forEach((err: string) => { Util.showMessage(err, "error-toast c_yellow z-depth-3"); });
                    }
                }, error => {
                    Util.EvalError(error, ClosureReturnComponent.alertComponent, this.authUrl);
                });
            } else {
                let comment = [{
                    USUARIO: new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName,
                    REFERENCIA: ClosureReturnComponent.reference,
                    FECHA: this.datePipe.transform(this.commentDate, 'dd/MM/yyyy'),
                    HORA: this.datePipe.transform(this.commentDate, "HH:mm:ss"),
                    MODULO: "ORDERS",
                    PANTALLA: ClosureReturnComponent.display,
                    STATUS: "ACTIVE",
                    COMENTARIO: ClosureReturnComponent.prefix + ": " + this.reasonToReturnComponent,
                }];
                this.postComment(comment).subscribe(result => {
                    Util.HideLoader();
                    if (result.json()["responseCode"] == 200) {
                        ClosureReturnComponent.callback();
                        BreadCrumbService.setRefreshComments(true);
                        this.cancel();
                    } else {
                        var errors = result.json()["responseText"].split(',');
                        errors.forEach((err: string) => { Util.showMessage(err, "error-toast c_yellow z-depth-3"); });
                    }
                }, error => {
                    Util.EvalError(error, ClosureReturnComponent.alertComponent, this.authUrl);
                });
            }
        }
    }
}
