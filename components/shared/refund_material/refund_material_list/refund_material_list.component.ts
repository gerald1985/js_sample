import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../../util/entities/session_info';
import { AlertComponent } from '../../../alerts/alert.component';
import { Util } from '../../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../../util/data_service';
import { Refund } from '../../../util/entities/refund';

@Component({
    selector: 'refund-material-list',
    templateUrl: './refund_material_list.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class RefundMaterialListComponent {

    public refunds: Refund[] = [];

    pageRefundMaterialList: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadRefundMaterialListUrl() {
        return this.http.get(this.baseUrl + "api/Material/EnumRefund?AUFNR=" + this.dataService.getWorkOrderNumber());
    }

    loadRefundCode() {
        return this.http.get(this.baseUrl + "api/Material/GetSequence?AUFNR=" + this.dataService.getWorkOrderNumber());
    }

    loadMaterialUnloading(refundCode: string, arbpl: string, status: string) {
        this.dataService.setrefundCode(refundCode);
        this.dataService.setCodDev(refundCode);
        this.dataService.setArbpl(arbpl);
        if (status == "APROBADO") {
            this.dataService.setIsFromMaterialReturnControl(true);
        }
        else
            this.dataService.setIsFromMaterialReturnControl(false);
        this.dataService.setLoadMaterialUnloading(true);
        this.dataService.setStatusRefund(status);
    }

    addRefund() {
        if (this.dataService.getReceptionStatus() != "PROCESO") {
            this.alertComponent.toastMessage = "Unicamente se permite modificar en estado PROCESO";
            this.alertComponent.errorMessage();
            return;
        }
        this.loadRefundCode().subscribe(result => {
            if (result.json()["responseCode"] == 200) {
                this.dataService.setrefundCode(result.json()["data"]);
                this.dataService.setStatusRefund("");//Se limpia el estado de la devolucion
                this.dataService.setIsFromMaterialReturnControl(false);
                this.dataService.setLoadMaterialUnloading(true);
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal(".refund_material_list_modal");
    }

    loadRefundMaterialList() {
        Util.ShowLoader();
        Util.openModal('.refund_material_list_modal');
        this.loadRefundMaterialListUrl().subscribe(result => {
            Util.HideLoader();
            this.refunds = Util.loadJsonWithToast<Refund[]>(this.refunds, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadRefundMaterialList()) {
            this.dataService.setLoadRefundMaterialList(false);
            if (Util.getPermissions(['97', '111', '136', '150', '164'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadRefundMaterialList();
            }
        }
        this.cdRef.detectChanges();
    }
}
