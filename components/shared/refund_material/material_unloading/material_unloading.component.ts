import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../../util/entities/session_info';
import { AlertComponent } from '../../../alerts/alert.component';
import { Util } from '../../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../../util/data_service';
import { MaterialUnloading } from '../../../util/entities/material_unloading';
import { SessionInfoService } from '../../../util/SessionInfoService';
import { BreadCrumbService } from '../../../util/breadcrumbService';
import { MaterialDevolution } from '../../../util/entities/material_devolution';
import { ClosureReturnComponent } from '../../closure_return/closure_return.component';

@Component({
    selector: 'material-unloading',
    templateUrl: './material_unloading.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class MaterialUnloadingComponent {
    public refund_code: string = "";
    public work_station: string = "";
    public refund_order: string = "";
    public cod_dev: string = "";
    public matUnloadings: MaterialUnloading[] = [];
    public matDevolution: MaterialDevolution[] = [];
    public matDevolutionDetailList: any[] = [];
    public isFromMaterialReturnControl: boolean = false;
    public reserva: string = "";
    public statusRefund: string = "";
    public sessionInfo: any;

    pageMatUnloading: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private sessionInfoService: SessionInfoService,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadMaterialUnloadingUrl() {
        if (this.dataService.getStatusRefund() != "APROBADO") {
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialUnloading?WAAUFNR=" + this.dataService.getWorkOrderNumber() + '&WACODEV=' + this.dataService.getrefundCode());
        } else {
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialUnloadingApproved?WAAUFNR=" + this.dataService.getWorkOrderNumber() + '&WACODEV=' + this.dataService.getrefundCode());
        }
    }

    loadMatDevolutionUrl() {
        return this.http.get(this.baseUrl + "api/materialreturncontrol/GetMatDevolution?WACODDEV=" + this.dataService.getCodDev());
    }

    loadMessengerFromRefund() {
        BreadCrumbService.setTypeReference("Devoluci&oacute;n de materiales");
        BreadCrumbService.setReference(this.dataService.getWorkOrderNumber().concat("-DEV-").concat(this.dataService.getrefundCode()));
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    saveRefund() {
        let coddev = this.dataService.getrefundCode();
        let materials = this.matUnloadings.filter(function (f) {
            return new MaterialUnloading(f)._MaterialUnloading.checked;
        }).map(function (m) {
            let mat = new MaterialUnloading(m)._MaterialUnloading;
            return {
                CODDEV: coddev,
                MATNR: mat.matnr,
                CANTDEV: mat.cantdev,
                MAKTX: mat.maktg,
                AUFNR: mat.aufnr,
                VORNR: mat.vornr,
                KTSCH: mat.ktsch,
                TXT: mat.ltxA1,
                MEINH: mat.meins,
                LGPBE: mat.lgpbe,
                CANTENTR: mat.bdmng
            };
        });

        let toSave = {
            CODDEV: coddev,
            AUFNR: this.dataService.getWorkOrderNumber(),
            RESERVA: this.reserva,
            Materials: materials
        };

        if (toSave.Materials.length == 0) {
            this.alertComponent.toastMessage = "Debes seleccionar al menos un material a devolver";
            this.alertComponent.warningMessage();
        } else {
            let context = this;
            let reference = toSave.AUFNR.concat("-DEV-" + toSave.CODDEV);
            ClosureReturnComponent.init(reference, "Devolución de materiales", context.alertComponent, "WORK ORDER", "DEV", function () {
                Util.ShowLoader();
                context.http.post(context.baseUrl + "api/Material/MaterialReturn", toSave, {}).subscribe(result => {
                    Util.HideLoader();
                    if (result.json()["responseCode"] == 200) {
                        context.alertComponent.toastMessage = "Operaci&oacute;n realizada con &eacute;xito";
                        context.alertComponent.successMessage();
                        context.cancel();
                        context.dataService.setLoadRefundMaterialList(true);
                    }
                    else {
                        context.alertComponent.toastMessage = result.json()["responseText"];
                        context.alertComponent.warningMessage();
                    }
                }, error => {
                    Util.EvalError(error, context.alertComponent, context.authUrl);
                });
            });
        }
    }

    havePermissionToApprove(): boolean {
        if (Math.max.apply(Math, Util.getPermissions(['124'], this.dataService.getMenuList())) < 4) {
            return false;
        }
        return true;
    }

    print(refund_code: string, order: string) {
        if (Util.getPermissions(['112', '123', '137', '151', '165'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
        } else {
            window.open(this.baseUrl + "/api/Material/PrintMaterialUnloading?WAAUFNR=" + order + "&WACODEV=" + refund_code, "_blank");
        }
    }

    evalCant(matUnloading: any) {
        let mat = new MaterialUnloading(matUnloading)._MaterialUnloading;
        let bdmng = parseFloat(parseFloat(mat.bdmng).toFixed(2));
        if (!mat.cantdev) {
            matUnloading.cantdev = bdmng;
        } else {
            let cantdev = parseFloat(parseFloat(mat.cantdev.toString()).toFixed(2));
            if (mat.cantdev <= 0) {
                matUnloading.cantdev = bdmng;
            }
            if (cantdev > bdmng) {
                matUnloading.cantdev = bdmng;
            }
            matUnloading.cantdev = parseFloat(matUnloading.cantdev).toFixed(2);
        }
    }

    cancel() {
        Util.closeModal(".material_unloading_modal");
    }

    loadMaterialUnloading() {
        Util.ShowLoader();
        Util.openModal('.material_unloading_modal');
        this.loadMaterialUnloadingUrl().subscribe(result => {
            Util.HideLoader();
            this.matUnloadings = Util.loadJsonWithToast<MaterialUnloading[]>(this.matUnloadings, result, this.alertComponent);
            if (this.matUnloadings.length > 0) {
                this.reserva = new MaterialUnloading(this.matUnloadings[0])._MaterialUnloading.rsnum;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMaterialDevolution() {
        Util.ShowLoader();
        Util.openModal('.material_unloading_modal');
        this.loadMatDevolutionUrl().subscribe(result => {
            Util.HideLoader();

            if (result != undefined) {

                this.matDevolution = Util.loadJsonWithToast<MaterialDevolution[]>(this.matDevolution, result, this.alertComponent);
                if (this.matDevolution.length > 0) {
                    this.reserva = new MaterialDevolution(this.matDevolution[0])._IMaterialDevolution.reserva;
                }
                var resultJSON = Util.getDataFromResponseJSON(result)[0];
                if (resultJSON != undefined) {
                    this.refund_code = resultJSON.coddev;
                    this.work_station = resultJSON.puestocrea;
                    this.refund_order = resultJSON.aufnr;
                    this.statusRefund = resultJSON.status;
                    this.matDevolutionDetailList = resultJSON.maT_DEV_DET_LIST;
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    confirmChangeStatus(paramsChangeStatusMatDev: any) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + "api/materialreturncontrol/ChangeStatus", paramsChangeStatusMatDev, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                if (result.json()["responseCode"] == 200) {

                    this.dataService.setReloadMatReturnControl(true);
                    this.dataService.setIsFromMaterialReturnControl(true);
                    this.dataService.setLoadMaterialUnloading(true);
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeStatusMatDevolution(status: string) {

        let UserName = new SessionInfo(this.sessionInfoService.sessionInfo)._SessionInfo.UserName;
        let reserva = this.reserva;
        let paramsChangeStatusMatDev = this.matDevolution.map(function (el) {
            let matDev = new MaterialDevolution(el)._IMaterialDevolution;

            return {
                CodDev: matDev.coddev,
                PUESTOCREA: matDev.puestocrea,
                PUESTOSUPER: UserName,
                AUFNR: matDev.aufnr,
                Status: status,
                FECHA_CREA: matDev.fechA_CREA,
                HORA_CREA: matDev.horA_CREA,
                RESERVA: reserva,
                FECHA_APRUEBA: Util.formatDate(new Date()),
                HORA_APRUEBA: Util.formatTime(new Date())

            }
        });
        if (status == "RECHAZADO") {
            let context = this;
            ClosureReturnComponent.init(this.refund_order.concat("-DEV-").concat(this.refund_code),
                "Devoluci&oacute;n de materiales", this.alertComponent, "Devolucion de materiales", "RECH", function () {
                    context.confirmChangeStatus(paramsChangeStatusMatDev[0]);
                });
        }
        else {
            this.confirmChangeStatus(paramsChangeStatusMatDev[0]);
        }
    }

    ngAfterViewChecked() {
        this.sessionInfo = this.sessionInfoService.getSessionInfo();
        if (this.dataService.getLoadMaterialUnloading()) {
            this.dataService.setLoadMaterialUnloading(false);
            if (Util.getPermissions(['98', '122'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                if (this.dataService.getIsFromMaterialReturnControl()) {
                    this.isFromMaterialReturnControl = this.dataService.getIsFromMaterialReturnControl();
                    this.loadMaterialDevolution();
                }
                else {
                    this.isFromMaterialReturnControl = this.dataService.getIsFromMaterialReturnControl();
                    this.refund_code = this.dataService.getrefundCode();
                    this.work_station = this.dataService.getArbplr();
                    this.refund_order = this.dataService.getWorkOrderNumber();
                    this.loadMaterialUnloading();
                }
            }
        }
        this.cdRef.detectChanges();
    }
}
