import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../util/data_service';
import { Reason } from '../../util/entities/reason';
import { SessionInfoService } from '../../util/SessionInfoService';
import { ClosureReturnComponent } from '../closure_return/closure_return.component';
import { AlertQuestionnaireComponent } from '../alert_questionnaire/alert_questionnaire.component';

@Component({
    selector: 'closure-reason',
    templateUrl: './closure_reason.component.html',
    styleUrls: [],
    providers: [AlertComponent, ClosureReturnComponent],
})

export class ClosureReasonComponent {
    public baseUrl: string = '';
    public loadReason: boolean = false;
    public reasons: Reason[] = [];
    public modelKey: string = "";

    pageModelReasons: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public questionAlert: ClosureReturnComponent,
        private sessionInfoService: SessionInfoService,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    loadReasonsURL() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumReason");
    }

    loadReasons() {
        Util.ShowLoader();
        this.loadReasonsURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.reasons = Util.loadJsonWithToast<Reason[]>(this.reasons, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    //evalPermission(permissionId: number, description: string) {
    //    return Util.havePermission(permissionId, description, this.dataService.getMenuList());
    //}

    setClosureReason(reason: any) {
        let closure = new Reason(reason)._Reason;
        let notify = {
            AUFNR: this.dataService.getWorkOrderNumber(),
            VORNR: this.dataService.getVORNR(),
            MOTIVO: closure.motivo,
        };
        if (closure.motivo == "PREP" || closure.motivo == "PSER") {
            reason.checked = false;
            if (closure.motivo == "PREP") {
                this.dataService.setLoadReplacement(true);
            }
            else {
                this.dataService.setLoadBlockerExternalServices(true);
            }
            //Util.ShowLoader();
            //this.http.get("api/KeyDamage/EvalOperations?order=" + this.dataService.getWorkOrderNumber() + "&ktsch=" + this.dataService.getModelKey() + '&motivo=' + closure.motivo).subscribe(result => {
            //    Util.HideLoader();
            //    if (result.json()["responseCode"] == 200) {
            //        reason.checked = false;
            //        if (closure.motivo == "PREP") {
            //            this.dataService.setLoadReplacement(true);
            //        }
            //        else {
            //            this.dataService.setLoadBlockerExternalServices(true);
            //        }
            //    } else {
            //        reason.checked = false;
            //        this.alertComponent.toastMessage = result.json()["responseText"];
            //        this.alertComponent.warningMessage();
            //        return false;
            //    }
            //}, error => {
            //    reason.checked = false;
            //    Util.EvalError(error, this.alertComponent, this.authUrl);
            //});
        } else {
            if (closure.motivo == "FCMO") {
                let reference = this.dataService.getWorkOrderNumber().concat("-OPER-" + this.dataService.getVORNR())
                let context = this;
                reason.checked = false;
                AlertQuestionnaireComponent.init("Estas seguro de Finalizar la Clave Modelo ?", function () {
                    context.setCustomReason(notify);
                }, function () { });
            }
            else if (closure.motivo == "NEJE") {
                let reference = this.dataService.getWorkOrderNumber().concat("-OPER-" + this.dataService.getVORNR())
                let context = this;
                reason.checked = false;
                this.dataService.setSaveNotificationNEJE(true);
                AlertQuestionnaireComponent.init("Hay trabajos pendientes ?", function () {
                    context.dataService.setLoadPendingWork(true);
                    Util.openModal('.pending_work_modal');
                }, function () {
                    ClosureReturnComponent.init(reference, "Paro de servicio", context.alertComponent, "WORK ORDER", "NEJE: ", function () { context.setCustomReason(notify); });
                });
            } else {
                this.setCustomReason(notify);
            }

        }
    }

    setCustomReason(notify: any) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + 'api/WO/OrderNotificationEnd', notify, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                Util.closeModal('.closure_reason_modal');
                if (result.json()["responseCode"] == 200) {
                    this.dataService.setReloadWODetail(true);
                    Util.closeModal('.notification_modal');
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal(".closure_reason_modal");
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadClosureReason()) {
            this.loadReasons();
            this.modelKey = this.dataService.getModelKey();
            this.dataService.setLoadClosureReason(false);
        }
        this.cdRef.detectChanges();
    }
}
