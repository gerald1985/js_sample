import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Router } from '@angular/router';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';
import { WOConcluded } from '../../util/entities/wo_concluded';

@Component({
    selector: 'wo-concluded',
    templateUrl: './wo_concluded.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class WOConcludedComponent {
    public navigation: string = "";
    public elemRendered = false;
    public wordersConcluded: WOConcluded[] = [];
    public woConcludedList: string[] = [];
    public filterWOConcluded: string = "-1";
    public refreshSelect: boolean = false;
    public havePermission: boolean = false;

    //initializing p to one
    pageWOConcluded: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {
    }

    ngOnInit() {

    }

    loadWOConcluded() {
        if (Util.getPermissions(['48', '56', '64', '74', '79', '83'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();

        let wordersConcludedBase: WOConcluded[] = [];

        this.http.get(this.baseUrl + "api/woconcluded/GetWOConcluded").subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                wordersConcludedBase = Util.loadJsonWithToast<WOConcluded[]>(wordersConcludedBase, result, this.alertComponent);

                if (this.filterWOConcluded != '-1') {
                    wordersConcludedBase = wordersConcludedBase.filter(f => new WOConcluded(f).IWOConcluded.aufnr === this.filterWOConcluded);
                }

                this.wordersConcluded = wordersConcludedBase;
                this.loadWOConcludedList();
                this.refreshSelect = true;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadWOConcludedList() {
        this.woConcludedList = this.wordersConcluded.map(function (e) {
            return new WOConcluded(e).IWOConcluded.aufnr;
        });

        this.woConcludedList = Array.from(new Set(this.woConcludedList.map((item: any) => item)));
    }

    changeFilterWOConcluded() {
        this.loadWOConcluded();
    }

    toRequirement(aufnr: any, nrequisa: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(aufnr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setRenderDeleteColumnMat(false);
        this.dataService.setShowOperationsMaterial(false)
        this.router.navigateByUrl('/orden-de-trabajo-supervisor');
    }

    ngAfterViewChecked() {
        //if (!this.elemRendered && (this.wordersConcluded.length > 0 || this.wordersConcluded != undefined)) {
        //    Util.loadMaterialElements();
        //    this.elemRendered = true;
        //    //BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada");
        //    //BreadCrumbService.changeBreadCrumbChild("OT Concluidas");
        //    Util.HideLoader();
        //}

        if (this.refreshSelect) {
            Util.loadMaterialBySelector("select.filter_sup", {}, "FormSelect");
            this.refreshSelect = false;
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}
