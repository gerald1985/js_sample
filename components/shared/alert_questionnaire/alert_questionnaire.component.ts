import { Component, Inject, OnInit, Injectable } from '@angular/core';
import { Util } from '../../util/util';
import { Http } from '@angular/http';

@Component({
    selector: 'alert-questionnaire',
    templateUrl: './alert_questionnaire.component.html',
    providers: [],
})

@Injectable()
export class AlertQuestionnaireComponent implements OnInit {
    public static question: string = "";
    private static callbackConfirm: any;
    private static callbackCancel: any;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string) {
    }

    ngOnInit() {

    }

    get staticQuestion() {
        return AlertQuestionnaireComponent.question;
    }

    public static init(question: string, callConfirm: () => void, callCancel: () => void) {
        AlertQuestionnaireComponent.question = question;
        this.callbackConfirm = callConfirm;
        this.callbackCancel = callCancel;
        Util.loadMaterialBySelector('.alert_questionnaire_modal', { dismissible: false }, 'Modal');
        Util.openModal('.alert_questionnaire_modal');
    }

    cancel() {
        AlertQuestionnaireComponent.callbackCancel();
        Util.closeModal('.alert_questionnaire_modal');
    }

    confirm() {
        AlertQuestionnaireComponent.callbackConfirm();
        Util.closeModal('.alert_questionnaire_modal');
    }
}
