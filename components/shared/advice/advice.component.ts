import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';
import { Advice } from '../../util/entities/Advice';

@Component({
    selector: 'Advice',
    templateUrl: './Advice.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class AdviceComponent {
    public Advices: Advice[] = [];
    public loadSource: boolean = false;
    public wAEQUNR: string = '';
    public wAAUFNR: string = '';

    //initializing p to one
    pageAdvice: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.loadSearchParams();
    }

    loadSearchParams() {
        this.wAEQUNR = this.dataService.getEquipoModel();
        this.wAAUFNR = this.dataService.getWorkOrderNumber();
    }

    loadNotification() {
        Util.ShowLoader();
        let params: URLSearchParams = new URLSearchParams();
        params.set('WAEQUNR', this.wAEQUNR);
        params.set('WAAUFNR', this.wAAUFNR);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.get(this.baseUrl + "api/Advice/GetByEquipment", requestOptions).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.Advices = Util.loadJsonWithToast<Advice[]>(this.Advices, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.advice_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadAdvices()) {
            this.dataService.setLoadAdvices(false);
            if (Util.getPermissions(['96', '110', '135', '149', '163'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadSearchParams();
                this.loadNotification();
            }
        }
        this.cdRef.detectChanges();
    }
}
