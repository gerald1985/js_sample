﻿/// <reference path="../../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { CommentComponent } from './comment.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';
import { AlertComponent } from '../../alerts/alert.component';
import { DataService } from '../../util/data_service';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPaginationModule } from 'ngx-pagination'; ;
import { WOAssignment } from '../../util/entities/wo_assignment';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../../util/util';
import { Comment } from '../../util/entities/comment'
import { MockResponse } from '../../../../test/mock/mock_reponse';
import { WorkOrderOperation } from '../../util/entities/work_order_operation';
import { WorkOrderHeader } from '../../util/entities/work_order_header';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { FormsModule } from '@angular/forms';
import { SessionInfoService } from '../../util/SessionInfoService';
import { SessionInfo } from '../../util/entities/session_info';


let fixture: ComponentFixture<CommentComponent>;
let comp: CommentComponent;

describe('Testing Comment component', () => {
    console.log('Testing Comment component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CommentComponent],
            imports: [HttpModule, RouterTestingModule, FormsModule],
            providers: [MockResponse,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" },
                AlertComponent, SessionInfoService]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(CommentComponent);
            fixture.detectChanges();
            comp = fixture.componentInstance;
            comp.sessionInfoService = TestBed.get(SessionInfoService);
            comp.sessionInfoService.setSessionInfo(TestBed.get(MockResponse).UserInfoResponse().data as SessionInfo);
            comp.sessionInfo = comp.sessionInfoService.getSessionInfo();
            });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should Create Comment', inject([XHRBackend], (mockBackend: any) => {

        var responses: Response[] = [];
        
        const mockResponse = TestBed.get(MockResponse).loadCommentsResponse();
        
        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(mockResponse) })));
        
        mockBackend.connections.subscribe((connection: any) => {
            var response = responses.shift();
            connection.mockRespond(response);
        });
        
        BreadCrumbService.setReference("882-8000053714")

        comp.commentsRequest(BreadCrumbService.getReference()).subscribe(result => {
            console.log(result);
            
            comp.commentsQueue = result.json()["data"] as Comment[];
            const quehueActors = comp.commentsQueue.map(data => new Comment(data).getComment().usuario.split("@")[0].replace(".", " "));
            comp.actors = quehueActors.filter((x, i, a) => x && a.indexOf(x) === i)
        });

        fixture.detectChanges();

        const actorList = fixture.nativeElement.querySelectorAll('#actorsList span');
        Array.prototype.forEach.call(actorList, (actor : any) => {
            expect(actor.textContent).toEqual(comp.actors);
        });
        

        const commentReference = fixture.nativeElement.querySelector('#commentReference');
        expect(commentReference.textContent).toEqual(BreadCrumbService.getReference());
    }));

    it('should Create / Update Comment', inject([XHRBackend], (mockBackend: any) => {

        var resultArray: any;
        var responses: Response[] = [];

        const mockResponse = TestBed.get(MockResponse).loadCommentsResponse();

        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(mockResponse) })));

        mockBackend.connections.subscribe((connection: any) => {
            var response = responses.shift();
            connection.mockRespond(response);
        });

        BreadCrumbService.setReference("882-8000053714")

        comp.commentsRequest(BreadCrumbService.getReference()).subscribe(result => {
            console.log(result);
            resultArray = result.json()["data"];
            comp.commentsQueue = result.json()["data"] as any[];
            const quehueActors = comp.commentsQueue.map(data => new Comment(data).getComment().usuario.split("@")[0].replace(".", " "));
            comp.actors = quehueActors.filter((x, i, a) => x && a.indexOf(x) === i)
        });
        fixture.detectChanges();

        const comments = Array.from(fixture.nativeElement.querySelectorAll('.comment'));
        let i: number = 0;
        comments.forEach((comment: any) => {
            expect(comment.textContent).toEqual(new Comment(comp.commentsQueue[i]).getComment().comentario);
            i++; 

            console.log(comment.textContent);
        });

        comp.messageToSend = "TestCasePrueba";
        fixture.detectChanges();

        const messageToSend = fixture.nativeElement.querySelector("#comment");
        console.log("contenedor de texto ", messageToSend);
        
        comp.createComment();
        comp.messageToSend = "TestCasePrueba";
        fixture.detectChanges();

        console.log(comp.messageToSend);
        let comment = {
            idcomment: "240",
            usuario: "luis.berrios@ccn.com.ni",
            referencia: BreadCrumbService.getReference(),
            fecha: comp.datePipe.transform(comp.commentDate, 'dd/MM/yyyy'),
            hora: comp.datePipe.transform(comp.commentDate, "HH:mm:ss"),
            modulo: "ORDERS",
            pantalla: "WORK ORDER",
            status: "ACTIVE",
            comentario: "TestCasePrueba",
        };

        console.log(comment);
        resultArray.push(comment)
        comp.commentsQueue = resultArray as Comment[];

        console.log(comp.commentsQueue);
        fixture.detectChanges();
        let lastComment: any = Array.from(fixture.nativeElement.querySelectorAll('.comment'))[3];
        console.log("last comment  ", lastComment);
        expect(lastComment.textContent).toEqual("TestCasePrueba");

        BreadCrumbService.setCommentId("240");

        comp.loadCommentInfoToUpdate("240", "Mensaje Actualizado");

        comp.createComment();

        resultArray[3].comentario = "Mensaje Actualizado";

        fixture.detectChanges();
        lastComment = Array.from(fixture.nativeElement.querySelectorAll('.comment'))[3];
        console.log("last comment  ", lastComment);
        expect(lastComment.textContent).toEqual("Mensaje Actualizado");

    }));
});