import { Component, Inject, ChangeDetectorRef, Input } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { Comment } from '../../util/entities/comment'
import { MessageUpdater } from '../../util/entities/message_updater';
import { DatePipe } from '@angular/common';
import { SessionInfoService } from '../../util/SessionInfoService';
import { BreadCrumbService } from '../../util/breadcrumbService';

@Component({
    selector: 'comment',
    templateUrl: './comment.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class CommentComponent {
    public commentsQueue: Comment[] = [];
    public baseUrl: string = '';
    public reference: string = '';
    public typeReference: string = "";
    datePipe: DatePipe = new DatePipe("es-NI");
    commentDate: any = Date.now();;
    messageToSend: string = "";
    sessionInfo: SessionInfo;

    public actors: string[] = []

    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public sessionInfoService: SessionInfoService) {
        this.baseUrl = baseUrl;

        //this.createComment(Util.interfaceObjects(this.commentDetail, message));
    }

    commentsRequest(reference: string) {
        return this.http.get(this.baseUrl + 'api/WO/GetCommentsByReference?reference=' + reference);
    }

    loadComments(reference: string) {
        if (reference != "") {
            this.typeReference = BreadCrumbService.getTypeReference();
            Util.ShowLoader();
            this.commentsRequest(reference).subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    this.commentsQueue = Util.loadJsonWithToast<Comment[]>(this.commentsQueue, result, this.alertComponent);
                    const quehueActors = this.commentsQueue.map(data => new Comment(data).getComment().usuario.split("@")[0].replace(".", " "));
                    this.actors = quehueActors.filter((x, i, a) => x && a.indexOf(x) === i)
                }

            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
    }



    loadCommentInfoToUpdate(idComment: string, newComment: string) {
        BreadCrumbService.setCommentId(idComment);
        this.messageToSend = newComment;
        let el = document.getElementById("comment");
        if (el != null)
            el.focus();
    }

    clean() {
        BreadCrumbService.setCommentId("");
        this.messageToSend = "";
    }

    updateComment() {
        let updatedComment = new MessageUpdater({
            IDCOMENT: BreadCrumbService.getCommentId(),
            NUEVOTEXTO: this.messageToSend
        })._MessageUpdater;
        Util.ShowLoader();
        this.putComment(updatedComment).subscribe(result => {
            if (result != undefined) {
                Util.HideLoader();
                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    this.loadComments(this.reference);
                    this.clean();
                } else {
                    var errors = result.json()["responseText"].split(',');
                    errors.forEach((err: string) => { Util.showMessage(err, "error-toast c_yellow z-depth-3"); });
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    putComment(updatedComment: any) {
        return this.http.put(this.baseUrl + 'api/WO/UpdateComment', updatedComment, {})
    }

    createComment() {
        if (BreadCrumbService.getCommentId() == "") {
            let comment = [{
                USUARIO: new SessionInfo(this.sessionInfo)._SessionInfo.UserName,
                REFERENCIA: BreadCrumbService.getReference(),
                FECHA: this.datePipe.transform(this.commentDate, 'dd/MM/yyyy'),
                HORA: this.datePipe.transform(this.commentDate, "HH:mm:ss"),
                MODULO: "ORDERS",
                PANTALLA: "WORK ORDER",
                STATUS: "ACTIVE",
                COMENTARIO: this.messageToSend,
            }];
            if (this.messageToSend == "") {
                this.alertComponent.toastMessage = "Ingrese un comentario.";
                this.alertComponent.errorMessage();
            } else {
                Util.ShowLoader();
                this.postComment(comment).subscribe(result => {
                    Util.HideLoader();
                    if (result != undefined) {
                        if (result.json()["responseCode"] == 200) {
                            this.alertComponent.toastMessage = "Operacion exitosa";
                            this.alertComponent.successMessage();
                            this.loadComments(this.reference);
                            this.clean();
                        } else {
                            var errors = result.json()["responseText"].split(',');
                            errors.forEach((err: string) => { Util.showMessage(err, "error-toast c_yellow z-depth-3"); });
                        }
                    }
                }, error => {
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
            }
        } else {
            this.updateComment();
        }

    }

    postComment(comment: any) {
        return this.http.post(this.baseUrl + 'api/WO/CreateComment', comment, {});
    }

    ngAfterViewChecked() {
        this.sessionInfo = this.sessionInfoService.getSessionInfo();
        if (this.reference != BreadCrumbService.getReference()) {
            this.reference = BreadCrumbService.getReference();
            this.loadComments(this.reference);
        }

        if (BreadCrumbService.getRefreshComments() && this.reference != "") {
            BreadCrumbService.setRefreshComments(false);
            this.loadComments(this.reference);
        }

        this.cdRef.detectChanges();
    }
}
