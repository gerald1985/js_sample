import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';
import { EquipmentHistory } from '../../util/entities/equipment_history';

@Component({
    selector: 'equipment-history',
    templateUrl: './equipment_history.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class EquipmentHistoryComponent {
    public sources: EquipmentHistory[] = [];
    private loadSource: boolean = false;
    private eQUIPO: string = '';
    private fE_REF: string = '';
    public fechaFilter: string = '';
    //initializing p to one
    pageEquipmentHistory: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.loadSearchParams();
        this.fechaFilter = this.fE_REF;
    }

    loadSearchParams() {
        this.eQUIPO = this.dataService.getEquipoModel();
        this.fE_REF = Util.dateAddYearInFormatYYYYMMDD(new Date(), -1);//requirement Current Date in Last Year (-1 Year)  
    }

    loadEquipmentHistory() {
        Util.ShowLoader();
        let params: URLSearchParams = new URLSearchParams();
        params.set('EQUIPO', this.eQUIPO);
        params.set('FE_REF', this.fE_REF);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.get(this.baseUrl + "api/EquipmentHistory/GetEquipmentHistory", requestOptions).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.sources = Util.loadJsonWithToast<EquipmentHistory[]>(this.sources, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        //this.dataService.setModelKeys([]);
        Util.closeModal('.equipment_history_modal');
    }

    filterByDate() {
        this.fE_REF = this.fechaFilter; //Change the date param to the API
        this.loadEquipmentHistory();
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadEquipmentHistory()) {
            this.dataService.setLoadEquipmentHistory(false);
            if (Util.getPermissions(['95', '109', '134', '148', '162'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadSearchParams();
                this.fechaFilter = this.fE_REF;
                this.loadEquipmentHistory();
            }
        }
        this.cdRef.detectChanges();
    }
}
