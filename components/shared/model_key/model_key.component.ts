import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../util/data_service';
import { ModelKey } from '../../util/entities/ModelKey';
import { SystemMK } from '../../util/entities/system_mk';

@Component({
    selector: 'model-key',
    templateUrl: './model_key.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class ModelKeyComponent {
    public baseUrl: string = '';
    public modelKeys: ModelKey[] = [];
    public systemMK: SystemMK[] = [];
    public filterCode: string = '';
    public filterSystem: string = '-1';
    public filterDescription: string = '';
    public renderSelect: boolean = false;
    public urlParameters: string = "";

    pageModelKey: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;

    }

    modelKeysRequest() {
        this.urlParameters = 'api/ModelKey/EnumModelKeys?equipModel=' + this.dataService.getEquipModel();
        if (this.filterSystem && this.filterSystem != '-1') {
            this.urlParameters += '&filterSystem=' + this.filterSystem;
        }

        if (this.filterCode && this.filterCode.trim().length > 0) {
            this.urlParameters += '&filterCode=' + this.filterCode;
        }

        if (this.filterDescription && this.filterDescription.trim().length > 0) {
            this.urlParameters += '&filterDescription=' + this.filterDescription;
        }
        return this.http.get(this.baseUrl + this.urlParameters);
    }

    SystemMKRequest() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumSystemMK?equipModel=' + this.dataService.getEquipModel());
    }

    AssociateRequest() {

        let obj = { Nrequisa: this.dataService.getNREQUISA(), Elements: this.modelKeys.filter(f => new ModelKey(f)._ModelKey.selected), AUFNR: this.dataService.getWorkOrderNumber() };
        return this.http.post(this.baseUrl + 'api/ModelKey/AssociateMK', obj, {});
    }

    loadModelKeys() {
        Util.ShowLoader();
        this.modelKeysRequest().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.modelKeys = Util.loadJsonWithToast<ModelKey[]>(this.modelKeys, result, this.alertComponent);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadSystemMK() {
        this.SystemMKRequest().subscribe(result => {
            if (result != undefined) {
                this.systemMK = Util.loadJsonWithToast<SystemMK[]>(this.systemMK, result, this.alertComponent);
                this.renderSelect = false;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    search() {
        this.loadModelKeys();
    }

    clean() {
        this.filterSystem = '-1';
        this.filterCode = '';
        this.filterDescription = '';
        this.renderSelect = false;
        this.modelKeys.length = 0;
    }

    addMK() {
        if (this.modelKeys.filter(f => new ModelKey(f)._ModelKey.selected).length == 0) {
            this.alertComponent.toastMessage = "Debes seleccionar al menos una clave modelo";
            this.alertComponent.warningMessage();
            return;
        }
        Util.ShowLoader();
        this.AssociateRequest().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                Util.transactionWithToast(result, this.alertComponent);
                this.dataService.setRenderRequirement(true);
                Util.closeModal('.model_key');
                this.clean();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        if (this.modelKeys.filter(f => new ModelKey(f)._ModelKey.selected).length > 0) {
            let temp: ModelKey[] = [];
            temp = this.modelKeys;
            Array.prototype.forEach.call(temp, function (el: ModelKey) {
                let reverse: ModelKey = new ModelKey(el);
                reverse._ModelKey.selected = false;
            });
            this.modelKeys = temp;
        }
        Util.closeModal(".model_key");
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadModelKeys()) {
            this.dataService.setLoadModelKeys(false);
            this.clean();
            if (Util.getPermissions(['88', '102', '127', '141', '155'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                Util.closeModal(".model_key");
            } else {
                this.loadSystemMK();
            }
        }
        this.cdRef.detectChanges();
    }
}
