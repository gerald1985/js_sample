import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../../util/entities/session_info';
import { AlertComponent } from '../../../alerts/alert.component';
import { Util } from '../../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../../util/data_service';
import { MaterialOper } from '../../../util/entities/material_oper';

@Component({
    selector: 'materials-authorizer',
    templateUrl: './materials_authorizer.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class MaterialsAuthorizerComponent {
    public baseUrl: string = '';
    public materials: MaterialOper[] = [];
    public showDeleteColumn: boolean = false;

    pageMaterialsAuthorizer: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    materialsByOper() {
        if (this.dataService.getLoadMaterialsFromOrder()) {
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialsByOrder?" + "ORDENID=" + this.dataService.getWorkOrderNumber() + "&VORNR=" + this.dataService.getVORNR());
        }
        else {
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialsByRequirement?" + "REQUID=" + this.dataService.getNREQUISA() + "&VORNR=" + this.dataService.getVORNR());
        }
    }

    //updateStatus(mat: any) {
    //    Util.ShowLoader();
    //    let toUpdate = {
    //        NREQUISA: mat.nrequisa,
    //        VORNR: mat.vornr,
    //        matnr: mat.matnr,
    //        CANT: mat.cant,
    //        AUTOR: mat.autor,
    //        CNINM: mat.cninm,
    //        PTBORR: mat.ptborr,
    //        CNTAUT: mat.cntaut,
    //        CNTDEV: mat.cntdev,
    //        DESCRIPCION: mat.descripcion,
    //        PUNITARIO: mat.punitario,
    //        MAKTX: mat.maktx,
    //        ACTIVO: mat.activo,
    //        MEINS: mat.meins,
    //        IsNew: mat.isnew
    //    };
    //    this.http.put(this.baseUrl + "api/Material/UpdateStatus", toUpdate, {}).subscribe(result => {
    //        Util.transactionWithToast(result, this.alertComponent);
    //    }, error => {
    //        Util.HideLoader();
    //        this.alertComponent.toastMessage = "Ocurrio un error inesperado con una consulta.";
    //        this.alertComponent.successMessage();
    //        console.error(error);
    //    });
    //}

    requirementMaterials() {
        let nrequisa = this.dataService.getNREQUISA();
        let vornr = this.dataService.getVORNR();
        let toSave = this.materials.map(function (m) {
            let mat = new MaterialOper(m)._Material;
            return {
                NREQUISA: nrequisa,
                VORNR: vornr,
                MATNR: mat.matnr,
                CANT: mat.cant,
                PUNITARIO: mat.punitario,
                MAKTX: mat.maktx,
                MEINS: mat.meins,
                IsNew: mat.isnew
            };
        });
        return this.http.post(this.baseUrl + "api/Material/RequirementMaterials", { AUFNR: this.dataService.getWorkOrderNumber(), RequirementMaterials: toSave }, {});
    }

    cerrarModal() {
        Util.closeModal('.materials_authorizer_modal');
    }

    //addMaterials() {
    //    this.dataService.setLoadSearchMaterials(true);
    //}

    //saveMaterials() {
    //    this.requirementMaterials().subscribe(result => {

    //    }, error => console.error(error));
    //}

    loadMaterialsAuthorizer() {
        Util.openModal('.materials_authorizer_modal');
        Util.ShowLoader();
        this.materialsByOper().subscribe(result => {
            Util.HideLoader();
            if (result != undefined)
                this.materials = Util.loadJsonWithToast<MaterialOper[]>(this.materials, result, this.alertComponent);
        }, error => {
            Util.HideLoader();
            this.alertComponent.toastMessage = "Ocurrio un error inesperado con una consulta.";
            this.alertComponent.successMessage();
            console.error(error);
        });
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadMaterials()) {
            this.dataService.setLoadMaterials(false);
            this.showDeleteColumn = this.dataService.getRenderDeleteColumnMat();
            this.loadMaterialsAuthorizer();
        }
        if (this.dataService.getLoadMaterialsToComponent()) {
            this.dataService.setLoadMaterialsToComponent(false);
            let matFromSearch = this.dataService.getMaterialsFromSearch() as MaterialOper[];
            let matToComponent: MaterialOper[] = [];
            let existingMaterials: MaterialOper[] = [];
            let temp = this.materials;
            Array.prototype.forEach.call(matFromSearch, function (el: any) {
                if (temp.map(function (m) { return new MaterialOper(m)._Material.matnr; }).indexOf(new MaterialOper(el)._Material.matnr) != -1) {
                    existingMaterials.push(el);
                }
                else {
                    el.punitario = el.verpr;
                    el.isnew = true;
                    el.cant = 1;
                    matToComponent.push(el);
                }
            });
            this.materials = this.materials.concat(matToComponent);
            this.dataService.setMaterialsFromSearch([]);
            if (existingMaterials.length > 0) {
                this.alertComponent.toastMessage = "Los siguientes materiales ya exist&iacute;an en lista: " + existingMaterials.map(m => new MaterialOper(m)._Material.matnr).join();
                this.alertComponent.warningMessage();
            }
        }
        this.cdRef.detectChanges();
    }
}
