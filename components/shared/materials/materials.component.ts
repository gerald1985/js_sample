import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../util/data_service';
import { MaterialOper } from '../../util/entities/material_oper';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { ReqOper } from '../../util/entities/requirement_operation';
import { ClosureReturnComponent } from '../closure_return/closure_return.component';
import { SessionInfoService } from '../../util/SessionInfoService';

@Component({
    selector: 'materials',
    templateUrl: './materials.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class MaterialsComponent {
    public baseUrl: string = '';
    public materials: MaterialOper[] = [];
    public showDeleteColumn: boolean = false;
    public showOperations: boolean = false;
    public loadByModelKey: boolean = false;
    public renderFromAuthorizer: boolean = false;
    public indexToRemove: number = 0;
    public KTSCH: string = "";
    public requirementStatus: string = "";
    public cm_Description: string = "";

    pageMaterials: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private sessionInfoService: SessionInfoService,
        private dataService: DataService) {
        this.baseUrl = baseUrl;
    }

    materialsByOper() {
        if (this.dataService.getLoadMaterialsFromOrder()) {
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialsByOrder?" + "ORDENID=" + this.dataService.getWorkOrderNumber() + "&VORNR=" + this.dataService.getVORNR());
        }
        else {
            if (this.loadByModelKey) {
                let nrequisa = '';
                let vornr = '';
                let indexToRemove = 0;
                let ktsch = "";
                let cmDescription = "";
                Array.prototype.every.call(this.dataService.getModelKeys(), function (value: any, index: number, _arr: any) {
                    nrequisa = (value as Ilst_ZADF_REQUOPR).nrequisa;
                    vornr = (value as Ilst_ZADF_REQUOPR).vornr;
                    ktsch = (value as Ilst_ZADF_REQUOPR).ktsch;
                    cmDescription = (value as Ilst_ZADF_REQUOPR).txtclave;
                    indexToRemove = index;
                    return false;
                });
                this.indexToRemove = indexToRemove;
                this.KTSCH = ktsch;
                this.cm_Description = cmDescription;
                return this.http.get(this.baseUrl + "api/Material/EnumMaterialsByRequirement?" + "REQUID=" + nrequisa + "&VORNR=" + vornr);
            }
            return this.http.get(this.baseUrl + "api/Material/EnumMaterialsByRequirement?" + "REQUID=" + this.dataService.getNREQUISA() + "&VORNR=" + this.dataService.getVORNR());
        }
    }

    evalKeyPress(event: any) {
        if (event.keyCode < 48 || event.keyCode > 57) {
            event.preventDefault();
        }
    }

    supervisorCondition(): boolean {
        return this.isSupervisor() && this.dataService.getRequirementStatus() != "INGR";
    }

    isSupervisor(): boolean {
        return new SessionInfo(this.sessionInfoService.sessionInfo)._SessionInfo.RoleName == "SupervisorADF"
    }

    evalCant(mat: any) {
        if (isNaN(parseInt(mat.cant))) {
            mat.cant = 1;
        }
        var value = parseFloat(mat.cant);
        if (!((value | 0) === value)) {
            mat.cant = 1;
        }
        mat.cost = (mat.punitario * mat.cant).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
    }

    evalCntaut(mat: any) {
        if (isNaN(parseInt(mat.cntaut))) {
            mat.cntaut = 1;
        }
        var value = parseFloat(mat.cntaut);
        if (!((value | 0) === value)) {
            mat.cntaut = 1;
        }
        if (mat.cntaut > mat.cant) {
            mat.cntaut = mat.cant;
        }
        this.cntautChange(mat);
    }

    cntautChange(material: IMaterialOper) {
        let reference: string = "";
        if (material.matnr == "0") {
            reference = this.dataService.getWorkOrderNumber().concat("-").concat(this.dataService.getNREQUISA()).concat("-").concat(this.dataService.getModelKey()).concat("-").concat(material.matnr).concat("-" + material.descripcion);
        }
        else
            reference = this.dataService.getWorkOrderNumber().concat("-").concat(this.dataService.getNREQUISA()).concat("-").concat(this.dataService.getModelKey()).concat("-").concat(material.matnr);
        let context = this;
        ClosureReturnComponent.init(reference, "", this.alertComponent, "WORK ORDER", "Cantidad sugerida", function () {

        });
    }

    updateStatus(mat: any) {
        if (this.requirementStatus != 'CREA') {
            this.alertComponent.toastMessage = '&Uacute;nicamente se permite borrar en estado CREA';
            this.alertComponent.warningMessage();
        } else {
            Util.ShowLoader();
            let matReceived = new MaterialOper(mat)._Material;
            if (matReceived.isnew) {
                Util.HideLoader();
                if (matReceived.cninm == "1") {
                    this.materials.splice(this.materials.map(function (e) { return new MaterialOper(e)._Material.descripcion; }).indexOf(matReceived.descripcion), 1);
                } else {
                    this.materials.splice(this.materials.map(function (e) { return new MaterialOper(e)._Material.matnr; }).indexOf(matReceived.matnr), 1);
                }
            }
            else {
                let toUpdate = this.fillMaterialObject(mat);
                toUpdate.PTBORR = true;
                this.http.put(this.baseUrl + "api/Material/UpdateStatus", toUpdate, {}).subscribe(result => {
                    if (result.json()["responseCode"] == 200) {
                        Util.HideLoader();
                        this.dataService.setRenderRequirement(true);
                        this.loadMaterials();
                        this.alertComponent.toastMessage = "Operacion exitosa";
                        this.alertComponent.successMessage();
                    }
                }, error => {
                    Util.EvalError(error, this.alertComponent, this.authUrl);
                });
            }
        }
    }

    clickRequest(event: any) {
        if (this.requirementStatus != "INGR" && !this.dataService.getRenderMaterialsFromAuthorizer()) {
            event.preventDefault();
            this.alertComponent.toastMessage = '&Uacute;nicamente se permite actualizar en estado INGR';
            this.alertComponent.warningMessage();
        } else if (this.dataService.getRenderMaterialsFromAuthorizer() && this.requirementStatus != "APRO") {
            event.preventDefault();
            this.alertComponent.toastMessage = '&Uacute;nicamente se permite actualizar en estado A';
            this.alertComponent.warningMessage();
        }
    }

    deleteRequest(material: any) {
        let mat = this.fillMaterialObject(material);
        this.http.put(this.baseUrl + "api/Material/DeleteRequest", mat, {}).subscribe(result => {
            Util.transactionWithToast(result, this.alertComponent);
            this.loadMaterials();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    fillMaterialObject(mat: any) {
        return {
            OrderNumber: this.dataService.getWorkOrderNumber(),
            NREQUISA: mat.nrequisa,
            VORNR: mat.vornr,
            MATNR: mat.matnr,
            CANT: mat.cant,
            AUTOR: mat.autor,
            CNINM: mat.cninm,
            PTBORR: mat.ptborr,
            CNTAUT: mat.cntaut,
            CNTDEV: mat.cntdev,
            DESCRIPCION: mat.descripcion,
            PUNITARIO: mat.punitario,
            MAKTX: mat.maktx,
            ACTIVO: mat.activo,
            MEINS: mat.meins,
            IsNew: mat.isnew
        };
    }

    getListMaterialsToApprove() {
        let nrequisa = this.dataService.getNREQUISA();
        let vornr = this.dataService.getVORNR();
        let toSave = this.materials.map(function (m) {
            let mat = new MaterialOper(m)._Material;
            return {
                NREQUISA: mat.nrequisa,
                VORNR: mat.vornr,
                MATNR: mat.matnr,
                CANT: mat.cant,
                AUTOR: mat.autor,
                CNINM: mat.cninm,
                PTBORR: mat.ptborr,
                CNTAUT: mat.cntaut,
                CNTDEV: mat.cntdev,
                DESCRIPCION: mat.descripcion,
                PUNITARIO: mat.punitario,
                MAKTX: mat.maktx,
                ACTIVO: mat.activo,
                MEINS: mat.meins,
                IsNew: mat.isnew
            };
        });
        return toSave;
    }

    getMaterialSuggestedAmount(mat: IMaterialOper) {
        let nrequisa = this.dataService.getNREQUISA();
        let vornr = this.dataService.getVORNR();
        let toSave = {
            NREQUISA: mat.nrequisa,
            VORNR: mat.vornr,
            MATNR: mat.matnr,
            CANT: mat.cant,
            AUTOR: mat.autor,
            CNINM: mat.cninm,
            PTBORR: mat.ptborr,
            CNTAUT: mat.cntaut,
            CNTDEV: mat.cntdev,
            DESCRIPCION: mat.descripcion,
            PUNITARIO: mat.punitario,
            MAKTX: mat.maktx,
            ACTIVO: mat.activo,
            MEINS: mat.meins,
            IsNew: mat.isnew
        };
        console.log(mat);
        return toSave;
    }

    getListMaterialsToSave() {
        let nrequisa = this.dataService.getNREQUISA();
        let vornr = this.dataService.getVORNR();
        let toSave = this.materials.map(function (m) {
            let mat = new MaterialOper(m)._Material;
            return {
                NREQUISA: mat.nrequisa,
                VORNR: mat.vornr,
                MATNR: mat.matnr,
                CANT: mat.cant,
                AUTOR: mat.autor,
                CNINM: mat.cninm,
                PTBORR: mat.ptborr,
                CNTAUT: mat.cntaut,
                CNTDEV: mat.cntdev,
                DESCRIPCION: mat.descripcion,
                PUNITARIO: mat.punitario,
                MAKTX: mat.maktx,
                ACTIVO: mat.activo,
                MEINS: mat.meins,
                IsNew: mat.isnew
            };
        });
        return { AUFNR: this.dataService.getWorkOrderNumber(), RequirementMaterials: toSave };
    }

    fillPricesUrl(toFill: MaterialOper[]) {
        return this.http.get(this.baseUrl + "api/Material/FillPrices?MaterialsToFill=" + toFill.map(m => new MaterialOper(m)._Material.matnr).join("&MaterialsToFill="), );
    }

    requirementMaterials() {
        return this.http.post(this.baseUrl + "api/Material/RequirementMaterials", this.getListMaterialsToSave(), {});
    }

    approveMaterials() {
        if (this.supervisorCondition()) {
            this.alertComponent.toastMessage = "Unicamente se permite actualizar en estado INGR";
            this.alertComponent.warningMessage();
            return;
        }
        Util.ShowLoader();
        if (this.materials.filter(function (f) {
            return new MaterialOper(f)._Material.ptborr;
        }).length > 0) {
            Util.HideLoader();
            this.alertComponent.toastMessage = "Existen materiales con petici&oacute;n de borrado, por lo que no se puede aprobar.";
            this.alertComponent.warningMessage();
        }
        else {
            this.http.post(this.baseUrl + "api/Material/ApproveMaterials", this.getListMaterialsToApprove(), {}).subscribe(result => {
                Util.HideLoader();
                if (result.json()["responseCode"] == 200) {
                    if (this.loadByModelKey && this.dataService.getModelKeys().length > 0) {
                        let temp: ReqOper[] = this.dataService.getModelKeys();
                        temp.splice(this.indexToRemove, 1);
                        this.dataService.setModelKeys(temp);
                        if (temp.length == 0) {
                            Util.closeModal('.materials_modal');
                        } else {
                            this.loadMaterials();
                        }
                    }
                    this.dataService.setRenderRequirement(true);
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.warningMessage();
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
    }

    cancel() {
        this.dataService.setModelKeys([]);
        Util.closeModal('.materials_modal');
    }

    addMaterials() {
        if (this.dataService.getRequirementStatus() != "CREA") {
            this.alertComponent.toastMessage = "&Uacute;nicamente se permite agregar en estado CREA";
            this.alertComponent.warningMessage();
        }
        else {
            this.dataService.setLoadSearchMaterials(true);
        }

    }

    saveMaterials() {
        if (this.requirementStatus != 'CREA') {
            this.alertComponent.toastMessage = '&Uacute;nicamente se permite actualizar en estado CREA';
            this.alertComponent.warningMessage();
        } else {
            Util.ShowLoader();
            this.requirementMaterials().subscribe(result => {
                if (result.json()["responseCode"] == 200) {
                    this.dataService.setRenderRequirement(true);
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                    Util.closeModal('.materials_modal');
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }

    }

    loadMaterials() {
        this.KTSCH = this.dataService.getModelKey();
        this.cm_Description = this.dataService.getCMDescription();
        Util.openModal('.materials_modal');
        Util.ShowLoader();
        this.materialsByOper().subscribe(result => {
            Util.HideLoader();
            if (result != undefined)
                this.materials = Util.loadJsonWithToast<MaterialOper[]>(this.materials, result, this.alertComponent);
            let temp: MaterialOper[] = [];
            let suggest = this.showOperations && !this.showDeleteColumn;
            Array.prototype.forEach.call(this.materials, function (el: any) {
                if (el.ptborr == '0') {
                    el.ptborr = false;
                }
                else {
                    el.ptborr = true;
                }
                if (suggest) {
                    el.cntaut = el.cant;
                }
                el.cost = (el.punitario * el.cant).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                temp.push(el);
            });
            this.materials = temp;
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMaterialMessenger(matnr: string, description: string) {
        BreadCrumbService.setTypeReference("Materiales");
        if (this.dataService.getLoadMaterialsFromOrder()) {
            BreadCrumbService.setReference(this.dataService.getWorkOrderNumber().concat("-").concat(this.dataService.getVORNR()).concat("-").concat(this.dataService.getModelKey()).concat("-").concat(matnr));
        }
        else {
            if (matnr == "0") {
                BreadCrumbService.setReference(this.dataService.getWorkOrderNumber().concat("-").concat(this.dataService.getNREQUISA()).concat("-").concat(this.dataService.getModelKey()).concat("-").concat(matnr).concat("-" + description));
            } else
                BreadCrumbService.setReference(this.dataService.getWorkOrderNumber().concat("-").concat(this.dataService.getNREQUISA()).concat("-").concat(this.dataService.getModelKey()).concat("-").concat(matnr));
        }
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadMaterials()) {
            this.dataService.setLoadMaterials(false);
            if (Util.getPermissions(['89', '103', '128', '142', '156'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                Util.closeModal(".materials_modal");
            } else {
                this.showDeleteColumn = this.dataService.getRenderDeleteColumnMat();
                this.showOperations = this.dataService.getShowOperationsMaterial();
                console.log(this.showOperations);
                //this.renderFromAuthorizer = this.dataService.getRenderMaterialsFromAuthorizer();
                this.loadMaterials();
            }
        }

        this.requirementStatus = this.dataService.getRequirementStatus();
        if (this.dataService.getLoadMaterialsToComponent()) {
            this.dataService.setLoadMaterialsToComponent(false);
            Util.ShowLoader();
            let matFromSearch = this.dataService.getMaterialsFromSearch();
            let matToComponent: MaterialOper[] = [];
            let existingMaterials: MaterialOper[] = [];
            let nrequisa = this.dataService.getNREQUISA();
            let vornr = this.dataService.getVORNR();
            let temp = this.materials;
            Array.prototype.forEach.call(matFromSearch, function (el: any) {
                let toEval = new MaterialOper(el)._Material;
                if (toEval.matnr == "0") {
                    if (temp.filter(f => new MaterialOper(f)._Material.descripcion == toEval.descripcion).length != 0) {
                        existingMaterials.push(el);
                    }
                    else {
                        el.nrequisa = nrequisa;
                        el.vornr = vornr;
                        el.punitario = el.verpr;
                        el.isnew = true;
                        el.cant = 1;
                        if (el.matnr != '0') {
                            el.cninm = '0';
                        }
                        el.cost = (el.punitario * el.cant).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        matToComponent.push(el);
                    }
                } else {
                    if (temp.map(function (m) { return new MaterialOper(m)._Material.matnr; }).indexOf(toEval.matnr) != -1) {
                        existingMaterials.push(el);
                    }
                    else {
                        el.nrequisa = nrequisa;
                        el.vornr = vornr;
                        el.punitario = el.verpr;
                        el.isnew = true;
                        el.cant = 1;
                        if (el.matnr != '0') {
                            el.cninm = '0';
                        }
                        el.cost = (el.punitario * el.cant).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        matToComponent.push(el);
                    }
                }
            });

            this.fillPricesUrl(matToComponent).subscribe(result => {
                if (result.json()["responseCode"] == 200) {
                    let filledPrices: MaterialOper[] = [];
                    filledPrices = Util.loadJsonWithToast<MaterialOper[]>(filledPrices, result, this.alertComponent);
                    Array.prototype.forEach.call(filledPrices, function (fe: any) {
                        let material = new MaterialOper(fe)._Material;
                        if (matToComponent.map(m => new MaterialOper(m)._Material.matnr).indexOf(material.matnr) != -1) {
                            let found = new MaterialOper(matToComponent[matToComponent.map(m => new MaterialOper(m)._Material.matnr).indexOf(material.matnr)]);
                            found._Material.punitario = material.punitario;
                            found._Material.cost = (parseFloat(material.punitario) * found._Material.cant).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
                        }
                    });
                    this.materials = this.materials.concat(matToComponent);
                    Util.HideLoader();
                } else {
                    this.alertComponent.toastMessage = "No se logr� obtener los precios de los materiales";
                    this.alertComponent.errorMessage();
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
            this.dataService.setMaterialsFromSearch([]);
        }
        if (this.dataService.getApproveMaterialsRequirement()) {
            this.dataService.setApproveMaterialsRequirement(false);
            Util.loadMaterialBySelector('.materials_modal', { dismissible: false }, 'Modal');
            this.showDeleteColumn = this.dataService.getRenderDeleteColumnMat();
            this.loadByModelKey = true;
            this.showOperations = this.dataService.getShowOperationsMaterial();
            this.loadMaterials();
        }
        this.cdRef.detectChanges();
    }
}
