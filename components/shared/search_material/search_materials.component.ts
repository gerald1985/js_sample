import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http } from '@angular/http';
import { DataService } from '../../util/data_service';
import { Material } from '../../util/entities/material';
import { MaterialOper } from '../../util/entities/material_oper';
import { MaterialGroup } from '../../util/entities/material_group';

@Component({
    selector: 'material-search',
    templateUrl: './search_materials.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class SearchMaterialsComponent {
    public baseUrl: string = '';
    public renderMaterials: boolean = false;
    public filterCode: string = "";
    public filterDescription: string = "";
    public materials: Material[] = [];
    public temporaryMaterials: Material[] = [];
    public groups: MaterialGroup[] = [];
    public filterGroup: string = "-1";
    public immediateConsumption: boolean = false;

    pageMaterialSearch: number = 1;
    pageMaterialSearchTemporary: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
        this.baseUrl = baseUrl;

    }

    materialURL() {
        let urlRequest = this.baseUrl + "api/Material/EnumMaterials";
        let matnrParameter = this.filterCode.trim().length > 0 ? "&matnr=" + this.filterCode.trim() : "";
        let descriptionParameter = this.filterDescription.trim().length > 0 ? "&description=" + this.filterDescription.trim() : "";


        if (this.filterGroup != '-1') {
            urlRequest += "?group=" + this.filterGroup + matnrParameter + descriptionParameter;
        } else if (this.filterCode.trim().length > 0) {
            urlRequest += "?matnr=" + this.filterCode.trim() + descriptionParameter;
        } else if (this.filterDescription.trim().length > 0) {
            urlRequest += "?description=" + this.filterDescription.trim();
        }
        return this.http.get(urlRequest);
    }

    immediateChange() {
        this.clean();
        if (this.immediateConsumption) {
            this.materials = [];
        }
        //else {
        //    this.filterMaterials();
        //    this.renderMaterials = true;
        //}
    }

    filterMaterials() {
        this.loadMaterials();
    }

    loadgroups() {
        Util.ShowLoader();
        this.http.get(this.baseUrl + "api/Catalog/EnumMaterialGroups").subscribe(result => {
            Util.HideLoader();
            if (result != undefined)
                this.groups = Util.loadJsonWithToast<MaterialGroup[]>(this.groups, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadSearchMaterials() {
        this.clean();
        this.immediateConsumption = false;
        this.materials = [];
        Util.openModal('.search_materials_modal');
        this.loadgroups();
    }

    loadMaterials() {
        Util.ShowLoader();
        this.materialURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined)
                this.materials = Util.loadJsonWithToast<Material[]>(this.materials, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    clean() {
        this.filterCode = '';
        this.filterDescription = '';
        this.filterGroup = '-1';
    }

    cancel() {
        Util.closeModal('.search_materials_modal');
    }

    onSelect(mat: IMaterial) {
        if (this.temporaryMaterials.map(m => new Material(m)._Material.matnr).indexOf(mat.matnr) == -1) {
            this.temporaryMaterials.push(mat as any);
        }
        this.materials.splice(this.materials.map(m => new Material(m)._Material.matnr).indexOf(mat.matnr), 1);
    }

    onDeselect(mat: IMaterial) {
        if (this.materials.map(m => new Material(m)._Material.matnr).indexOf(mat.matnr) == -1) {
            this.materials.push(mat as any);
        }
        this.temporaryMaterials.splice(this.temporaryMaterials.map(m => new Material(m)._Material.matnr).indexOf(mat.matnr), 1);
    }

    addMaterial() {
        if (this.immediateConsumption) {
            if (this.filterDescription.trim().length > 0) {
                let temp = new Material({ matnr: '0', descripcion: this.filterDescription.trim(), maktx: '', selected: true, cninm: '1', meins: '', verpr: '0' })._Material as any;
                this.temporaryMaterials.push(temp);
            }
            else {
                this.alertComponent.toastMessage = "La descripci&oacute;n es requerida";
                this.alertComponent.warningMessage();
                return;
            }
        }
        let materialsFromSearch = this.temporaryMaterials.filter(f => new Material(f)._Material.selected) as MaterialOper[];
        if (materialsFromSearch.length == 0) {
            this.alertComponent.toastMessage = 'Debes seleccionar al menos un material';
            this.alertComponent.warningMessage();
        } else {
            this.dataService.setMaterialsFromSearch(materialsFromSearch);
            this.dataService.setLoadMaterialsToComponent(true);
            this.clean();
            this.temporaryMaterials.length = 0;
            Util.closeModal('.search_materials_modal');
        }
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadSearchMaterials()) {
            this.dataService.setLoadSearchMaterials(false);
            if (Util.getPermissions(['90', '104', '129', '143', '157'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                Util.closeModal(".search_materials_modal");
            } else
                this.loadSearchMaterials();
        }
        this.cdRef.detectChanges();
    }
}
