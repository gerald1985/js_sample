import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';
import { EquipmentDetail } from '../../util/entities/equipment_detail';

@Component({
    selector: 'equipment-detail',
    templateUrl: './equipment_detail.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class EquipmentDetailComponent {
    public details: EquipmentDetail[] = [];
    public footer: EquipmentDetail;
    public wARENTNR: string = '';
    public wAEQUNR: string = '';
    public descEquip: string = '';
    public descSubEquip: string = '';
    public numSubEquip: string = '';
    public fecRecep: string = '';
    public hrRecep: string = '';
    public tiempoTaller: string = "";
    public totalCola: number = 0;
    public totalExecution: number = 0;
    public totalStop: number = 0;
    public totalTheoretical: number = 0;
    public showFooter: boolean = false;

    //initializing p to one
    pageEquipmentDetail: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.loadSearchParams();
    }

    loadSearchParams() {
        this.wARENTNR = this.dataService.getRentnr();
        this.wAEQUNR = this.dataService.getEQUNR();
    }

    loadHeaderEquipmentDetail() {
        Util.openModal('.equipment_detail_modal');
        Util.ShowLoader();
        let params: URLSearchParams = new URLSearchParams();
        params.set('WARENTNR', this.wARENTNR);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.get(this.baseUrl + "api/EquipmentDetail/GetHeaderEquipmentDetail", requestOptions).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                var headerResult = Util.getDataFromResponseJSON(result)[0];

                if (headerResult != undefined) {
                    this.wAEQUNR = headerResult.equipos;
                    this.descEquip = headerResult.desceqs;
                    this.descSubEquip = headerResult.desceqi;
                    this.numSubEquip = headerResult.equipoi;
                    this.fecRecep = headerResult.fechA_REC;
                    this.hrRecep = headerResult.horA_REC;
                    this.tiempoTaller = headerResult.tiempoTaller;
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadEquipmentDetail() {
        let params: URLSearchParams = new URLSearchParams();
        params.set('WARENTNR', this.wARENTNR);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.get(this.baseUrl + "api/EquipmentDetail/GetEquipmentDetail", requestOptions).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.details = Util.loadJsonWithToast<EquipmentDetail[]>(this.details, result, this.alertComponent);
                if (this.details.length > 0) {
                    this.footer = result.json()["footer"] as EquipmentDetail;
                    this.showFooter = true;
                }
                else
                    this.showFooter = false;

            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.equipment_detail_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadEquipmentDetails()) {
            this.dataService.setLoadEquipmentDetails(false);
            if (Util.getPermissions(['100'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadSearchParams();
                this.loadHeaderEquipmentDetail();
                this.loadEquipmentDetail();
            }
        }
        this.cdRef.detectChanges();
    }
}
