import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';
import { AlertControl } from '../../util/entities/alert';

@Component({
    selector: 'alert-control',
    templateUrl: './alert_control.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class AlertControlComponent {
    public AlertsControl: AlertControl[] = [];

    //initializing p to one
    pageAlertControl: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    ngOnInit() {
        this.loadAlertsControl();
    }

    loadAlertsControl() {
        Util.ShowLoader();
        let AlertsControlBase: AlertControl[] = [];
        this.http.get(this.baseUrl + "api/alert/GetAlerts").subscribe(result => {
            Util.HideLoader();

            AlertsControlBase = Util.loadJsonWithToast<AlertControl[]>(AlertsControlBase, result, this.alertComponent);
            Array.prototype.forEach.call(AlertsControlBase, function (el: AlertControl) {
                let alert: AlertControl = new AlertControl(el);

                alert.IAlertControl.retraso = Util.convertMinToDDHHMMSS(parseInt(alert.IAlertControl.retraso));
            })

            this.AlertsControl = AlertsControlBase;

        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.alert_control_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadAlertControl()) {
            this.dataService.setLoadAlertControl(false);
            if (Util.getPermissions(['125', '139', '153', '167', '168'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                this.loadAlertsControl();
            }
        }
        this.cdRef.detectChanges();
    }
}
