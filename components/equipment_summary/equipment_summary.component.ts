import { Component, Inject, OnInit, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { EquipmentSummary } from '../util/entities/equipment_summary';

@Component({
    selector: 'equipment-summary',
    templateUrl: './equipment_summary.component.html',
    providers: [AlertComponent]
})
export class EquipmentSummaryComponent implements OnInit {
    public navigation: string = "";
    public equipmentSummary: EquipmentSummary[] = [];
    public equipmentSummaryCreated: EquipmentSummary[] = [];
    public equipmentSummaryProcess: EquipmentSummary[] = [];
    public equipmentSummaryConcluded: EquipmentSummary[] = [];
    public havePermission: boolean = false;
    public elemRendered: boolean = false;

    pageEquipmentSummaryCreated: number = 1;
    pageEquipmentSummaryProcess: number = 1;
    pageEquipmentSummaryConcluded: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    equipmentSummaryUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumEquipmentSummary');
    }

    ngOnInit() {
        //this.loadEquipmentSummary();
    }

    loadEquipmentSummary() {

        if (Util.getPermissions(['49', '57', '65', '75', '80', '84'], this.dataService.getMenuList()).length != 0) {
            this.havePermission = true;
        }
        else {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }

        Util.ShowLoader();
        this.equipmentSummaryUrl().subscribe(result => {
            if (result != undefined) {
                this.equipmentSummary = Util.loadJsonWithToast<EquipmentSummary[]>(this.equipmentSummary, result, this.alertComponent);
                this.equipmentSummaryCreated = this.equipmentSummary.filter(f => new EquipmentSummary(f)._EquipmentSummary.statusdr == "CREADO");
                this.equipmentSummaryProcess = this.equipmentSummary.filter(f => new EquipmentSummary(f)._EquipmentSummary.statusdr == "PROCESO");
                this.equipmentSummaryConcluded = this.equipmentSummary.filter(f => new EquipmentSummary(f)._EquipmentSummary.statusdr == "CONCLUIDO");
                this.elemRendered = true;
            }
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });
    }

    ngAfterViewChecked() {
        if (this.elemRendered) {
            this.elemRendered = false;
            Util.loadMaterialElements();
            //BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada t&eacute;cnico");
            //BreadCrumbService.changeBreadCrumbChild("Resumen x Equipo");
            Util.HideLoader();
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}