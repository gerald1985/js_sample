import { Component, Inject, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { MaterialsComponent } from '../shared/materials/materials.component';
import { MaterialReturnControl } from '../util/entities/material_return_control';
import { Supervisor } from '../util/entities/supervisor';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { KeyDamageStatus } from '../util/entities/key_damage_status';
import { Provider } from '../util/entities/provider';


@Component({
    selector: 'material-return-control',
    templateUrl: './material_return_control.component.html',
    providers: [AlertComponent]
})
export class MaterialReturnControlComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public matreturncontrols: MaterialReturnControl[] = [];
    matRCSupervisors: Supervisor[] = [];
    filterSelectedSupervisorMatRC: string = "-1";
    public statusList: string[] = [];
    public filterStatus: string = "-1";
    public wAPUESTOSUPER: string = "";
    public refreshSelect: boolean = false;
    public havePermission: boolean = false;

    //initializing p to one
    pageDevSup: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService,
        private sessionInfoService: SessionInfoService) {
    }

    ngOnInit() {
        //this.loadSupervisor();        
    }

    filterSupURl() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }

    loadSupervisor() {
        if (Util.getPermissions(['54'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.filterSupURl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.matRCSupervisors = Util.loadJsonWithToast<Supervisor[]>(this.matRCSupervisors, result, this.alertComponent);
                var user = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
                var found = this.matRCSupervisors.find(function (elem) { return new Supervisor(elem)._Supervisor.usuario.toUpperCase() == user; });

                if (found != undefined) {
                    this.filterSelectedSupervisorMatRC = new Supervisor(found)._Supervisor.usuario;
                }
                else {
                    this.filterSelectedSupervisorMatRC = new Supervisor(this.matRCSupervisors[0])._Supervisor.usuario;
                }

                this.wAPUESTOSUPER = this.filterSelectedSupervisorMatRC;

                //Load Table On Init after load the Supervisor Filter
                this.loadMaterialReturnControl();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeSupervisor() {
        this.wAPUESTOSUPER = this.filterSelectedSupervisorMatRC;
        this.loadMaterialReturnControl();
    }

    loadMatReturnControl() {
        return this.http.get(this.baseUrl + 'api/materialreturncontrol/GetMaterialReturnControl?WAPUESTOSUPER=' + this.wAPUESTOSUPER);
    }

    loadMaterialReturnControl() {
        let externalworkcontrolsBase: MaterialReturnControl[] = [];
        this.loadMatReturnControl().subscribe(result => {
            if (result != undefined) {
                externalworkcontrolsBase = Util.loadJsonWithToast<MaterialReturnControl[]>(this.matreturncontrols, result, this.alertComponent);

                this.loadStatusListMRC(externalworkcontrolsBase);
                if (this.filterStatus != '-1' && this.filterStatus != "Todos") {
                    externalworkcontrolsBase = externalworkcontrolsBase.filter(f => new MaterialReturnControl(f)._IMaterialReturnControl.status === this.filterStatus);
                }

                this.matreturncontrols = externalworkcontrolsBase;
                this.refreshSelect = true;
            }
        })
    }

    loadStatusListMRC(controlbase: any[]) {
        this.statusList = controlbase.map(function (e) {
            return new MaterialReturnControl(e)._IMaterialReturnControl.status;
        });
        this.statusList = Util.getUniqueValuesFromArray(this.statusList);
        if (this.statusList.filter(f => f == "Pendiente de aprobar").length == 0) {
            this.filterStatus = "Todos";
        } else if (this.statusList.length > 0 && this.filterStatus == "-1") {
            this.filterStatus = "Pendiente de aprobar";
        }
    }

    changeFilterStatus() {
        this.loadMaterialReturnControl();
    }

    loadMatDevolution(coddev: string, order: string) {
        this.dataService.setCodDev(coddev);
        this.dataService.setrefundCode(coddev);
        this.dataService.setWorkOrderNumber(order);
        this.dataService.setIsFromMaterialReturnControl(true);
        this.dataService.setLoadMaterialUnloading(true);
    }

    ngAfterViewChecked() {

        if (this.dataService.getReloadMatReturnControl()) {
            this.dataService.setReloadMatReturnControl(false);
            this.loadMaterialReturnControl();
        }

        if (this.refreshSelect) {
            Util.loadMaterialBySelector("select.filter_sup", {}, "FormSelect");
            this.refreshSelect = false;
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}