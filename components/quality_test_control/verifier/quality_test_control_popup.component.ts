import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { SessionInfo } from '../../util/entities/session_info';
import { AlertComponent } from '../../alerts/alert.component';
import { Util } from '../../util/util';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { DataService } from '../../util/data_service';

@Component({
    selector: 'quality-test-control-popup',
    templateUrl: './quality_test_control_popup.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class QualityTestControlPopupComponent {
    public qualityTestControl: any;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    ngOnInit() {
    }

    accept() {
        Util.ShowLoader();

        this.http.put(this.baseUrl + 'api/qualitytestcontrol/ConfirmQualityTestControl', this.qualityTestControl, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.dataService.setLoadQualityTestControlFromPopup(true);
                    }
                    else {
                        this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });

        //Refresh Grid
        let thisModal = this;
        setTimeout(function () {
            Util.closeModal('.quality_test_control_popup_modal');
            thisModal.dataService.setLoadQualityTestControlFromPopup(true);
        }, 2000);
    }

    decline() {
        Util.ShowLoader();

        this.http.put(this.baseUrl + 'api/qualitytestcontrol/DeclineQualityTestControl', this.qualityTestControl, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                    }
                    else {
                        this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });

        //Refresh Grid
        let thisModal = this;
        setTimeout(function () {
            Util.closeModal('.quality_test_control_popup_modal');
            thisModal.dataService.setLoadQualityTestControlFromPopup(true);
        }, 2000);
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadQualityTestControl()) {
            this.qualityTestControl = this.dataService.getQualityTestControl();
            this.dataService.setLoadQualityTestControl(false);
        }
        this.cdRef.detectChanges();
    }
}
