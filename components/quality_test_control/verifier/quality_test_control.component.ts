import { Component, Inject, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Util } from "../../util/util";
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Router } from '@angular/router';
import { DataService } from '../../util/data_service';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { MaterialsComponent } from '../../shared/materials/materials.component';
import { SessionInfoService } from '../../util/SessionInfoService';
import { SessionInfo } from '../../util/entities/session_info';
import { QualityTestControlGrouping } from '../../util/entities/quality_test_control_grouping';
import { last } from '@angular/router/src/utils/collection';
import { QualityTestControl } from '../../util/entities/quality_test_control';
import { AlertQuestionnaireComponent } from '../../shared/alert_questionnaire/alert_questionnaire.component';

@Component({
    selector: 'quality-test-control',
    templateUrl: './quality_test_control.component.html',
    providers: [AlertComponent]
})
export class QualityTestControlComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public qualityTestControls: QualityTestControlGrouping[] = [];
    public havePermission: boolean = false;

    //initializing p to one
    pageQualityTestControl: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService,
        private sessionInfoService: SessionInfoService) {
        if (Util.getPermissions(['62', '70', '77', '78', '86'], this.dataService.getMenuList()).length != 0) {
            this.havePermission = true;

        }
        else {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
        }
    }

    ngOnInit() {

        this.loadQualityTestControl();
    }

    loadTestControl() {
        return this.http.get(this.baseUrl + 'api/qualitytestcontrol/GetPruebaCalidad');
    }

    loadQualityTestControl() {
        Util.ShowLoader();
        let qualityTestControlsBase: QualityTestControlGrouping[] = [];
        this.loadTestControl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                qualityTestControlsBase = Util.loadJsonWithToast<QualityTestControlGrouping[]>(qualityTestControlsBase, result, this.alertComponent);
                let context = this;
                Array.prototype.forEach.call(qualityTestControlsBase, function (el: any) {
                    context.evalChecks(el);
                });
                this.qualityTestControls = qualityTestControlsBase;
            }
        })
    }

    evalChecks(t: any) {
        let test = new QualityTestControlGrouping(t)._IQualityTestControlGrouping;
        t.pbchecked = (test.qualityTestControlList as any[]).filter(function (f) { return new QualityTestControl(f)._IQualityTestControl.pb == "1" }).length > 0;
        t.pcchecked = (test.qualityTestControlList as any[]).filter(function (f) { return new QualityTestControl(f)._IQualityTestControl.pc == "1" }).length > 0;
    }

    checkRoadTest(tests: any) {
        let orders = tests as QualityTestControl[];
        return orders.filter(function (f) {
            let quality = new QualityTestControl(f)._IQualityTestControl; return quality.pruebA_CARRETERA == "1";
        }).length > 0;
    }

    enableTouchApp(tests: IQualityTestControl[]) {
        if (this.checkStatus(tests) && ((this.checkPb(tests) && !this.checkRoadTest(tests)) || (this.checkRoadTest(tests) && this.checkPCC(tests)))) {
            return true;
        }
        return false;
    }

    checkPb(tests: IQualityTestControl[]) {
        return tests.filter(function (f) {
            let quality = new QualityTestControl(f)._IQualityTestControl; return quality.pb == "1";
        }).length > 0;
    }

    checkPCC(tests: IQualityTestControl[]) {
        return tests.filter(function (f) {
            let quality = new QualityTestControl(f)._IQualityTestControl; return quality.pc == "1";
        }).length > 0;
    }

    checkStatus(tests: any) {
        let orders = tests as QualityTestControl[];
        return orders.filter(function (f) {
            let quality = new QualityTestControl(f)._IQualityTestControl; return quality.statord == "VERI" || quality.statord == "CONC";
        }).length == orders.length;
    }

    PBClick(qualityTestControl: any, e: any) {
        e.preventDefault();
        this.questionIsAcepted(qualityTestControl, false);
    }

    PCClick(qualityTestControl: any, e: any) {
        e.preventDefault();
        this.questionIsAcepted(qualityTestControl, true);
    }

    questionIsAcepted(qualityTestControl: any, changeIsPc: boolean) {
        let context = this;
        AlertQuestionnaireComponent.init("La prueba de calidad es aceptada ?", function () {
            if (changeIsPc)
                qualityTestControl.pcchecked = !qualityTestControl.pcchecked;
            else
                qualityTestControl.pbchecked = !qualityTestControl.pbchecked;
            context.questionAcepted(qualityTestControl);
        }, function () {
            context.questionDeclined(qualityTestControl);
        });
    }

    questionDeclined(qualityTestControl: any) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + 'api/qualitytestcontrol/DeclineQualityTestControl', qualityTestControl, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion Exitosa";
                    this.alertComponent.successMessage();
                    this.loadQualityTestControl();
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    questionAcepted(qualityTestControl: any) {
        Util.ShowLoader();
        let updPcPb = {
            WARENTNR: qualityTestControl.rentnr,
            WAAUFNR: "",//Se obtiene en el controller
            WAPC: qualityTestControl.pcchecked == true ? '1' : '0',
            WAPB: qualityTestControl.pbchecked == true ? '1' : '0',
            WAUSUARIO: new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName
        };

        let params: URLSearchParams = new URLSearchParams();
        params.set('especialidad', qualityTestControl.especialidad == null ? '' : qualityTestControl.especialidad);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.put(this.baseUrl + 'api/qualitytestcontrol/UpdatePcPb', updPcPb, requestOptions)
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.loadQualityTestControl();
                    }
                    else {
                        this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    concluir(qualityTestControl: any, e: any) {
        Util.ShowLoader();

        this.http.put(this.baseUrl + 'api/qualitytestcontrol/ConfirmQualityTestControl', qualityTestControl, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.loadQualityTestControl();
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    ngAfterViewChecked() {
        if (!this.elemRendered && (this.qualityTestControls.length > 0 || this.qualityTestControls != undefined)) {
            Util.loadMaterialElements();
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada verificador");
            BreadCrumbService.changeBreadCrumbChild("Control Prueba de Calidad");
            Util.HideLoader();
        }

        if (this.dataService.getLoadQualityTestControlFromPopup()) {
            this.dataService.setLoadQualityTestControlFromPopup(false);
            this.loadQualityTestControl();
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}