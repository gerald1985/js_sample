﻿///// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
//import { assert } from 'chai';
//import { ExternalWorkControlComponent } from './external_work_control.component';
//import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';
//import { AlertComponent } from '../alerts/alert.component';
//import { DataService } from '../util/data_service';
//import { RouterTestingModule } from '@angular/router/testing';
//import { NgxPaginationModule } from 'ngx-pagination';
//import { ExternalWorkControl } from '../util/entities/external_work_control';

//import {
//    HttpModule,
//    Http,
//    Response,
//    ResponseOptions,
//    XHRBackend
//} from '@angular/http';
//import { MockBackend } from '@angular/http/testing';
//import { Util } from '../util/util';
//import { MockResponse } from '../../../test/mock/mock_reponse';


//let fixture: ComponentFixture<ExternalWorkControlComponent>;
//let comp: ExternalWorkControlComponent;

//describe('Testing ExternalWorkControl component', () => {
//    console.log('Testing ExternalWorkControl component');
//    beforeEach(async(() => {
//        TestBed.configureTestingModule({
//            declarations: [ExternalWorkControlComponent],
//            imports: [NgxPaginationModule, HttpModule, RouterTestingModule],
//            providers: [MockResponse,
//                { provide: XHRBackend, useClass: MockBackend },
//                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" },
//                AlertComponent, DataService]
//        }).compileComponents().then(() => {
//            fixture = TestBed.createComponent(ExternalWorkControlComponent);
//            fixture.detectChanges();
//            comp = fixture.componentInstance;
//            });
//        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
//    }));

//    it('should validate mockResponse on View and click on workOrder of ExternalWorkControl', inject([XHRBackend], (mockBackend: any) => {

//        let dataService = TestBed.get(DataService);
        
//        const mockResponse = TestBed.get(MockResponse).WOAsigmentResponse();

//        mockBackend.connections.subscribe((connection:any) => {
//            connection.mockRespond(new Response(new ResponseOptions({
//                body: JSON.stringify(mockResponse)
//            })));
//        });

//        comp.loadAssignedWorkOrders().subscribe(result => {
//            console.log(result);
//            console.log("----------------------", dataService.getArbplr());
//            comp.externalworkcontrols = result.json()["data"] as ExternalWorkControl[];
//            console.log(result.json()["data"]);
//            dataService.setWorkOrderQuantity(comp.externalworkcontrols.length);
//        });;
//        fixture.detectChanges();
//        console.log(dataService.getArbplr());
        
//        const dom = fixture.nativeElement.querySelector('table tr td:first-child a');
//        console.log(dom);
//        expect(dom.textContent).toEqual('AUFNR-TEST');
//        let navigateSpy = spyOn(comp.router, "navigateByUrl")
//        dom.click();
//        expect(navigateSpy).toHaveBeenCalledWith('/control-de-trabajo-externo');
//    }));
//});