﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { TopMenuComponent } from './topmenu.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../util/util';
import { MockResponse } from '../../../test/mock/mock_reponse';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { AlertComponent } from '../alerts/alert.component';


let fixture: ComponentFixture<TopMenuComponent>;
let comp: TopMenuComponent;

describe('Testing TopMenu component', () => {
    console.log('Testing TopMenu component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [TopMenuComponent],
            imports: [HttpModule],
            providers: [MockResponse, AlertComponent,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" }, SessionInfoService]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(TopMenuComponent);
            fixture.detectChanges();
            comp = fixture.componentInstance;
            });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should validate mockResponse on TopMenu User info', inject([XHRBackend], (mockBackend: any) => {

        let sessionService = TestBed.get(SessionInfoService) as SessionInfoService;
        
        const mockResponse = TestBed.get(MockResponse).UserInfoResponse();
        mockBackend.connections.subscribe((connection:any) => {
            connection.mockRespond(new Response(new ResponseOptions({
                body: JSON.stringify(mockResponse)
            })));
        });

        sessionService.loadSessionInfo().subscribe(result => {
            console.log(result);
            sessionService.setSessionInfo(result.json()["data"] as SessionInfo);
            console.log(result.json()["data"]);
        });;
        fixture.detectChanges();
        
        const dom = fixture.nativeElement.querySelector('.login-info');
        console.log(dom);
        expect(dom.textContent).toEqual('luis.berrios@ccn.com.ni');

    }));
});