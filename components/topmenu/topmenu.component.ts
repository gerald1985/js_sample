import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { SessionInfo } from '../util/entities/session_info';
import { AlertComponent } from '../alerts/alert.component';
import { Util } from '../util/util';
import { Http } from '@angular/http';
import { BreadCrumbService } from '../util/breadcrumbService';
import { SessionInfoService } from '../util/SessionInfoService';
import { DataService } from '../util/data_service';
import { interval } from 'rxjs';
import { AlertControl } from '../util/entities/alert';

@Component({
    selector: 'top-menu',
    templateUrl: './topmenu.component.html',
    styleUrls: [],
    providers: [AlertComponent],
})

export class TopMenuComponent {
    public sessionInfo: SessionInfo;
    public breadCrumbs: string = "";
    public alertColor: string = "c_gray";
    public alertCount: number = 0;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private sessionInfoService: SessionInfoService,
        private dataService: DataService) {
        this.checkAlertControlStatus();
        interval(5000 * 60).subscribe(x => { this.checkAlertControlStatus(); });
    }

    checkAlertControlStatus() {
        this.http.get(this.baseUrl + "api/alert/GetAlerts").subscribe(result => {
            let AlertsControlBase: AlertControl[] = [];
            AlertsControlBase = Util.loadJsonWithToast<AlertControl[]>(AlertsControlBase, result, this.alertComponent);
            if (AlertsControlBase.length > 0) {
                this.alertColor = "c_red";
            }
            else
                this.alertColor = "c_gray";
            this.alertCount = AlertsControlBase.length;
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    showHideMenu(){
        var parentContainer = document.querySelector(".parent-container.unselected");
        if (parentContainer != null) {
            parentContainer.className = parentContainer.className.replace("unselected","") + "selected";
        } else {
            parentContainer = document.querySelector(".parent-container.selected");
            if (parentContainer != null) {
                parentContainer.className = parentContainer.className.replace("selected","")  + "unselected";
            }
        }
    }

    showAlerts() {
        this.dataService.setLoadAlertControl(true);
        Util.openModal(".alert_control_modal");
    }

    ngAfterViewChecked() {
        this.breadCrumbs = BreadCrumbService.breadcrumbs;
        this.sessionInfo = this.sessionInfoService.getSessionInfo();
        this.cdRef.detectChanges();
    } 
}
