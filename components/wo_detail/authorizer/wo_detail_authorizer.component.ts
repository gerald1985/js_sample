import { Component, Inject, OnInit, Input, SimpleChange, SimpleChanges, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Util } from "../../util/util";
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { WOAssignment } from '../../util/entities/wo_assignment';
import { WorkOrderOperation } from '../../util/entities/work_order_operation';
import { WorkOrderHeader } from '../../util/entities/work_order_header';
import { Router } from '@angular/router';
import { DataService } from '../../util/data_service';
import { CommentComponent } from '../../shared/comments/comment.component';
import { KeyDamageStatus } from '../../util/entities/key_damage_status';

@Component({
    selector: 'wo_detail_authorizer',
    templateUrl: './wo_detail_authorizer.component.html',
    providers: [AlertComponent]
})

export class WODetailAuthorizerComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public workOrderOperations: WorkOrderOperation[] = [];
    public workOrderHeader: WorkOrderHeader[] = [];
    public WorkOrderQuantity: number = 0;
    public lightColor: string = "";
    public elemLoaded = false;
    public keyDamageStatus: KeyDamageStatus[] = [];

    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //initializing p to one
    pageWoDetailAuthorizer: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {

    }

    loadWOAssigned() {
        this.router.navigateByUrl('/bandeja-de-entrada-autorizador');
    }

    ngOnInit() {
        this.WorkOrderQuantity = this.dataService.getWorkOrderQuantity();
        this.lightColor = this.dataService.getLightColor();
        this.loadHeader().subscribe(result => {
            this.workOrderHeader = Util.loadJsonWithToast<WorkOrderHeader[]>(this.workOrderHeader, result, this.alertComponent);
            if (this.workOrderHeader.length > 0) {
                this.dataService.setEquipModel(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.modelo);
                this.dataService.setRentnr(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.rentnr);
            }
        }, error => console.error(error));
        this.loadOperations();
    }

    loadMaterialsAuthorizer(vornr: string) {
        this.dataService.setVORNR(vornr);
        this.dataService.setLoadMaterialsFromOrder(true);
        this.dataService.setLoadMaterials(true);
    }

    loadOperations() {
        this.loadOperationsByOrderNumber().subscribe(result => {
            this.workOrderOperations = Util.loadJsonWithToast<WorkOrderOperation[]>(this.workOrderOperations, result, this.alertComponent);
            this.elemLoaded = true;
        }, error => console.error(error));
    }

    public loadHeader() {
        return this.http.get(this.baseUrl + "api/WO/GetOrderHeader?workOrderNumber=" + this.dataService.getWorkOrderNumber());
    }

    public loadLastNotification() {
        return this.http.get(this.baseUrl + "api/WO/GetLastNotification?AUFNR=" + this.dataService.getWorkOrderNumber() + '&VORNR=' + this.dataService.getVORNR());
    }

    public loadOperationsByOrderNumber() {
        return this.http.get(this.baseUrl + "api/WO/GetOperationsByOrderNumber?workOrderNumber=" + this.dataService.getWorkOrderNumber())
    }

    ngAfterViewChecked() {
        if (!this.elemRendered && this.elemLoaded) {
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Orden de Trabajo");
            BreadCrumbService.changeBreadCrumbChild("Detalle de Orden");
            Util.loadMaterialElements();
            Util.HideLoader();
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
        }
        if (this.dataService.getReloadWODetail()) {
            Util.ShowLoader();
            this.loadOperations();
            this.dataService.setReloadWODetail(false);
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
            Util.HideLoader();
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}