﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { WODetailComponent } from './wo_detail.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';
import { AlertComponent } from '../alerts/alert.component';
import { DataService } from '../util/data_service';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPaginationModule } from 'ngx-pagination'; ;
import { WOAssignment } from '../util/entities/wo_assignment';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../util/util';
import { MockResponse } from '../../../test/mock/mock_reponse';
import { WorkOrderOperation } from '../util/entities/work_order_operation';
import { WorkOrderHeader } from '../util/entities/work_order_header';
import { BreadCrumbService } from '../util/breadcrumbService';


let fixture: ComponentFixture<WODetailComponent>;
let comp: WODetailComponent;

describe('Testing WODetail component', () => {
    console.log('Testing WODetail component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WODetailComponent],
            imports: [NgxPaginationModule, HttpModule, RouterTestingModule],
            providers: [MockResponse,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" },
                AlertComponent, DataService]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(WODetailComponent);
            fixture.detectChanges();
            comp = fixture.componentInstance;
            });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should validate Header on WorkOrderDetail', inject([XHRBackend], (mockBackend: any) => {

        let dataService = TestBed.get(DataService);
        dataService.setWorkOrderNumber("");
        dataService.setWorkOrderQuantity("");

        const mockResponse = TestBed.get(MockResponse).GetOrderHeaderResponse();

        mockBackend.connections.subscribe((connection: any) => {
            connection.mockRespond(new Response(new ResponseOptions({
                body: JSON.stringify(mockResponse)
            })));
        });

        comp.loadHeader().subscribe(result => {
            console.log(result);
            comp.workOrderHeader = result.json()["data"] as WorkOrderHeader[];
            console.log(result.json()["data"]);
            dataService.setWorkOrderQuantity(comp.workOrderHeader.length);
        });

        comp.loadOperationsByOrderNumber().subscribe(result => {
            console.log(result);
            comp.workOrderOperations = result.json()["data"] as WorkOrderOperation[];
            console.log(result.json()["data"]);
            dataService.setWorkOrderQuantity(comp.workOrderOperations.length);
            comp.elemLoaded = true;
        });

        fixture.detectChanges();
        console.log(dataService.getArbplr());

        const orden_header = fixture.nativeElement.querySelector('.orden_header');
        const bahia_header = fixture.nativeElement.querySelector('.bahia_header');
        const equipo_header = fixture.nativeElement.querySelector('.equipo_header');
        const km_header = fixture.nativeElement.querySelector('.km_header');
        const cost_header = fixture.nativeElement.querySelector('.cost_header');
        const wo_quantity = fixture.nativeElement.querySelector("#woQuantity");

        console.log(orden_header, bahia_header, equipo_header, km_header, cost_header);

        expect(orden_header.textContent).toEqual('8000053714');
        expect(bahia_header.textContent).toEqual('1');
        expect(equipo_header.textContent).toEqual('954');
        expect(km_header.textContent).toEqual('0');
        expect(cost_header.textContent).toEqual('$0.0');
        expect(wo_quantity.textContent).toEqual('OT Asignadas (' + comp.WorkOrderQuantity + ')');

        fixture.nativeElement.querySelector("#modalLoader").click();

        expect(BreadCrumbService.getReference()).toEqual("882-" + orden_header.textContent);

        let navigateSpy = spyOn(comp.router, "navigateByUrl")
        wo_quantity.click();
        
        expect(navigateSpy).toHaveBeenCalledWith('/bandeja-de-entrada-tecnico');
    }));
});