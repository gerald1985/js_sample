import { Component, Inject, OnInit, Input, ChangeDetectorRef, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { WorkOrderOperation } from '../util/entities/work_order_operation';
import { WorkOrderHeader } from '../util/entities/work_order_header';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { CommentComponent } from '../shared/comments/comment.component';
import { KeyDamageStatus } from '../util/entities/key_damage_status';
import { Provider } from '../util/entities/provider';
import { SessionInfoService } from '../util/SessionInfoService';

@Component({
    selector: 'wo-detail',
    templateUrl: './wo_detail.component.html',
    providers: [AlertComponent]
})
export class WODetailComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public workOrderOperations: WorkOrderOperation[] = [];
    public workOrderHeader: WorkOrderHeader[] = [];
    public providers: Provider[] = [];
    public WorkOrderQuantity: number = 0;
    public lightColor: string = "";
    public elemLoaded = false;
    public keyDamageStatus: KeyDamageStatus[] = [];
    public showNotifications: boolean = false;
    public sessionInfo: any;
    public sessionInfoIsLoaded: boolean = false;
    public enableFinishWo: boolean = false;

    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //initializing p to one
    pageWODetail: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private zone: NgZone,
        public router: Router,
        private sessionInfoService: SessionInfoService,
        private dataService: DataService) {

        Util.havePermission(['87', '101', '126', '140', '154'], this.dataService, this.alertComponent, this.router);

    }

    providerUrl() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumProvider");
    }

    loadWOAssigned() {
        if (this.sessionInfoIsLoaded) {
            if (this.sessionInfo != undefined && this.sessionInfo.RoleName == "TecnicoADF") {
                this.router.navigateByUrl('/bandeja-de-entrada-tecnico');
            } else if (this.sessionInfo != undefined && this.sessionInfo.RoleName == "AutorizadorADF") {
                this.router.navigateByUrl('/bandeja-de-entrada-autorizador');
            }
            else if (this.sessionInfo != undefined) {
                this.router.navigateByUrl('/bandeja-de-entrada');
            }
        }
    }

    ngOnInit() {
        this.WorkOrderQuantity = this.dataService.getWorkOrderQuantity();
        this.lightColor = this.dataService.getLightColor();
        this.loadHeader().subscribe(result => {
            this.workOrderHeader = Util.loadJsonWithToast<WorkOrderHeader[]>(this.workOrderHeader, result, this.alertComponent);
            if (this.workOrderHeader.length > 0) {
                this.dataService.setEquipModel(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.modelo);
                this.dataService.setEquipoModel(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.equnr);
                this.dataService.setRentnr(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.rentnr);
                this.dataService.setwoPC_Status(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.pC_STATUS);
                this.dataService.setCostoOrderHeader(new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader.costo);
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
        this.loadOperations();
    }

    loadCheckList(status: string, vornr: string) {
        this.dataService.setVORNR(vornr);
        this.dataService.setCheckListIsModifiable(status == "PROC");
        this.dataService.setLoadCheckList(true);
        Util.openModal('.checklist_modal');
    }

    finishOT() {
        this.dataService.setPendingWorkConfirmWO(this.dataService.getWorkOrderNumber());
        this.dataService.setSaveNotificationNEJE(false);
        this.dataService.setLoadPendingWorkConfirm(true);
        Util.openModal(".pending_work_confirm_modal");
    }

    providerClick(operation: any, e: any) {
        if (this.sessionInfo.RoleName == "AutorizadorADF") {
            e.preventDefault();
        } else {
            let oper = operation as IWorkOrderOperation;
            if (oper.ulT_MOTIVO == "FCMO" || oper.ulT_MOTIVO == "NEJE") {
                e.preventDefault();
                this.alertComponent.toastMessage = "La operaci&oacute;n ya fue concluida o no se ejecuto";
                this.alertComponent.warningMessage();
            } else if (this.sessionInfo == undefined || (this.sessionInfo != undefined && this.sessionInfo.RoleName == "TecnicoADF")) {
                e.preventDefault();
                this.alertComponent.toastMessage = "La operaci&oacute;n solicitada no es permitida";
                this.alertComponent.warningMessage();
            }
        }
    }

    preventProviderChange(e: any, srvex: string) {
        if (this.showNotifications) {
            e.preventDefault();
        }
        else {
            if (!srvex) {
                e.preventDefault();
            }
        }
    }

    providerChange(operation: any) {
        Util.ShowLoader();
        let oper = operation as IWorkOrderOperation;
        let note = this.providers.find(function (el) {
            let prov = new Provider(el)._Provider;
            return prov.lifnr === oper.lifnr;
        });
        let toSave = {
            ORDENID: this.dataService.getWorkOrderNumber(),
            OPERID: oper.vornr,
            PROVEE: oper.lifnr,
            NOTA: (oper.lifnr.trim() == "" || note == undefined) ? "" : new Provider(note)._Provider.namE1,
            IND_SERV: oper.srvex
        };
        this.http.put(this.baseUrl + 'api/WO/UpdateOperationProvider', toSave, {}).subscribe(result => {
            Util.HideLoader();
            if (result.json()["responseCode"] == 200) {
                this.loadOperations();
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMaterials(vornr: string, ktsch: string, cm_description: string) {
        this.dataService.setVORNR(vornr);
        this.dataService.setModelKey(ktsch);
        this.dataService.setCMDescription(cm_description);
        this.dataService.setLoadMaterialsFromOrder(true);
        this.dataService.setLoadMaterials(true);
        this.dataService.setShowOperationsMaterial(false);
    }

    showRefundMaterialList() {
        this.dataService.setLoadRefundMaterialList(true);
    }

    loadOperations() {
        this.loadOperationsByOrderNumber().subscribe(result => {
            this.workOrderOperations = Util.loadJsonWithToast<WorkOrderOperation[]>(this.workOrderOperations, result, this.alertComponent);
            Array.prototype.forEach.call(this.workOrderOperations, function (el: any) {
                el.srvex = el.srvex == "true";
                el.haycomentario = el.haycomentario == "true";
            });
            this.providerUrl().subscribe(resultProvider => {
                this.providers = Util.loadJsonWithToast<Provider[]>(this.providers, resultProvider, this.alertComponent);
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });

            this.elemLoaded = true;
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    public loadHeader() {
        return this.http.get(this.baseUrl + "api/WO/GetOrderHeader?workOrderNumber=" + this.dataService.getWorkOrderNumber());
    }

    public loadLastNotification() {
        return this.http.get(this.baseUrl + "api/WO/GetLastNotification?AUFNR=" + this.dataService.getWorkOrderNumber() + '&VORNR=' + this.dataService.getVORNR());
    }

    public loadOperationsByOrderNumber() {
        return this.http.get(this.baseUrl + "api/WO/GetOperationsByOrderNumber?workOrderNumber=" + this.dataService.getWorkOrderNumber())
    }

    notify(noti: any) {
        Util.ShowLoader();

        let workOrderHeader = new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader;
        let operation = new WorkOrderOperation(noti)._WorkOrderOperation;

        this.dataService.setRenderNotificationStart(true);
        this.dataService.setModelKey(operation.ktsch);
        this.dataService.setVORNR(operation.vornr);
        this.dataService.setCMDescription(operation.txt);
        this.dataService.setReasonTime("00:00");
        this.dataService.setEffectiveTime(parseInt(operation.tpO_EFE));
        this.dataService.setStandardTime(parseInt(operation.tpO_STD));
        this.dataService.setLoadDataToBarChart(true);
        this.dataService.setTimeFromExtenarlWorkControl(false);
        this.dataService.setIsNegativeTime(false);
        this.dataService.setCheckListIsModifiable(operation.ulT_MOTIVO == "PROC");

        BreadCrumbService.setTypeReference("Notificaci&oacute;n CM");
        BreadCrumbService.setReference(workOrderHeader.aufnr.concat('-').concat("OPER-").concat(operation.vornr));
        this.loadLastNotification().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.keyDamageStatus = Util.loadJsonWithToast<KeyDamageStatus[]>(this.keyDamageStatus, result, this.alertComponent);
                if (this.keyDamageStatus.length > 0) {
                    let kdamage = new KeyDamageStatus(this.keyDamageStatus[0])._KeyDamageStatus;
                    this.dataService.setRenderNotificationStart(kdamage.motivo != 'PROC');

                    var diffBetwTimes = Math.floor(parseInt(operation.tpO_STD)) - Math.floor(parseInt(operation.tpO_EFE));
                    this.dataService.setIsNegativeTime(diffBetwTimes < 0);
                    this.dataService.setReasonTime(Util.secondsToTime(diffBetwTimes * 60, false));
                }
            }
            Util.openModal('.notification_modal');
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMessengerFromOrderHeader() {

        let workOrderHeader = new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader;
        //
        BreadCrumbService.setTypeReference("Orden de Trabajo");
        BreadCrumbService.setReference(workOrderHeader.equnr.concat("-").concat(workOrderHeader.aufnr));
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    loadMessengerFromOperation(operation: string) {

        let workOrderHeader = new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader;
        //
        BreadCrumbService.setTypeReference("Orden de Trabajo");
        BreadCrumbService.setReference(workOrderHeader.aufnr.concat("-OPER-").concat(operation));
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    //evalPermission(permissionId: number, description: string) {
    //    return Util.havePermission(permissionId, description, this.dataService.getMenuList());
    //}

    showEquipmentHistory() {
        this.dataService.setLoadEquipmentHistory(true);
        Util.openModal(".equipment_history_modal");
    }

    showAdvices() {
        this.dataService.setLoadAdvices(true);
        Util.openModal(".advice_modal");
    }

    ngAfterViewChecked() {
        if (!this.sessionInfoIsLoaded || this.sessionInfo == undefined) {
            this.sessionInfoIsLoaded = true;
            this.sessionInfo = this.sessionInfoService.getSessionInfo();
        }
        if (!this.elemRendered && this.elemLoaded && this.providers.length > 0) {
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Orden de Trabajo");
            BreadCrumbService.changeBreadCrumbChild("Detalle de Orden");
            Util.HideLoader();
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
        }
        if (this.dataService.getReloadWODetail()) {
            Util.ShowLoader();
            this.dataService.setReloadWODetail(false);
            this.loadOperations();
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
            Util.HideLoader();
        }
        if (this.workOrderOperations.length > 0 &&
            this.workOrderOperations.length == this.workOrderOperations.filter(function (f) { let vornr = new WorkOrderOperation(f)._WorkOrderOperation; return vornr.ulT_MOTIVO == "FCMO" || vornr.ulT_MOTIVO == "NEJE" }).length) {
            this.enableFinishWo = true;
        } else {
            this.enableFinishWo = false;
        }
        //this.showNotifications = this.dataService.getShowOperationsMaterial();
        this.WorkOrderQuantity = this.dataService.getWorkOrderQuantity();
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}