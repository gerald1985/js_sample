import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { DataService } from '../util/data_service';
import { SessionInfoService } from '../util/SessionInfoService';
import { AlertComponent } from '../alerts/alert.component';
import { Util } from "../util/util";
import { Http } from '@angular/http';
import { DatePipe } from '@angular/common';
import { SessionInfo } from '../util/entities/session_info';
import { BreadCrumbService } from '../util/breadcrumbService';
import { Menu } from '../util/entities/menu';
import { Router, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    providers: [DataService, SessionInfoService, AlertComponent],
})
export class AppComponent implements OnInit {
    fullYear: Number = new Date().getFullYear();
    public reasonToReturn: string = "";
    datePipe: DatePipe = new DatePipe("es-NI");
    commentDate: any = Date.now();
    public menuInfo: Menu[] = [];
    public menuInfoIsLoaded: boolean = false;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, public alertComponent: AlertComponent,
        public sessionInfoService: SessionInfoService, @Inject('AUTH_URL') private authUrl: string,
        private router: Router, private dataService: DataService, private cdRef: ChangeDetectorRef) {
        this.router.events.subscribe(event => {
            
            if (event instanceof NavigationStart) {
                Util.ShowLoader();
                window.history.forward();
            }
            if (event instanceof NavigationEnd) {
                Util.HideLoader();
            }
            if (event instanceof NavigationError) {
                Util.HideLoader();
            }
        });
    }

    loadMenuInfo() {
        this.loadMenuInfoUrl().subscribe(result => {
            this.menuInfo = Util.loadJsonWithToast<Menu[]>(this.menuInfo, result, this.alertComponent);
            this.dataService.setMenuList(this.menuInfo);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    public loadMenuInfoUrl() {
        return this.http.get(this.baseUrl + 'api/Application/MenuByRole');
    }

    openReasonToReturn() {
        this.reasonToReturn = "";
        Util.ShowLoader();
        Util.openModal(".closure_return_modal");
        Util.HideLoader();
    }

    postComment(comment: any) {
        return this.http.post(this.baseUrl + 'api/WO/CreateComment', comment, {});
    }

    addReasonToReturn() {
        if (this.reasonToReturn.trim() == "") {
            this.alertComponent.toastMessage = "Por favor introduce la raz&oacute;n de retorno.";
            this.alertComponent.warningMessage();
        }
        else {
            Util.ShowLoader();
            let comment = [{
                USUARIO: new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName,
                REFERENCIA: BreadCrumbService.getReference(),
                FECHA: this.datePipe.transform(this.commentDate, 'dd/MM/yyyy'),
                HORA: this.datePipe.transform(this.commentDate, "HH:mm:ss"),
                MODULO: "ORDERS",
                PANTALLA: "WORK ORDER",
                STATUS: "ACTIVE",
                COMENTARIO: "Devolucion: " + this.reasonToReturn,
            }];
            this.postComment(comment).subscribe(result => {
                Util.HideLoader();
                if (result.json()["responseCode"] == 200) {
                    Util.ShowLoader();
                    this.http.post(this.baseUrl + 'api/Requirement/WithoutSend', { NREQUISA: this.dataService.getNREQUISA() }, {}).subscribe(resultReturn => {
                        Util.HideLoader();
                        if (resultReturn.json()["responseCode"] == 200) {
                            this.dataService.setLoadRequirementFromSupervisor(true);
                            this.dataService.setRenderRequirement(true);
                            Util.closeModal(".closure_return_modal");
                        }
                        else {
                            this.alertComponent.toastMessage = resultReturn.json()["responseText"];
                            this.alertComponent.warningMessage();
                        }
                    }, error => {
                        Util.EvalError(error, this.alertComponent, this.authUrl);
                    });
                } else {
                    var errors = result.json()["responseText"].split(',');
                    errors.forEach((err: string) => { Util.showMessage(err, "error-toast c_yellow z-depth-3"); });
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadReasonToReturnReqFromSupervisor()) {
            this.dataService.setLoadReasonToReturnReqFromSupervisor(false);
            this.openReasonToReturn();
        }
        if (!this.menuInfoIsLoaded) {
            this.menuInfoIsLoaded = true;
            this.loadMenuInfo();
        }
        this.cdRef.detectChanges();
    }

    ngOnInit() {
    }
}
