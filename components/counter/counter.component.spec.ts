/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { CounterComponent } from './counter.component';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { HttpModule } from '@angular/http'

let fixture: ComponentFixture<CounterComponent>;
describe('Counter component', () => {
    beforeEach(() => {
        
    });

    beforeEach(async(() => {
        TestBed.configureTestingModule({ declarations: [CounterComponent], imports: [HttpModule], }).compileComponents().then(() => {
            fixture = TestBed.createComponent(CounterComponent);
            fixture.detectChanges();
        });
        
    }));

    it('should create an instance of Counter', () => {
        expect(new CounterComponent()).toBeTruthy();
    });

    it('should display a title', async(() => {
        const titleText = fixture.nativeElement.querySelector('h1').textContent;
        expect(titleText).toEqual('Counter');
    }));

    it('should start with count 0, then increments by 1 when clicked', async(() => {
        fixture.detectChanges();
        const countElement = fixture.nativeElement.querySelector('strong');
        expect(countElement.textContent).toEqual('0');
        const incrementButton = fixture.nativeElement.querySelector('button');
        incrementButton.click();
        fixture.detectChanges();
        expect(countElement.textContent).toEqual('1');
    }));
});
