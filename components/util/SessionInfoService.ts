﻿import { Injectable, Inject } from "@angular/core";
import { SessionInfo } from "./entities/session_info";
import { Http } from "@angular/http";
import { AlertComponent } from "../alerts/alert.component";
import { Util } from "./util";
import { DataService } from "./data_service";

@Injectable()
export class SessionInfoService {
    public sessionInfo: SessionInfo;

    public setSessionInfo(sessionInfo: SessionInfo) {
        this.sessionInfo = sessionInfo;

    }

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string, public alertComponent: AlertComponent, private dataService: DataService) {
        if (this.getSessionInfo() == null || this.getSessionInfo() == undefined) {
            this.loadSessionInfo().subscribe(result => {
                this.sessionInfo = Util.loadJsonWithToast<SessionInfo>(this.sessionInfo, result, this.alertComponent);
                this.setSessionInfo(this.sessionInfo);
                this.dataService.setArbpl(new SessionInfo(this.sessionInfo)._SessionInfo.Arbpl);
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }

        

    }

    public getSessionInfo() {
        return this.sessionInfo;
    }

    public loadSessionInfo() {
        return this.http.get(this.baseUrl + 'api/Application/UserInfo');
    }
}