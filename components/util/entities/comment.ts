﻿import { Component } from '@angular/core';

export class Comment {

    private _Comment: IComment;

    constructor(comment: any) {
        this._Comment = comment as IComment;
    }

    getComment(){
        return this._Comment;
    }
    
}