﻿import { Component } from '@angular/core';

export class KeyDamageStatus {
    public _KeyDamageStatus: IKeyDamageStatus;

    constructor(kds: any) {
        this._KeyDamageStatus = kds as IKeyDamageStatus;
    }
}