﻿import { Component } from '@angular/core';

export class NoticeType {
    public _NoticeType: INoticeType;

    constructor(noticeType: any) {
        this._NoticeType = noticeType as INoticeType;
    }
}