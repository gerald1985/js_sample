﻿import { Component } from '@angular/core';

export class PendingWO implements IPendingWorkDetail {
    aufnr: string = "";
    id_trab_pend: string = "";
    ktsch: string = "";
    kurztext: string = "";
    mandt: string = "";
    descripcion: string = "";
    status: string = "";
    fegrp: string = "";
    fecod: string = "";
    statusEliminado: boolean = false;
    qmart: string = "";
    qmartx: string = "";
}