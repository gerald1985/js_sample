﻿import { Component } from '@angular/core';

export class PendingWorkDetail {
    public _PendingWorkDetail: IPendingWorkDetail;

    constructor(pendingworkdetail: any) {
        this._PendingWorkDetail = pendingworkdetail as IPendingWorkDetail;

        if (this._PendingWorkDetail.status === "ELIMINADO")
            this._PendingWorkDetail.statusEliminado = true;
        else
            this._PendingWorkDetail.statusEliminado = false;
    }
}