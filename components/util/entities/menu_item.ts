﻿import { Component } from '@angular/core';

export class MenuItemPermissionDTO {
    public _MenuItem: IMenuItemPermission;

    constructor(item: any) {
        this._MenuItem = item as IMenuItemPermission;
    }
}