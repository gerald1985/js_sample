﻿import { Component } from '@angular/core';

export class Supervisor {
    public _Supervisor: ISupervisor;

    constructor(supervisor: any) {
        this._Supervisor = supervisor as ISupervisor;
    }
}