﻿import { Component } from '@angular/core';

export class WorkOrderOperation {
    public _WorkOrderOperation: IWorkOrderOperation;

    constructor(workOrderOperation: any) {
        this._WorkOrderOperation = workOrderOperation as IWorkOrderOperation;
    }
}