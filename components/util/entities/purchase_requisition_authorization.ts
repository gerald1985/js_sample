﻿import { Component } from '@angular/core';

export class PurchaseRequisitionAuthorization {
    public _IPurchaseRequisitionAuthorization: IPurchaseRequisitionAuthorization;

    constructor(purchaserequisitionauthorization: any) {
        this._IPurchaseRequisitionAuthorization = purchaserequisitionauthorization as IPurchaseRequisitionAuthorization;
        
        //Checked CI
        if (this._IPurchaseRequisitionAuthorization.ci === "1")
            this._IPurchaseRequisitionAuthorization.ciChecked = true;
        else
            this._IPurchaseRequisitionAuthorization.ciChecked = false;

        //Checked Procesa
        if (this._IPurchaseRequisitionAuthorization.procesa === "1")
            this._IPurchaseRequisitionAuthorization.procesachecked = true;
        else
            this._IPurchaseRequisitionAuthorization.procesachecked = false;
    }
}