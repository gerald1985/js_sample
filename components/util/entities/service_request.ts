﻿import { Component } from '@angular/core';

export class ServiceRequest implements IServiceRequest {
    noEquipo: string = "";
    noOrden: string = "";
    fecha: string = "";
    hora: string = "";
    pieza: string = "";
    tipoReparacion: string = "";
    proveedor: string = "";
    fechaEntrega: string = "";
    horaEntrega: string = "";
    fechaRetorno: string = "";
    horaRetorno: string = "";
    tiempoEstimado: string = "";
    formattedHours: string = "";
    vornr: string = "";
}

//export class ServiceRequest {
//    public _ServiceRequest: IServiceRequest;

//   constructor(serviceRequest: any) {
//       this._ServiceRequest = serviceRequest as IServiceRequest;
//    }
//}