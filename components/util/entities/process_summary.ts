﻿import { Component } from '@angular/core';

export class ProcessSummary {
    public _ProcessSummary: IProcessSummary;

    constructor(process_summary: any) {
        this._ProcessSummary = process_summary as IProcessSummary;
    }
}