﻿import { Component } from '@angular/core';
import { WorkForce } from './work_force';
import { Bay } from './bay';

export class Reception {

    public _Reception: IReception;
    public selectedBay: any;
    public selectedArbpl: any;
    public statusChecked: any;
    constructor(reception: any) {
        this._Reception = reception as IReception;
    }
    
}