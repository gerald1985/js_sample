﻿import { Component } from '@angular/core';

export class QualityTestControlGrouping {
    public _IQualityTestControlGrouping: IQualityTestControlGrouping;

    constructor(qualitytestcontrolgrouping: any) {
        this._IQualityTestControlGrouping = qualitytestcontrolgrouping as IQualityTestControlGrouping;

        //Checked PB/PC
        if (this._IQualityTestControlGrouping.pb === "1")
            this._IQualityTestControlGrouping.pcchecked = true;
        else
            this._IQualityTestControlGrouping.pcchecked = false;

        if (this._IQualityTestControlGrouping.pc === "1")
            this._IQualityTestControlGrouping.pcchecked = true;
        else
            this._IQualityTestControlGrouping.pcchecked = false;
    }
}