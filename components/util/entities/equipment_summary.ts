﻿import { Component } from '@angular/core';

export class EquipmentSummary {
    public _EquipmentSummary: IEquipmentSummary;

    constructor(equipment_summary: any) {
        this._EquipmentSummary = equipment_summary as IEquipmentSummary;
    }
}