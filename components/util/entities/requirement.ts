﻿import { Component } from '@angular/core';

export class Requirement {

    public _Requirement: IRequirement;

    constructor(requirement: any) {
        this._Requirement = requirement as IRequirement;
    }
}