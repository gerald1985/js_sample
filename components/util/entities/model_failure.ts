﻿import { Component } from '@angular/core';

export class ModelFailure {
    public _IModulFailure: IModulFailure;

    constructor(modelfailure: any) {
        this._IModulFailure = modelfailure as IModulFailure;
    }
}