﻿import { Injectable } from "@angular/core";
import { MaterialOper } from "./entities/material_oper";
import { ReqOper } from "./entities/requirement_operation";
import { Menu } from "./entities/menu";

@Injectable()
export class DataService {
    private workOrderNumber: string = "";
    private arbpl: string = "";
    private workOrderQuantity: number = 0
    private lightColor: string = "";
    private equipModel: string = "";
    private equipoModel: string = "";
    private nrequisa: string = "";
    private renderRequirement: string = "";
    private rentnr: string = "";
    private vornr: string = "";
    private equnr: string = "";
    private coddev: string = "";
    private renderNotificationStart: string = "";
    private loadClosureReason: string = "";
    private model_key: string = "";
    private reasonTime: string = "";
    private reloadWODetail: string = "";
    private reloadExternalWC: string = "";
    private loadCheckList: string = "";
    private loadSearchMaterials: string = "";
    private cmDescription: string = "";
    private loadMaterials: string = "";
    private loadEquipmentHistory: string = "";
    private loadEquipmentDetails: string = "";
    private loadServicesFromExternalWC: string = "";
    private loadAdvices: string = "";
    private loadAlertControl: string = "";
    private effectiveTime: number = 0;
    private standardTime: number = 0;
    private renderDataToBarChart: string = "";
    private renderTimeLeft: string = "";
    private loadDataExternalWC: any;
    private loadDataService: any;
    private serviceHeader: string = "";
    private loadQualityTestControls: any;
    private materialsFromSearch: MaterialOper[] = [];
    private loadMaterialsToComponent: string = "";
    private loadMaterialsFromOrder: string = "";
    private renderDeleteColumnMat: string = "";
    private showOperationsMaterial: string = "";
    private loadReplacement: string = "";
    private loadBlockerExternalServices: string = "";
    private loadApproveMaterialsRequirement: string = "";
    private modelKeys: ReqOper[] = [];
    private loadRefundMaterialList: string = "";
    private loadMaterialUnloading: string = "";
    private isFromMaterialReturnControl: string = "";
    private refundCode: string = "";
    private loadReasonToReturnReqFromSupervisor: string = "";
    private loadRequirementFromSupervisor: string = "";
    private requirementStatus: string = "";
    private menuList: Menu[] = [];
    private loadModelKeys: string = "";
    private loadExternalWC: string = "";
    private loadExternalWCReception: string = "";
    private loadServicePrintTemplate: string = "";
    private loadPendingWorkConfirm: string = "";
    private loadQualityTestControl: string = "";
    private loadQualityTestControlFromPopup: string = "";
    private loadPendingWork: string = "";
    private checkListIsModifiable: string = "";
    private pendingWorkWO: string = "";
    private isNegativeTime: string = "";
    private provider: string = "";
    private providerDescription: string = "";
    private printIsRequestService: string = "";
    private reloadMatReturnControl: string = "";
    private woPC_Status: string = "";
    private saveNotificationNEJE: string = "";
    private renderMaterialsFromAuthorizer: string = "";
    private costoOrderHeader: string = "";
    private rootUrl: string = "";
    private loadPendingWorksFromFinishWO: string = "";
    private KTEXT: string = "";
    private receptionStatus: string = "";
    private statusRefund: string = "";

    public setWorkOrderNumber(workOrderNumber: string) {
        this.workOrderNumber = workOrderNumber;
        sessionStorage.setItem('workOrderNumber', this.workOrderNumber);
    }

    public setwoPC_Status(woPC_Status: string) {
        this.woPC_Status = woPC_Status;
        sessionStorage.setItem('woPC_Status', this.woPC_Status);
    }

    public getwoPC_Status() {
        this.woPC_Status = sessionStorage.getItem('woPC_Status') || this.woPC_Status;
        return this.woPC_Status;
    }

    public setKTEXT(ktext: string) {
        this.KTEXT = ktext;
        sessionStorage.setItem('KTEXT', this.KTEXT);
    }

    public getKTEXT() {
        this.KTEXT = sessionStorage.getItem('KTEXT') || this.KTEXT;
        return this.KTEXT;
    }

    public setrefundCode(refundCode: string) {
        this.refundCode = refundCode;
        sessionStorage.setItem('refundCode', this.refundCode);
    }

    public getrefundCode() {
        this.refundCode = sessionStorage.getItem('refundCode') || this.refundCode;
        return this.refundCode;
    }

    public setProviderDescription(provider: string) {
        this.providerDescription = provider;
        sessionStorage.setItem('providerDescription', this.providerDescription);
    }

    public getProviderDescription() {
        this.providerDescription = sessionStorage.getItem('providerDescription') || this.providerDescription;
        return this.providerDescription;
    }

    public setReceptionStatus(status: string) {
        this.receptionStatus = status;
        sessionStorage.setItem('receptionStatus', this.receptionStatus);
    }

    public getReceptionStatus() {
        this.receptionStatus = sessionStorage.getItem('receptionStatus') || this.receptionStatus;
        return this.receptionStatus;
    }

    public setProvider(provider: string) {
        this.provider = provider;
        sessionStorage.setItem('provider', this.provider);
    }

    public getProvider() {
        this.provider = sessionStorage.getItem('provider') || this.provider;
        return this.provider;
    }

    public setMenuList(menu: Menu[]) {
        this.menuList = menu;
        sessionStorage.setItem('menuList', JSON.stringify(this.menuList));
    }

    public getMenuList() {
        this.menuList = JSON.parse(sessionStorage.getItem('menuList') || JSON.stringify(this.menuList));
        return this.menuList;
    }

    public havePermission(menuId: string) {
        return this.getMenuList().filter(function (m) {
            let menu = new Menu(m)._Menu;
            return menu.menuId == menuId;
        }).length > 0;
    }

    public getSingleToBarChart() {
        var standart = this.getStandartTime() / 60;
        var effective = this.getEffectiveTime() / 60;
        return [
            {
                "name": "Teorico",
                "value": standart
            },
            {
                "name": "Real",
                "value": effective
            }];
    }

    public setModelKeys(modelKeys: ReqOper[]) {
        this.modelKeys = modelKeys;
        sessionStorage.setItem('modelKeys', JSON.stringify(this.modelKeys));
    }

    public getModelKeys() {
        this.modelKeys = JSON.parse(sessionStorage.getItem('modelKeys') || JSON.stringify(this.modelKeys));
        return this.modelKeys;
    }

    public getCheckListIsModifiable() {
        this.checkListIsModifiable = sessionStorage.getItem('checkListIsModifiable') || this.checkListIsModifiable;
        return this.checkListIsModifiable === "TRUE";
    }

    public setCheckListIsModifiable(load: boolean) {
        this.checkListIsModifiable = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('checkListIsModifiable', this.checkListIsModifiable);
    }

    public getPrintIsRequestService() {
        this.printIsRequestService = sessionStorage.getItem('printIsRequestService') || this.printIsRequestService;
        return this.printIsRequestService === "TRUE";
    }

    public setPrintIsRequestService(isRequest: boolean) {
        this.printIsRequestService = isRequest ? "TRUE" : "FALSE";
        sessionStorage.setItem('printIsRequestService', this.printIsRequestService);
    }

    public getIsNegativeTime() {
        this.isNegativeTime = sessionStorage.getItem('isNegativeTime') || this.isNegativeTime;
        return this.isNegativeTime === "TRUE";
    }

    public setIsNegativeTime(isNegative: boolean) {
        this.isNegativeTime = isNegative ? "TRUE" : "FALSE";
        sessionStorage.setItem('isNegativeTime', this.isNegativeTime);
    }

    public getLoadModelKeys() {
        this.loadModelKeys = sessionStorage.getItem('loadModelKeys') || this.loadModelKeys;
        return this.loadModelKeys === "TRUE";
    }

    public setLoadModelKeys(load: boolean) {
        this.loadModelKeys = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadModelKeys', this.loadModelKeys);
    }

    public getLoadRequirementFromSupervisor() {
        this.loadRequirementFromSupervisor = sessionStorage.getItem('loadRequirementFromSupervisor') || this.loadRequirementFromSupervisor;
        return this.loadRequirementFromSupervisor === "TRUE";
    }

    public setLoadRequirementFromSupervisor(load: boolean) {
        this.loadRequirementFromSupervisor = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadRequirementFromSupervisor', this.loadRequirementFromSupervisor);
    }

    public getLoadReasonToReturnReqFromSupervisor() {
        this.loadReasonToReturnReqFromSupervisor = sessionStorage.getItem('loadReasonToReturnReqFromSupervisor') || this.loadReasonToReturnReqFromSupervisor;
        return this.loadReasonToReturnReqFromSupervisor === "TRUE";
    }

    public setLoadReasonToReturnReqFromSupervisor(load: boolean) {
        this.loadReasonToReturnReqFromSupervisor = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadReasonToReturnReqFromSupervisor', this.loadReasonToReturnReqFromSupervisor);
    }

    public getStatusRefund() {
        this.statusRefund = sessionStorage.getItem('statusRefund') || this.statusRefund;
        return this.statusRefund;
    }

    public setStatusRefund(status: string) {
        this.statusRefund = status;
        sessionStorage.setItem('statusRefund', this.statusRefund);
    }

    public getLoadMaterialUnloading() {
        this.loadMaterialUnloading = sessionStorage.getItem('loadMaterialUnloading') || this.loadMaterialUnloading;
        return this.loadMaterialUnloading === "TRUE";
    }

    public setLoadMaterialUnloading(load: boolean) {
        this.loadMaterialUnloading = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadMaterialUnloading', this.loadMaterialUnloading);
    }

    public getLoadRefundMaterialList() {
        this.loadRefundMaterialList = sessionStorage.getItem('loadRefundMaterialList') || this.loadRefundMaterialList;
        return this.loadRefundMaterialList === "TRUE";
    }

    public setSaveNotificationNEJE(load: boolean) {
        this.saveNotificationNEJE = load ? "TRUE" : "FALSE";
        localStorage.setItem('saveNotificationNEJE', this.saveNotificationNEJE);
    }

    public getSaveNotificationNEJE() {
        this.saveNotificationNEJE = localStorage.getItem('saveNotificationNEJE') || this.saveNotificationNEJE;
        return this.saveNotificationNEJE === "TRUE";
    }

    public setIsFromMaterialReturnControl(load: boolean) {
        this.isFromMaterialReturnControl = load ? "TRUE" : "FALSE";
        localStorage.setItem('isFromMaterialReturnControl', this.isFromMaterialReturnControl);
    }

    public getIsFromMaterialReturnControl() {
        this.isFromMaterialReturnControl = localStorage.getItem('isFromMaterialReturnControl') || this.isFromMaterialReturnControl;
        return this.isFromMaterialReturnControl === "TRUE";
    }

    public setLoadRefundMaterialList(load: boolean) {
        this.loadRefundMaterialList = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadRefundMaterialList', this.loadRefundMaterialList);
    }

    public getApproveMaterialsRequirement() {
        this.loadApproveMaterialsRequirement = sessionStorage.getItem('loadApproveMaterialsRequirement') || this.loadApproveMaterialsRequirement;
        return this.loadApproveMaterialsRequirement === "TRUE";
    }

    public setApproveMaterialsRequirement(load: boolean) {
        this.loadApproveMaterialsRequirement = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadApproveMaterialsRequirement', this.loadApproveMaterialsRequirement);
    }

    public getLoadBlockerExternalServices() {
        this.loadBlockerExternalServices = sessionStorage.getItem('loadBlockerExternalServices') || this.loadBlockerExternalServices;
        return this.loadBlockerExternalServices === "TRUE";
    }

    public setLoadBlockerExternalServices(load: boolean) {
        this.loadBlockerExternalServices = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadBlockerExternalServices', this.loadBlockerExternalServices);
    }

    public getLoadReplacement() {
        this.loadReplacement = sessionStorage.getItem('loadReplacement') || this.loadReplacement;
        return this.loadReplacement === "TRUE";
    }

    public setLoadReplacement(load: boolean) {
        this.loadReplacement = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadReplacement', this.loadReplacement);
    }

    public getShowOperationsMaterial() {
        this.showOperationsMaterial = sessionStorage.getItem('showOperationsMaterial') || this.showOperationsMaterial;
        return this.showOperationsMaterial === "TRUE";
    }

    public setShowOperationsMaterial(load: boolean) {
        this.showOperationsMaterial = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('showOperationsMaterial', this.showOperationsMaterial);
    }

    public getRenderMaterialsFromAuthorizer() {
        this.renderMaterialsFromAuthorizer = sessionStorage.getItem('renderMaterialsFromAuthorizer') || this.renderMaterialsFromAuthorizer;
        return this.renderMaterialsFromAuthorizer === "TRUE";
    }

    public setRenderMaterialsFromAuthorizer(load: boolean) {
        this.renderMaterialsFromAuthorizer = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderMaterialsFromAuthorizer', this.renderMaterialsFromAuthorizer);
    }

    public getRenderDeleteColumnMat() {
        this.renderDeleteColumnMat = sessionStorage.getItem('renderDeleteColumnMat') || this.renderDeleteColumnMat;
        return this.renderDeleteColumnMat === "TRUE";
    }

    public setRenderDeleteColumnMat(load: boolean) {
        this.renderDeleteColumnMat = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderDeleteColumnMat', this.renderDeleteColumnMat);
    }

    public getLoadMaterialsFromOrder() {
        this.loadMaterialsFromOrder = sessionStorage.getItem('loadMaterialsFromOrder') || this.loadMaterialsFromOrder;
        return this.loadMaterialsFromOrder === "TRUE";
    }

    public setLoadMaterialsFromOrder(load: boolean) {
        this.loadMaterialsFromOrder = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadMaterialsFromOrder', this.loadMaterialsFromOrder);
    }

    public getLoadMaterialsToComponent() {
        this.loadMaterialsToComponent = sessionStorage.getItem('loadMaterialsToComponent') || this.loadMaterialsToComponent;
        return this.loadMaterialsToComponent === "TRUE";
    }

    public setLoadMaterialsToComponent(load: boolean) {
        this.loadMaterialsToComponent = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadMaterialsToComponent', this.loadMaterialsToComponent);
    }

    public setMaterialsFromSearch(materials: MaterialOper[]) {
        this.materialsFromSearch = materials;
        sessionStorage.setItem('materialsFromSearch', JSON.stringify(this.materialsFromSearch));
    }

    public getMaterialsFromSearch() {
        this.materialsFromSearch = JSON.parse(sessionStorage.getItem('materialsFromSearch') || JSON.stringify(this.materialsFromSearch));
        return this.materialsFromSearch;
    }

    public getWorkOrderNumber() {
        this.workOrderNumber = sessionStorage.getItem('workOrderNumber') || this.workOrderNumber;
        return this.workOrderNumber;
    }

    public setReasonTime(time: string) {
        this.reasonTime = time;
        sessionStorage.setItem('reasonTime', this.reasonTime);
    }

    public getReasonTime() {
        this.reasonTime = sessionStorage.getItem('reasonTime') || this.reasonTime;
        return "Tiempo restante: " + this.reasonTime;
    }

    public setEffectiveTime(time: number) {
        this.effectiveTime = time;
        sessionStorage.setItem('effectiveTime', this.effectiveTime.toString());
    }

    public getEffectiveTime() {
        this.effectiveTime = parseInt(sessionStorage.getItem('effectiveTime') || this.effectiveTime.toString());
        return this.effectiveTime;
    }

    public setStandardTime(time: number) {
        this.standardTime = time;
        sessionStorage.setItem('standardTime', this.standardTime.toString());
    }

    public getStandartTime() {
        this.standardTime = parseInt(sessionStorage.getItem('standardTime') || this.standardTime.toString());
        return this.standardTime;
    }

    public setCMDescription(description: string) {
        this.cmDescription = description;
        sessionStorage.setItem('CMDescription', this.cmDescription);
    }

    public getCMDescription() {
        this.cmDescription = sessionStorage.getItem('CMDescription') || this.cmDescription;
        return this.cmDescription;
    }

    public setRequirementStatus(status: string) {
        this.requirementStatus = status;
        sessionStorage.setItem('requirementStatus', this.requirementStatus);
    }

    public getRequirementStatus() {
        this.requirementStatus = sessionStorage.getItem('requirementStatus') || this.requirementStatus;
        return this.requirementStatus;
    }

    public setModelKey(mk: string) {
        this.model_key = mk;
        sessionStorage.setItem('ModelKey', this.model_key);
    }

    public getModelKey() {
        this.model_key = sessionStorage.getItem('ModelKey') || this.model_key;
        return this.model_key;
    }

    public setCostoOrderHeader(costo: string) {
        this.costoOrderHeader = costo;
        sessionStorage.setItem('costoOrderHeader', this.costoOrderHeader);
    }

    public getCostoOrderHeader() {
        this.costoOrderHeader = sessionStorage.getItem('costoOrderHeader') || this.costoOrderHeader;
        return this.costoOrderHeader;
    }

    public setRentnr(rentnr: string) {
        this.rentnr = rentnr;
        sessionStorage.setItem('RENTNR', this.rentnr);
    }

    public getRentnr() {
        this.rentnr = sessionStorage.getItem('RENTNR') || this.rentnr;
        return this.rentnr;
    }

    public setEQUNR(equnr: string) {
        this.equnr = equnr;
        sessionStorage.setItem('EQUNR', this.equnr);
    }

    public getEQUNR() {
        this.equnr = sessionStorage.getItem('EQUNR') || this.equnr;
        return this.equnr;
    }

    public setVORNR(vornr: string) {
        this.vornr = vornr;
        sessionStorage.setItem('vornr', this.vornr);
    }

    public getVORNR() {
        this.vornr = sessionStorage.getItem('vornr') || this.vornr;
        return this.vornr;
    }

    public setCodDev(coddev: string) {
        this.coddev = coddev;
        localStorage.setItem('coddev', this.coddev);
    }

    public getCodDev() {
        this.coddev = localStorage.getItem('coddev') || this.coddev;
        return this.coddev;
    }

    public setLoadDataToBarChart(load: boolean) {
        this.renderDataToBarChart = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderDataToBarChart', this.renderDataToBarChart);
    }

    public getLoadDataToBarChart() {
        this.renderDataToBarChart = sessionStorage.getItem('renderDataToBarChart') || this.renderDataToBarChart;
        return this.renderDataToBarChart === "TRUE";
    }

    public setLoadExternalWorkControl(load: boolean) {
        this.loadExternalWC = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadExternalWC', this.loadExternalWC);
    }

    public getLoadExternalWorkControl() {
        this.loadExternalWC = sessionStorage.getItem('loadExternalWC') || this.loadExternalWC;
        return this.loadExternalWC === "TRUE";
    }

    public setLoadExternalWorkControlReception(load: boolean) {
        this.loadExternalWCReception = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadExternalWCReception', this.loadExternalWCReception);
    }

    public getLoadExternalWorkControlReception() {
        this.loadExternalWCReception = sessionStorage.getItem('loadExternalWCReception') || this.loadExternalWCReception;
        return this.loadExternalWCReception === "TRUE";
    }

    public setExtenarlWorkControl(loadDataEWC: any) {
        this.loadDataExternalWC = loadDataEWC ? loadDataEWC : "";
        sessionStorage.setItem('loadDataExternalWC', JSON.stringify(this.loadDataExternalWC));
    }

    public getExtenarlWorkControl() {
        this.loadDataExternalWC = JSON.parse(sessionStorage.getItem('loadDataExternalWC') || JSON.stringify(this.loadDataExternalWC));
        return this.loadDataExternalWC;
    }

    public setLoadServicePrintTemplate(load: boolean) {
        this.loadServicePrintTemplate = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadServicePrintTemplate', this.loadServicePrintTemplate);
    }

    public getLoadServicePrintTemplate() {
        this.loadServicePrintTemplate = sessionStorage.getItem('loadServicePrintTemplate') || this.loadServicePrintTemplate;
        return this.loadServicePrintTemplate === "TRUE";
    }

    public setServicePrintTemplateData(loadDataService: any) {
        this.loadDataService = loadDataService ? loadDataService : "";
        sessionStorage.setItem('loadDataService', JSON.stringify(this.loadDataService));
    }

    public getServicePrintTemplateData() {
        this.loadDataService = JSON.parse(sessionStorage.getItem('loadDataService') || JSON.stringify(this.loadDataService));
        return this.loadDataService;
    }

    public setHeaderServicePrintTemplate(serviceHeader: string) {
        this.serviceHeader = serviceHeader ? serviceHeader : "";
        sessionStorage.setItem('serviceHeader', this.serviceHeader);
    }

    public getHeaderServicePrintTemplate() {
        this.serviceHeader = sessionStorage.getItem('serviceHeader') || this.serviceHeader;
        return this.serviceHeader;
    }

    public setQualityTestControl(qualityTestControl: any) {
        this.loadQualityTestControls = qualityTestControl ? qualityTestControl : "";
        sessionStorage.setItem('loadQualityTestControls', JSON.stringify(this.loadQualityTestControls));
    }

    public getQualityTestControl() {
        this.loadQualityTestControls = JSON.parse(sessionStorage.getItem('loadQualityTestControls') || JSON.stringify(this.loadQualityTestControls));
        return this.loadQualityTestControls;
    }

    public setLoadPendingWorkFromFinishWO(load: boolean) {
        this.loadPendingWorksFromFinishWO = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadPendingWorksFromFinishWO', this.loadPendingWorksFromFinishWO);
    }

    public getLoadPendingWorkFromFinishWO() {
        this.loadPendingWorksFromFinishWO = sessionStorage.getItem('loadPendingWorksFromFinishWO') || this.loadPendingWorksFromFinishWO;
        return this.loadPendingWorksFromFinishWO === "TRUE";
    }

    public setLoadPendingWork(load: boolean) {
        this.loadPendingWork = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadPendingWork', this.loadPendingWork);
    }

    public getLoadPendingWork() {
        this.loadPendingWork = sessionStorage.getItem('loadPendingWork') || this.loadPendingWork;
        return this.loadPendingWork === "TRUE";
    }

    public setLoadPendingWorkConfirm(load: boolean) {
        this.loadPendingWorkConfirm = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadPendingWorkConfirm', this.loadPendingWorkConfirm);
    }

    public getLoadPendingWorkConfirm() {
        this.loadPendingWorkConfirm = sessionStorage.getItem('loadPendingWorkConfirm') || this.loadPendingWorkConfirm;
        return this.loadPendingWorkConfirm === "TRUE";
    }

    public setPendingWorkConfirmWO(pendingWorkWO: string) {
        this.pendingWorkWO = pendingWorkWO;
        sessionStorage.setItem('pendingWorkWO', this.pendingWorkWO);
    }

    public getPendingWorkConfirmWO() {
        this.pendingWorkWO = sessionStorage.getItem('pendingWorkWO') || this.pendingWorkWO;
        return this.pendingWorkWO;
    }

    public setLoadQualityTestControl(load: boolean) {
        this.loadQualityTestControl = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadQualityTestControl', this.loadQualityTestControl);
    }

    public getLoadQualityTestControl() {
        this.loadQualityTestControl = sessionStorage.getItem('loadQualityTestControl') || this.loadQualityTestControl;
        return this.loadQualityTestControl === "TRUE";
    }

    public setLoadQualityTestControlFromPopup(load: boolean) {
        this.loadQualityTestControlFromPopup = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadQualityTestControlFromPopup', this.loadQualityTestControlFromPopup);
    }

    public getLoadQualityTestControlFromPopup() {
        this.loadQualityTestControlFromPopup = sessionStorage.getItem('loadQualityTestControlFromPopup') || this.loadQualityTestControlFromPopup;
        return this.loadQualityTestControlFromPopup === "TRUE";
    }

    public setTimeFromExtenarlWorkControl(load: boolean) {
        this.renderTimeLeft = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderTimeLeft', this.renderTimeLeft);
    }

    public getTimeFromExtenarlWorkControl() {
        this.renderTimeLeft = sessionStorage.getItem('renderTimeLeft') || this.renderTimeLeft;
        return this.renderTimeLeft === "TRUE";
    }

    public setLoadSearchMaterials(load: boolean) {
        this.loadSearchMaterials = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadSearchMaterials', this.loadSearchMaterials);
    }

    public getLoadSearchMaterials() {
        this.loadSearchMaterials = sessionStorage.getItem('loadSearchMaterials') || this.loadSearchMaterials;
        return this.loadSearchMaterials === "TRUE";
    }

    public setLoadMaterials(load: boolean) {
        this.loadMaterials = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadMaterials', this.loadMaterials);
    }

    public getLoadMaterials() {
        this.loadMaterials = sessionStorage.getItem('loadMaterials') || this.loadMaterials;
        return this.loadMaterials === "TRUE";
    }

    public setLoadEquipmentHistory(load: boolean) {
        this.loadEquipmentHistory = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadEquipmentHistory', this.loadEquipmentHistory);
    }

    public getLoadEquipmentHistory() {
        this.loadEquipmentHistory = sessionStorage.getItem('loadEquipmentHistory') || this.loadEquipmentHistory;
        return this.loadEquipmentHistory === "TRUE";
    }

    public setLoadEquipmentDetails(load: boolean) {
        this.loadEquipmentDetails = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadEquipmentDetails', this.loadEquipmentDetails);
    }

    public getLoadEquipmentDetails() {
        this.loadEquipmentDetails = sessionStorage.getItem('loadEquipmentDetails') || this.loadEquipmentDetails;
        return this.loadEquipmentDetails === "TRUE";
    }

    public setLoadServicesFromExternalWC(load: boolean) {
        this.loadServicesFromExternalWC = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadServicesFromExternalWC', this.loadServicesFromExternalWC);
    }

    public getLoadServicesFromExternalWC() {
        this.loadServicesFromExternalWC = sessionStorage.getItem('loadServicesFromExternalWC') || this.loadServicesFromExternalWC;
        return this.loadServicesFromExternalWC === "TRUE";
    }

    public setLoadAdvices(load: boolean) {
        this.loadAdvices = load ? "TRUE" : "FALSE";
        localStorage.setItem('loadAdvices', this.loadAdvices);
    }

    public getLoadAdvices() {
        this.loadAdvices = localStorage.getItem('loadAdvices') || this.loadAdvices;
        return this.loadAdvices === "TRUE";
    }

    public setLoadAlertControl(load: boolean) {
        this.loadAlertControl = load ? "TRUE" : "FALSE";
        localStorage.setItem('loadAlertControl', this.loadAlertControl);
    }

    public getLoadAlertControl() {
        this.loadAlertControl = localStorage.getItem('loadAlertControl') || this.loadAlertControl;
        return this.loadAlertControl === "TRUE";
    }

    public setLoadClosureReason(load: boolean) {
        this.loadClosureReason = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadClosureReason', this.loadClosureReason);
    }

    public getLoadClosureReason() {
        this.loadClosureReason = sessionStorage.getItem('loadClosureReason') || this.loadClosureReason;
        return this.loadClosureReason === "TRUE";
    }

    public setReloadWODetail(reload: boolean) {
        this.reloadWODetail = reload ? "TRUE" : "FALSE";
        sessionStorage.setItem('RealoadWODetail', this.reloadWODetail);
    }

    public getLoadCheckList() {
        this.loadCheckList = sessionStorage.getItem('loadCheckList') || this.loadCheckList;
        return this.loadCheckList === "TRUE";
    }

    public setLoadCheckList(load: boolean) {
        this.loadCheckList = load ? "TRUE" : "FALSE";
        sessionStorage.setItem('loadCheckList', this.loadCheckList);
    }

    public getReloadWODetail() {
        this.reloadWODetail = sessionStorage.getItem('RealoadWODetail') || this.reloadWODetail;
        return this.reloadWODetail === "TRUE";
    }

    public setReloadMatReturnControl(reload: boolean) {
        this.reloadMatReturnControl = reload ? "TRUE" : "FALSE";
        sessionStorage.setItem('reloadMatReturnControl', this.reloadMatReturnControl);
    }

    public getReloadMatReturnControl() {
        this.reloadMatReturnControl = sessionStorage.getItem('reloadMatReturnControl') || this.reloadMatReturnControl;
        return this.reloadMatReturnControl === "TRUE";
    }

    public setReloadExternalWC(reload: boolean) {
        this.reloadExternalWC = reload ? "TRUE" : "FALSE";
        sessionStorage.setItem('reloadExternalWC', this.reloadExternalWC);
    }

    public getReloadExternalWC() {
        this.reloadExternalWC = sessionStorage.getItem('reloadExternalWC') || this.reloadExternalWC;
        return this.reloadExternalWC === "TRUE";
    }

    public setRenderRequirement(render: boolean) {
        this.renderRequirement = render ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderRequirement', this.renderRequirement);
    }

    public getRenderRequirement() {
        this.renderRequirement = sessionStorage.getItem('renderRequirement') || this.renderRequirement;
        return this.renderRequirement === "TRUE";
    }

    public setRenderNotificationStart(render: boolean) {
        this.renderNotificationStart = render ? "TRUE" : "FALSE";
        sessionStorage.setItem('renderNotificationStart', this.renderNotificationStart);
    }

    public getRenderNotificationStart() {
        this.renderNotificationStart = sessionStorage.getItem('renderNotificationStart') || this.renderNotificationStart;
        return this.renderNotificationStart === "TRUE";
    }

    public setNREQUISA(req: string) {
        this.nrequisa = req;
        sessionStorage.setItem('NREQUISA', this.nrequisa);
    }

    public getNREQUISA() {
        this.nrequisa = sessionStorage.getItem('NREQUISA') || this.nrequisa;
        return this.nrequisa;
    }

    public setEquipModel(equip: string) {
        this.equipModel = equip;
        sessionStorage.setItem('equipModel', this.equipModel);
    }

    public getEquipModel() {
        this.equipModel = sessionStorage.getItem('equipModel') || this.equipModel;
        return this.equipModel;
    }

    public setEquipoModel(equipo: string) {
        this.equipoModel = equipo;
        sessionStorage.setItem('equipoModel', this.equipoModel);
    }

    public getEquipoModel() {
        this.equipoModel = sessionStorage.getItem('equipoModel') || this.equipoModel;
        return this.equipoModel;
    }

    public setLightColor(lightColor: string) {
        this.lightColor = lightColor;
        sessionStorage.setItem('lightColor', this.lightColor);
    }

    public getLightColor() {
        this.lightColor = sessionStorage.getItem('lightColor') || this.lightColor;
        return this.lightColor;
    }

    public setArbpl(arbpl: string) {
        this.arbpl = arbpl;
        sessionStorage.setItem('arbpl', this.arbpl);
    }

    public getArbplr() {
        this.arbpl = sessionStorage.getItem('arbpl') || this.arbpl;
        return this.arbpl;
    }

    public setRootUrl(rootUrl: string) {
        this.rootUrl = rootUrl;
        sessionStorage.setItem('rootUrl', this.rootUrl);
    }

    public getRootUrl() {
        this.rootUrl = sessionStorage.getItem('rootUrl') || this.rootUrl;
        return this.rootUrl;
    }

    public setWorkOrderQuantity(quantity: number) {
        this.workOrderQuantity = quantity;
        sessionStorage.setItem('quantity', String(this.workOrderQuantity));
    }

    public getWorkOrderQuantity() {
        this.workOrderQuantity = Number(sessionStorage.getItem('quantity')) || this.workOrderQuantity;
        return this.workOrderQuantity;
    }
}