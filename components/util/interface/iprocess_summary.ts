﻿interface IProcessSummary {
    mandt: string,
    rentnr: string,
    aufnr: string,
    activ: string,
    femod: string,
    hrmod: string,
    usuario: string,
    statord: string,
    equnr: string,
    tipov: string,
    ktext: string,
    descripst: string,
}