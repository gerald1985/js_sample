﻿interface IRequirementAuto {
    nrequisa: string,
    aufnr: string,
    equnr: string,
    anios: string,
    tiempo: string,
    staT_EQ: string,
    cosT_ORD: string,
    cosT_REQU: number,
    espec: string,
    inRange: boolean,
}