﻿interface IReception {
    rentnr: string,
    aufnr: string,
    equnr: string,
    tipov: string,
    idclase: string,
    gstrp: string,
    gltrp: string,
    arbpl: string,
    fedoc_er: string,
    femod: string,
    pC_STATUS: string,
    ktext: string,
    bahia: string,
    statord: string,
    descripord: string,
    statordColor: string,
    hrdoc_er: string,
    hrmod: string,
    pC_STATUSChecked: boolean,
}

interface IProvider {
    lifnr: string,
    namE1: string,
}

interface IWOReception {
    ReceptionDate: Date,
    AssignmentDate: Date,
    EquipmentNum: number,
    OrderNum: string,
    OrderDesc: string,
    WorkForce: IWorkForce,
    Bay: number,
}

interface IWorkCenter {
    objty: string,
    objid: number,
    arbpl: string,
    lvorm: string,
    xsprr: string,
    veran: string,
}

interface IWorkForce extends IWorkCenter {
    Puesto: string,
    Especialidad: string,
}

interface IBay {
    bahia: number,
    descrip: string,
}

interface ISessionInfo {
    UserName: string,
    UserId: string,
    RoleName: string,
    Arbpl: string,
}

interface ISupervisor {
    usuario: string,
    iD_ZADF_CLA_VEH: string,
}

interface IWOAssignment {
    arbpl: string,
    aufnr: string,
    capmtxt: string,
    equnr: string,
    eqktx: string,
    gstrp: string,
    gsuzp: string,
    durac: string,
    statord: string,
    tpoefec: string,
    ttranscurrido: string,
    lightcolor: string,
    effectiveTime: string,
}

interface IWorkOrderOperation {
    arbpl: string,
    tpO_STD: string,
    tpO_EFE: string,
    aufnr: string,
    vornr: string,
    origen: string,
    nota: string,
    srvex: boolean,
    tot_mat: string,
    ulT_MOTIVO: string,
    r_chklist: string,
    lifnr: string,
    ktsch: string,
    txt: string,
    existCheckList: boolean,
    pendingCheckList: boolean,
    notWorkedCheckList: boolean,
    checked: boolean,
    haycomentario: string,
}

interface IComment {
    idcomment: string,
    usuario: string,
    referencia: string,
    fecha: string,
    hora: string,
    modulo: string,
    pantalla: string,
    status: string,
    comentario: string,
}

interface IMessageUpdater {
    IDCOMENT: string,
    NUEVOTEXTO: string,
}

interface IOrderHeader {
    aufnr: string,
    equnr: string,
    bahia: string,
    costo: string,
    kmprg: string,
    rentnr: string,
    modelo: string,
    duracion: string,
    observacion: string,
    pC_STATUS: string,
}

//Requirement
interface IRequirement {
    nrequisa: string,
    aufnr: string,
    fecreac: string,
    hrcreac: string,
    mecanic: string,
    revision: string,
    ferevis: string,
    hrrevis: string,
    usuario: string,
    pndaut: string,
    aufnrdes: string,
    status: string,
    costo: string,
    lst_ZADF_REQUOPR: Ilst_ZADF_REQUOPR[],
}

interface Ilst_ZADF_REQUOPR extends IRequirement {
    vornr: string,
    ktsch: string,
    autor: string,
    srvex: string,
    arbpl: string,
    texto: string,
    txtclave: string,
    activo: string,
    proveedor: string,
    peticionborrado: boolean,
    lst_ZADF_REQUMAT: Ilst_ZADF_REQUMAT[],
}

interface Ilst_ZADF_REQUMAT extends Ilst_ZADF_REQUOPR {
    matnr: string,
    cant: string,
    cninm: string,
    ptborr: string,
    cntaut: string,
    cntdev: string,
    descripcion: string,
    punitario: string,
    maktx: string,
}

interface IRequirementSup extends IRequirement {
    equnr: string,
    anios: string,
    espec: string,
    tiempo: string,
    staT_EQ: string,
    approved: boolean,
    checked: boolean,
    comentario: string,
}
//Finish

//Model Key 
interface IModelKey {
    id_CM: string,
    id_MODELO: string,
    id_SISTEMA: string,
    tiempo_STANDARD: string,
    prueba_CARRETERA: string,
    cm_ANTERIOR: string,
    ltext: string,
    selected: boolean,
}


interface ISystemMK {
    iD_SISTEMA: string,
    descripcion: string,
}


//Finish

//Notification

interface IKeyDamageStatus {
    mandt: string,
    aufnr: string,
    vornr: string,
    notif: string,
    fenot: string,
    hrnot: string,
    motivo: string,
    tiempo: string,
}


interface IReason {
    mandt: string,
    motivo: string,
    descrip: string,
    tipon: string,
    checked: boolean,
}


interface IInstruction {
    mandt: string,
    descrip: string,
    aufnr: string,
    vornr: string,
    codinst: string,
    verif: string,
}

//Finish

//Material

interface IMaterial {
    matnr: string,
    matkl: string,
    maktx: string,
    wgbez: string,
    verpr: string,
    meins: string,
    selected: boolean,
}

interface IMaterialOper extends IMaterial {
    nrequisa: string,
    vornr: string,
    cant: number,
    autor: string,
    cninm: string,
    ptborr: string,
    cntaut: string,
    cntdev: string,
    descripcion: string,
    punitario: string,
    activo: string,
    isnew: boolean,
    cost: string,
    canT_FALT: number,
}

interface IServiceRequest {
    noEquipo: string,
    noOrden: string,
    fecha: string,
    hora: string,
    pieza: string,
    tipoReparacion: string,
    proveedor: string,
    fechaEntrega: string,
    horaEntrega: string,
    fechaRetorno: string,
    horaRetorno: string,
    tiempoEstimado: string
}

//Finish

//Refund

interface IRefund {
    codev: string,
    puestocrea: string,
    puestosuper: string,
    aufnr: string,
    status: string,
    fechA_CREA: string,
    horA_CREA: string,
    fechA_APRUEBA: string,
    horA_APRUEBA: string,
    reserva: string,
}

interface IMaterialUnloading {
    aufnr: string,
    vornr: string,
    ktsch: string,
    matnr: string,
    aufpl: string,
    steus: string,
    ltxA1: string,
    rsnum: string,
    rspos: string,
    maktg: string,
    bdmng: string,
    meins: string,
    lgpbe: string,
    cantdev: number,
    cantdevuelta: string,
    checked: boolean,
}

//Finish

//Equipment History

interface IEquipmentHistory {
    equnr: string,
    aufnr: string,
    vornr: string,
    gltrp: string,
    klm: string,
    ktsch: string,
    ltxa1: string,
    arbpl: string,
    rsnum: string,
    component_history_list: IComponent_History_List[],
}

interface IComponent_History_List extends IEquipmentHistory {
    matnr: string,
    maktx: string,
    cant: string,
    unidad: string
}

//Finish

//Equipment Detail

interface IEquipmentDetail {
    MANDT: string,
    RENTNR: string,
    AUFNR: string,
    DESCORDEN: string,
    ESPECIALIDAD: string,
    FECHA_INICIO: string,
    HORA_INICIO: string,
    FECHA_FIN: string,
    HORA_FIN: string,
    TCOLA: string,
    TEJECUCION: string,
    TPARO: string,
    TPLANIFICADO: string,
    TDESVIO: string,
    ESTATUS: string
}


//Finish

//External Work Control

interface IExternalWorkControl {
    aufnr: string,
    vornr: string,
    notif: string,
    equnr: string,
    lifnr: string,
    name1: string,
    ktsch: string,
    ltxA1: string,
    fenot: string,
    hrnot: string,
    motivo: string,
    lightcolor: string
}


//Finish

//Notification

interface IAdvice {
    mandt: string,
    qmnum: string,
    equnr: string,
    qmart: string,
    qmtxt: string,
    qmdat: string,
    mzeit: string,
    aufnr: string,
    qmartx: string,
    eqktx: string,
    estatus: string,
    textaveria: string,
    kurztext: string,
    fetxt: string,
    aviso_text_list: AVISO_TEXT_LIST
}


interface AVISO_TEXT_LIST extends IAdvice {
    ltext: string
}

//Finish

//Material Return Control

interface IMaterialReturnControl {
    coddev: string,
    puestocrea: string,
    puestosuper: string,
    aufnr: string,
    status: string,
    fecha_crea: string,
    hora_crea: string,
    fecha_aprueba: string,
    hora_aprueba: string
}


//Finish

//Equipment History

interface IMaterialDevolution {
    coddev: string,
    puestocrea: string,
    puestosuper: string,
    aufnr: string,
    status: string,
    fechA_CREA: string,
    horA_CREA: string,
    fechA_APRUEBA: string,
    horA_APRUEBA: string,
    equnr: string,
    comentarios: string,
    reserva: string,
    maT_DEV_DET_LIST: IMAT_DEV_DET_LIST[],
}

interface IMAT_DEV_DET_LIST extends IMaterialDevolution {
    matnr: string,
    cantdev: string,
    status: string,
    maktx: string,
    cantentr: string,
    aufnr: string,
    vornr: string,
    ktsch: string,
    txt: string,
    meinh: string,
}

//Finish

//Alert Control

interface IAlertControl {
    mandt: string,
    rentnr: string,
    aufnr: string,
    motivo: string,
    retraso: string,
    equnr: string,
    descripcv: string
}


//Finish


//WO Concluded

interface IWOConcluded {
    mandt: string,
    rentnr: string,
    aufnr: string,
    modifnr: string,
    bahia: string,
    arbpl: string,
    activ: string,
    femod: string,
    hrmod: string,
    usuario: string,
    statord: string,
    equnr: string,
    tipov: string,
    pc_status: string,
    pc: string,
    pb: string
}


//Finish

//Model Failure

interface IModulFailure {
    katalogart: string,
    codegruppe: string,
    kurztext: string
}


//Finish


//Key Damage

interface IKeyDamage {
    fegrp: string,
    fecod: string,
    ktsch: string,
    txt: string
}


//Finish

//Pending Work Detail

interface IPendingWorkDetail {
    mandt: string,
    id_trab_pend: string,
    aufnr: string,
    ktsch: string,
    kurztext: string,//Descripcion del tipo de falla
    status: string,
    descripcion: string//Texto largo,
    statusEliminado: boolean,
    qmart: string,
    qmartx: string,
}


//Finish

//Quality Test Control

interface IQualityTestControlGrouping {
    rentnr: string,
    equnr: string,
    especialidad: string,
    pc: string,
    pb: string,
    pcchecked: boolean,
    pbchecked: boolean,
    qualityTestControlList: IQualityTestControl[],
}

interface IQualityTestControl extends IQualityTestControlGrouping {
    mandt: string,
    rentnr: string,
    aufnr: string,
    modifnr: string,
    bahia: string,
    arbpl: string,
    activ: string,
    femod: string,
    hrmod: string,
    usuario: string,
    statord: string,
    equnr: string,
    tipov: string,
    pc_status: string,
    pc: string,
    pb: string,
    especialidad: string,
    pruebA_CARRETERA: string,
}


//Finish

//Purchase Requisition Authorization

interface IPurchaseRequisitionAuthorization {
    mandt: string,
    nrequisa: string,
    aufnr: string,
    aufnrdes: string,
    equnr: string,
    ci: string,
    ciChecked: boolean,
    procesa: string,
    procesachecked: boolean,
    tomada: string,
}

//Finish

//Purchase Requisition Active

interface IPurchaseRequisitionActive {
    mandt: string,
    rentnr: string,
    equnr: string,
    aufnr: string,
    ilart: string,
    aedat: string,
    capmtxt: string,
    enlazar: boolean,
}

//Finish

//Activity Class Rol

interface IActivityClassRol {
    mandt: string,
    claseactividadpm: string,
    rol: string
}

//Finish

