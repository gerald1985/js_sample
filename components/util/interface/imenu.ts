﻿interface IMenu {
    menuId: string,
    description: string,
    menuTypeDesc: string,
    parentId: string,
    parentDescription: string,
    menuItemPermissionDTO: IMenuItemPermission,
}

interface IMenuItemPermission {
    permissionId: string,
    actionPermissionId: string,
    description: string,
}