﻿interface IEquipmentSummary {
    equnr :string,
    rentnr :string,
    hequi :string,
    deqsup :string,
    respon :string,
    fechA_R :string,
    horA_R :string,
    tiempot :string,
    cantot :string,
    statusdr: string,
    tdescrip: string,
}