﻿import { Injectable } from '@angular/core';

@Injectable()
export class BreadCrumbService {

    private static commentId: string = "";
    private static breadCrumbRoot: string = "";
    private static breadCrumbChild: string = "";
    public static breadcrumbs: string = "";
    private static reference: string = "";
    private static typeReference: string = "";
    private static refreshComments: boolean = false;

    public static changeBreadCrumbChild(bcChild: string) {
        this.breadCrumbChild = bcChild;
        this.changeBreadCrumb();
    }

    public static changeBreadCrumbRoot(bcRoot: string) {
        this.breadCrumbRoot = bcRoot;
        this.changeBreadCrumb();
    }

    public static changeBreadCrumb() {
        this.breadcrumbs = this.breadCrumbRoot + " / " + this.breadCrumbChild;
    }

    public static newBreadCrumbRoot(bcRoot: string) {
        this.breadCrumbRoot = bcRoot;
        this.breadCrumbChild = "";
        this.changeBreadCrumb();
    }

    public static setRefreshComments(refreshComments: boolean) {
        this.refreshComments = refreshComments;
    }

    public static getRefreshComments() {
        return this.refreshComments;
    }

    public static setReference(reference:string) {
        this.reference = reference;
    }

    public static getReference() {
        return this.reference;
    }

    public static setTypeReference(tReference: string) {
        this.typeReference = "&#91;" + tReference +"&#93;";
    }

    public static getTypeReference() {
        return this.typeReference;
    }

    static getCommentId() {
        return this.commentId
    }
    static setCommentId(commentId: string) {
        this.commentId = commentId
    }
}