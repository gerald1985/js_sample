﻿import "materialize-css";
import { AlertComponent } from "../alerts/alert.component";
import { BreadCrumbService } from "./breadcrumbService";
import { Response } from "@angular/http";
import { Menu } from "./entities/menu";
import { MenuItemPermissionDTO } from "./entities/menu_item";
import { DataService } from "./data_service";
import { Router } from "@angular/router";
declare var M: any;
export class Util {
    public static loadMaterialElements() {
        Util.loadMaterialBySelector(".sidenav", { draggable: true, preventScrolling: true }, "Sidenav");
        Util.loadMaterialBySelector(".tabs", {}, "Tabs");
        Util.loadMaterialBySelector(".modal", {}, "Modal");
        Util.loadMaterialBySelector(".dropdown-trigger", {}, "Dropdown");
        Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
    }

    public static getM_DomElements() {
        return M;
    }

    public static loadMaterialBySelector(selector: string, options: any, materialType: any) {
        let materialDom = Array.from(document.querySelectorAll(selector));
        let dom: any;
        switch (materialType) {
            case "Sidenav": {
                this.InitEl(M.Sidenav, materialDom, options);
                break;
            }
            case "Tabs": {
                this.InitEl(M.Tabs, materialDom, options);
                break;
            }
            case "Modal": {
                this.InitEl(M.Modal, materialDom, options);
                break;
            }
            case "Dropdown": {
                this.InitEl(M.Dropdown, materialDom, options);
                break;
            }
            case "Tooltip": {
                this.InitEl(M.Tooltip, materialDom, options);
                break;
            }
        }
    }

    public static havePermission(menuIds: string[], dataService: DataService, alertComponent: AlertComponent, router: Router) {
        let menus: Menu[] = Array.prototype.filter.call(dataService.getMenuList(), function (m: Menu) {
            let menu = new Menu(m);
            return menuIds.indexOf(menu._Menu.menuId) > -1;
        });
        if (menus.length == 0) {
            alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            alertComponent.warningMessage();
            router.navigateByUrl(dataService.getRootUrl());
        }
    }

    public static getPermissions(menuIds: string[], menuList: Menu[]): number[] {
        let menus: Menu[] = Array.prototype.filter.call(menuList, function (m: Menu) {
            let menu = new Menu(m);
            return menuIds.indexOf(menu._Menu.menuId) > -1;
        });
        let permissions: string[] = menus.map(function (m) {
            let menuItem = new MenuItemPermissionDTO(new Menu(m)._Menu.menuItemPermissionDTO);
            return menuItem._MenuItem.actionPermissionId;
        });
        return permissions.reverse().map(function (m) { return parseInt(m); });
    }



    public static evalMenu(menuId: string, parentId: string, menuList: Menu[]) {
        let found = false;
        Array.prototype.some.call(menuList, function (m: Menu) {
            let menu = new Menu(m);
            let menuItem = new MenuItemPermissionDTO(menu._Menu.menuItemPermissionDTO);
            if (menu._Menu.menuId == menuId && menu._Menu.parentId == parentId && menu._Menu.menuTypeDesc == "Menu") {
                found = true;
                return true;
            }
        });
        return found;
    }

    public static getUniqueValuesFromArray(_array: any[]) {
        let uniq = new Set(_array.map(e => JSON.stringify(e)));
        return Array.from(uniq).map(e => JSON.parse(e));
    }

    public static diff_minutes(dt2: Date, dt1: Date) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.round(diff);

    }

    public static EvalError(error: any, alertComponent: AlertComponent, loginUrl: string) {
        this.HideLoader();
        if (error.status == 401 || error.status == 0) {
            window.location.href = loginUrl;
        } else {
            alertComponent.toastMessage = "Ocurrio un error inesperado al consultar al servidor, si el problema persiste refresca la página o consulta con el administrador.";
            alertComponent.successMessage();
        }
    }

    public static timeToMinutes(time: string) {
        var hours = parseInt(time.split(':')[0]),
            minutes = parseInt(time.split(':')[1]),
            seconds = parseInt(time.split(':')[2]),
            timeInminutes = (hours * 60) + minutes + (seconds / 60);
        return timeInminutes;
    }

    public static secondsToTime(seconds: number, includeSeconds: boolean) {
        var days = Math.floor(seconds / ((seconds < 0 ? -3600 : 3600) * 24));
        seconds -= days * (seconds < 0 ? -3600 : 3600) * 24;
        var hrs = Math.floor(seconds / (seconds < 0 ? -3600 : 3600));
        seconds -= hrs * (seconds < 0 ? -3600 : 3600);
        var mnts = Math.floor(seconds / (seconds < 0 ? -60 : 60));
        seconds -= mnts * 60;
        if (includeSeconds) {
            return (seconds < 0 ? "-" : "") + days + " días " + hrs + " Hrs " + mnts + " Min " + seconds + " Segundos";
        }
        return (seconds < 0 ? "-" : "") + days + " días " + hrs + " Hrs " + mnts + " Min";
    }

    public static setTime(date: Date, time: string) {
        date.setHours(parseInt(time.split(':')[0]));
        date.setMinutes(parseInt(time.split(':')[1]));
        date.setSeconds(parseInt(time.split(':')[2]));
        return date;
    }

    public static InitEl(dom: any, materialDom: any, options: any) {
        materialDom.forEach(function (el: any) {
            dom.init(el, options);
        });
    }

    public static ShowLoader() {
        var loader = document.getElementsByTagName('loader');
        if (loader) {
            loader[0].classList.remove('visibility_hide');
        }
    }

    public static HideLoader() {
        var loader = document.getElementsByTagName('loader');
        if (loader) {
            loader[0].classList.add('visibility_hide');
        }
    }

    public static showMessage(message: string, classes: string, duration: number = 4000) {
        M.toast({ html: message, classes: classes, displayLength: duration });
    }

    public static loadJsonWithToast<T>(elem: T, result: any, alertComponent: AlertComponent) {
        if (result.json()["responseCode"] == 200) {
            elem = result.json()["data"] as T;
        } else {
            alertComponent.toastMessage = result.json()["responseText"];
            alertComponent.errorMessage();
        };
        return elem;
    }

    public static getDataFromResponseJSON(result: Response) {
        return result.json()["data"];
    }

    public static transactionWithToast(result: any, alertComponent: AlertComponent) {

        if (result.json()["responseCode"] == 200) {
            alertComponent.toastMessage = "Operacion exitosa";
            alertComponent.successMessage();
        } else {
            alertComponent.toastMessage = "Operacion fallida, consulte con el administrador de sistema";
            alertComponent.errorMessage();
        }
    }

    public static openModal(selector: string) {
        let materialDom = document.querySelector(selector);
        BreadCrumbService.setCommentId("");
        M.Modal.getInstance(materialDom).open();
    }

    public static closeModal(selector: string) {
        let materialDom = document.querySelector(selector);
        BreadCrumbService.setCommentId("");
        M.Modal.getInstance(materialDom).close();
    }

    public static interfaceObjects<T>(elem: T, data: any) {
        elem = data as T;
        return elem;
    }

    public static getCurrentDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        return new Date(yyyy, mm, dd);
    }

    public static getCurrentDateTime() {
        return new Date();
    }

    public static dateAddDays(date: Date, days: number) {
        date.setDate(date.getDate() + days);
        return date;
    }

    public static dateAddYearInFormatYYYYMMDD(date: Date, year: number) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yyyy = date.getFullYear();

        var dayText = dd.toString();
        if (dd < 10) {
            dayText = '0' + dd;
        }

        var monthText = mm.toString();
        if (mm < 10) {
            monthText = '0' + mm;
        }

        var yearText = yyyy + year;

        return yearText + "-" + monthText + "-" + dayText;
    }

    public static formatDate(date: Date) {

        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();

        var dayText = day.toString();
        if (day < 10) {
            dayText = '0' + day;
        }

        var monthText = month.toString();
        if (month < 10) {
            monthText = '0' + month;
        }

        return dayText + "/" + monthText + "/" + year;
    }

    public static formatDateISO(date: Date, addMonth: number = 1) {

        var day = date.getDate();
        var month = date.getMonth() + addMonth;
        var year = date.getFullYear();

        var dayText = day.toString();
        if (day < 10) {
            dayText = '0' + day;
        }

        var monthText = month.toString();
        if (month < 10) {
            monthText = '0' + month;
        }

        return year + "-" + monthText + "-" + dayText;
    }

    public static formatTime(dateTime: Date) {
        var hours = dateTime.getHours();
        var minutes = dateTime.getMinutes();

        var hoursText = hours.toString();
        if (hours < 10) {
            hoursText = '0' + hours;
        }

        var minutesText = minutes.toString();
        if (minutes < 10) {
            minutesText = '0' + minutes
        }

        //eturn "00:00";
        return hoursText + ":" + minutesText + ":00";
        //return hours + ":" + minutes;

    }

    public static diff_Days_FormatDDMMYYYY(date: string) {//Format DDMMYYYY
        var dateSplit = date.split('/');
        var newDateString = dateSplit[1] + '/' + dateSplit[0] + '/' + dateSplit[2];//Format MMDDYYYY

        var newDate = new Date(newDateString);
        var today = new Date();

        var dif = today.getTime() - newDate.getTime();
        var days = Math.floor(dif / (1000 * 60 * 60 * 24));

        return days;
    }

    public static convertMinToDDHHMMSS(timeInMin: number) {
        var seconds = Math.floor(timeInMin * 60);//1 min = 60 seconds

        var days = Math.floor(seconds / (3600 * 24));//1 hr = 60 min, 1 min = 60 seconds, 1 hr = 3600 seconds
        seconds -= days * 3600 * 24;

        var hrs = Math.floor(seconds / 3600);//1 hr = 60 min, 1 min = 60 seconds
        seconds -= hrs * 3600;

        var mnts = Math.floor(seconds / 60);//1 hr = 60 min

        seconds -= mnts * 60;

        return days + " days, " + hrs + " Hrs, " + mnts + " Minutes, " + seconds + " Seconds";
    }
}