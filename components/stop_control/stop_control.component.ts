import { Component, Inject, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { StopControl } from '../util/entities/stop_control';
import { Provider } from '../util/entities/provider';

@Component({
    selector: 'stop-control',
    templateUrl: './stop_control.component.html',
    providers: [AlertComponent]
})
export class StopControlComponent {
    public stopControls: StopControl[] = [];
    public stopControlsIsLoaded: boolean = false;
    public providers: Provider[] = [];
    public renderElements: boolean = false;
    public havePermission: boolean = false;

    //initializing p to one
    pageStopControl: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {
    }

    stopControlsUrl() {
        return this.http.get(this.baseUrl + "api/WO/EnumStopControl");
    }

    loadStopControls() {
        if (Util.getPermissions(['60', '68', '73'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.stopControlsUrl().subscribe(result => {
            Util.HideLoader();
            this.stopControls = Util.loadJsonWithToast<StopControl[]>(this.stopControls, result, this.alertComponent);
            this.renderElements = true;
            this.providerUrl().subscribe(resultProvider => {
                this.providers = Util.loadJsonWithToast<Provider[]>(this.providers, resultProvider, this.alertComponent);
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });
    }

    providerUrl() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumProvider");
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
}