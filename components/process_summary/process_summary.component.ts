import { Component, Inject, OnInit, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { ProcessSummary } from '../util/entities/process_summary';

@Component({
    selector: 'process-summary',
    templateUrl: './process_summary.component.html',
    providers: [AlertComponent]
})
export class ProcessSummaryComponent implements OnInit {
    public navigation: string = "";
    public processSummaryReceived: ProcessSummary[] = [];
    public processSummaryAsigned: ProcessSummary[] = [];
    public processSummaryReprocess: ProcessSummary[] = [];
    public processSummaryInspected: ProcessSummary[] = [];
    public processSummaryQualityTest: ProcessSummary[] = [];
    public processSummaryFinished: ProcessSummary[] = [];
    public requestFinished: number = 0;
    public havePermission: boolean = false;

    pageProcessSummaryReceived: number = 1;
    pageProcessSummaryAsigned: number = 1;
    pageProcessSummaryReprocess: number = 1;
    pageProcessSummaryInspected: number = 1;
    pageProcessSummaryQualityTest: number = 1;
    pageProcessSummaryFinished: number = 1;
    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private dataService: DataService) {
    }

    processSummaryReceivedUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=RECEP');
    }

    pageProcessSummaryAsignedUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=ASIGN');
    }

    pageProcessSummaryReprocessUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=PROCE');
    }

    pageProcessSummaryInspectedUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=FINBH');
    }

    pageProcessSummaryQualityTestUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=VERI');
    }

    pageProcessSummaryFinishedUrl() {
        return this.http.get(this.baseUrl + 'api/Summary/EnumProcessSummary?status=CONC');
    }

    ngOnInit() {
        //this.loadProcessSummary();
    }

    loadProcessSummary() {
        if (Util.getPermissions(['50', '58', '66', '76', '81', '85'], this.dataService.getMenuList()).length != 0) {
            this.havePermission = true;
        }
        else {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            return;
        }

        Util.ShowLoader();
        this.processSummaryReceivedUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryReceived = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryReceived, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });

        this.pageProcessSummaryAsignedUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryAsigned = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryAsigned, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });

        this.pageProcessSummaryReprocessUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryReprocess = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryReprocess, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });

        this.pageProcessSummaryInspectedUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryInspected = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryInspected, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });

        this.pageProcessSummaryQualityTestUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryQualityTest = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryQualityTest, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });

        this.pageProcessSummaryFinishedUrl().subscribe(result => {
            if (result != undefined) {
                this.processSummaryFinished = Util.loadJsonWithToast<ProcessSummary[]>(this.processSummaryFinished, result, this.alertComponent);
                this.requestFinished++;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl)
        });
    }

    ngAfterViewChecked() {
        if (this.requestFinished == 6) {
            this.requestFinished = 0;
            Util.loadMaterialElements();
            //BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada t&eacute;cnico");
            //BreadCrumbService.changeBreadCrumbChild("Resumen x Proceso");
            Util.HideLoader();
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}