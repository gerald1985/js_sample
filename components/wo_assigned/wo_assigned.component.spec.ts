﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { WOAssignedComponent } from './wo_assigned.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';
import { AlertComponent } from '../alerts/alert.component';
import { DataService } from '../util/data_service';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPaginationModule } from 'ngx-pagination'; ;
import { WOAssignment } from '../util/entities/wo_assignment';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../util/util';
import { MockResponse } from '../../../test/mock/mock_reponse';


let fixture: ComponentFixture<WOAssignedComponent>;
let comp: WOAssignedComponent;

describe('Testing WOASsigned component', () => {
    console.log('Testing WOASsigned component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WOAssignedComponent],
            imports: [NgxPaginationModule, HttpModule, RouterTestingModule],
            providers: [MockResponse,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" },
                AlertComponent, DataService]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(WOAssignedComponent);
            fixture.detectChanges();
            comp = fixture.componentInstance;
            });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should validate mockResponse on View and click on workOrder of WOAssigned', inject([XHRBackend], (mockBackend: any) => {

        let dataService = TestBed.get(DataService);
        
        const mockResponse = TestBed.get(MockResponse).WOAsigmentResponse();

        mockBackend.connections.subscribe((connection:any) => {
            connection.mockRespond(new Response(new ResponseOptions({
                body: JSON.stringify(mockResponse)
            })));
        });

        comp.loadAssignedWorkOrdersUrl().subscribe(result => {
            console.log(result);
            console.log("----------------------", dataService.getArbplr());
            comp.woassignments = result.json()["data"] as WOAssignment[];
            console.log(result.json()["data"]);
            dataService.setWorkOrderQuantity(comp.woassignments.length);
        });;
        fixture.detectChanges();
        console.log(dataService.getArbplr());
        
        const dom = fixture.nativeElement.querySelector('table tr td:first-child a');
        console.log(dom);
        expect(dom.textContent).toEqual('AUFNR-TEST');
        let navigateSpy = spyOn(comp.router, "navigateByUrl")
        dom.click();
        expect(navigateSpy).toHaveBeenCalledWith('/orden-de-trabajo-tecnico');
    }));
});