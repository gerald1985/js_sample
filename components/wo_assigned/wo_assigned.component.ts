import { Component, Inject, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { MaterialsComponent } from '../shared/materials/materials.component';

@Component({
    selector: 'wo-assigned',
    templateUrl: './wo_assigned.component.html',
    providers: [AlertComponent]
})
export class WOAssignedComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public woassignments: WOAssignment[] = [];
    public baseAddress: string = "";
    public lightColor: string = "";
    public havePermission: boolean = false;

    //initializing p to one
    pageWoAssigned: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService) {

        if (Util.getPermissions(['47'], this.dataService.getMenuList()).length != 0) {
            this.havePermission = true;
        }
        else {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
        }
    }

    loadWODetail(workOrderNumber: string, lightColor: string, equipoNumber: string, receptionStatus: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(workOrderNumber);
        this.dataService.setLightColor(lightColor);
        this.dataService.setRenderDeleteColumnMat(true);
        this.dataService.setShowOperationsMaterial(true);
        this.dataService.setEquipoModel(equipoNumber);
        this.dataService.setRenderMaterialsFromAuthorizer(false);
        this.dataService.setReceptionStatus(receptionStatus);
        this.router.navigateByUrl('/orden-de-trabajo-tecnico');
    }

    ngOnInit() {
        this.loadAssignedWorkOrders();
    }

    loadAssignedWorkOrders() {

        Util.ShowLoader();
        let woassignmentsBase: WOAssignment[] = [];
        this.loadAssignedWorkOrdersUrl().subscribe(result => {
            woassignmentsBase = Util.loadJsonWithToast<WOAssignment[]>(woassignmentsBase, result, this.alertComponent);
            Array.prototype.forEach.call(woassignmentsBase, function (el: WOAssignment) {
                let assignment: WOAssignment = new WOAssignment(el);
                let percent = parseInt(assignment._WOAssignment.durac) * 0.75;
                let secondsTpoefec = parseInt(assignment._WOAssignment.tpoefec) * 60;
                let secondsDurac = parseInt(assignment._WOAssignment.durac) * 60;
                if (parseInt(assignment._WOAssignment.tpoefec) < percent || (parseInt(assignment._WOAssignment.tpoefec) == 0 && percent == 0 && parseInt(assignment._WOAssignment.durac) == 0)) {
                    assignment._WOAssignment.lightcolor = "bck_gray1";
                }
                else if (parseInt(assignment._WOAssignment.tpoefec) >= percent && parseInt(assignment._WOAssignment.tpoefec) <= parseInt(assignment._WOAssignment.durac)) {
                    assignment._WOAssignment.lightcolor = "bck_yellow";
                }
                else {
                    assignment._WOAssignment.lightcolor = "bck_red";
                }
                assignment._WOAssignment.tpoefec = Util.secondsToTime(secondsTpoefec, false);
                assignment._WOAssignment.durac = Util.secondsToTime(secondsDurac, false);
            });
            this.woassignments = woassignmentsBase;
            this.dataService.setWorkOrderQuantity(this.woassignments.length);
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadAssignedWorkOrdersUrl() {
        return this.http.get(this.baseUrl + 'api/WOAssignment/EnumAsygnedWorkOrder?ARBPL=' + this.dataService.getArbplr());
    }

    ngAfterViewChecked() {
        if (!this.elemRendered && (this.woassignments.length > 0 || this.woassignments != undefined)) {
            Util.loadMaterialElements();
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada t&eacute;cnico");
            BreadCrumbService.changeBreadCrumbChild("OT Asignadas");
            Util.HideLoader();
        }
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}