﻿import { Component, OnInit } from '@angular/core';
import { Util } from '../util/util';

@Component({
    templateUrl: './alert.component.html'
})
export class AlertComponent implements OnInit {
    public toastMessage: any;
    constructor() {

    }

    warningMessage() {
        Util.showMessage(this.toastMessage, "warning-toast c_yellow z-depth-3");
    }

    successMessage() {
        Util.showMessage(this.toastMessage, "success-toast c_green_1 z-depth-3", 800);
    }

    errorMessage() {
        Util.showMessage(this.toastMessage, "error-toast c_yellow z-depth-3");
    }

    ngOnInit() {
    }
}
