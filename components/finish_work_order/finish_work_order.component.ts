import { Component, Inject, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { FinishWorkOrder } from '../util/entities/finish_work_order';
import { Supervisor } from '../util/entities/supervisor';
import { SessionInfo } from '../util/entities/session_info';
import { SessionInfoService } from '../util/SessionInfoService';
import { WorkForce } from '../util/entities/work_force';
import { Bay } from '../util/entities/bay';
import { ClosureReturnComponent } from '../shared/closure_return/closure_return.component';

@Component({
    selector: 'finish-work-order',
    templateUrl: './finish_work_order.component.html',
    providers: [AlertComponent]
})
export class FinishWorkOrderComponent {
    public source: FinishWorkOrder[] = [];
    public finishWorkOrders: FinishWorkOrder[] = [];
    public supervisor: Supervisor[] = [];
    public statuses: FinishWorkOrder[] = [];
    public workForces: WorkForce[] = [];
    public bays: Bay[] = [];
    public filterSelectedSupervisor: string = "";
    public filterSelectedStatus: string = "-1";
    public havePermission: boolean = false;

    //initializing p to one
    pageFinishWorkOrder: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        private sessionInfoService: SessionInfoService,
        public router: Router,
        private dataService: DataService) {
    }

    finishWorkOrderUrl() {
        return this.http.get(this.baseUrl + "api/WO/EnumFinishWO?WAUSUARIO=" + this.filterSelectedSupervisor);
    }

    bayURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBays');
    }

    supervisorUrl() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }

    workForcesURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumWorkForces');
    }

    loadPendingWorks(finishWorkOrder: IFinishWorkOrder) {
        this.dataService.setWorkOrderNumber(finishWorkOrder.aufnr);
        this.dataService.setEquipoModel(finishWorkOrder.equnr);
        this.dataService.setLoadPendingWork(true);
        this.dataService.setLoadPendingWorkFromFinishWO(true);
        this.dataService.setKTEXT(finishWorkOrder.ktext);
        this.dataService.setArbpl(finishWorkOrder.arbpl);
        Util.openModal(".pending_work_modal");
    }

    loadSupervisors() {
        if (Util.getPermissions(['55'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.workForcesURL().subscribe(result => {
            if (result != undefined)
                this.workForces = Util.loadJsonWithToast<WorkForce[]>(this.workForces, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });

        this.bayURL().subscribe(result => {
            if (result != undefined)
                this.bays = Util.loadJsonWithToast<Bay[]>(this.bays, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });

        this.supervisorUrl().subscribe(resultSupervisor => {
            this.supervisor = Util.loadJsonWithToast(this.supervisor, resultSupervisor, this.alertComponent);
            var user = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
            var found = this.supervisor.find(function (elem) { return new Supervisor(elem)._Supervisor.usuario.toUpperCase() == user; });
            if (found != undefined) {
                this.filterSelectedSupervisor = new Supervisor(found)._Supervisor.usuario;
            } else {
                this.filterSelectedSupervisor = new Supervisor(this.supervisor[0])._Supervisor.usuario;
            }
            this.loadFinishWorkOrders();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    returnWO(finishWorkOrder: any) {
        let wo = new FinishWorkOrder(finishWorkOrder)._FinishWorkOrder;
        let context = this;
        ClosureReturnComponent.init(wo.equnr.concat("-").concat(wo.arbpl), "Fin OT", this.alertComponent, "WORK ORDER", "RECH: ", function () {
            context.orderReject(wo.aufnr);
        });
    }

    orderReject(order: string) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + "api/WO/OrderReject", { AUFNR: order }, {}).subscribe(result => {
            if (result.json()["responseCode"] == 200) {
                this.loadFinishWorkOrders();
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadMessenger(order: IFinishWorkOrder) {
        BreadCrumbService.setTypeReference("Orden de Trabajo");
        BreadCrumbService.setReference(order.equnr.concat("-").concat(order.aufnr));
        if (document.querySelector(".comment_modal") != undefined && document.querySelector(".comment_modal") != null) {
            Util.openModal(".comment_modal");
        }
    }

    orderPC() {
        Util.ShowLoader();
        let toPC = this.finishWorkOrders.filter(f => new FinishWorkOrder(f)._FinishWorkOrder.checked)
            .map(function (m) {
                let fWO = new FinishWorkOrder(m)._FinishWorkOrder;
                return {
                    RENTNR: fWO.rentnr,
                    AUFNR: fWO.aufnr,
                    ARBPL: fWO.arbpl,
                    BAHIA: fWO.bahia,
                    PC_STATUS: fWO.pC_STATUS
                };
            });
        if (toPC.length == 0) {
            Util.HideLoader();
            this.alertComponent.toastMessage = "Debe seleccionar al menos un elemento.";
            this.alertComponent.warningMessage();
        }
        else {
            this.http.put(this.baseUrl + "api/WO/OrderPC", toPC, {}).subscribe(result => {
                Util.HideLoader();
                if (result.json()["responseCode"] == 200) {
                    this.loadFinishWorkOrders();
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.warningMessage();
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        }
    }

    changeStatus() {
        if (this.filterSelectedStatus == "-1") {
            this.finishWorkOrders = this.source;
        } else {
            this.finishWorkOrders = this.source.filter(f => new FinishWorkOrder(f)._FinishWorkOrder.statord == this.filterSelectedStatus);
        }
    }

    changeSupervisor() {
        this.loadFinishWorkOrders();
    }

    loadFinishWorkOrders() {
        Util.ShowLoader();
        this.finishWorkOrderUrl().subscribe(result => {
            this.source = Util.loadJsonWithToast(this.source, result, this.alertComponent);
            this.finishWorkOrders = this.source;
            let statuses: any[] = [];
            Array.prototype.forEach.call(this.source.filter(f => new FinishWorkOrder(f)._FinishWorkOrder.statord.trim() != ""), function (el: any) {
                let finishWO = new FinishWorkOrder(el)._FinishWorkOrder;
                if (statuses.map(function (m) { return new FinishWorkOrder(m)._FinishWorkOrder.statord; }).indexOf(finishWO.statord) == -1) {
                    statuses.push(finishWO);
                }
            });
            this.statuses = statuses;
            Util.HideLoader();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
}