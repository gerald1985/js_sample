import { Component, Inject, OnInit, SimpleChange, ChangeDetectorRef } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http } from '@angular/http';
import { MaterialsComponent } from '../shared/materials/materials.component';
import { ExternalWorkControl } from '../util/entities/external_work_control';
import { Supervisor } from '../util/entities/supervisor';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { KeyDamageStatus } from '../util/entities/key_damage_status';
import { Provider } from '../util/entities/provider';


@Component({
    selector: 'external-work-control',
    templateUrl: './external_work_control.component.html',
    providers: [AlertComponent]
})
export class ExternalWorkControlComponent implements OnInit {
    public navigation: string = "";
    public elemRendered = false;
    public externalworkcontrols: ExternalWorkControl[] = [];
    extWCSupervisors: Supervisor[] = [];
    filterSelectedSupervisorEWC: string = "-1";
    public statusList: string[] = [];
    public filterStatus: string = "-1";
    public wAUSUARIO: string = "";
    public refreshSelect: boolean = false;
    public keyDamageStatus: KeyDamageStatus[] = [];
    public providers: Provider[] = [];
    public havePermission: boolean = false;

    //initializing p to one
    pageExternalWorkControl: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService,
        private sessionInfoService: SessionInfoService) {
    }

    ngOnInit() {
        //this.loadSupervisor();
    }

    filterSupURl() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }

    loadSupervisor() {
        if (Util.getPermissions(['53'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.filterSupURl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.extWCSupervisors = Util.loadJsonWithToast<Supervisor[]>(this.extWCSupervisors, result, this.alertComponent);
                var user = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
                var found = this.extWCSupervisors.find(function(elem) { return new Supervisor(elem)._Supervisor.usuario.toUpperCase() == user; });

                if (found != undefined) {
                    this.filterSelectedSupervisorEWC = new Supervisor(found)._Supervisor.usuario;
                }
                else {
                    this.filterSelectedSupervisorEWC = new Supervisor(this.extWCSupervisors[0])._Supervisor.usuario;
                }

                this.wAUSUARIO = this.filterSelectedSupervisorEWC;

                //Load Table On Init after load the Supervisor Filter
                this.loadExternalWC();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeSupervisor() {
        this.wAUSUARIO = this.filterSelectedSupervisorEWC;
        this.loadExternalWC();
    }

    loadExternalWCURL() {
        return this.http.get(this.baseUrl + 'api/externalworkcontrol/GetExternalWorkControl?WAUSUARIO=' + this.wAUSUARIO);
    }

    loadExternalWC() {
        let externalworkcontrolsBase: ExternalWorkControl[] = [];
        this.loadExternalWCURL().subscribe(result => {
            if (result != undefined) {
                externalworkcontrolsBase = Util.loadJsonWithToast<ExternalWorkControl[]>(this.externalworkcontrols, result, this.alertComponent);

                if (this.filterStatus != '-1') {
                    externalworkcontrolsBase = externalworkcontrolsBase.filter(f => new ExternalWorkControl(f)._ExternalWorkControl.motivo === this.filterStatus);
                }

                Array.prototype.forEach.call(externalworkcontrolsBase, function(el: ExternalWorkControl) {
                    let assignment: ExternalWorkControl = new ExternalWorkControl(el);
                    switch (assignment._ExternalWorkControl.motivo) {
                        case "PROC": {
                            assignment._ExternalWorkControl.lightcolor = "bck_red";
                            break;
                        }
                        case "FCMO": {
                            assignment._ExternalWorkControl.lightcolor = "bck_yellow";
                            break;
                        }
                        case "PSER": {
                            assignment._ExternalWorkControl.lightcolor = "bck_gray1";
                            break;
                        }
                        case "": {
                            assignment._ExternalWorkControl.fenot = "";
                            assignment._ExternalWorkControl.hrnot = "";
                            assignment._ExternalWorkControl.lightcolor = "bck_green_3";
                            break;
                        }//Another status?
                    }
                });

                this.externalworkcontrols = externalworkcontrolsBase;
                this.loadStatusListEWC();
                this.loadProviders();
                this.refreshSelect = true;
            }
        });
    }

    loadStatusListEWC() {
        this.statusList = this.externalworkcontrols.map(function(e) {
            return new ExternalWorkControl(e)._ExternalWorkControl.motivo;
        }).filter(function(f) { return f.trim().length > 0; });

        this.statusList = Array.from(new Set(this.statusList.map((item: any) => item)));
    }

    changeFilterStatus() {
        this.loadExternalWC();
    }

    loadProviders() {
        this.providerUrlEWC().subscribe(resultProvider => {
            this.providers = Util.loadJsonWithToast<Provider[]>(this.providers, resultProvider, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    providerUrlEWC() {
        return this.http.get(this.baseUrl + "api/Catalog/EnumProvider");
    }

    providerChange(externalworkcontrol: any) {

        Util.ShowLoader();

        let externalWC = externalworkcontrol as IExternalWorkControl;

        let note = this.providers.find(function(el) {
            let prov = new Provider(el)._Provider;
            return prov.lifnr === externalworkcontrol.lifnr;
        });
        let toSave = {
            ORDENID: externalWC.aufnr, //this.dataService.getWorkOrderNumber(),
            OPERID: externalWC.vornr,
            PROVEE: externalWC.lifnr,
            NOTA: (externalWC.lifnr.trim() == "" || note == undefined) ? "" : new Provider(note)._Provider.namE1,
            IND_SERV: "1"
        };
        this.http.put(this.baseUrl + 'api/WO/UpdateOperationProvider', toSave, {}).subscribe(result => {
            Util.HideLoader();
            if (result.json()["responseCode"] == 200) {
                this.loadExternalWC();
            }
            else {
                this.alertComponent.toastMessage = result.json()["responseText"];
                this.alertComponent.warningMessage();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    getProviderDescription(providerCode: string) {
        let found = this.providers.filter(function(f) { return new Provider(f)._Provider.lifnr == providerCode; })[0];
        if (found != null) {
            return new Provider(found)._Provider.namE1;
        }
        return "";
    }

    notify(noti: any) {
        Util.ShowLoader();
        //let workOrderHeader = new WorkOrderHeader(this.workOrderHeader[0])._WorkOrderHeader;
        let operation = new ExternalWorkControl(noti)._ExternalWorkControl;
        this.dataService.setWorkOrderNumber(operation.aufnr);
        this.dataService.setEQUNR(operation.equnr);
        this.dataService.setProvider(operation.lifnr);

        this.dataService.setProviderDescription(this.getProviderDescription(operation.lifnr));
        this.dataService.setRenderNotificationStart(true);
        this.dataService.setModelKey(operation.ktsch);
        this.dataService.setVORNR(operation.vornr);
        this.dataService.setCMDescription(operation.ltxA1);
        this.dataService.setReasonTime("00:00");
        this.dataService.setEffectiveTime(parseInt(operation.fenot));
        this.dataService.setStandardTime(parseInt(operation.fenot));
        this.dataService.setLoadDataToBarChart(true);
        this.dataService.setTimeFromExtenarlWorkControl(true);
        this.dataService.setExtenarlWorkControl(operation);
        this.dataService.setLoadExternalWorkControl(true);

        BreadCrumbService.setTypeReference("Notificaci&oacute;n");
        BreadCrumbService.setReference(operation.vornr);
        this.loadLastNotification(operation.aufnr, operation.vornr).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {

                this.keyDamageStatus = Util.loadJsonWithToast<KeyDamageStatus[]>(this.keyDamageStatus, result, this.alertComponent);
                if (this.keyDamageStatus.length > 0) {
                    let kdamage = new KeyDamageStatus(this.keyDamageStatus[0])._KeyDamageStatus;
                    this.dataService.setRenderNotificationStart(kdamage.motivo != 'PROC');

                    var lastNotification = new Date(kdamage.fenot + " " + kdamage.hrnot);
                    var nowDate = new Date();
                    var diffBetwLastNow = Util.diff_minutes(nowDate, lastNotification) + parseInt(kdamage.tiempo);
                    this.dataService.setReasonTime((Util.timeToMinutes(operation.fenot) - diffBetwLastNow).toString());
                }
            }
            Util.openModal('.notification_modal');
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    public loadLastNotification(AUFNR: string, VORNR: string) {
        return this.http.get(this.baseUrl + "api/WO/GetLastNotification?AUFNR=" + AUFNR + '&VORNR=' + VORNR);
    }

    toRequirement(aufnr: any, nrequisa: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(aufnr);
        this.dataService.setNREQUISA(nrequisa);
        this.dataService.setRenderDeleteColumnMat(false);
        this.dataService.setShowOperationsMaterial(false)
        this.router.navigateByUrl('/orden-de-trabajo-supervisor');
    }

    ngAfterViewChecked() {
        if (this.dataService.getReloadExternalWC()) {
            Util.ShowLoader();
            this.dataService.setReloadExternalWC(false);
            this.loadExternalWC();
            Util.HideLoader();
        }

        if (this.refreshSelect) {
            Util.loadMaterialBySelector("select.filter_sup", {}, "FormSelect");
            this.refreshSelect = false;
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}