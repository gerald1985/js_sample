import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { SessionInfo } from '../util/entities/session_info';
import { SessionInfoService } from '../util/SessionInfoService';
import { PurchaseRequisitionActive } from '../util/entities/purchase_requisition_active';
import { ActivityClassRol } from '../util/entities/activity_class_rol';

@Component({
    selector: 'assign-reception-ot',
    templateUrl: './assign_reception_ot.component.html',
    providers: [AlertComponent]
})
export class AssignReceptionOTComponent implements OnInit {
    public purchaseRequisitionActivesWithoutFilter: PurchaseRequisitionActive[] = [];
    public purchaseRequisitionActives: PurchaseRequisitionActive[] = [];
    public activityClassRols: ActivityClassRol[] = [];
    public purchaseRequisitionActivePosts: PurchaseRequisitionActive[] = [];
    public navigation: string = "";
    public elemRendered = false;
    public rolUser: string = "";
    public classAPMList: string[] = [];
    public filterClassAPM: string = "-1";
    public filterWhenLoad: boolean = false;
    public refreshSelect: boolean = false;
    public havePermission: boolean = false;

    //initializing p to one
    pageAssignReceptionOT: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService,
        private sessionInfoService: SessionInfoService) {
    }

    ngOnInit() {
        //this.loadPurchaseRequisitionAuthorizations();
    }

    purchaseRequisitionActiveUrl() {
        return this.http.get(this.baseUrl + "api/assignreceptionot/GetPurchaseRequisitionActive");
    }

    activityClassRolUrl() {
        return this.http.get(this.baseUrl + "api/assignreceptionot/GetActivityClassRol?rol=" + this.rolUser);
    }

    loadPurchaseRequisitionAuthorizations() {
        if (Util.getPermissions(['59', '72', '82'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        //To Load the activity class rol before charge the grid
        this.rolUser = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
        this.loadActivityClassRol();
    }

    loadList() {
        Util.ShowLoader();
        this.purchaseRequisitionActiveUrl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.purchaseRequisitionActivesWithoutFilter = Util.loadJsonWithToast<PurchaseRequisitionActive[]>(this.purchaseRequisitionActivesWithoutFilter, result, this.alertComponent);

                this.loadPurchaseRequisitionAuthorizationsList();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadActivityClassRol() {
        Util.ShowLoader();
        this.activityClassRolUrl().subscribe(result => {
            Util.HideLoader();
            this.refreshSelect = true;
            if (result != undefined) {
                this.activityClassRols = Util.loadJsonWithToast<ActivityClassRol[]>(this.activityClassRols, result, this.alertComponent);
                if (this.activityClassRols.length > 0) {
                    this.filterClassAPM = new ActivityClassRol(this.activityClassRols[0])._IActivityClassRol.claseactividadpm;
                }
            }
            this.loadList();
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadPurchaseRequisitionAuthorizationsList() {

        if (this.filterClassAPM != '-1' && this.filterClassAPM != "OTROS") {
            this.purchaseRequisitionActives = this.purchaseRequisitionActivesWithoutFilter.filter(f => new PurchaseRequisitionActive(f)._IPurchaseRequisitionActive.ilart.toUpperCase() === this.filterClassAPM);
            this.filterWhenLoad = true;
        } else if (this.filterClassAPM == "OTROS") {
            this.purchaseRequisitionActives = this.purchaseRequisitionActivesWithoutFilter.filter(function (f) {
                let purchaseActive = new PurchaseRequisitionActive(f)._IPurchaseRequisitionActive;
                return purchaseActive.ilart != "6" && purchaseActive.ilart != "43";
            });
            this.filterWhenLoad = true;
        } else if (this.filterClassAPM == "-1") {
            this.purchaseRequisitionActives = this.purchaseRequisitionActivesWithoutFilter;
        }
    }

    changeFilterClassAPM() {
        this.loadPurchaseRequisitionAuthorizationsList();
    }

    enlazarClick(purchase: any, e: any) {
    }

    enlazarChange(purchase: any) {
    }

    enlazar() {
        //Select records with the checked "enlazar" == true
        this.purchaseRequisitionActivePosts = this.purchaseRequisitionActives.filter(f => new PurchaseRequisitionActive(f)._IPurchaseRequisitionActive.enlazar === true);

        this.http.post(this.baseUrl + 'api/assignreceptionot/PostPurchaseRequisitionActive', this.purchaseRequisitionActivePosts, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.filterClassAPM = '-1';
                        this.loadPurchaseRequisitionAuthorizations();
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    ngAfterViewChecked() {
        //if (!this.elemRendered && (this.purchaseRequisitionActives.length > 0 || this.purchaseRequisitionActives != undefined)) {
        //    this.elemRendered = true;
        //    BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada programador");
        //    BreadCrumbService.changeBreadCrumbChild("Asignar Recepci&oacute;n OT");
        //    Util.HideLoader();
        //}

        if (this.refreshSelect) {
            Util.loadMaterialBySelector("select.filter_sup", {}, "FormSelect");
            this.refreshSelect = false;
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}