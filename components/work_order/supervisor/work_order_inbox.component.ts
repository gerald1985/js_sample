import { Component, Input, Inject, ChangeDetectorRef } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Supervisor } from '../../util/entities/supervisor';

@Component({
    selector: 'work-order-inbox-supervisor',
    templateUrl: './work_order_inbox.component.html',
    providers: [AlertComponent]
})
export class WorkOrderInboxSupComponent {
    public navigation: string = "";
    public workOrderNumber: string = "";
    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef) {
        
    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
    }
    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}
