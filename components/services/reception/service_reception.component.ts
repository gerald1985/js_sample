import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Util } from "../../util/util";
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { RequirementSup } from '../../util/entities/requirement_sup';
import { DataService } from '../../util/data_service';
import { Supervisor } from '../../util/entities/supervisor';
import { SessionInfo } from '../../util/entities/session_info';
import { SessionInfoService } from '../../util/SessionInfoService';
import { ServiceRequest } from '../../util/entities/service_request';

@Component({
    selector: 'service-reception',
    templateUrl: './service_reception.component.html',
    providers: [AlertComponent]
})

export class ServiceReceptionComponent implements OnInit {

    public authUrl: string;
    public serviceRequest: ServiceRequest = new ServiceRequest();
    public buttonEnabled: boolean = true;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService,
        public router: Router, ) {
        this.authUrl = authUrl;
    }

    ngOnInit() {
    }

    stringToDate(fechaRetorno: string) {
        var dateString = fechaRetorno;
        var dateParts: any = dateString.split("/");
        return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]);
    }

    GetEstimatedTimeUrl() {
        return this.http.get(this.baseUrl + `api/Requirement/GetEstimatedTime?ReturnDate=${this.serviceRequest.fechaRetorno}&ReturnTime=${this.serviceRequest.horaRetorno}&DateOfDelivery=${this.serviceRequest.fechaEntrega}&DeliveryTime=${this.serviceRequest.horaEntrega}`);
    }

    loadService() {
        //Llamado a la api interna
        Util.ShowLoader();
        this.http.get(this.baseUrl + 'api/Requirement/GetServiceRequest?noOrden=' + this.serviceRequest.noOrden + '&provider=' + this.dataService.getProvider())
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    this.serviceRequest = Util.loadJsonWithToast<ServiceRequest>(this.serviceRequest, result, this.alertComponent);
                    this.serviceRequest.fecha = Util.formatDate(new Date());
                    this.serviceRequest.noEquipo = this.dataService.getEQUNR();
                    this.serviceRequest.hora = Util.formatTime(Util.getCurrentDateTime());
                    this.serviceRequest.fechaRetorno = Util.formatDateISO(new Date());
                    this.serviceRequest.horaRetorno = Util.formatTime(Util.getCurrentDateTime());
                    this.serviceRequest.fechaEntrega = Util.formatDateISO(this.stringToDate(this.serviceRequest.fechaEntrega));
                    this.calculateHours()
                }
            }, error => {
                Util.HideLoader();
                this.alertComponent.toastMessage = "Ocurrio un error inesperado con una consulta.";
                this.alertComponent.successMessage();
            });
    }

    enableButton() {
        if (this.serviceRequest.fechaRetorno == "" || this.serviceRequest.horaRetorno == "") {
            this.buttonEnabled = false;
        } else {
            this.buttonEnabled = true;
            this.calculateHours();
        }

    }

    putCall() {
        Util.ShowLoader();
        let notify = {
            AUFNR: this.dataService.getWorkOrderNumber(),
            VORNR: this.dataService.getVORNR(),
            MOTIVO: "FCMO"
        };
        this.http.put(this.baseUrl + 'api/WO/OrderNotificationEnd', notify, {}).subscribe(resultNotify => {
            Util.HideLoader();
            if (resultNotify != undefined) {

                if (resultNotify.json()["responseCode"] == 200) {
                    Util.ShowLoader();
                    this.serviceRequest.vornr = this.dataService.getVORNR();
                    this.http.put(this.baseUrl + 'api/Requirement/UpdateServiceRequest', this.serviceRequest, {})
                        .subscribe(result => {
                            Util.HideLoader();
                            if (result != undefined) {
                                if (result.json()["responseCode"] == 200) {

                                    this.showPrintModal();
                                    this.dataService.setReloadExternalWC(true);
                                    Util.closeModal(".notification_modal");
                                    this.alertComponent.toastMessage = "Operacion Exitosa";
                                    this.alertComponent.successMessage();
                                } else if (result.json()["responseCode"] == 409) {
                                    Util.openModal(".model_key");
                                }
                                else {
                                    this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";
                                    this.alertComponent.errorMessage();
                                }
                            }
                        }, error => {
                            Util.HideLoader();
                            Util.EvalError(error, this.alertComponent, this.authUrl);
                        });

                }
                else {
                    this.alertComponent.toastMessage = resultNotify.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    showPrintModal() {

        //Show print Modal
        this.dataService.setHeaderServicePrintTemplate('Recepcion de Servicio');
        this.dataService.setServicePrintTemplateData(this.serviceRequest);
        this.dataService.setPrintIsRequestService(false);
        this.dataService.setLoadServicePrintTemplate(true);
        this.cancel();
        Util.openModal('.service_print_template_modal');
    }

    saveService() {
        Util.ShowLoader();
        var format: string[] = this.serviceRequest.horaRetorno.split(":");
        if (format.length < 3) {
            this.serviceRequest.horaRetorno += ":00";
        }
        this.putCall();

    }

    calculateHours() {
        this.GetEstimatedTimeUrl().subscribe(result => {
            if (result.json()["responseCode"] == 200) {
                this.serviceRequest.tiempoEstimado = result.json()["hours"];
                this.serviceRequest.formattedHours = result.json()["formattedHours"];
            }
        })
        //var endDate: any = new Date(this.serviceRequest.fechaRetorno + " " + this.serviceRequest.horaRetorno);
        //var startDate: any = new Date(this.serviceRequest.fechaEntrega + " " + this.serviceRequest.horaEntrega);
        //var diff: any = endDate - startDate;

        //var value = diff / 3600000;
        //this.serviceRequest.tiempoEstimado = value.toFixed(2).toString();
    }

    cancel() {
        Util.closeModal('.service_reception_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadServicesFromExternalWC()) {
            this.dataService.setLoadServicesFromExternalWC(false);
        }

        if (this.dataService.getLoadExternalWorkControlReception()) {
            this.dataService.setLoadExternalWorkControlReception(false);
            if (Util.getPermissions(['120'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                let externalWorkControl = this.dataService.getExtenarlWorkControl();
                this.serviceRequest.noOrden = externalWorkControl.aufnr;
                this.loadService();
            }
        }
        this.cdRef.detectChanges();
    }

}


