import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Util } from "../../util/util";
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { RequirementSup } from '../../util/entities/requirement_sup';
import { DataService } from '../../util/data_service';
import { SessionInfo } from '../../util/entities/session_info';
import { SessionInfoService } from '../../util/SessionInfoService';
import { ServiceRequest } from '../../util/entities/service_request';
import * as jsPDF from 'jspdf';

@Component({
    selector: 'service-print-template',
    templateUrl: './service_print_template.component.html',
    providers: [AlertComponent]
})

export class ServicePrintTemplateComponent implements OnInit {
    public authUrl: string;
    public serviceRequest: ServiceRequest = new ServiceRequest();
    public encabezadoServicio: string = '';
    public setVisiblePrintBtn: boolean = true;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService,
        public router: Router, ) {
        this.authUrl = authUrl;
    }

    GetEstimatedTimeUrl() {
        return this.http.get(this.baseUrl + `api/Requirement/GetEstimatedTime?ReturnDate=${this.serviceRequest.fechaRetorno}&ReturnTime=${this.serviceRequest.horaRetorno}&DateOfDelivery=${this.serviceRequest.fechaEntrega}&DeliveryTime=${this.serviceRequest.horaEntrega}`);
    }

    ngOnInit() {
        Util.loadMaterialElements();
    }

    calculateHours() {
        this.GetEstimatedTimeUrl().subscribe(result => {
            if (result.json()["responseCode"] == 200) {
                this.serviceRequest.tiempoEstimado = result.json()["hours"];
                this.serviceRequest.formattedHours = result.json()["formattedHours"];
            }
        })
    }

    printService() {
        //Hide Btn Print
        this.setVisiblePrintBtn = false;
        if (this.dataService.getPrintIsRequestService()) {
            if (Util.getPermissions(['119'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
            } else {
                let printUrl = this.baseUrl + "api/Requirement/PrintServiceRequest?Order=" + this.dataService.getWorkOrderNumber();
                printUrl += "&Equipment=" + this.dataService.getEQUNR();
                printUrl += "&CodeProvider=" + this.dataService.getProvider();
                window.open(printUrl, "_blank");
            }
        } else {
            if (Util.getPermissions(['121'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
            } else {
                let printUrl = this.baseUrl + "api/Requirement/PrintServiceReception?Order=" + this.dataService.getWorkOrderNumber();
                printUrl += "&Equipment=" + this.dataService.getEQUNR();
                printUrl += "&CodeProvider=" + this.dataService.getProvider();
                window.open(printUrl, "_blank");
            }
        }


        ////Print HTML to PDF
        //var doc = new jsPDF('p', 'pt', 'a4');
        //var htmlModal = document.getElementById('service_print_template');
        //console.log(htmlModal);
        //let noOrden = this.serviceRequest.noOrden;
        //doc.addHTML(htmlModal, 10, 15, null, function () {
        //    doc.save('Servicio_NoOrden' + noOrden + '.pdf')
        //});

        //Show Btn Print
        let serviceThis = this;
        setTimeout(function () {
            serviceThis.setVisiblePrintBtn = true;
        }, 1000 / 60);
    }

    cancel() {
        Util.closeModal('.service_print_template_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadServicePrintTemplate()) {
            this.dataService.setLoadServicePrintTemplate(false);
            let serviceData = this.dataService.getServicePrintTemplateData();
            this.encabezadoServicio = this.dataService.getHeaderServicePrintTemplate();
            this.serviceRequest.fecha = serviceData.fecha;
            this.serviceRequest.hora = serviceData.hora;
            this.serviceRequest.fechaEntrega = serviceData.fechaEntrega;
            this.serviceRequest.fechaRetorno = serviceData.fechaRetorno;
            this.serviceRequest.horaEntrega = serviceData.horaEntrega;
            this.serviceRequest.horaRetorno = serviceData.horaRetorno;
            this.serviceRequest.noEquipo = serviceData.noEquipo;
            this.serviceRequest.noOrden = serviceData.noOrden;
            this.calculateHours();
            //this.serviceRequest.tiempoEstimado = serviceData.tiempoEstimado;
            //this.serviceRequest.formattedHours = serviceData.formattedHours;
            this.serviceRequest.pieza = serviceData.pieza;
            this.serviceRequest.tipoReparacion = serviceData.tipoReparacion;
            this.serviceRequest.proveedor = serviceData.proveedor;
        }
        this.cdRef.detectChanges();
    }
}