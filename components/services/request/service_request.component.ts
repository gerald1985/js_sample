import { Component, Inject, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';
import { Util } from "../../util/util";
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { RequirementSup } from '../../util/entities/requirement_sup';
import { DataService } from '../../util/data_service';
import { Supervisor } from '../../util/entities/supervisor';
import { SessionInfo } from '../../util/entities/session_info';
import { SessionInfoService } from '../../util/SessionInfoService';
import { ServiceRequest } from '../../util/entities/service_request';

@Component({
    selector: 'service-request',
    templateUrl: './service_request.component.html',
    providers: [AlertComponent]
})

export class ServiceRequestComponent implements OnInit {
    public authUrl: string;
    public serviceRequest: ServiceRequest = new ServiceRequest();
    public serviceGetRequest: ServiceRequest = new ServiceRequest();
    public buttonEnabled: boolean = false;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService,
        public router: Router, ) {
        this.authUrl = authUrl;
    }

    ngOnInit() {
        this.loadDates();
    }

    loadDates() {
        this.serviceRequest.fecha = Util.formatDate(new Date());
        this.serviceRequest.hora = Util.formatTime(Util.getCurrentDateTime());
        this.serviceRequest.fechaEntrega = Util.formatDateISO(new Date());
        this.serviceRequest.fechaRetorno = Util.formatDateISO(new Date());
        this.serviceRequest.horaEntrega = Util.formatTime(Util.getCurrentDateTime());
        this.serviceRequest.horaRetorno = Util.formatTime(Util.getCurrentDateTime());
        Util.HideLoader();
    }

    changeEstimatedTime() {
        if (this.serviceRequest.tiempoEstimado != null && this.serviceRequest.tiempoEstimado != "") {
            this.serviceRequest.fechaRetorno = Util.formatDateISO(Util.dateAddDays(new Date(), parseInt(this.serviceRequest.tiempoEstimado)));

        } else if ((parseInt(this.serviceRequest.tiempoEstimado) | -1) == -1) {
            this.serviceRequest.fechaRetorno = Util.formatDateISO(new Date());
        }
    }

    enableButton() {
        if (this.serviceRequest.pieza == "" ||
            this.serviceRequest.tipoReparacion == "" ||
            this.serviceRequest.tiempoEstimado == null || this.serviceRequest.tiempoEstimado === "" ||
            this.serviceRequest.fechaEntrega == "" ||
            this.serviceRequest.fechaRetorno == "") {
            this.buttonEnabled = false;
        } else {
            this.buttonEnabled = true;
        }
    }

    postCall() {
        this.serviceRequest.vornr = this.dataService.getVORNR();
        this.http.post(this.baseUrl + 'api/Requirement/CreateServiceRequest', this.serviceRequest, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {

                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                    } else if (result.json()["responseCode"] == 409) {
                        Util.openModal(".model_key");
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    putCall() {
        this.serviceRequest.vornr = this.dataService.getVORNR();
        this.http.put(this.baseUrl + 'api/Requirement/UpdateServiceRequest', this.serviceRequest, {})
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                    } else if (result.json()["responseCode"] == 409) {
                        Util.openModal(".model_key");
                    }
                    else {
                        this.alertComponent.toastMessage = result.json()["responseText"];
                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
    }

    saveService() {
        var formatRetorno: string[] = this.serviceRequest.horaRetorno.split(":");
        if (formatRetorno.length < 3) {
            this.serviceRequest.horaRetorno += ":00";
        }
        var formatEntrega: string[] = this.serviceRequest.horaRetorno.split(":");
        if (formatEntrega.length < 3) {
            this.serviceRequest.horaRetorno += ":00";
        }
        this.http.get(this.baseUrl + 'api/Requirement/GetServiceRequest?noOrden=' + this.serviceRequest.noOrden + '&provider=' + this.dataService.getProvider())
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    this.serviceGetRequest = Util.loadJsonWithToast<ServiceRequest>(this.serviceRequest, result, this.alertComponent);
                    if (this.serviceGetRequest.noEquipo == null) {
                        this.postCall();
                    } else {
                        this.putCall();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });
        Util.ShowLoader();

        //Changes Order Status
        this.notificationStart();

        //Show print Modal
        this.dataService.setHeaderServicePrintTemplate('Solicitud de Servicio');
        this.dataService.setPrintIsRequestService(true);
        this.dataService.setServicePrintTemplateData(this.serviceRequest);
        this.dataService.setLoadServicePrintTemplate(true);
        this.cancel();
        Util.openModal('.service_print_template_modal');
    }

    notificationStart() {
        Util.ShowLoader();
        let order = { AUFNR: this.serviceRequest.noOrden, RENTNR: this.dataService.getRentnr(), VORNR: this.dataService.getVORNR() };

        this.http.put(this.baseUrl + 'api/WO/OrderNotification', order, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                if (result.json()["responseCode"] == 200) {
                    this.dataService.setRenderNotificationStart(false);
                    this.dataService.setReloadExternalWC(true);
                    this.alertComponent.toastMessage = "Operacion exitosa";
                    this.alertComponent.successMessage();
                } else {
                    var errors = result.json()["responseText"].split(',');
                    errors.forEach((err: string) => { Util.showMessage(err, "warning-toast c_yellow z-depth-3"); });
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    getService() {
        return this.http.get(this.baseUrl + 'api/Requirement/GetService?noServicio=123456');
    }

    loadService() {

        //Llamado a la api interna
        Util.ShowLoader();
        this.getService().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.serviceRequest = Util.loadJsonWithToast<ServiceRequest>(this.serviceRequest, result, this.alertComponent);
            }
        }, error => {
            Util.HideLoader();
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    cancel() {
        Util.closeModal('.service_request_modal');
    }

    ngAfterViewChecked() {
        if (this.dataService.getLoadServicesFromExternalWC()) {
            this.dataService.setLoadServicesFromExternalWC(false);
        }

        if (this.dataService.getLoadExternalWorkControl()) {
            this.dataService.setLoadExternalWorkControl(false);
            if (Util.getPermissions(['118'], this.dataService.getMenuList()).length == 0) {
                this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
                this.alertComponent.warningMessage();
                this.cancel();
            } else {
                let externalWorkControl = this.dataService.getExtenarlWorkControl();
                this.serviceRequest.noEquipo = externalWorkControl.equnr;
                this.serviceRequest.noOrden = externalWorkControl.aufnr;
                this.serviceRequest.proveedor = this.dataService.getProviderDescription();
                this.serviceRequest.pieza = "";
                this.serviceRequest.tiempoEstimado = "";
                this.serviceRequest.tipoReparacion = "";
                this.loadDates();
            }
        }
        this.cdRef.detectChanges();
    }
}