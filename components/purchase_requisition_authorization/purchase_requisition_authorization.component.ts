import { Component, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { BreadCrumbService } from '../util/breadcrumbService';
import { WOAssignment } from '../util/entities/wo_assignment';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
import { Http, RequestOptions, URLSearchParams } from '@angular/http';
import { PurchaseRequisitionAuthorization } from '../util/entities/purchase_requisition_authorization';
import { SessionInfo } from '../util/entities/session_info';
import { SessionInfoService } from '../util/SessionInfoService';
import { ClosureReturnComponent } from '../shared/closure_return/closure_return.component';

@Component({
    selector: 'purchase-requisition-authorization',
    templateUrl: './purchase_requisition_authorization.component.html',
    providers: [AlertComponent]
})
export class PurchaseRequisitionAuthorizationComponent implements OnInit {
    public purchaseRequisitionAuthorizations: PurchaseRequisitionAuthorization[] = [];
    public purchaseRequisitionAuthorizationsToSave: PurchaseRequisitionAuthorization[] = [];
    public navigation: string = "";
    public elemRendered = false;
    public filterStatus: boolean = false;
    public havePermission: boolean = false;

    //initializing p to one
    pagePurchase: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string,
        @Inject('AUTH_URL') private authUrl: string,
        private alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef,
        public router: Router,
        private dataService: DataService,
        private sessionInfoService: SessionInfoService) {
        if (Util.getPermissions(['71'], this.dataService.getMenuList()).length != 0) {
            this.havePermission = true;
        }
        else {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
        }
    }

    ngOnInit() {
        this.loadPurchaseRequisitionAuthorizations();
    }

    purchaseRequisitionAuthorizationUrl() {
        if (this.filterStatus) {
            return this.http.get(this.baseUrl + "api/purchaserequisitionauthorization/GetPurchaseRequisitionAuth?TPORDEN=1");
        }
        return this.http.get(this.baseUrl + "api/purchaserequisitionauthorization/GetPurchaseRequisitionAuth");
    }

    loadPurchaseRequisitionAuthorizations() {
        Util.ShowLoader();
        let purchaseRequisitionAuthorizationsBase: PurchaseRequisitionAuthorization[] = [];

        this.purchaseRequisitionAuthorizationUrl().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                purchaseRequisitionAuthorizationsBase = Util.loadJsonWithToast(purchaseRequisitionAuthorizationsBase, result, this.alertComponent);

                Array.prototype.forEach.call(purchaseRequisitionAuthorizationsBase, function (el: PurchaseRequisitionAuthorization) {
                    let item: PurchaseRequisitionAuthorization = new PurchaseRequisitionAuthorization(el);
                });

                this.purchaseRequisitionAuthorizations = purchaseRequisitionAuthorizationsBase;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    loadWODetail(workOrderNumber: string, lightColor: string, equipoNumber: string) {
        Util.ShowLoader();
        this.dataService.setWorkOrderNumber(workOrderNumber);
        this.dataService.setLightColor(lightColor);
        this.dataService.setRenderDeleteColumnMat(true);
        this.dataService.setShowOperationsMaterial(true);
        this.dataService.setEquipoModel(equipoNumber);
        this.router.navigateByUrl('/orden-de-trabajo-tecnico');
    }

    processClick(purchase: any, e: any) {
    }

    processChange(purchase: any) {
    }

    process() {
        Util.ShowLoader();

        let params: URLSearchParams = new URLSearchParams();
        params.set('usuario', new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName);

        let requestOptions = new RequestOptions();
        requestOptions.search = params;

        this.http.put(this.baseUrl + 'api/purchaserequisitionauthorization/PutPurchaseRequisitionAuthorization', this.purchaseRequisitionAuthorizations, requestOptions)
            .subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    if (result.json()["responseCode"] == 200) {
                        this.alertComponent.toastMessage = "Operacion Exitosa";
                        this.alertComponent.successMessage();
                        this.loadPurchaseRequisitionAuthorizations();
                    }
                    else {
                        if (result.json()["responseText"] != null || result.json()["responseText"] != '')
                            this.alertComponent.toastMessage = result.json()["responseText"]
                        else
                            this.alertComponent.toastMessage = "Ocurrio un error inesperado al guardar el la solicitud.";

                        this.alertComponent.errorMessage();
                    }
                }
            }, error => {
                Util.HideLoader();
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });

    }

    returnPurchase(purchase: any) {
        let context = this;
        let reference = purchase.aufnr.concat("-REQ-" + purchase.nrequisa);
        ClosureReturnComponent.init(reference, "Procesar Requisa", context.alertComponent, "Procesar Requisa", purchase.nrequisa, function () { context.setStatusOrder(purchase); });
    }

    setStatusOrder(purchase: any) {
        Util.ShowLoader();
        this.http.put(this.baseUrl + 'api/purchaserequisitionauthorization/PutRequirementToCrea', purchase, {}).subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                Util.closeModal('.closure_reason_modal');
                if (result.json()["responseCode"] == 200) {
                    this.alertComponent.toastMessage = "Operacion Exitosa";
                    this.alertComponent.successMessage();
                    this.loadPurchaseRequisitionAuthorizations();
                }
                else {
                    this.alertComponent.toastMessage = result.json()["responseText"];
                    this.alertComponent.errorMessage();
                }
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    ngAfterViewChecked() {
        if (!this.elemRendered && (this.purchaseRequisitionAuthorizations.length > 0 || this.purchaseRequisitionAuthorizations != undefined)) {
            this.elemRendered = true;
            BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada programador");
            BreadCrumbService.changeBreadCrumbChild("Procesar Requisa");
            Util.HideLoader();
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}