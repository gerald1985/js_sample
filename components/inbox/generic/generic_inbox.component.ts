import { Component, Input, Inject, ChangeDetectorRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Supervisor } from '../../util/entities/supervisor';
import { SessionInfoService } from '../../util/SessionInfoService';
import { DataService } from '../../util/data_service';
import { WOAssignmentComponent } from '../../wo_assignment/wo_assignment.component';
import { ApproveRequirementSupComponent } from '../../requirement/supervisor/approve_requirement/approve_requirement.component';
import { ExternalWorkControlComponent } from '../../external_work_control/external_work_control.component';
import { MaterialReturnControlComponent } from '../../material_return_control/material_return_control.component';
import { FinishWorkOrderComponent } from '../../finish_work_order/finish_work_order.component';
import { WOConcludedComponent } from '../../shared/wo_concluded/wo_concluded.component';
import { EquipmentSummaryComponent } from '../../equipment_summary/equipment_summary.component';
import { ProcessSummaryComponent } from '../../process_summary/process_summary.component';
import { QualityTestControlComponent } from '../../quality_test_control/verifier/quality_test_control.component';

@Component({
    selector: 'generic-inbox',
    templateUrl: './generic_inbox.component.html',
    styleUrls: [],
    providers: [AlertComponent, SessionInfoService, DataService]
})
export class GenericInboxComponent implements OnInit, AfterViewInit {
    @ViewChild(WOAssignmentComponent) wo_assignmentChild: WOAssignmentComponent;
    @ViewChild(WOConcludedComponent) wo_concludedChild: WOConcludedComponent;
    @ViewChild(EquipmentSummaryComponent) equipment_summaryChild: EquipmentSummaryComponent;
    @ViewChild(ProcessSummaryComponent) process_summaryChild: ProcessSummaryComponent;
    @ViewChild(QualityTestControlComponent) quality_test_controlChild: QualityTestControlComponent;

    public navigation: string = "";
    public isRendered: boolean = false;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
        switch (breadcrumb) {
            case "Asignar recepcion OT":
                this.wo_assignmentChild.loadWoAssignment();
                break;
            case "OT Concluida":
                this.wo_concludedChild.loadWOConcluded();
                break;
            case "Resumen por equipo":
                this.equipment_summaryChild.loadEquipmentSummary();
                break;
            case "Resumen por proceso":
                this.process_summaryChild.loadProcessSummary();
                break;
            case "Prueba de calidad":
                this.quality_test_controlChild.loadQualityTestControl();
                break;
            default:
                break;
        }
    }

    ngAfterViewInit() {
        this.isRendered = true;

    }

    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        if (this.isRendered && this.dataService.getMenuList().length != 0) {
            this.isRendered = false;
            Util.loadMaterialElements();
        }
        this.cdRef.detectChanges();
    }

    evalMenu(menuId: string) {
        return Util.evalMenu(menuId, "6", this.dataService.getMenuList());
    }

    ngOnInit() {
    }
}
