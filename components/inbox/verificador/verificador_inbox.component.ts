import { Component, Input, Inject, ChangeDetectorRef, OnInit } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { SessionInfoService } from '../../util/SessionInfoService';
import { DataService } from '../../util/data_service';

@Component({
    selector: 'verificador-inbox',
    templateUrl: './verificador_inbox.component.html',
    styleUrls: [],
    providers: [AlertComponent, SessionInfoService, DataService]
})
export class VerificadorInboxComponent implements OnInit {
    public navigation: string = "";

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
    }
    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }

    existPermission(menuId: string) {
        return this.dataService.havePermission(menuId);
    }

    ngOnInit() {

    }
}
