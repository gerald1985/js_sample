﻿import { Component, Input, Inject, ChangeDetectorRef, AfterViewInit, ViewChild } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Supervisor } from '../../util/entities/supervisor';
import { AuthorizeRequirementComponent } from '../../requirement/authorizer/authorize_requirement/authorize_requirement.component';

@Component({
    selector: 'authorizer-inbox',
    templateUrl: './authorizer_inbox.component.html',
    styleUrls: [],
    providers: [AlertComponent]
})

export class AuthorizerInboxComponent implements AfterViewInit {
    @ViewChild(AuthorizeRequirementComponent) authorize_requirementChild: AuthorizeRequirementComponent;

    public navigation: string = "";

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
        switch (breadcrumb) {
            case "Autorizar requisas":
                this.authorize_requirementChild.loadRequirements();
                break;
            default:
                break;
        }
    }

    ngAfterViewInit() {
        Util.loadMaterialElements();
    }

    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}
