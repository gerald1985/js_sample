import { Component, Input, Inject, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Supervisor } from '../../util/entities/supervisor';
import { WOAssignedComponent } from '../../wo_assigned/wo_assigned.component';
import { WOConcludedComponent } from '../../shared/wo_concluded/wo_concluded.component';
import { EquipmentSummaryComponent } from '../../equipment_summary/equipment_summary.component';
import { ProcessSummaryComponent } from '../../process_summary/process_summary.component';
import { DataService } from '../../util/data_service';

@Component({
    selector: 'technician-inbox',
    templateUrl: './technician_inbox.component.html',
    styleUrls: [],
    providers: [AlertComponent]
})
export class TechnicianInboxComponent {
    @ViewChild(WOAssignedComponent) wo_assignetChild: WOAssignedComponent;
    @ViewChild(WOConcludedComponent) wo_concludedChild: WOConcludedComponent;
    @ViewChild(EquipmentSummaryComponent) equipment_summaryChild: EquipmentSummaryComponent;
    @ViewChild(ProcessSummaryComponent) process_summaryChild: ProcessSummaryComponent;
    public navigation: string = "";



    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') authUrl: string, public alertComponent: AlertComponent,
        private dataService: DataService, private cdRef: ChangeDetectorRef) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
        switch (breadcrumb) {
            case "OT Asignadas":
                this.wo_assignetChild.loadAssignedWorkOrders();
                break;
            case "OT Concluida":
                this.wo_concludedChild.loadWOConcluded();
                break;
            case "Resumen x Equipo":
                this.equipment_summaryChild.loadEquipmentSummary();
                break;
            case "Resumen x Proceso":
                this.process_summaryChild.loadProcessSummary();
                break;
            default:
        }
    }

    evalMenu(menuId: string) {
        return Util.evalMenu(menuId, "1", this.dataService.getMenuList());
    }

    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }
}
