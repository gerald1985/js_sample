import { Component, Input, Inject, ChangeDetectorRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Bay } from '../../util/entities/bay';
import { Http } from '@angular/http';
import { Util } from '../../util/util';
import { WorkForce } from '../../util/entities/work_force';
import { AlertComponent } from '../../alerts/alert.component';
import { BreadCrumbService } from '../../util/breadcrumbService';
import { Supervisor } from '../../util/entities/supervisor';
import { SessionInfoService } from '../../util/SessionInfoService';
import { DataService } from '../../util/data_service';
import { PurchaseRequisitionAuthorizationComponent } from '../../purchase_requisition_authorization/purchase_requisition_authorization.component';
import { AssignReceptionOTComponent } from '../../assign_reception_ot/assign_reception_ot.component';
import { StopControlComponent } from '../../stop_control/stop_control.component';
import { EquipmentSummaryComponent } from '../../equipment_summary/equipment_summary.component';
import { ProcessSummaryComponent } from '../../process_summary/process_summary.component';

@Component({
    selector: 'programmer-inbox',
    templateUrl: './programmer_inbox.component.html',
    styleUrls: [],
    providers: [AlertComponent, SessionInfoService, DataService]
})
export class ProgrammerInboxComponent implements OnInit, AfterViewInit {
    @ViewChild(PurchaseRequisitionAuthorizationComponent) purchase_requisition_authorizationChild: PurchaseRequisitionAuthorizationComponent;
    @ViewChild(AssignReceptionOTComponent) assign_reception_otChild: AssignReceptionOTComponent;
    @ViewChild(StopControlComponent) stop_controlChild: StopControlComponent;
    @ViewChild(EquipmentSummaryComponent) equipment_summaryChild: EquipmentSummaryComponent;
    @ViewChild(ProcessSummaryComponent) process_summaryChild: ProcessSummaryComponent;

    public navigation: string = "";

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, @Inject('AUTH_URL') authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef,
        private dataService: DataService, private sessionInfoService: SessionInfoService) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
        switch (breadcrumb) {
            case "Procesar Requisa":
                this.purchase_requisition_authorizationChild.loadPurchaseRequisitionAuthorizations();
                break;
            case "Asignar recepci\u00f3n OT":
                this.assign_reception_otChild.loadPurchaseRequisitionAuthorizations();
                break;
            case "Control paros OT":
                this.stop_controlChild.loadStopControls();
                break;
            case "Resumen por equipo":
                this.equipment_summaryChild.loadEquipmentSummary();
                break;
            case "Resumen por proceso":
                this.process_summaryChild.loadProcessSummary();
                break;
            default:
                break;
        }
    }
    ngAfterViewChecked() {
        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();
    }

    ngAfterViewInit() {
        Util.loadMaterialElements();
    }

    existPermission(menuId: string) {
        return this.dataService.havePermission(menuId);
    }

    ngOnInit() {

    }
}
