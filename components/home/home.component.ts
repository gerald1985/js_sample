import { Component, OnInit, ChangeDetectorRef, Inject } from '@angular/core';
import { Util } from "../util/util";
import { AlertComponent } from '../alerts/alert.component';
import { Http } from '@angular/http';
import { BreadCrumbService } from '../util/breadcrumbService';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { Router } from '@angular/router';
import { DataService } from '../util/data_service';
@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    providers: [AlertComponent]
})
export class HomeComponent implements OnInit {
    public navigation: string = "";
    public sessionInfo: SessionInfo;
    public isLoadedSession: boolean = false;

    constructor(http: Http, @Inject('BASE_URL') baseUrl: string, public alertComponent: AlertComponent,
        private cdRef: ChangeDetectorRef, public router: Router,
        private sessionInfoService: SessionInfoService, private dataService: DataService) {

    }

    public changeInboxTab(breadcrumb: string) {
        BreadCrumbService.changeBreadCrumbChild(breadcrumb);
        this.navigation = BreadCrumbService.breadcrumbs;
    }
    ngAfterViewChecked() {
        BreadCrumbService.newBreadCrumbRoot("Inicio");
        this.navigation = BreadCrumbService.breadcrumbs;
        Util.HideLoader();
        this.sessionInfo = this.sessionInfoService.getSessionInfo();
        if (this.sessionInfo != null && !this.isLoadedSession && this.dataService.getMenuList().length > 0) {
            this.isLoadedSession = true;
            switch (new SessionInfo(this.sessionInfo)._SessionInfo.RoleName) {
                case 'SupervisorADF': this.router.navigateByUrl('/bandeja-de-entrada');
                    this.dataService.setRootUrl('/bandeja-de-entrada');
                    break;
                case 'VerificadorADF': this.router.navigateByUrl('/bandeja-de-entrada-verificador');
                    this.dataService.setRootUrl('/bandeja-de-entrada-verificador');
                    break;
                case 'TecnicoADF': this.router.navigateByUrl('/bandeja-de-entrada-tecnico');
                    this.dataService.setRootUrl('/bandeja-de-entrada-tecnico');
                    break;
                case 'AutorizadorADF': this.router.navigateByUrl('/bandeja-de-entrada-autorizador');
                    this.dataService.setRootUrl('/bandeja-de-entrada-autorizador');
                    break;
                case 'ProgramadorADF': this.router.navigateByUrl('/bandeja-de-entrada-programador');
                    this.dataService.setRootUrl('/bandeja-de-entrada-programador');
                    break;
                case 'GenericoADF': this.router.navigateByUrl('/bandeja-de-entrada-generico');
                    this.dataService.setRootUrl('/bandeja-de-entrada-generico');
                    break;
                default:
                    break;
            }
        }
        this.cdRef.detectChanges();
    }

    ngOnInit() {
        Util.loadMaterialElements();

    }
}
