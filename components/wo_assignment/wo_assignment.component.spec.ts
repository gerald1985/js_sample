﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { assert } from 'chai';
import { WOAssignmentComponent } from './wo_assignment.component';
import { TestBed, async, ComponentFixture, inject, tick, fakeAsync } from '@angular/core/testing';
import { AlertComponent } from '../alerts/alert.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NgxPaginationModule } from 'ngx-pagination'; ;
import { WOAssignment } from '../util/entities/wo_assignment';

import {
    HttpModule,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Util } from '../util/util';
import { MockResponse } from '../../../test/mock/mock_reponse';
import { BreadCrumbService } from '../util/breadcrumbService';
import { Reception } from "../util/entities/reception";
import { Bay } from "../util/entities/bay";
import { WorkForce } from '../util/entities/work_force';
import { Supervisor } from '../util/entities/supervisor';
import { ChangeDetectorRef } from '@angular/core';
import { FormsModule } from '@angular/forms';


let fixture: ComponentFixture<WOAssignmentComponent>;
let comp: WOAssignmentComponent;

describe('Testing WOAssigment component', () => {
    console.log('Testing WOAssigment component');
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [WOAssignmentComponent],
            imports: [NgxPaginationModule, HttpModule, RouterTestingModule, FormsModule],
            providers: [MockResponse,
                { provide: XHRBackend, useClass: MockBackend },
                { provide: "BASE_URL", useValue: "http://127.0.0.1:62001/" },
                AlertComponent, ChangeDetectorRef]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(WOAssignmentComponent);
            fixture.detectChanges();
            comp = fixture.componentInstance;
        });
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
        
    }));

    it('should validate WOAssigment info', inject([XHRBackend], (mockBackend: any) => {
        var responses: Response[] = [];

        const EnumBaysResponse = TestBed.get(MockResponse).EnumBaysResponse();
        const EnumWorkForces = TestBed.get(MockResponse).EnumWorkForces();
        const EnumBayBySupResponse = TestBed.get(MockResponse).EnumBayBySupResponse();
        const EnumReceptionsReception = TestBed.get(MockResponse).EnumReceptionsReception();


        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(EnumBaysResponse) })));
        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(EnumWorkForces) })));
        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(EnumBayBySupResponse) })));
        responses.push(new Response(new ResponseOptions({ body: JSON.stringify(EnumReceptionsReception) })));

        mockBackend.connections.subscribe((connection: any) => {
            var response = responses.shift();
            console.log("The response ", response)
            connection.mockRespond(response);
        });


        comp.bayURL().subscribe(result => {
            console.log("1");
            comp.bays = result.json()["data"] as Bay[];
        });
        fixture.detectChanges();
        comp.workForcesURL().subscribe(result => {
            console.log("2");
            comp.workForces = result.json()["data"] as WorkForce[];
        });
        fixture.detectChanges();
        comp.bayBySupURl().subscribe(result => {
            console.log("3");
            console.log(result);
            comp.supervisors = result.json()["data"] as Supervisor[];
            comp.filterSelectedSupervisor = new Supervisor(comp.supervisors[0]);
        });
        fixture.detectChanges();
        comp.receptionsBySupURL().subscribe(result => {
            console.log(result);
            comp.receptions = result.json()["data"] as Reception[];
        });

        fixture.detectChanges();

        const firstRowValues = fixture.nativeElement.querySelector(".reception-number");
        console.log("first-row",firstRowValues);
        expect(firstRowValues.textContent).toEqual("906");
        comp.loadReceptionsBySupervisor();
        comp.changeSupervisor();
        comp.changeBay(comp.bays[0]);
        comp.changeRepose();
        comp.changeEquipment();
        comp.renderSelect();
        fixture.detectChanges();
        comp.loadReceptionsArray(comp.receptions);
        let reception = comp.receptions[0];
        reception.selectedArbpl = comp.workForces[0];
        reception.selectedBay = comp.bays[0];
        reception.statusChecked = "X";
        comp.changeReception(reception);
    }));
});