import { Component, Inject, OnInit, Input, SimpleChange, SimpleChanges, ChangeDetectorRef, ApplicationRef } from '@angular/core';
import { Http } from '@angular/http';
import { Util } from "../util/util";
import { Reception } from "../util/entities/reception";
import { Bay } from "../util/entities/bay";
import { WorkForce } from '../util/entities/work_force';
import { AlertComponent } from '../alerts/alert.component';
import { Supervisor } from '../util/entities/supervisor';
import { BreadCrumbService } from '../util/breadcrumbService';
import { SessionInfoService } from '../util/SessionInfoService';
import { SessionInfo } from '../util/entities/session_info';
import { DataService } from '../util/data_service';

@Component({
    selector: 'wo-assignment',
    templateUrl: './wo_assignment.component.html'
})
export class WOAssignmentComponent implements OnInit {
    public receptions: Reception[] = [];
    public bays: Bay[] = [];
    workForces: WorkForce[] = [];
    supervisors: Supervisor[] = [];
    public elemRendered = false;
    public arrayLoaded = false;
    public navigation: string = "";
    filterSelectedBay: string = "";
    filterSelectedSupervisor: Supervisor;
    public filterEquipment: string = "";
    public filterRepose: boolean = false;
    public receptionsLoaded = false;
    public paginationRendered = false;
    public havePermission: boolean = false;


    //sorting
    key: string = ''; //set default
    reverse: boolean = false;

    sort(key: string) {
        this.key = key;
        this.reverse = !this.reverse;
    }

    //evalPermission(permissionId: number, description: string) {
    //    return Util.havePermission(permissionId, description, this.dataService.getMenuList());
    //}

    //initializing p to one
    pageWo_assignment: number = 1;

    constructor(private http: Http, @Inject('BASE_URL') private baseUrl: string, @Inject('AUTH_URL') private authUrl: string,
        public alertComponent: AlertComponent, private cdRef: ChangeDetectorRef, private sessionInfoService: SessionInfoService,
        private dataService: DataService) {

    }

    bayURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBays');
    }

    workForcesURL() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumWorkForces');
    }

    bayBySupURl() {
        return this.http.get(this.baseUrl + 'api/Catalog/EnumBayBySup');
    }


    ngOnInit() {
        this.loadWoAssignment();
    }

    loadWoAssignment() {
        if (Util.getPermissions(['51', '42'], this.dataService.getMenuList()).length == 0) {
            this.alertComponent.toastMessage = "No cuentas con los permisos suficientes";
            this.alertComponent.warningMessage();
            this.havePermission = false;
            return;
        }
        this.havePermission = true;
        Util.ShowLoader();
        this.bayURL().subscribe(result => {
            if (result != undefined)
                this.bays = Util.loadJsonWithToast<Bay[]>(this.bays, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });

        this.workForcesURL().subscribe(result => {
            if (result != undefined)
                this.workForces = Util.loadJsonWithToast<WorkForce[]>(this.workForces, result, this.alertComponent);
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });

        this.bayBySupURl().subscribe(result => {
            if (result != undefined) {
                this.supervisors = Util.loadJsonWithToast<Supervisor[]>(this.supervisors, result, this.alertComponent);
                var user = new SessionInfo(this.sessionInfoService.getSessionInfo())._SessionInfo.UserName;
                var found = this.supervisors.find(function (elem) { return new Supervisor(elem)._Supervisor.usuario.toUpperCase() == user; });
                if (found != undefined) {
                    this.filterSelectedSupervisor = new Supervisor(found);
                }
                else {
                    this.filterSelectedSupervisor = new Supervisor(this.supervisors[0]);
                }
                this.loadReceptionsBySupervisor();
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
        Util.HideLoader();
    }

    loadReceptionsArray(receptions: Reception[]) {
        let self = this;
        receptions.forEach(function (reception) {
            var selectedWFRecord = self.workForces.find(workforce => new WorkForce(workforce)._WorkForce.arbpl.toString() === new Reception(reception)._Reception.arbpl);
            if (selectedWFRecord != undefined) {
                reception.selectedArbpl = selectedWFRecord;
            } else {
                reception.selectedArbpl = -1;
            }
            var selectedBayRecord = self.bays.find(bay => new Bay(bay)._Bay.bahia.toString() === new Reception(reception)._Reception.bahia)
            if (selectedBayRecord != undefined) {
                reception.selectedBay = selectedBayRecord;
            } else {
                reception.selectedBay = -1;
            }
        })
    }
    receptionsBySupURL() {
        return this.http.get(this.baseUrl + 'api/WOAssignment/EnumReceptions?supervisor=' + this.filterSelectedSupervisor._Supervisor.usuario);
    }
    receptionsURL() {
        return this.http.get(this.baseUrl + 'api/WOAssignment/EnumReceptions?supervisor=' + this.filterSelectedSupervisor._Supervisor.usuario + "&bay=" + this.filterSelectedBay + "&equipment=" + this.filterEquipment + "&repose=" + this.filterRepose)
    }

    loadReceptionsBySupervisor() {
        this.receptionsBySupURL().subscribe(result => {
            if (result != undefined) {
                this.receptions = Util.loadJsonWithToast<Reception[]>(this.receptions, result, this.alertComponent);
                this.setStateColor();
                this.receptionsLoaded = true;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    setStateColor() {
        let receptionBase: Reception[] = [];
        receptionBase = this.receptions;
        this.loadReceptionsArray(receptionBase);
        this.receptionsLoaded = true;
        Array.prototype.forEach.call(receptionBase, function (el: Reception) {
            let recptn: Reception = new Reception(el);
            switch (recptn._Reception.statord) {
                case "FINBH": {
                    recptn._Reception.statordColor = "bck_green_3";
                    break;
                }
                case "VERI": {
                    recptn._Reception.statordColor = "bck_green_3";
                    break;
                }
                case "CONC": {
                    recptn._Reception.statordColor = "bck_green_3";
                    break;
                }
                case "PROCE": {
                    recptn._Reception.statordColor = "bck_yellow";
                    break;
                }
                case "REPROC": {
                    recptn._Reception.statordColor = "bck_yellow";
                    break;
                }
                case "ASIGN": {
                    recptn._Reception.statordColor = "bck_yellow";
                    break;
                }
                case "RECEP": {
                    recptn._Reception.statordColor = "bck_gray1";
                    break;
                }
                case "RECH": {
                    recptn._Reception.statordColor = "bck_red";
                    break;
                }
            }
            if (recptn._Reception.pC_STATUS == "X" || recptn._Reception.statord == "RECEP") {
                recptn._Reception.pC_STATUSChecked = true;
                recptn._Reception.pC_STATUS = "X";
            } else {
                recptn._Reception.pC_STATUSChecked = false;
            }
        });
        this.receptions = receptionBase;
    }

    loadReceptions() {
        Util.ShowLoader();
        this.receptionsURL().subscribe(result => {
            Util.HideLoader();
            if (result != undefined) {
                this.receptions = Util.loadJsonWithToast<Reception[]>(this.receptions, result, this.alertComponent);
                this.setStateColor();
                this.loadReceptionsArray(this.receptions);
                this.receptionsLoaded = true;
            }
        }, error => {
            Util.EvalError(error, this.alertComponent, this.authUrl);
        });
    }

    changeEquipment() {
        this.loadReceptions();
    }

    changeBay(filter: any) {
        this.loadReceptions();
    }

    changeSupervisor() {
        this.loadReceptions();
    }

    changeRepose() {
        this.loadReceptions();
    }

    renderSelect() {
        this.paginationRendered = true;
    }

    changeReception(reception: Reception) {
        Util.ShowLoader();
        var workForceInstance = new WorkForce(reception.selectedArbpl);
        var bayInstance = new Bay(reception.selectedBay);
        var receptionInstance = new Reception(reception);

        if (workForceInstance._WorkForce.arbpl != undefined
            && workForceInstance._WorkForce.arbpl != null &&
            bayInstance._Bay.bahia != undefined &&
            bayInstance._Bay.bahia != null) {

            receptionInstance._Reception.arbpl = workForceInstance._WorkForce.arbpl;
            receptionInstance._Reception.bahia = bayInstance._Bay.bahia.toString();
            receptionInstance._Reception.pC_STATUS = receptionInstance._Reception.pC_STATUS;
            this.http.put(this.baseUrl + 'api/WOAssignment/UpdatePCs', reception, {}).subscribe(result => {
                Util.HideLoader();
                if (result != undefined) {
                    Util.transactionWithToast(result, this.alertComponent);
                    if (result.json()["responseCode"] == 200) {
                        this.loadReceptions();
                    }
                }
            }, error => {
                Util.EvalError(error, this.alertComponent, this.authUrl);
            });

        } else {
            Util.HideLoader();
            this.alertComponent.toastMessage = "Seleccione el cargo y bahia para la recepcion: " + receptionInstance._Reception.rentnr;
            this.alertComponent.errorMessage();
        }

    }

    loadEquipmentDetails(rentnr: string, equnr: string) {
        this.dataService.setRentnr(rentnr);
        this.dataService.setEQUNR(equnr);
        this.dataService.setLoadEquipmentDetails(true);
    }

    ngAfterViewChecked() {
        if (!this.elemRendered && this.bays.length > 0 && this.workForces.length > 0 && this.supervisors.length > 0) {
            this.elemRendered = true;
            Util.HideLoader();
            BreadCrumbService.newBreadCrumbRoot("Bandeja de entrada supervisor");
            BreadCrumbService.changeBreadCrumbChild("Asignaci&oacute;n OT");
        }

        if (this.receptionsLoaded) {
            this.dataService.setWorkOrderQuantity(this.receptions.length);
            Util.loadMaterialBySelector(".tooltipped", {}, "Tooltip");
            this.receptionsLoaded = false;
        }

        if (this.paginationRendered) {
            Util.loadMaterialBySelector("select.filter", {}, "FormSelect");
            this.paginationRendered = false;
        }

        this.navigation = BreadCrumbService.breadcrumbs;
        this.cdRef.detectChanges();

    }
}