﻿$(function () {

});

function OnSuccess(data) {
    //TODO

    $('#permissionForm').html(data.response);
    alertFunctions.basicAlert($('#AlertTitle').val(), $('#AlertText').val(), 'success');
}

function OnFailure(data) {
    //TODO

    console.log(data)
}

$('#permissionForm').on('change', 'input:checkbox.ckbPermission', function () {
    var indice = $(this).attr('data-indice');
    console.log(indice)
    if ($(this).prop('checked')) {
        $('#model\\.Permisos\\[' + indice + '\\]\\.NombrePermiso').attr('value', $(this).attr('data-method'));
        $('#model\\.Permisos\\[' + indice + '\\]\\.NombreController').attr('value', $(this).attr('data-controller'));
    } else {
        $('#model\\.Permisos\\[' + indice + '\\]\\.NombrePermiso').attr('value', '');
        $('#model\\.Permisos\\[' + indice + '\\]\\.NombreController').attr('value', '');
    }
});