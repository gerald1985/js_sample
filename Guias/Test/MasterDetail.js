﻿$(function () {
    chargeForeign();
    orderElements();
});

function OnSuccess(data) {
    //TODO

    $('#masterForm').html(data.response);
    alertFunctions.basicAlert($('#AlertTitle').val(), $('#AlertText').val(), 'success');
}

function OnFailure(data) {
    //TODO

    console.log(data)
}

//Master - Detail

function addChild() {
    partialFunctions.addChild($('#url_newChild').val(), $('#tblChilds'), function () { orderElements(); });
}

function rmvChild(elem) {
    partialFunctions.removeChild(elem, 'tr');
    orderElements();
}

function orderElements() {
    partialFunctions.orderElements($('#tblChilds'), 'tr', 'modelAttr', 'Childs');
}

function chargeForeign() {
    $.ajax({
        url: $('#url_chargeSelect').val(),
        type: "GET",
        async: false,
        cache: false,
        dataType: 'json',
        success: function (data) {
            $('#IdForeignMaster').append(data.response);
        },
        error: function () {
            alertFunctions.basicAlert($('#AlertTitle').val(), '¡Error al agregar nuevo elemento!', "error");
        }
    });
}