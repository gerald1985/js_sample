﻿$(function () {
    MemberSearchFunctions.init();
});

$(document).on('click', '#BinnacleSearchButton', function () {
    if (!ValidateFilter()) return;

    var pageToSend = parseInt($('.cap.current').attr('data'));
    if (isNaN(pageToSend)) {
        gateway.getDataByData_Callback($('#Url_Get_Binnacle').val(),
            {
                Search: $('#BinnacleSearchText').val(),
                Number: 1,
                MemberId: $('#BinnacleSearchMember').val(),
                StartActionDate: $('#BinnacleStartDate').val(),
                EndActionDate: $('#BinnacleEndDate').val()
            },
            'BinnacleContainer',
            function () { MemberSearchFunctions.loadValuesInButtons(); });
    } else {
        gateway.getDataByData_Callback($('#Url_Get_Binnacle').val(),
            {
                Search: $('#BinnacleSearchText').val(),
                Number: pageToSend,
                MemberId: $('#BinnacleSearchMember').val(),
                StartActionDate: $('#BinnacleStartDate').val(),
                EndActionDate: $('#BinnacleEndDate').val()
            },
            'BinnacleContainer',
            function () { MemberSearchFunctions.loadValuesInButtons(); });
    }
});

function ValidateFilter() {
    var resp = 0;

    resp += validationsForm.text('BinnacleStartDate');
    resp += validationsForm.text('BinnacleEndDate');

    if ($('#BinnacleStartDate').val() != '' && $('#BinnacleEndDate').val()) {
        var startDate = $('#BinnacleStartDate').val().split('-');
        var endDate = $('#BinnacleEndDate').val().split('-');

        if (moment(startDate[2] + '-' + startDate[1] + '-' + startDate[0]).diff(moment(endDate[2] + '-' + endDate[1] + '-' + endDate[0]), 'days') > 0) {
            $('#BinnacleEndDate').parent().addClass('mdc-text-field--invalid');
            alertFunctions.basicAlert($('#AlertTitle').val(), 'El orden de las fechas no es correcto.', 'warning');
            resp++;
        } else {
            $('#BinnacleEndDate').parent().removeClass('mdc-text-field--invalid');
        }
    }

    return resp == 0;
}

$(document).on('click', '#BinnacleRemoveSearchButton', function () {
    $('#BinnacleSearchText').val('');
    $('#BinnacleSearchMember').empty();
    $('#BinnacleSearchMember').val('');
    var dateNow = new Date();
    $('#BinnacleEndDate').val(moment(dateNow).format('DD-MM-YYYY'));
    dateNow.setMonth(dateNow.getMonth() - 1);
    $('#BinnacleStartDate').val(moment(dateNow).format('DD-MM-YYYY'));

    gateway.getDataByData_Callback($('#Url_Get_Binnacle').val(),
        {
            Search: $('#BinnacleSearchText').val(),
            Number: 1,
            MemberId: $('#BinnacleSearchMember').val(),
            StartActionDate: $('#BinnacleStartDate').val(),
            EndActionDate: $('#BinnacleEndDate').val()
        },
        'BinnacleContainer',
        function () { MemberSearchFunctions.loadValuesInButtons(); });
});