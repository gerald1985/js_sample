﻿$(function () {

});

function DeleteInterestTheme(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    InterestThemeId: InterestThemeId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                'GridInterestTheme'
            );
        });
}

function DeleteInterestThemes() {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'InterestThemes',
                'GridInterestTheme',
                'GridInterestTheme',
                {
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}