﻿$(function () {
    $('.rate_lm').each(function (index, item) {
        var rate = $(this).attr('data-rateyo_rate');
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: "15px",
            readOnly: true,
        });
    });
});

function addSubscription(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_Add').val(),
                { InterestThemeId: InterestThemeId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toTheme').click();
                    });
                }
            );
        });
}

function removeSubscription(InterestThemeId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                { InterestThemeId: InterestThemeId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Eliminado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toTheme').click();
                    });
                }
            );
        });
}

