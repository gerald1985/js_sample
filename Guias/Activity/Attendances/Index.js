﻿var panels = null;
var searchTextTemporal = '';

$(function () {
    initSubscriptionsTabs();
    AttendanceFilter();
});

function initSubscriptionsTabs() {
    var dynamicTabBar = new mdc.tabs.MDCTabBar(document.querySelector('#tab-bar-attendances'));
    panels = document.querySelector('.panels');

    dynamicTabBar.tabs.forEach(function (tab) { tab.preventDefaultOnClick = true; });

    dynamicTabBar.listen('MDCTabBar:change', function (item) {
        updatePanel(item.detail.activeTabIndex);
    });
}

function updatePanel(index) {
    var activePanel = panels.querySelector('.panel.active');
    if (activePanel) activePanel.classList.remove('active');

    var newActivePanel = panels.querySelector('.panel:nth-child(' + (index + 1) + ')');
    if (newActivePanel) newActivePanel.classList.add('active');
}

function AttendanceFilter() {
    searchTextTemporal = $('#inputGridSearch_Att').val();

    var inputGrid = document.getElementById('inputGridSearch_Att');
    if (inputGrid) {
        inputGrid.addEventListener('keyup', function (event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                document.getElementById("SearchDiv_Att").click();
            }
        });
    }

    $('#SearchDiv_Att').on('click', function () {
        if ($('#inputGridSearch_Att').val() == '') return;

        gateway.getDataByData_Callback($('#Url_Get_Pagination').val(),
            {
                SearchText: $('#inputGridSearch_Att').val(),
                TabId: $('#tab-bar-attendances').find('.mdc-tab--active:first').attr('aria-controls'),
            },
            'panels_attendance_container',
            function () {
                initSubscriptionsTabs();
                AttendanceFilter();
            });
    });

    $('#SearchCloseDiv_Att').on('click', function () {
        if (searchTextTemporal != '') {
            gateway.getDataByData_Callback($('#Url_Get_Pagination').val(),
                {
                    Search: null,
                    TabId: $('#tab-bar-attendances').find('.mdc-tab--active:first').attr('aria-controls'),
                },
                'panels_attendance_container',
                function () {
                    initSubscriptionsTabs();
                    AttendanceFilter();
                });
        } else {
            $('#inputGridSearch_Att').val('');
        }
    });

    $('.rate_lm').each(function (index, item) {
        var rate = $(this).attr('data-rateyo_rate');
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: "15px",
            readOnly: true,
        });
    });
}

function addAttendance(EventId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_Add').val(),
                { EventId: EventId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toAttendances').click();
                    });
                }
            );
        });
}

function removeAttendance(EventId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                { EventId: EventId },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Eliminado exitosamente.', 'success', function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('Url_toAttendances').click();
                    });
                }
            );
        });
}