﻿$(function () {
    
});

function ValidateExternalGuest() {
    var resp = 0;

    resp += validationsForm.select('IDType');
    resp += validationsForm.select('GenderId');
    resp += validationsForm.select('CountryId');

    return resp === 0;
}

$('#IDType').on('change', function () {
    var elem = $(this);
    if ($(elem).val() === '' || $(elem).val() === '0' || $(elem).val() === $('#IDTypeBlank').val()) {
        $('#IDNumber').removeAttr('required');
        $('#IDNumber').val('');
        $('#IDNumber').attr('readonly', true);
        $('#IDNumber').attr('disabled', true);
        $('#IDNumber').parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
        $('#IDNumber').parents('.mdc-text-field').addClass('mdc-text-field--disabled');
    } else {
        $('#IDNumber').attr('required', true);
        $('#IDNumber').removeAttr('readonly');
        $('#IDNumber').removeAttr('disabled');
        $('#IDNumber').parents('.mdc-text-field').removeClass('mdc-text-field--disabled');
        regexInputs.IdentificationTypeMask('IDNumber', $(elem).val());
    }
});

function OnSuccess_ExternalGuest(res) {
    pageFunctions.showElement($('#preloader'));
    location.href = $('#Url_ToLogin').val();
}

$(document).on('change', '#BirthDate', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');
    $(this).parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
});