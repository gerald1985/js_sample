﻿$(function () {
    initConfirmationsTabs();
});

$('#tab-bar-confirmation a').on('click', function () {
    if ($(this).attr('data-tab-href') !== undefined && $(this).attr('data-tab-href') !== null) {
        pageFunctions.showElement($('#preloader'));
        location.href = $(this).attr('data-tab-href');
    }
});

function initConfirmationsTabs() {
    var dynamicTabBar = new mdc.tabs.MDCTabBar(document.querySelector('#tab-bar-confirmation'));
    panels = document.querySelector('.panels');

    dynamicTabBar.tabs.forEach(function (tab) { tab.preventDefaultOnClick = true; });

    dynamicTabBar.listen('MDCTabBar:change', function (item) {
        updatePanel(item.detail.activeTabIndex);
    });
}

function updatePanel(index) {
    var activePanel = panels.querySelector('.panel.active');
    if (activePanel) activePanel.classList.remove('active');

    var newActivePanel = panels.querySelector('.panel:nth-child(' + (index + 1) + ')');
    if (newActivePanel) newActivePanel.classList.add('active');
}

function SaveMemberAsistances(panel) {
    var IDs = [];
    $('tr.is-selected', "#" + panel).each(function (index, item) {
        if ($(item).attr('data') != undefined && $(item).attr('data') != null) {
            IDs.push($(item).attr('data'));
        }
    });

    if (IDs.length == 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), '¡Debe seleccionar por lo menos un registro!', "warning");
        return;
    }

    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.saveEntity_Callback(
                $('#Url_SaveMember').val(),
                {
                    EventId: $('#EventId').val(),
                    VerifiedIds: IDs,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                function (data) {
                    $('#' + panel).html(data.response);
                    paginationCustomFunctions.init();
                    reloadElements();
                    alertFunctions.basicAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success');
                }
            );
        });
}