﻿$(function () {
    $('#Body').summernote({
        airMode: false,
        lang: 'es-ES',
        height: 450,
        tabsize: 2,
        minHeight: null,
        maxHeight: null,
        dialogsInBody: true,
    });
});

var dialogEmail = new mdc.dialog.MDCDialog(document.querySelector('#mdc-dialog_Template'));

document.querySelector('#BtnAddEmail').addEventListener('click', function (evt) {
    dialogEmail.show();
})

function ValidateTemplate() {
    var resp = 0;

    resp += validationsForm.text('Name');

    if ($('#Body').summernote('isEmpty')) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'El contenido de la plantilla esta vacío.', 'warning');
        resp++;
    }

    return resp == 0;
}

function OnSuccess_Template(res) {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}

$('#FormEmailTemplate').on('submit', function (e) {
    e.preventDefault();

    if ($('#FormEmailTemplate').hasClass('SendEmail_Press')) {
        if (!ValidateTemplate()) return false;

        var address = '';
        if ($('.EmailToSend').length > 0) {
            $('.EmailToSend').each(function (index, item) {
                if (index == 0) address += $(item).html();
                else address += ', ' + $(item).html();
            });
            address += ', ' +$('#Email').val();
        } else {
            address += $('#Email').val();
        }

        gateway.saveEntity_Callback($('#Url_SendEmail').val(),
            {
                template: {
                    TemplateId: 0,
                    Name: $('#Name').val(),
                    Body: $('#Body').val(),

                    Description: address,
                }
            },
            function (resp) {
                alertFunctions.basicAlert($('#AlertTitle').val(), '¡Realizado con éxito!', "success");
                console.log(resp)
            });
    } else {
        $('#EmailContainer').append('<div class="inline_divs pad_tb_03"><label class="EmailToSend">' + $('#Email').val() + '</label><a class="mar_lr_05 wh1 bck_pos_green icon_close_3 multi_icon pointer removeEmail"></a></div>');

        $('#Email').val('');
        dialogEmail.close();
    }
});

$(document).on('click', '.removeEmail', function () {
    $(this).parent().remove();
});

$('#BtnSendEmail').on('click', function () {
    $('#FormEmailTemplate').addClass('SendEmail_Press');
    $('#FormEmailTemplate').removeClass('ValidateEmail_Press');
});

$('#BtnValidateEmail').on('click', function () {
    $('#FormEmailTemplate').addClass('ValidateEmail_Press');
    $('#FormEmailTemplate').removeClass('SendEmail_Press');
});