﻿$(function () {
    
});

$('#SectionTypeId').on('change', function () {
    gateway.editEntity($('#Url_SectionType').val(), { SectionTypeId: $(this).val() }, 'SectionTypeContainer')
});

function ValidateSection() {
    var resp = 0;

    resp += validationsForm.select('SectionTypeId');
    resp += validationsForm.select('SystemStatusId');
    resp += validationsForm.image('SendFile', 'FileLocation');

    return resp == 0;
}

function OnSuccess_Section() {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}