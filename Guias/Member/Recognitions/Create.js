﻿$(function () {

});

function ValidateRecognition() {
    var resp = 0;

    if ($('#SystemStatusId').val() == '') {
        $('#selectSystemStatusId').addClass('mdc-text-field--invalid');
        resp++;
    } else {
        $('#selectSystemStatusId').removeClass('mdc-text-field--invalid');
    }

    if ($('#SendFile').val() == '' && $('#FileLocation').val() == '') {
        $('#SendFile-helper-text').addClass('mdc-text-field--invalid');
        resp++;
    } else {
        $('#SendFile-helper-text').removeClass('mdc-text-field--invalid');
    }

    return resp == 0;
}

function OnSuccess_Recognition() {
    document.getElementById('DivToList').click();
}