﻿$(function () {
    $('.rate_lm').each(function (index, item) {
        var rate = $(this).attr('data-rateyo_rate');
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: "15px",
            readOnly: true,
        });
    });
});