﻿$(function () {

});

function ValidateMemberPosition() {
    var resp = 0;

    resp += validationsForm.select('PositionId');

    if ($('#EndDate').val() === '') {
        $('#Date-helper-text').addClass('hide');
    } else {
        var startDate = $('#StartDate').val().split('-');
        var endDate = $('#EndDate').val().split('-');

        if (moment(startDate[2] + '-' + startDate[1] + '-' + startDate[0]).diff(moment(endDate[2] + '-' + endDate[1] + '-' + endDate[0]), 'days') >= 0) {
            $('#Date-helper-text').removeClass('hide');
            resp++;
        } else {
            $('#Date-helper-text').addClass('hide');
        }
    }

    return resp === 0;
}

$(document).on('mouseenter', 'label#EndDate_clear, label#ExpirationDate_clear', function () {
    $(this).removeClass('icon_date');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label#EndDate_clear, label#ExpirationDate_clear', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_date');
});
$(document).on('change', '#StartDate, #ExpirationDate', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');
});
$(document).on('change', '#EndDate', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');

    if ($('#EndDate').val() != '') {
        $('#ExpirationDate').val('');
        $('#ExpirationDate').attr('readonly', true);
        $('#ExpirationDate').parent().addClass('mdc-text-field--disabled');
    }
});
$(document).on('click', 'label#EndDate_clear', function () {
    $('#EndDate').val('');

    $('#ExpirationDate').val('');
    $('#ExpirationDate').removeAttr('readonly');
    $('#ExpirationDate').parent().removeClass('mdc-text-field--disabled');
});
$(document).on('click', 'label#ExpirationDate_clear', function () {
    $('#ExpirationDate').val('');
});

function OnSuccess_MemberPosition(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('toSaveMemberPosition').click();
    });
}

function DeleteMemberPosition(MemberId, MemberPositionId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                {
                    MemberId: MemberId,
                    MemberPositionId: MemberPositionId,
                },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success", function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('toSaveMemberPosition').click();
                    });
                }
            );
        });
}