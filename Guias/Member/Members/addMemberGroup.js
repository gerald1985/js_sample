﻿var init_select_location = false;
$(function () {
    if ($('#MemberGroupId').val() != '' && $('#MemberGroupId').val() != '0') {
        init_select_location = true;
        $('#SectionId').trigger('change');
    }
});

$(document).on('change', '#SectionId', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    var data = null;
    if ($('#MemberGroupId').val() != '' && $('#MemberGroupId').val() != '0') {
        data = { SectionId: id, Disabled: true }
    } else {
        data = { SectionId: id }
    }

    gateway.getDataByData_Callback($('#Url_SelectProgression').val(), data, 'progressionSelectContainer', function () {
        if (init_select_location) {
            init_select_location = false;
            $('#ProgressionId').val($('#ProgressionId_hf').val()).trigger('change');
        } 
        else
            $('#ProgressionId').trigger('change');
    });
});

function ValidateMemberGroup() {
    var resp = 0;

    resp += validationsForm.select('GroupId');
    resp += validationsForm.select('SectionId');
    resp += validationsForm.select('ProgressionId');

    if ($('#EndingDate').val() != '') {
        var startDate = $('#StartDate').val().split('-');
        var endDate = $('#EndingDate').val().split('-');

        if (moment(startDate[2] + '-' + startDate[1] + '-' + startDate[0]).diff(moment(endDate[2] + '-' + endDate[1] + '-' + endDate[0]), 'days') >= 0) {
            $('#Date-helper-text').removeClass('hide');
            resp++;
        } else {
            $('#Date-helper-text').addClass('hide');
        }
    } else {
        $('#Date-helper-text').addClass('hide');
    }

    return resp == 0;
}

$(document).on('mouseenter', 'label#EndingDate_clear', function () {
    $(this).removeClass('icon_date');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label#EndingDate_clear', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_date');
});
$(document).on('change', '#EndingDate', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');
});
$(document).on('click', 'label#EndingDate_clear', function () {
    $('#EndingDate').val('');
});

function OnSuccess_MemberGroup(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('toSaveMemberGroup').click();
    });
}

function DeleteMemberGroup(MemberId, MemberGroupId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                {
                    MemberId: MemberId,
                    MemberGroupId: MemberGroupId,
                },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success", function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('toSaveMemberGroup').click();
                    });
                }
            );
        });
}