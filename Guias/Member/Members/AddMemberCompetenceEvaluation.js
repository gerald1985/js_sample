﻿$(function () {
    
});

function ValidateMemberCompetenceEvaluation() {
    var resp = 0;

    $('#select_form_container').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.littleDivContainer('containerForValidation', 'containerTitleForValidation', 'containerBodyForValidation');

    return resp == 0;
}

function OnSuccess_MemberCompetenceEvaluation(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('toSaveMemberCompetenceEvaluation').click();
    });
}

function DeleteMemberCompetenceEvaluation(MemberId, EvaluationDate) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity_Callback(
                $('#Url_Delete').val(),
                {
                    MemberId: MemberId,
                    EvaluationDate: EvaluationDate,
                },
                function () {
                    alertFunctions.callBackAlert($('#AlertTitle').val(), '¡Modificación exitosa!', "success", function () {
                        pageFunctions.showElement($('#preloader'));
                        document.getElementById('toSaveMemberCompetenceEvaluation').click();
                    });
                }
            );
        });
}