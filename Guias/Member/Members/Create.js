﻿var init_select_location = false;
$(function () {

    if ($('#MemberId_Young').val() != '' && $('#MemberId_Young').val() != '0') {
        init_select_location = true;
        $('#ProvinceId_Young').val($('#ProvinceId_hf').val()).trigger('change');
    }

    if ($('#MemberId_Old').val() != '' && $('#MemberId_Old').val() != '0') {
        init_select_location = true;
        $('#ProvinceId_Old').val($('#ProvinceId_hf').val()).trigger('change');
    }

    MemberSearchFunctions.init();
    MemberSearchFunctions.loadTSESearch(
        function (member) {
            $('#Names_Young').val(member.Names.capitalize(true));
            MDCElemsFunctions.inputFilled('Names_Young');
            $('#Surnames_Young').val(member.Surnames.capitalize(true));
            MDCElemsFunctions.inputFilled('Surnames_Young');
            $('#BirthDate_Young').val(moment(member.BirthDate).format('DD-MM-YYYY'));
            MDCElemsFunctions.inputFilled('BirthDate_Young');
            $('#GenderId_Young').val(member.GenderId).trigger('change');
        },
        function (member) {
            $('#Names_Old').val(member.Names.capitalize(true));
            MDCElemsFunctions.inputFilled('Names_Old');
            $('#Surnames_Old').val(member.Surnames.capitalize(true));
            MDCElemsFunctions.inputFilled('Surnames_Old');
            $('#BirthDate_Old').val(moment(member.BirthDate).format('DD-MM-YYYY'));
            MDCElemsFunctions.inputFilled('BirthDate_Old');
            $('#GenderId_Old').val(member.GenderId).trigger('change');
        });
});

$(document).on('change', '#ProvinceId_Young', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectCanton').val(), { ProvinceId: id, Model: 'Young', ElemId: 'CantonId_Young' }, 'CantonContainer_Young', function () {
        if (init_select_location)
            $('#CantonId_Young').val($('#CantonId_hf').val()).trigger('change');
        else
            $('#CantonId_Young').trigger('change');
    }, false);
});

$(document).on('change', '#CantonId_Young', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectDistrict').val(), { CantonId: id, Model: 'Young', ElemId: 'DistrictId_Young' }, 'DistrictContainer_Young', function () {
        if (init_select_location) {
            $('#DistrictId_Young').val($('#DistrictId_hf').val()).trigger('change');
        }
        else
            $('#DistrictId_Young').trigger('change');
    });
});

$(document).on('change', '#DistrictId_Young', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    if (init_select_location) {
        init_select_location = false;
    }
    else {
        if (id > 0) $('#PostalCode_Young').val($('#DistrictId_Young option:selected').attr('PostalCode'));
        else $('#PostalCode_Young').val('00000');
    }
});

$(document).on('change', '#ProvinceId_Old', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectCanton').val(), { ProvinceId: id, Model: 'Old', ElemId: 'CantonId_Old' }, 'CantonContainer_Old', function () {
        if (init_select_location)
            $('#CantonId_Old').val($('#CantonId_hf').val()).trigger('change');
        else
            $('#CantonId_Old').trigger('change');
    }, false);
});

$(document).on('change', '#CantonId_Old', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectDistrict').val(), { CantonId: id, Model: 'Old', ElemId: 'DistrictId_Old' }, 'DistrictContainer_Old', function () {
        if (init_select_location)
            $('#DistrictId_Old').val($('#DistrictId_hf').val()).trigger('change');
        else
            $('#DistrictId_Old').trigger('change');
    });
});

$(document).on('change', '#DistrictId_Old', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    if (init_select_location) {
        init_select_location = false;
    }
    else {
        if (id > 0) $('#PostalCode_Old').val($('#DistrictId_Old option:selected').attr('PostalCode'));
        else $('#PostalCode_Old').val('00000');
    }
});

$(document).on('change', '#ProvinceId_Tutor', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    var data = null;
    if ($('#MemberId_Tutor').val() == '' || $('#MemberId_Tutor').val() == '0') data = { ProvinceId: id, Model: 'Tutor', ElemId: 'CantonId_Tutor', InModal: true };
    else data = { ProvinceId: id, Model: 'Tutor', ElemId: 'CantonId_Tutor', InModal: true, Disabled: true };

    gateway.getDataByData_Callback($('#Url_SelectCanton').val(), data, 'CantonContainer_Tutor', function () {
        if (init_select_location)
            $('#CantonId_Tutor').val($('#CantonId_hf_Tutor').val()).trigger('change');
        else
            $('#CantonId_Tutor').trigger('change');
    }, false);
});

$(document).on('change', '#CantonId_Tutor', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    var data = null;
    if ($('#MemberId_Tutor').val() == '' || $('#MemberId_Tutor').val() == '0') data = { CantonId: id, Model: 'Tutor', ElemId: 'DistrictId_Tutor', InModal: true };
    else data = { CantonId: id, Model: 'Tutor', ElemId: 'DistrictId_Tutor', InModal: true, Disabled: true };

    gateway.getDataByData_Callback($('#Url_SelectDistrict').val(), data, 'DistrictContainer_Tutor', function () {
        if (init_select_location) {
            $('#DistrictId_Tutor').val($('#DistrictId_hf_Tutor').val()).trigger('change');
        }
        else
            $('#DistrictId_Tutor').trigger('change');
    });
});

$(document).on('change', '#DistrictId_Tutor', function () {
    var id = ($(this).val() == '' ? -1 : $(this).val());
    if (init_select_location) {
        init_select_location = false;
    }
    else {
        if (id > 0) $('#PostalCode_Tutor').val($('#DistrictId_Tutor option:selected').attr('PostalCode'));
        else $('#PostalCode_Tutor').val('00000');
    }
});

$('#IDType_Young').on('change', function () {
    var elem = $(this);
    if ($(elem).val() == '' || $(elem).val() == '0' || $(elem).val() == $('#IDTypeBlank_Young').val()) {
        $('#IDNumber_Young').removeAttr('required');
        $('#IDNumber_Young').val('');
        $('#IDNumber_Young').attr('readonly', true);
        $('#IDNumber_Young').attr('disabled', true);
        $('#IDNumber_Young').parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
        $('#IDNumber_Young').parents('.mdc-text-field').addClass('mdc-text-field--disabled');
    } else {
        $('#IDNumber_Young').attr('required', true);
        $('#IDNumber_Young').removeAttr('readonly');
        $('#IDNumber_Young').removeAttr('disabled');
        $('#IDNumber_Young').parents('.mdc-text-field').removeClass('mdc-text-field--disabled');
        regexInputs.IdentificationTypeMask('IDNumber_Young', $(elem).val(), {
            onComplete: function (data) {
                if ($('#MemberId_Young').val() == null || $('#MemberId_Young').val() == undefined || $('#MemberId_Young').val() == '' || $('#MemberId_Young').val() == 0) {
                    $('#BtnSearchMember_TSE_Container_Young').removeClass('hide');
                }
            },
            onChange: function (data) {
                if ($('#IDNumber_Young').val().length != 11) {
                    $('#BtnSearchMember_TSE_Container_Young').addClass('hide');
                }
            },
        })
    }
});

$('#IDType_Old').on('change', function () {
    var elem = $(this);
    if ($(elem).val() == '' || $(elem).val() == '0' || $(elem).val() == $('#IDTypeBlank_Old').val()) {
        $('#IDNumber_Old').removeAttr('required');
        $('#IDNumber_Old').val('');
        $('#IDNumber_Old').attr('readonly', true);
        $('#IDNumber_Old').attr('disabled', true);
        $('#IDNumber_Old').parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
        $('#IDNumber_Old').parents('.mdc-text-field').addClass('mdc-text-field--disabled');
    } else {
        $('#IDNumber_Old').attr('required', true);
        $('#IDNumber_Old').removeAttr('readonly');
        $('#IDNumber_Old').removeAttr('disabled');
        $('#IDNumber_Old').parents('.mdc-text-field').removeClass('mdc-text-field--disabled');
        regexInputs.IdentificationTypeMask('IDNumber_Old', $(elem).val(), {
            onComplete: function (data) {
                if ($('#MemberId_Old').val() == null || $('#MemberId_Old').val() == undefined || $('#MemberId_Old').val() == '' || $('#MemberId_Old').val() == 0) {
                    $('#BtnSearchMember_TSE_Container_Old').removeClass('hide');
                }
            },
            onChange: function (data) {
                if ($('#IDNumber_Old').val().length != 11) {
                    $('#BtnSearchMember_TSE_Container_Old').addClass('hide');
                }
            },
        })
    }
});

function ValidateMember_Young() {
    var resp = 0;

    resp += validationsForm.image('SendFile_Young', 'Photo_Young');
    resp += validationsForm.select('NationalityId_Young');
    resp += validationsForm.select('IDType_Young');
    resp += validationsForm.select('GenderId_Young');
    resp += validationsForm.select('PositionId_Young');
    resp += validationsForm.select('MemberStatusId_Young');
    resp += validationsForm.select('ProvinceId_Young');
    resp += validationsForm.select('CantonId_Young');
    resp += validationsForm.select('DistrictId_Young');

    //resp += validationsForm.select('ReligionId_Young');
    //resp += validationsForm.select('EducationLevelId_Young');
    //resp += validationsForm.select('SchoolId_Young');

    if ($('#accept_Young').prop('checked')) {
        $('#accept_Young').parents('.text-right').removeClass('mdc-text-field--invalid');
    } else {
        $('#accept_Young').parents('.text-right').addClass('mdc-text-field--invalid');
        resp++;
    }

    $('#MemberPositionContainerYoung').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberKinshipContainerYoung').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberLanguageContainerYoung').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberSchoolContainerYoung').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.divContainers('containerForValidation_Young', 'containerTitleForValidation_Young', 'containerBodyForValidation_Young');

    if (resp > 0) alertFunctions.basicAlert($('#AlertTitle').val(), 'Faltan campos por completar.', 'warning');
    return resp == 0;
}

$('#btnAddKinshipYoung').on('click', function () {
    var index = $('.KinshipContainerYoung').length;
    gateway.add_GetDataByData($('#Url_MemberKinship').val(), { index: index, model: 'Young' }, 'MemberKinshipContainerYoung');
});

$(document).on('click', 'a.removeKinshipYoung', function () {
    $(this).parents('div.KinshipContainerYoung').remove();

    //Order
    var index = 0;
    $('.KinshipContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Young.MemberKinships[' + index + '].MemberKinshipId');
        $(this).find('input[type=hidden]').attr('id', 'MemberKinships_MemberKinshipId_' + index + '_Young');

        $(this).find('.BtnMemberSearchDialog').attr('data-KinSelectId', 'MemberKinships_KinId_' + index + '_Young');
        $(this).find('select[data=KinId]').attr('name', 'Young.MemberKinships[' + index + '].KinId');
        $(this).find('select[data=KinId]').attr('id', 'MemberKinships_KinId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=KinId]').attr('id', 'MemberKinships_KinId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=KinshipId]').attr('name', 'Young.MemberKinships[' + index + '].KinshipId');
        $(this).find('select[data=KinshipId]').attr('id', 'MemberKinships_KinshipId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=KinshipId]').attr('id', 'MemberKinships_KinshipId_' + index + '_YoungSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('.social_network_selector_Young').on('click', function () {
    var index = $('.SocialNetworkContainerYoung').length;
    var type = $(this).attr('data');
    $(this).parent().addClass('hide');
    var containerParent = $(this).parents('[data-social_menu=social_menu_new_Young]');
    gateway.add_GetDataByData_Callback($('#Url_MemberSocialNetwork').val(), { index: index, type: type, model: 'Young' }, 'MemberSocialNetworkContainerYoung', function () {
        $('#social_menu_new_Young').removeClass('mdc-menu--open');
        $('#btnAddSocialNetworkYoung').removeClass('active');
        $('.SocialNetworkContainerYoung:last').find('input[type=text]').focus();

        $(containerParent).removeClass('social_menu_new_popup');
    });
});

$(document).on('mouseenter', 'label.icon_input_Social', function () {
    $(this).removeClass($(this).attr('data-icon'));
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label.icon_input_Social', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass($(this).attr('data-icon'));
});
$(document).on('focusin', 'input.icon_input_Social', function () {
    var elem = $(this).parent().find('label.icon_input_Social');
    $(elem).removeClass($(elem).attr('data-icon'));
    $(elem).addClass('icon_close_3');
});
$(document).on('focusout', 'input.icon_input_Social', function () {
    var elem = $(this).parent().find('label.icon_input_Social');
    $(elem).addClass($(elem).attr('data-icon'));
    $(elem).removeClass('icon_close_3');
});

$(document).on('click', 'label.icon_input_Social', function () {
    var tabType = $(this).attr('data-tab-type');
    var type = $(this).parent().find('input[type=hidden][data=type]').val();
    if (tabType == 'Young') {
        $('.social_network_selector_Young[data=' + type + ']').parent().removeClass('hide');
    } else if (tabType == 'Old') {
        $('.social_network_selector_Old[data=' + type + ']').parent().removeClass('hide');
    } else if (tabType == 'Tutor') {
        $('.social_network_selector_Tutor[data=' + type + ']').parent().removeClass('hide');
    }
    $(this).parents('div.text-left').remove();

    //order
    var index = 0;
    $('.SocialNetworkContainer' + tabType).each(function (index, item) {
        $(this).find('p.mdc-text-field-helper-text').attr('id', 'MemberSocialNetworks_Nickname_' + index + '_' + tabType + '-helper-text');
        $(this).find('input[type=hidden][data=id]').attr('name', tabType + '.MemberSocialNetworks[' + index + '].MemberSocialNetworkId');
        $(this).find('input[type=hidden][data=id]').attr('id', 'MemberSocialNetworks_MemberSocialNetworkId_' + index + '_' + tabType);

        $(this).find('input[type=hidden][data=type]').attr('name', tabType + '.MemberSocialNetworks[' + index + '].TypeId');
        $(this).find('input[type=hidden][data=type]').attr('id', 'MemberSocialNetworks_TypeId_' + index + '_' + tabType);

        $(this).find('label').attr('for', 'MemberSocialNetworks_Nickname_' + index + '_Young');
        $(this).find('input[type=text]').attr('name', tabType + '.MemberSocialNetworks[' + index + '].Nickname');
        $(this).find('input[type=text]').attr('id', 'MemberSocialNetworks_Nickname_' + index + '_' + tabType);
        $(this).find('input[type=text]').attr('aria-controls', 'MemberSocialNetworks_Nickname_' + index + '_' + tabType + '-helper-text');

        index++;
    });
});

$('#btnAddPhoneNumberYoung').on('click', function () {
    var index = $('.PhoneNumberContainerYoung').length;
    gateway.add_GetDataByData($('#Url_MemberPhoneNumber').val(), { index: index, model: 'Young' }, 'MemberPhoneNumberContainerYoung');
});

$(document).on('mouseenter', 'label.icon_input_PhoneNumber', function () {
    $(this).removeClass('icon_number');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label.icon_input_PhoneNumber', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_number');
});
$(document).on('focusin', 'input.icon_input_PhoneNumber', function () {
    var elem = $(this).parent().find('label.icon_input_PhoneNumber');
    $(elem).removeClass('icon_number');
    $(elem).addClass('icon_close_3');
});
$(document).on('focusout', 'input.icon_input_PhoneNumber', function () {
    var elem = $(this).parent().find('label.icon_input_PhoneNumber');
    $(elem).addClass('icon_number');
    $(elem).removeClass('icon_close_3');
});

$(document).on('click', 'label.icon_input_PhoneNumber', function () {
    var tabType = $(this).attr('data-tab-type');
    var index = 0;

    $(this).parents('div.PhoneNumberContainer' + tabType).remove();

    $('.PhoneNumberContainer' + tabType).each(function (index, item) {
        $(this).find('p.mdc-text-field-helper-text').attr('id', 'MemberPhoneNumbers_PhoneNumber_' + index + '_' + tabType + '-helper-text');
        $(this).find('input[type=hidden]').attr('name', tabType + '.MemberPhoneNumbers[' + index + '].MemberPhoneNumberId');
        $(this).find('input[type=hidden]').attr('id', 'MemberPhoneNumbers_MemberPhoneNumberId_' + index + '_' + tabType);

        $(this).find('label').attr('for', 'MemberPhoneNumbers_PhoneNumber_' + index + '_' + tabType);
        $(this).find('input[type=text]').attr('name', tabType + '.MemberPhoneNumbers[' + index + '].PhoneNumber');
        $(this).find('input[type=text]').attr('id', 'MemberPhoneNumbers_PhoneNumber_' + index + '_' + tabType);
        $(this).find('input[type=text]').attr('aria-controls', 'MemberPhoneNumbers_PhoneNumber_' + index + '_' + tabType + '-helper-text');

        index++;
    });
});

$('#btnAddLanguageYoung').on('click', function () {
    var index = $('.LanguageContainerYoung').length;
    gateway.add_GetDataByData($('#Url_MemberLanguage').val(), { index: index, model: 'Young' }, 'MemberLanguageContainerYoung');
});

$(document).on('click', 'a.removeLanguageYoung', function () {
    $(this).parents('div.LanguageContainerYoung').remove();

    //Order
    var index = 0;
    $('.LanguageContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Young.MemberLanguages[' + index + '].MemberLanguageId');
        $(this).find('input[type=hidden]').attr('id', 'MemberLanguages_MemberLanguageId_' + index + '_Young');

        $(this).find('select[data=LanguageId]').attr('name', 'Young.MemberLanguages[' + index + '].LanguageId');
        $(this).find('select[data=LanguageId]').attr('id', 'MemberLanguages_LanguageId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=LanguageId]').attr('id', 'MemberLanguages_LanguageId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=LanguageReadingId]').attr('name', 'Young.MemberLanguages[' + index + '].LanguageReadingId');
        $(this).find('select[data=LanguageReadingId]').attr('id', 'MemberLanguages_LanguageReadingId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=LanguageReadingId]').attr('id', 'MemberLanguages_LanguageReadingId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=LanguageWritingId]').attr('name', 'Young.MemberLanguages[' + index + '].LanguageWritingId');
        $(this).find('select[data=LanguageWritingId]').attr('id', 'MemberLanguages_LanguageWritingId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=LanguageWritingId]').attr('id', 'MemberLanguages_LanguageWritingId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=LanguageConversationId]').attr('name', 'Young.MemberLanguages[' + index + '].LanguageConversationId');
        $(this).find('select[data=LanguageConversationId]').attr('id', 'MemberLanguages_LanguageConversationId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=LanguageConversationId]').attr('id', 'MemberLanguages_LanguageConversationId_' + index + '_YoungSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('#btnAddNationalEventOld').on('click', function () {
    var index = $('.NationalEventContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberNationalEvent').val(), { index: index, model: 'Old' }, 'MemberNationalEventContainerOld');
});

$(document).on('click', 'span.removeNationalEventOld', function () {
    $(this).parents('div.NationalEventContainerOld').remove();

    //Order
    var index = 0;
    $('.NationalEventContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberNationalEvents[' + index + '].MemberNationalEventId');
        $(this).find('input[type=hidden]').attr('id', 'MemberNationalEvents_MemberNationalEventId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberNationalEvents[' + index + '].NationalEventId');
        $(this).find('select').attr('id', 'MemberNationalEvents_NationalEventId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberNationalEvents_NationalEventId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text]').attr('name', 'Old.MemberNationalEvents[' + index + '].NationalEventDate');
        $(this).find('input[type=text]').attr('id', 'MemberNationalEvents_NationalEventDate_' + index + '_Old');
        $(this).find('input[type=text]').attr('aria-controls', 'MemberNationalEvents_NationalEventDate_' + index + '_Old-helper-text');

        $(this).find('input[type=text]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberNationalEvents_NationalEventDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text]').parents('.text-left').find('label').attr('for', 'MemberNationalEvents_NationalEventDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
    dateTimePicker.init();
});

$('#btnAddInternationalEventOld').on('click', function () {
    var index = $('.InternationalEventContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberInternationalEvent').val(), { index: index, model: 'Old' }, 'MemberInternationalEventContainerOld');
});

$(document).on('click', 'span.removeInternationalEventOld', function () {
    $(this).parents('div.InternationalEventContainerOld').remove();

    //Order
    var index = 0;
    $('.InternationalEventContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberInternationalEvents[' + index + '].MemberInternationalEventId');
        $(this).find('input[type=hidden]').attr('id', 'MemberInternationalEvents_MemberInternationalEventId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberInternationalEvents[' + index + '].InternationalEventId');
        $(this).find('select').attr('id', 'MemberInternationalEvents_InternationalEventId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberInternationalEvents_InternationalEventId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text]').attr('name', 'Old.MemberInternationalEvents[' + index + '].InternationalEventDate');
        $(this).find('input[type=text]').attr('id', 'MemberInternationalEvents_InternationalEventDate_' + index + '_Old');
        $(this).find('input[type=text]').attr('aria-controls', 'MemberInternationalEvents_InternationalEventDate_' + index + '_Old-helper-text');

        $(this).find('input[type=text]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberInternationalEvents_InternationalEventDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text]').parents('.text-left').find('label').attr('for', 'MemberInternationalEvents_InternationalEventDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
    dateTimePicker.init();
});

$('#btnAddCourseOld').on('click', function () {
    var index = $('.CourseContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberCourse').val(), { index: index, model: 'Old' }, 'MemberCourseContainerOld');
});

$(document).on('click', 'span.removeCourseOld', function () {
    $(this).parents('div.CourseContainerOld').remove();

    //Order
    var index = 0;
    $('.CourseContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberCourses[' + index + '].MemberCourseId');
        $(this).find('input[type=hidden]').attr('id', 'MemberCourses_MemberCourseId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberCourses[' + index + '].CourseId');
        $(this).find('select').attr('id', 'MemberCourses_CourseId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberCourses_CourseId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text]').attr('name', 'Old.MemberCourses[' + index + '].CourseDate');
        $(this).find('input[type=text]').attr('id', 'MemberCourses_CourseDate_' + index + '_Old');
        $(this).find('input[type=text]').attr('aria-controls', 'MemberCourses_CourseDate_' + index + '_Old-helper-text');

        $(this).find('input[type=text]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberCourses_CourseDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text]').parents('.text-left').find('label').attr('for', 'MemberCourses_CourseDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
    dateTimePicker.init();
});

$('#btnAddWorkshopOld').on('click', function () {
    var index = $('.WorkshopContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberWorkshop').val(), { index: index, model: 'Old' }, 'MemberWorkshopContainerOld');
});

$(document).on('click', 'span.removeWorkshopOld', function () {
    $(this).parents('div.WorkshopContainerOld').remove();

    //Order
    var index = 0;
    $('.WorkshopContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberWorkshops[' + index + '].MemberWorkshopId');
        $(this).find('input[type=hidden]').attr('id', 'MemberWorkshops_MemberWorkshopId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberWorkshops[' + index + '].WorkshopId');
        $(this).find('select').attr('id', 'MemberWorkshops_WorkshopId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberWorkshops_WorkshopId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text]').attr('name', 'Old.MemberWorkshops[' + index + '].WorkshopDate');
        $(this).find('input[type=text]').attr('id', 'MemberWorkshops_WorkshopDate_' + index + '_Old');
        $(this).find('input[type=text]').attr('aria-controls', 'MemberWorkshops_WorkshopDate_' + index + '_Old-helper-text');

        $(this).find('input[type=text]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberWorkshops_WorkshopDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text]').parents('.text-left').find('label').attr('for', 'MemberWorkshops_WorkshopDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
    dateTimePicker.init();
});

$('#btnAddActivityOld').on('click', function () {
    var index = $('.ActivityContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberActivity').val(), { index: index, model: 'Old' }, 'MemberActivityContainerOld');
});

$(document).on('click', 'span.removeActivityOld', function () {
    $(this).parents('div.ActivityContainerOld').remove();

    //Order
    var index = 0;
    $('.ActivityContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberActivities[' + index + '].MemberActivityId');
        $(this).find('input[type=hidden]').attr('id', 'MemberActivities_MemberActivityId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberActivities[' + index + '].ActivityId');
        $(this).find('select').attr('id', 'MemberActivities_ActivityId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberActivities_ActivityId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text]').attr('name', 'Old.MemberActivities[' + index + '].ActivityDate');
        $(this).find('input[type=text]').attr('id', 'MemberActivities_ActivityDate_' + index + '_Old');
        $(this).find('input[type=text]').attr('aria-controls', 'MemberActivities_ActivityDate_' + index + '_Old-helper-text');

        $(this).find('input[type=text]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberActivities_ActivityDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text]').parents('.text-left').find('label').attr('for', 'MemberActivities_ActivityDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
    dateTimePicker.init();
});

$('#btnAddOtherStudyOld').on('click', function () {
    var index = $('.OtherStudyContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberOtherStudy').val(), { index: index, model: 'Old' }, 'MemberOtherStudyContainerOld');
});

$(document).on('click', 'a.removeOtherStudyOld', function () {
    $(this).parents('div.OtherStudyContainerOld').remove();

    //Order
    var index = 0;
    $('.OtherStudyContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberOtherStudies[' + index + '].MemberOtherStudyId');
        $(this).find('input[type=hidden]').attr('id', 'MemberOtherStudies_MemberOtherStudyId_' + index + '_Old');

        $(this).find('select[data=StudyTypeId]').attr('name', 'Old.MemberOtherStudies[' + index + '].StudyTypeId');
        $(this).find('select[data=StudyTypeId]').attr('id', 'MemberOtherStudies_StudyTypeId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=StudyTypeId]').attr('id', 'MemberOtherStudies_StudyTypeId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text][data=StudyName]').attr('name', 'Old.MemberOtherStudies[' + index + '].StudyName');
        $(this).find('input[type=text][data=StudyName]').attr('id', 'MemberOtherStudies_StudyName_' + index + '_Old');
        $(this).find('input[type=text][data=StudyName]').attr('aria-controls', 'MemberOtherStudies_StudyName_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StudyName]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberOtherStudies_StudyName_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StudyName]').parents('.text-left').find('label').attr('for', 'MemberOtherStudies_StudyName_' + index + '_Old');

        $(this).find('input[type=text][data=StudyPlace]').attr('name', 'Old.MemberOtherStudies[' + index + '].StudyPlace');
        $(this).find('input[type=text][data=StudyPlace]').attr('id', 'MemberOtherStudies_StudyPlace_' + index + '_Old');
        $(this).find('input[type=text][data=StudyPlace]').attr('aria-controls', 'MemberOtherStudies_StudyPlace_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StudyPlace]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberOtherStudies_StudyPlace_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StudyPlace]').parents('.text-left').find('label').attr('for', 'MemberOtherStudies_StudyPlace_' + index + '_Old');

        $(this).find('input[type=text][data=StartDate]').attr('name', 'Old.MemberOtherStudies[' + index + '].StartDate');
        $(this).find('input[type=text][data=StartDate]').attr('id', 'MemberOtherStudies_StartDate_' + index + '_Old');
        $(this).find('input[type=text][data=StartDate]').attr('aria-controls', 'MemberOtherStudies_StartDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberOtherStudies_StartDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('label').attr('for', 'MemberOtherStudies_StartDate_' + index + '_Old');

        $(this).find('input[type=text][data=EndDate]').attr('name', 'Old.MemberOtherStudies[' + index + '].EndDate');
        $(this).find('input[type=text][data=EndDate]').attr('id', 'MemberOtherStudies_EndDate_' + index + '_Old');
        $(this).find('input[type=text][data=EndDate]').attr('aria-controls', 'MemberOtherStudies_EndDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberOtherStudies_EndDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('label').attr('for', 'MemberOtherStudies_EndDate_' + index + '_Old');

        index++;
    });
});

$('#btnAddKinshipOld').on('click', function () {
    var index = $('.KinshipContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberKinship').val(), { index: index, model: 'Old' }, 'MemberKinshipContainerOld');
});

$(document).on('click', 'a.removeKinshipOld', function () {
    $(this).parents('div.KinshipContainerOld').remove();

    //Order
    var index = 0;
    $('.KinshipContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberKinships[' + index + '].MemberKinshipId');
        $(this).find('input[type=hidden]').attr('id', 'MemberKinships_MemberKinshipId_' + index + '_Old');

        $(this).find('.BtnMemberSearchDialog').attr('data-KinSelectId', 'MemberKinships_KinId_' + index + '_Old');
        $(this).find('select[data=KinId]').attr('name', 'Old.MemberKinships[' + index + '].KinId');
        $(this).find('select[data=KinId]').attr('id', 'MemberKinships_KinId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=KinId]').attr('id', 'MemberKinships_KinId_' + index + '_OldSelectDiv');

        $(this).find('select[data=KinshipId]').attr('name', 'Old.MemberKinships[' + index + '].KinshipId');
        $(this).find('select[data=KinshipId]').attr('id', 'MemberKinships_KinshipId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=KinshipId]').attr('id', 'MemberKinships_KinshipId_' + index + '_OldSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('.social_network_selector_Old').on('click', function () {
    var index = $('.SocialNetworkContainerOld').length;
    var type = $(this).attr('data');
    $(this).parent().addClass('hide');
    var containerParent = $(this).parents('[data-social_menu=social_menu_new_Old]');
    gateway.add_GetDataByData_Callback($('#Url_MemberSocialNetwork').val(), { index: index, type: type, model: 'Old' }, 'MemberSocialNetworkContainerOld', function () {
        $('#social_menu_new_Old').removeClass('mdc-menu--open');
        $('#btnAddSocialNetworkOld').removeClass('active');
        $('.SocialNetworkContainerOld:last').find('input[type=text]').focus();

        $(containerParent).removeClass('social_menu_new_popup');
    });
});

$('#btnAddPhoneNumberOld').on('click', function () {
    var index = $('.PhoneNumberContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberPhoneNumber').val(), { index: index, model: 'Old' }, 'MemberPhoneNumberContainerOld');
});

$('#btnAddLanguageOld').on('click', function () {
    var index = $('.LanguageContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberLanguage').val(), { index: index, model: 'Old' }, 'MemberLanguageContainerOld');
});

$(document).on('click', 'a.removeLanguageOld', function () {
    $(this).parents('div.LanguageContainerOld').remove();

    //Order
    var index = 0;
    $('.LanguageContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberLanguages[' + index + '].MemberLanguageId');
        $(this).find('input[type=hidden]').attr('id', 'MemberLanguages_MemberLanguageId_' + index + '_Old');

        $(this).find('select[data=LanguageId]').attr('name', 'Old.MemberLanguages[' + index + '].LanguageId');
        $(this).find('select[data=LanguageId]').attr('id', 'MemberLanguages_LanguageId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=LanguageId]').attr('id', 'MemberLanguages_LanguageId_' + index + '_OldSelectDiv');

        $(this).find('select[data=LanguageReadingId]').attr('name', 'Old.MemberLanguages[' + index + '].LanguageReadingId');
        $(this).find('select[data=LanguageReadingId]').attr('id', 'MemberLanguages_LanguageReadingId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=LanguageReadingId]').attr('id', 'MemberLanguages_LanguageReadingId_' + index + '_OldSelectDiv');

        $(this).find('select[data=LanguageWritingId]').attr('name', 'Old.MemberLanguages[' + index + '].LanguageWritingId');
        $(this).find('select[data=LanguageWritingId]').attr('id', 'MemberLanguages_LanguageWritingId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=LanguageWritingId]').attr('id', 'MemberLanguages_LanguageWritingId_' + index + '_OldSelectDiv');

        $(this).find('select[data=LanguageConversationId]').attr('name', 'Old.MemberLanguages[' + index + '].LanguageConversationId');
        $(this).find('select[data=LanguageConversationId]').attr('id', 'MemberLanguages_LanguageConversationId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=LanguageConversationId]').attr('id', 'MemberLanguages_LanguageConversationId_' + index + '_OldSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('#GenderId_Young').on('change', function () {
    if ($('#GenderId_Young').val() != null && $('#GenderId_Young').val() != '' && $('#GenderId_Young').val() != 0) {
        $('.PositionContainerYoung').remove();
        gateway.add_GetDataByData($('#Url_MemberPosition').val(), { index: 0, model: 'Young', GenderId: $('#GenderId_Young').val() }, 'MemberPositionContainerYoung');
    }
});

$('#btnAddPositionYoung').on('click', function () {
    if ($('#GenderId_Young').val() == null || $('#GenderId_Young').val() == '' || $('#GenderId_Young').val() == 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Debe seleccionar un género antes.', 'warning');
    } else {
        var index = $('.PositionContainerYoung').length;
        gateway.add_GetDataByData($('#Url_MemberPosition').val(), { index: index, model: 'Young', GenderId: $('#GenderId_Young').val(), MemberId: $('#MemberId_Young').val() }, 'MemberPositionContainerYoung');
    }
});

$(document).on('click', 'a.removePositionYoung', function () {
    $(this).parents('div.PositionContainerYoung').remove();

    //Order
    var index = 0;
    $('.PositionContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Young.MemberPositions[' + index + '].MemberPositionId');
        $(this).find('input[type=hidden]').attr('id', 'MemberPositions_MemberPositionId_' + index + '_Young');

        $(this).find('select').attr('name', 'Young.MemberPositions[' + index + '].PositionId');
        $(this).find('select').attr('id', 'MemberPositions_PositionId_' + index + '_Young');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberPositions_PositionId_' + index + '_YoungSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('#GenderId_Old').on('change', function () {
    if ($('#GenderId_Old').val() != null && $('#GenderId_Old').val() != '' && $('#GenderId_Old').val() != 0) {
        $('.PositionContainerOld').remove();
        gateway.add_GetDataByData($('#Url_MemberPosition').val(), { index: 0, model: 'Old', GenderId: $('#GenderId_Old').val() }, 'MemberPositionContainerOld');
    }
});

$('#btnAddPositionOld').on('click', function () {
    if ($('#GenderId_Old').val() == null || $('#GenderId_Old').val() == '' || $('#GenderId_Old').val() == 0) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Debe seleccionar un género antes.', 'warning');
    } else {
        var index = $('.PositionContainerOld').length;
        gateway.add_GetDataByData($('#Url_MemberPosition').val(), { index: index, model: 'Old', GenderId: $('#GenderId_Old').val(), MemberId: $('#MemberId_Old').val() }, 'MemberPositionContainerOld');
    }
});

$(document).on('click', 'a.removePositionOld', function () {
    $(this).parents('div.PositionContainerOld').remove();

    //Order
    var index = 0;
    $('.PositionContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberPositions[' + index + '].MemberPositionId');
        $(this).find('input[type=hidden]').attr('id', 'MemberPositions_MemberPositionId_' + index + '_Old');

        $(this).find('select').attr('name', 'Old.MemberPositions[' + index + '].PositionId');
        $(this).find('select').attr('id', 'MemberPositions_PositionId_' + index + '_Old');
        $(this).find('div.visualSelectContainer').attr('id', 'MemberPositions_PositionId_' + index + '_OldSelectDiv');

        index++;
    });

    materialSelects.init();
});

$('#btnAddSchoolYoung').on('click', function () {
    var index = $('.SchoolContainerYoung').length;
    gateway.add_GetDataByData($('#Url_MemberSchool').val(), { index: index, model: 'Young' }, 'MemberSchoolContainerYoung');
});

$(document).on('click', 'a.removeSchoolYoung', function () {
    $(this).parents('div.SchoolContainerYoung').remove();

    //Order
    var index = 0;
    $('.SchoolContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Young.MemberSchools[' + index + '].MemberSchoolId');
        $(this).find('input[type=hidden]').attr('id', 'MemberSchools_MemberSchoolId_' + index + '_Young');

        $(this).find('.BtnMemberSchoolSearchDialog').attr('data-SchoolSelectId', 'MemberSchools_SchoolId_' + index + '_Young');
        $(this).find('select[data=SchoolId]').attr('name', 'Young.MemberSchools[' + index + '].SchoolId');
        $(this).find('select[data=SchoolId]').attr('id', 'MemberSchools_SchoolId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=SchoolId]').attr('id', 'MemberSchools_SchoolId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=DegreeId]').attr('name', 'Young.MemberSchools[' + index + '].DegreeId');
        $(this).find('select[data=DegreeId]').attr('id', 'MemberSchools_DegreeId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=DegreeId]').attr('id', 'MemberSchools_DegreeId_' + index + '_YoungSelectDiv');

        $(this).find('select[data=TitleId]').attr('name', 'Young.MemberSchools[' + index + '].TitleId');
        $(this).find('select[data=TitleId]').attr('id', 'MemberSchools_TitleId_' + index + '_Young');
        $(this).find('div.visualSelectContainer[data=TitleId]').attr('id', 'MemberSchools_TitleId_' + index + '_YoungSelectDiv');

        $(this).find('input[type=text][data=StartDate]').attr('name', 'Young.MemberSchools[' + index + '].StartDate');
        $(this).find('input[type=text][data=StartDate]').attr('id', 'MemberSchools_StartDate_' + index + '_Young');
        $(this).find('input[type=text][data=StartDate]').attr('aria-controls', 'MemberSchools_StartDate_' + index + '_Young-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberSchools_StartDate_' + index + '_Young-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('label').attr('for', 'MemberSchools_StartDate_' + index + '_Young');

        $(this).find('input[type=text][data=EndDate]').attr('name', 'Young.MemberSchools[' + index + '].EndDate');
        $(this).find('input[type=text][data=EndDate]').attr('id', 'MemberSchools_EndDate_' + index + '_Young');
        $(this).find('input[type=text][data=EndDate]').attr('aria-controls', 'MemberSchools_EndDate_' + index + '_Young-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberSchools_EndDate_' + index + '_Young-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('label').attr('for', 'MemberSchools_EndDate_' + index + '_Young');

        index++;
    });

    materialSelects.init();
});

$('#btnAddSchoolOld').on('click', function () {
    var index = $('.SchoolContainerOld').length;
    gateway.add_GetDataByData($('#Url_MemberSchool').val(), { index: index, model: 'Old' }, 'MemberSchoolContainerOld');
});

$(document).on('click', 'a.removeSchoolOld', function () {
    $(this).parents('div.SchoolContainerOld').remove();

    //Order
    var index = 0;
    $('.SchoolContainerOld').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'Old.MemberSchools[' + index + '].MemberSchoolId');
        $(this).find('input[type=hidden]').attr('id', 'MemberSchools_MemberSchoolId_' + index + '_Old');

        $(this).find('.BtnMemberSchoolSearchDialog').attr('data-SchoolSelectId', 'MemberSchools_SchoolId_' + index + '_Old');
        $(this).find('select[data=SchoolId]').attr('name', 'Old.MemberSchools[' + index + '].SchoolId');
        $(this).find('select[data=SchoolId]').attr('id', 'MemberSchools_SchoolId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=SchoolId]').attr('id', 'MemberSchools_SchoolId_' + index + '_OldSelectDiv');

        $(this).find('select[data=DegreeId]').attr('name', 'Old.MemberSchools[' + index + '].DegreeId');
        $(this).find('select[data=DegreeId]').attr('id', 'MemberSchools_DegreeId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=DegreeId]').attr('id', 'MemberSchools_DegreeId_' + index + '_OldSelectDiv');

        $(this).find('select[data=TitleId]').attr('name', 'Old.MemberSchools[' + index + '].TitleId');
        $(this).find('select[data=TitleId]').attr('id', 'MemberSchools_TitleId_' + index + '_Old');
        $(this).find('div.visualSelectContainer[data=TitleId]').attr('id', 'MemberSchools_TitleId_' + index + '_OldSelectDiv');

        $(this).find('input[type=text][data=StartDate]').attr('name', 'Old.MemberSchools[' + index + '].StartDate');
        $(this).find('input[type=text][data=StartDate]').attr('id', 'MemberSchools_StartDate_' + index + '_Old');
        $(this).find('input[type=text][data=StartDate]').attr('aria-controls', 'MemberSchools_StartDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberSchools_StartDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=StartDate]').parents('.text-left').find('label').attr('for', 'MemberSchools_StartDate_' + index + '_Old');

        $(this).find('input[type=text][data=EndDate]').attr('name', 'Old.MemberSchools[' + index + '].EndDate');
        $(this).find('input[type=text][data=EndDate]').attr('id', 'MemberSchools_EndDate_' + index + '_Old');
        $(this).find('input[type=text][data=EndDate]').attr('aria-controls', 'MemberSchools_EndDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('p.mdc-text-field-helper-text').attr('id', 'MemberSchools_EndDate_' + index + '_Old-helper-text');
        $(this).find('input[type=text][data=EndDate]').parents('.text-left').find('label').attr('for', 'MemberSchools_EndDate_' + index + '_Old');

        index++;
    });

    materialSelects.init();
});

function ValidateMember_Old() {
    var resp = 0;

    resp += validationsForm.image('SendFile_Old', 'Photo_Old');
    resp += validationsForm.select('NationalityId_Old');
    resp += validationsForm.select('IDType_Old');
    resp += validationsForm.select('GenderId_Old');
    resp += validationsForm.select('PositionId_Old');
    resp += validationsForm.select('MemberStatusId_Old');
    resp += validationsForm.select('ProvinceId_Old');
    resp += validationsForm.select('CantonId_Old');
    resp += validationsForm.select('DistrictId_Old');

    //resp += validationsForm.select('ReligionId_Old');
    //resp += validationsForm.select('EducationLevelId_Old');
    //resp += validationsForm.select('ProfessionId_Old');

    if ($('#accept_Old').prop('checked')) {
        $('#accept_Old').parents('.text-right').removeClass('mdc-text-field--invalid');
    } else {
        $('#accept_Old').parents('.text-right').addClass('mdc-text-field--invalid');
        resp++;
    }

    $('#MemberPositionContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberKinshipContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberLanguageContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberCourseContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberWorkshopContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberActivityContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberNationalEventContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberInternationalEventContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberSchoolContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    $('#MemberOtherStudyContainerOld').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    resp += validationsForm.divContainers('containerForValidation_Old', 'containerTitleForValidation_Old', 'containerBodyForValidation_Old');

    if (resp > 0) alertFunctions.basicAlert($('#AlertTitle').val(), 'Faltan campos por completar.', 'warning');
    return resp == 0;
}

function OnSuccess_Member(res) {
    var objResp = JSON.parse(res);
    pageFunctions.showElement($('#preloader'));

    if (objResp.ResponseText != null && objResp.ResponseText != 0) {
        location.href = $('#Url_DetailsMember').val().replace('-1', objResp.ResponseText);
    } else {
        document.getElementById('DivToList').click();
    }
}

$(document).on('change', '#IDType_Tutor', function () {
    var elem = $(this);
    if ($(elem).val() == '' || $(elem).val() == '0' || $(elem).val() == $('#IDTypeBlank_Tutor').val()) {
        $('#IDNumber_Tutor').removeAttr('required');
        $('#IDNumber_Tutor').val('');
        $('#IDNumber_Tutor').attr('readonly', true);
        $('#IDNumber_Tutor').attr('disabled', true);
        $('#IDNumber_Tutor').parents('.mdc-text-field').removeClass('mdc-text-field--invalid');
        $('#IDNumber_Tutor').parents('.mdc-text-field').addClass('mdc-text-field--disabled');
    } else {
        $('#IDNumber_Tutor').attr('required', true);
        $('#IDNumber_Tutor').removeAttr('readonly');
        $('#IDNumber_Tutor').removeAttr('disabled');
        $('#IDNumber_Tutor').parents('.mdc-text-field').removeClass('mdc-text-field--disabled');
        regexInputs.IdentificationTypeMask('IDNumber_Tutor', $(elem).val())
    }
});

$('.social_network_selector_Tutor').on('click', function () {
    var index = $('.SocialNetworkContainerTutor').length;
    var type = $(this).attr('data');
    $(this).parent().addClass('hide');
    var containerParent = $(this).parents('[data-social_menu=social_menu_new_Tutor]');
    gateway.add_GetDataByData_Callback($('#Url_MemberSocialNetwork').val(), { index: index, type: type, model: 'Tutor' }, 'MemberSocialNetworkContainerTutor', function () {
        $('#social_menu_new_Tutor').removeClass('mdc-menu--open');
        $('#btnAddSocialNetworkTutor').removeClass('active');
        $('.SocialNetworkContainerTutor:last').find('input[type=text]').focus();

        $(containerParent).removeClass('social_menu_new_popup');
    });
});

function ValidateMember_Tutor() {
    var resp = 0;

    resp += validationsForm.image('SendFile_Tutor', 'Photo_Tutor');
    resp += validationsForm.select('GenderId_Tutor');
    resp += validationsForm.select('NationalityId_Tutor');
    resp += validationsForm.select('IDType_Tutor');

    resp += validationsForm.select('ProvinceId_Tutor');
    resp += validationsForm.select('CantonId_Tutor');
    resp += validationsForm.select('DistrictId_Tutor');

    resp += validationsForm.divContainers('containerForValidation_Tutor', 'containerTitleForValidation_Tutor', 'containerBodyForValidation_Tutor');
    return resp == 0;
}

function OnSuccess_MemberTutor(res) {
    res = JSON.parse(res)
    $('#MemberTutorContainerYoung').append(res.ResponseText);
    //Order
    var index = 0;
    $('.TutorContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden][data=MemberTutorId]').attr('name', 'Young.MemberTutors[' + index + '].MemberTutorId');
        $(this).find('input[type=hidden][data=MemberTutorId]').attr('id', 'MemberTutors_MemberTutorId_' + index + '_Young');

        $(this).find('input[type=hidden][data=TutorId]').attr('name', 'Young.MemberTutors[' + index + '].TutorId');
        $(this).find('input[type=hidden][data=TutorId]').attr('id', 'MemberTutors_TutorId_' + index + '_Young');

        index++;
    });

    gateway.getDataByData_Callback($('#Url_MemberTutorDialog').val(), { MemberId: 0 }, 'MemberTutorDialogContainer', function () {
        dialogTutor.close();
    });
}

$(document).on('click', 'a.removeTutorYoung', function () {
    $(this).parents('div.TutorContainerYoung').remove();

    //Order
    var index = 0;
    $('.TutorContainerYoung').each(function (index, item) {
        $(this).find('input[type=hidden][data=MemberTutorId]').attr('name', 'Young.MemberTutors[' + index + '].MemberTutorId');
        $(this).find('input[type=hidden][data=MemberTutorId]').attr('id', 'MemberTutors_MemberTutorId_' + index + '_Young');

        $(this).find('input[type=hidden][data=TutorId]').attr('name', 'Young.MemberTutors[' + index + '].TutorId');
        $(this).find('input[type=hidden][data=TutorId]').attr('id', 'MemberTutors_TutorId_' + index + '_Young');

        index++;
    });
});

$(document).on('change', '#ActiveMember', function () {
    var elem = $(this);
    if ($(elem).prop('checked')) {
        gateway.getDataByData($('#Url_MemberTutorDialog').val(), { MemberId: 0, Active: true }, 'MemberTutorDialogContainer');
    } else {
        gateway.getDataByData($('#Url_MemberTutorDialog').val(), { MemberId: 0, Active: false }, 'MemberTutorDialogContainer');
    }
});

$(document).on('change', '#NamesSurnames_Tutor', function () {
    var id = $(this).val();
    gateway.getDataByData_Callback($('#Url_MemberTutorDialog').val(), { MemberId: id, Active: true }, 'MemberTutorDialogContainer', function () {
        MemberSearchFunctions.loadValuesInButtons();
        init_select_location = true;
        $('#ProvinceId_Tutor').val($('#ProvinceId_hf_Tutor').val()).trigger('change');
    });
});

$(document).on('click', '#btnAddPhoneNumberTutor', function () {
    var index = $('.PhoneNumberContainerTutor').length;
    gateway.add_GetDataByData($('#Url_MemberPhoneNumber').val(), { index: index, model: 'Tutor' }, 'MemberPhoneNumberContainerTutor');
});

function AddMemberTutor() {
    if ($('#NamesSurnames_Tutor').val() == '' || $('#NamesSurnames_Tutor').val() == '0') {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Debe seleccionar un miembro primero.', 'warning');
        return;
    }
    gateway.add_GetDataByData_Callback($('#Url_MemberTutor').val(), { MemberId: $('#NamesSurnames_Tutor').val() }, 'MemberTutorContainerYoung', function () {
        //Order
        var index = 0;
        $('.TutorContainerYoung').each(function (index, item) {
            $(this).find('input[type=hidden][data=MemberTutorId]').attr('name', 'Young.MemberTutors[' + index + '].MemberTutorId');
            $(this).find('input[type=hidden][data=MemberTutorId]').attr('id', 'MemberTutors_MemberTutorId_' + index + '_Young');

            $(this).find('input[type=hidden][data=TutorId]').attr('name', 'Young.MemberTutors[' + index + '].TutorId');
            $(this).find('input[type=hidden][data=TutorId]').attr('id', 'MemberTutors_TutorId_' + index + '_Young');

            index++;
        });

        dialogTutor.close();
        gateway.getDataByData($('#Url_MemberTutorDialog').val(), { MemberId: 0 }, 'MemberTutorDialogContainer');
    });
}

$(document).on('mouseenter', 'label#StartDate_Young_clear, label#PromiseDate_Young_clear, label#EndDateWork_Young_clear, label#StartDate_Old_clear, label#PromiseDate_Old_clear, label#EndDateWork_Old_clear', function () {
    $(this).removeClass('icon_date');
    $(this).addClass('icon_close_3');
    $(this).addClass('pointer');
});
$(document).on('mouseleave', 'label#StartDate_Young_clear, label#PromiseDate_Young_clear, label#EndDateWork_Young_clear, label#StartDate_Old_clear, label#PromiseDate_Old_clear, label#EndDateWork_Old_clear', function () {
    $(this).removeClass('icon_close_3');
    $(this).removeClass('pointer');
    $(this).addClass('icon_date');
});
$(document).on('change', '#StartDate_Young, #PromiseDate_Young, #EndDateWork_Young, #StartDate_Old, #PromiseDate_Old, #EndDateWork_Old', function () {
    $(this).parents('.mdc-text-field').addClass('mdc-text-field--upgraded');
    $(this).parents('.mdc-text-field').find('.mdc-text-field__label').addClass('mdc-text-field__label--float-above');
});

$(document).on('click', 'label#StartDate_Young_clear', function () {
    $('#StartDate_Young').val('');
});
$(document).on('click', 'label#PromiseDate_Young_clear', function () {
    $('#PromiseDate_Young').val('');
});
$(document).on('click', 'label#EndDateWork_Young_clear', function () {
    $('#EndDateWork_Young').val('');
});
$(document).on('click', 'label#StartDate_Old_clear', function () {
    $('#StartDate_Old').val('');
});
$(document).on('click', 'label#PromiseDate_Old_clear', function () {
    $('#PromiseDate_Old').val('');
});
$(document).on('click', 'label#EndDateWork_Old_clear', function () {
    $('#EndDateWork_Old').val('');
});

$('#IsCurrentWork_Young').on('change', function () {
    if ($(this).is(':checked')) {
        $('#EndDateWork_Young').val('');
        $('#EndDateWork_Young').attr('readonly', true);
        $('#EndDateWork_Young').parent().addClass('mdc-text-field--disabled');
    } else {
        $('#EndDateWork_Young').val('');
        $('#EndDateWork_Young').removeAttr('readonly');
        $('#EndDateWork_Young').parent().removeClass('mdc-text-field--disabled');
    }
});
$('#IsCurrentWork_Old').on('change', function () {
    if ($(this).is(':checked')) {
        $('#EndDateWork_Old').val('');
        $('#EndDateWork_Old').attr('readonly', true);
        $('#EndDateWork_Old').parent().addClass('mdc-text-field--disabled');
    } else {
        $('#EndDateWork_Old').val('');
        $('#EndDateWork_Old').removeAttr('readonly');
        $('#EndDateWork_Old').parent().removeClass('mdc-text-field--disabled');
    }
});
