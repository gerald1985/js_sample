﻿$(function () {

});

$('#CategoryOptionContainer input[type=radio]').on('change', function () {

    if ($(this).attr('data-CategoryType') === 'Young') {
        $('#BirthDate').val($('#MaxDate_DefaultYoung').val());
        $('#BirthDate').bootstrapMaterialDatePicker('setMinDate', moment($('#MinDate_DefaultYoung').val(), "DD-MM-YYYY"));
        $('#BirthDate').bootstrapMaterialDatePicker('setMaxDate', moment($('#MaxDate_DefaultYoung').val(), "DD-MM-YYYY"));
    } else if ($(this).attr('data-CategoryType') === 'Old') {
        $('#BirthDate').val($('#MaxDate_DefaultOld').val());
        $('#BirthDate').bootstrapMaterialDatePicker('setMinDate', null);
        $('#BirthDate').bootstrapMaterialDatePicker('setMaxDate', moment($('#MaxDate_DefaultOld').val(), "DD-MM-YYYY"));
    }

    var model = $(this).val();
    gateway.getDataByData($('#Url_Position').val(), { MemberId: $('#MemberId_CC').val(), CategoryId: $('#CategoryId').val(), model: model, GenderId: $('#GenderId').val() }, 'PositionContainer');
});

function ValidateMemberOverstep() {
    var resp = 0;

    resp += validationsForm.select('PositionId');

    return resp == 0;
}

function OnSuccess_Overstep(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Registro actualizado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}
