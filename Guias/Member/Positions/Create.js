﻿$(function () {

});

$('#PositionTypeId_Young').on('change', function () {
    gateway.editEntity($('#Url_PositionType').val(), { PositionTypeId: parseInt($(this).val()), Type: $('#PositionTypeYoungId').val(), GenderId: $('#GenderId_Young').val() }, 'PositionTypeContainerYoung');
});

$('#GenderId_Young').on('change', function () {
    gateway.editEntity($('#Url_PositionType').val(), { PositionTypeId: ($('#PositionTypeId_Young').val() === '' ? 0 : $('#PositionTypeId_Young').val()), Type: $('#PositionTypeYoungId').val(), GenderId: parseInt($(this).val()) }, 'PositionTypeContainerYoung');
});

$('#PositionTypeId_Old').on('change', function () {
    gateway.editEntity($('#Url_PositionType').val(), { PositionTypeId: parseInt($(this).val()), Type: $('#PositionTypeOldId').val() }, 'PositionTypeContainerOld');
});

function ValidatePosition_Young() {
    var resp = 0;

    resp += validationsForm.select('PositionTypeId_Young');
    resp += validationsForm.select('GenderId_Young');

    return resp == 0;
}

function ValidatePosition_Old() {
    var resp = 0;

    resp += validationsForm.select('PositionTypeId_Old');
    resp += validationsForm.select('GenderId_Old');

    return resp == 0;
}

function OnSuccess_Position(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}