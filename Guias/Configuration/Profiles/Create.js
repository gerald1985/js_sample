﻿$(function () {

});

function ValidateProfile() {
    var resp = 0;
    resp += validationsForm.image('SendFile_Profile', 'Photo_Profile');

    if ($('#actualpass').val() == '' && ($('#newpass').val() == '' || $('#confirmpass').val() == '')) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Para actualizar la información del perfil es necesario ingresar la contaseña actual.', 'warning');
        resp++;
    } 

    return resp == 0;
}

function ValidateProfileSecurity() {
    var resp = 0;

    if ($('#actualpassSecurity').val() == '') {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Para actualizar la contraseña es necesario ingresar la contraseña actual.', 'warning');
        resp++;
    } else if ($('#newpass').val() == '' || $('#confirmpass').val() == '') {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Para actualizar la contraseña es necesario ingresar la contraseña nueva y la confirmación.', 'warning');
        resp++;
    } else if ($('#newpass').val() != $('#confirmpass').val()) {
        alertFunctions.basicAlert($('#AlertTitle').val(), 'Nuevas contraseñas no coinciden.', 'warning');
        resp++;
    }

    return resp == 0;
}

function OnSuccess_Profile() {
    pageFunctions.showElement($('#preloader'));
    document.getElementById('DivToList').click();
}