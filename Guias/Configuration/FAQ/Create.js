﻿$(function () {

});

function ToEditFaq(FaqId, Type) {
    gateway.editEntity($('#Url_ToEdit').val(), { FaqId: FaqId, FaqModuleId: Type}, 'ToAdd_Section')
}

function ValidateFaq() {
    var resp = 0;
    $('#TypeModule-helper-text').removeClass('c_red');

    if ($('#TypeModule').val() == '') {
        $('#TypeModule-helper-text').addClass('c_red');
        resp++;
    }

    return resp == 0;
}

function DeleteFaq(FaqId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                { FaqId: FaqId },
                'Aggregates_Section'
            );
        });
}
