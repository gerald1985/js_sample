﻿$(function () {

});

function ValidateUserRole() {
    var resp = 0;

    resp = ValidateContainer('UserRoleContainer', resp);
    if (resp > 0) alertFunctions.basicAlert($('#AlertTitle').val(), 'Existen roles iguales seleccionados.', 'warning');

    $('.RoleContainer').find('select').each(function (index, item) {
        resp += validationsForm.select($(item).attr('id'));
    });

    return resp == 0;
}

function OnSuccess_UserRole(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        document.getElementById('DivToList').click();
    });
}

$('#btnAddRole').on('click', function () {
    var index = $('.RoleContainer').length;
    gateway.add_GetDataByData($('#Url_UserRole').val(), { index: index }, 'UserRoleContainer');
});

$(document).on('click', 'a.removeRole', function () {
    $(this).parents('div.RoleContainer').remove();

    //Order
    var index = 0;
    $('.RoleContainer').each(function (index, item) {
        $(this).find('input[type=hidden]').attr('name', 'User.UserRoles[' + index + '].UserRoleId');
        $(this).find('input[type=hidden]').attr('id', 'UserRoles_UserRoleId_' + index);

        $(this).find('select').attr('name', 'User.UserRoles[' + index + '].RoleId');
        $(this).find('select').attr('id', 'UserRoles_RoleId_' + index);
        $(this).find('div.visualSelectContainer').attr('id', 'UserRoles_RoleId_' + index + 'SelectDiv');

        index++;
    });

    materialSelects.init();
});

function ValidateContainer(ContainerClassName, VarResp) {
    var list = [];
    $('#' + ContainerClassName).find('select').each(function (index, item) {
        $(list).each(function (ind, itm) {
            if ($(item).val() == itm) {
                VarResp++;
                $('#select' + $(item).attr('id')).addClass('mdc-text-field--invalid');
                return false;
            }
        });
        list.push($(item).val());
    });

    return VarResp;
}