﻿$(function () {
    
});

$('.baseTabBar').on('click', function () {
    var BaseCatalogId = $(this).attr('data-moduleId');
    var TabId = $(this).attr('data-rel');

    $('.base_catalog_tab').empty();

    gateway.getDataByData($('#Url_GetCatalog').val(), { BaseCatalogId: BaseCatalogId }, TabId);
});

$('.baseTabBar').on('click', function () {
    var tabId = $(this).attr('data-rel');

    $('.base_catalog_tab').removeClass('active');
    $('#' + tabId).addClass('active');
});

$('nav.body-menu a').on('click', function () {
    if ($(this).attr('href') !== null && $(this).attr('href') !== undefined) {
        pageFunctions.showElement($('#preloader'));
    }
});

function DeleteGeneralCatalog(BaseCatalogId) {
    var panelId = document.querySelector('.base_catalog_tab.active').id;

    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    ItemOf: $('#ItemOf').val(),
                    BaseCatalogId: BaseCatalogId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                panelId
            );
        });
}

function DeleteGeneralCatalogs() {
    var panelId = document.querySelector('.base_catalog_tab.active').id;

    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'GeneralCatalogs',
                panelId,
                panelId,
                {
                    ItemOf: $('#ItemOf').val(),
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}