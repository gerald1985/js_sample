﻿$(function () {

});

function ToEditAgeRange(AgeRangeId) {
    gateway.editEntity($('#Url_ToEdit').val(), { AgeRangeId: AgeRangeId }, 'ToAdd_Section')
}

function ValidateAgeRange() {
    var resp = 0;
    $('#MinMaxAge-helper-text').addClass('hide');

    var min = parseInt($('#MinimumAge').val());
    var max = parseInt($('#MaximumAge').val());

    if (min > max || min == max) {
        $('#MinMaxAge-helper-text').removeClass('hide');
        $('#rangesContainer').addClass('mdc-text-field--invalid');
        resp++;
    }

    return resp == 0;
}
