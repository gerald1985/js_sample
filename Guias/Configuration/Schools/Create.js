﻿var init_select_location = false;
$(function () {

    if ($('#SchoolId').val() != '' && $('#SchoolId').val() != '0') {
        init_select_location = true;
        $('#ProvinceId').val($('#ProvinceId_hf').val()).trigger('change');
    }
});

function ToEditSchool(SchoolId) {
    gateway.getDataByData_Callback($('#Url_ToEdit').val(), { SchoolId: SchoolId }, 'ToAdd_Section', null, true, function () {
        init_select_location = true;
        $('#ProvinceId').val($('#ProvinceId_hf').val()).trigger('change');
    });
}

$(document).on('change', '#ProvinceId', function () {
    var id = ($(this).val() === '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectCanton').val(), { ProvinceId: id }, 'CantonContainer', null, true, function () {
        if (init_select_location)
            $('#CantonId').val($('#CantonId_hf').val()).trigger('change');
        else
            $('#CantonId').trigger('change');
    });
});

$(document).on('change', '#CantonId', function () {
    var id = ($(this).val() === '' ? -1 : $(this).val());
    gateway.getDataByData_Callback($('#Url_SelectDistrict').val(), { CantonId: id }, 'DistrictContainer', function () {
        if (init_select_location) {
            $('#DistrictId').val($('#DistrictId_hf').val()).trigger('change');
            init_select_location = false;
        }
        else
            $('#DistrictId').trigger('change');
    });
});