﻿$(function () {
    
});

function DeleteSchool(SchoolId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    SchoolId: SchoolId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                'GridSchools'
            );
        });
}

function DeleteSchools() {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'Schools',
                'GridSchools',
                'GridSchools',
                {
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}