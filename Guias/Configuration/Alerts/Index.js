﻿$(function () {
    
});

function OnSuccess_Alerts(res) {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", "success", function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('BtnToAlerts').click();
    });
}

$('.Customize_Check_Container input:checkbox').on('change', function () {
    if ($(this).prop('checked')) {
        if ($('.Customize_Container input:checkbox:checked').length == 0) {
            $('.Customize_Container input:checkbox').prop('checked', true);
        }
    }
});

$('.Customize_Container input:checkbox').on('change', function () {
    if ($('.Customize_Container input:checkbox:checked').length == 0) {
        $('.Customize_Check_Container input:checkbox').not('[disabled]').prop('checked', false);
    }
});