﻿var RequestQualification = null;
$(function () {
    RequestQualification = parseFloat($('#rate_user').attr('data-rateyo_rate'));

    $('#rate_user').rateYo({
        rating: $('#rate_user').attr('data-rateyo_rate'),
        fullStar: true,
        starWidth: "15px"
    }).on('rateyo.set', function (e, data) {
        if (data.rating != RequestQualification) {
            pageFunctions.showElement($('#preloader'));
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: $('#Url_SaveQualification').val(),
                data: JSON.stringify({ RequestId: $('#RequestId').val(), Qualification: data.rating }),
                success: function (resp) {
                    $('#rate_user_visual').html(data.rating);
                    RequestQualification = data.rating;
                },
                error: function (resp) {
                    console.log(resp);
                    $("#rate_user").rateYo("option", "rating", RequestQualification);
                },
                complete: function () {
                    pageFunctions.hideElement($('#preloader'));
                }
            });
        }
    });

    document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
});

function downloadFile(url) {
    imagesInputs.downloadFiles(url);
}

$('#BtnSendComment').on('click', function () {
    gateway.saveEntity_Callback($('#Url_SaveComment').val(),
        {
            comment: {
                RequestCommentId: 0,
                RequestId: $('#RequestId').val(),
                Comment: $('#comment').val(),
                Commentator: $('#CommentatorId').val(),
            }
        },
        function (resp) {
            $('#RequestCommentContainer').html(resp.response);
            $('#comment').val('');
            document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
        });
});

function OnSuccess_Request() {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), 'Guardado exitosamente.', 'success', function () {
        pageFunctions.showElement($('#preloader'));
        location.href = $('#Url_ToDetails').val();
    });
}

$('#BtnPdfExport').on('click', function () {
    window.open($('#Url_ExportRequest').val(), '_blank');
});