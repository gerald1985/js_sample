﻿$(function () {
    MemberSearchFunctions.init();

    $('.rate_lm').each(function (index, item) {
        var rate = $(this).attr('data-rateyo_rate');
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: "15px",
            readOnly: true,
        });
    });
});

function downloadFile(url) {
    imagesInputs.downloadFiles(url);
}

function ValidateRequest() {
    var resp = 0;
    resp += validationsForm.select('AssignedTo');

    return resp == 0;
}

function OnSuccess_Request() {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}

$('#BtnPdfExport').on('click', function () {
    window.open($('#Url_ExportRequest').val(), '_blank');
});