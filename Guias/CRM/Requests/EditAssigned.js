﻿var init_select_location = false
$(function () {
    if ($('#RequestId').val() != '' && $('#RequestId').val() != 0) {
        init_select_location = true;
        $('#TopicTypeId').val($('#TopicTypeId_hf').val()).trigger('change');
    }

    $('.rate_lm').each(function (index, item) {
        var rate = $(this).attr('data-rateyo_rate');
        $(this).rateYo({
            rating: rate,
            halfStar: true,
            starWidth: "15px",
            readOnly: true,
        });
    });

    document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
});

function downloadFile(url) {
    imagesInputs.downloadFiles(url);
}

$('#TopicTypeId').on('change', function () {
    gateway.getDataByData_Callback($('#Url_RequestTopic').val(), { TopicTypeId: $('#TopicTypeId').val() }, 'RequestTopicContainer', function () {
        if (init_select_location) {
            init_select_location = false;
            $('#TopicId').val($('#TopicId_hf').val()).trigger('change');
        }
    });
});

function ValidateRequest() {
    var resp = 0;
    resp += validationsForm.select('RequestStatusId');
    resp += validationsForm.select('PriorityId');
    resp += validationsForm.select('TopicId');

    return resp == 0;
}

function OnSuccess_Request() {
    pageFunctions.hideElement($('#preloader'));
    alertFunctions.callBackAlert($('#AlertTitle').val(), "Operación realizada con éxito", 'success', function () {
        pageFunctions.showElement($('#preloader'));
        document.getElementById('DivToList').click();
    });
}

$('#BtnSendComment').on('click', function () {
    gateway.saveEntity_Callback($('#Url_SaveComment').val(),
        {
            comment: {
                RequestCommentId: 0,
                RequestId: $('#RequestId').val(),
                Comment: $('#comment').val(),
                Commentator: $('#CommentatorId').val(),
            }
        },
        function (resp) {
            $('#RequestCommentContainer').html(resp.response);
            $('#comment').val('');
            document.getElementById('RequestCommentContainer').scrollTop = document.getElementById('RequestCommentContainer').scrollHeight;
        });
});

$('#RequestStatusId').on('change', function () {
    if ($(this).val() == $('#StatusCanceledId').val()) {
        $('#CommentContainer').removeClass('hide');
        $('#QualifyingComment').attr('required', true);
        $('#QualifyingComment').val('');
    } else {
        $('#CommentContainer').addClass('hide');
        $('#QualifyingComment').removeAttr('required');
        $('#QualifyingComment').val('');
    }
});

$('#BtnPdfExport').on('click', function () {
    window.open($('#Url_ExportRequest').val(), '_blank');
});
