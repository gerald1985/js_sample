﻿$(function () {
    
});

function DeletePriority(PriorityId) {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntity(
                $('#Url_Delete').val(),
                {
                    PriorityId: PriorityId,
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                },
                'GridPriorities'
            );
        });
}

function DeletePriorities() {
    alertFunctions.questionAlert(
        $('#AlertTitle').val(),
        $('#VerificationText').val(),
        function () {
            gateway.deleteEntitiesPag(
                $('#Url_Delete').val(),
                'Priorities',
                'GridPriorities',
                'GridPriorities',
                {
                    Search: $('#inputGridSearch').val(),
                    Number: parseInt($('.cap.current').attr('data'))
                }
            );
        });
}
