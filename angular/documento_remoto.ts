import { Component, ViewChild, Renderer, ElementRef, ViewChildren } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

import { LeybookApiProvider } from '../../providers/leybook-api/leybook-api';
import { ConfiguracionService } from '../../providers/configuracion/configuracion.service'
import * as Mark from 'mark.js';
import * as scrollElmIntoView from 'scroll-into-view';


@IonicPage({
  name: 'doc_remoto',
  segment: 'doc_remoto/:id',
  defaultHistory: ['HomePage']
})
@Component({
  selector: 'page-documento-remoto',
  templateUrl: 'documento_remoto.html',
})
export class DocumentoRemotoPage {
  ley: Object;

  markInstance: any;
  markResults:any = [];
  currMarkIndx = -1;
  stringToSearch: string;
  showSearchBar: boolean = false;
  @ViewChild('contenido')
  contenidoElement: ElementRef;

  @ViewChildren('[data-markjs="true"]') marks;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: LeybookApiProvider,
    public platform: Platform,
    public configObj: ConfiguracionService,
    private renderer: Renderer,
    private elementRef: ElementRef
  ) {
  }

  ionViewDidLoad() {
    const id = this.navParams.get('id');
    this.api.getLey(id, data => {
      this.ley = data;
    });
  }

  cargarPDF($event, leyId) {
    $event.preventDefault();
    this.api.abrirPDF(leyId);
  }

  onEnter(value:string) {
    this.stringToSearch = value;
    this.searchString(this.stringToSearch);
  }

  toggleSearchBar() {
    this.showSearchBar = !this.showSearchBar;
  }

  closeBar() {
    this.showSearchBar = false;
    this.clearMarkedMatches();
    this.stringToSearch = null;
  }

  searchString(value){
    // Search entered string from search bar
    // Clear previous marked searches if any
    this.clearMarkedMatches();

    // Create variable for target(s) container
    var rootContentNode = this.contenidoElement.nativeElement; //document.getElementById('contenido');
    var searchStr = value.trim();// Current entered string

    // Create Mark instance (using JavaScript object array instead of jQuery object array)
    this.markInstance = new Mark(rootContentNode);
    // Supply the string to search with options
    this.markInstance.mark(searchStr);

    // Find array of marked elements
    this.markResults = rootContentNode.querySelectorAll('mark');
    this.currMarkIndx = -1; // Reset the index counter

    this.showCurrentMark('next');
  }

  cleanDataMembers() {
    if (this.markInstance) {
      this.markInstance.unmark();
    }
    this.markResults = [];
    this.currMarkIndx = -1;
  }

  clearMarkedMatches(){
    this.cleanDataMembers();
  }

  showCurrentMark(direction){
    // Update index to highlight current mark
    if(this.markResults.length){
      if(direction == 'next'){
        if(this.currMarkIndx == -1){
          this.currMarkIndx = 0;
        }
        else{
          this.currMarkIndx == this.markResults.length - 1 ? this.currMarkIndx = 0 : this.currMarkIndx++;
        }
      }
      else if(direction == 'prev'){
        if(this.currMarkIndx == -1){
          this.currMarkIndx = this.markResults.length - 1;
        }
        else{
          this.currMarkIndx == 0 ? this.currMarkIndx = this.markResults.length - 1 : this.currMarkIndx--;
        }
      }

      this.scrollToSearched();
    }
    else{
      console.log('No result found.');
    }
  }

  scrollToSearched(){
    // Scroll and highlight mark
    if(this.markResults.length){
      var position,
          currentMark = this.markResults[this.currMarkIndx];

      // Remove "current" class from all elements
      [].forEach.call(document.querySelectorAll('.current'), function(el) {
        el.classList.remove("current");
      });

      if(currentMark){
        currentMark.classList.add('current');
        // Scroll to currently higligted search
        scrollElmIntoView(currentMark, { time: 250 });
      }
    }
    else{
      console.log('No result found.');
    }
  }

  printCountLabel() {
    const labelToPrint = `${this.currMarkIndx + 1}/${this.markResults.length}`;
    return labelToPrint;
  }

  ionViewDidLeave() {
    this.closeBar();
  }
}
