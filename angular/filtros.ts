import { Injectable } from '@angular/core';

/*
* El objetivo de este servicio es tener un singleton 
* donde compartir los datos de los filtros seleccionados en toda la aplicación
*/
@Injectable()
export class FiltrosProvider {

  terminos: string;
  materia: string;
  rango: string;
  fechaPublicacionInicio: string;
  fechaPublicacionFin: string;
  fechaAprobacionInicio: string;
  fechaAprobacionFin: string;
  pagina: number;

  constructor() {
  }

  setTerminos(terminos) {
    this.terminos = terminos;
  }

  setMateria(materia) {
    this.materia = materia;
  }

  setRango(rango) {
    this.rango = rango;
  }

  setFechaPublicacionInicio(fechaInicio) {
    this.fechaPublicacionInicio = fechaInicio;
  }

  setFechaPublicacionFin(fechaFin) {
    this.fechaPublicacionFin = fechaFin;
  }

  setFechaAprobacionInicio(fechaInicio) {
    this.fechaAprobacionInicio = fechaInicio;
  }

  setFechaAprobacionFin(fechaFin) {
    this.fechaAprobacionFin = fechaFin;
  }

  setPagina(pagina) {
    this.pagina = pagina;
  }

  extraerFiltros() {
    const filtros = {};
    if (this.terminos) {
      filtros['q'] = this.terminos;
    }
    if (this.materia) {
      filtros['materia'] = this.materia;
    }
    if (this.rango) {
      filtros['rango'] = this.rango;
    }
    if (this.fechaPublicacionInicio) {
      filtros['fecha_publicacion_desde'] = this.fechaPublicacionInicio;
    }
    if (this.fechaPublicacionFin) {
      filtros['fecha_publicacion_hasta'] = this.fechaPublicacionFin;
    }
    if (this.fechaAprobacionInicio) {
      filtros['fecha_aprobacion_desde'] = this.fechaAprobacionInicio;
    }
    if (this.fechaAprobacionFin) {
      filtros['fecha_aprobacion_hasta'] = this.fechaAprobacionFin;
    }
    if (this.pagina) {
      filtros['page'] = this.pagina;
    }
    return filtros;
  }
}
