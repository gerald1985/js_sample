import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';

import { LeybookApiProvider } from '../providers/leybook-api/leybook-api';
import { FiltrosProvider } from '../providers/filtros/filtros';

@Component({
  selector: 'search-bar',
  templateUrl: 'search_bar.html',
})
export class SearchBar {
  terminos: string;
  constructor(private navCtrl: NavController,
    private api: LeybookApiProvider,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public filtros: FiltrosProvider) {

    if (this.filtros.terminos) {
      this.terminos = this.filtros.terminos;
    }
  }

  onSearch() {
    const filtros = this.filtros.extraerFiltros();
    if (this.terminos) {
      filtros['q'] = this.terminos;
      this.filtros.setTerminos(this.terminos);

      this.api.getResultados(filtros,
        data => {
          this.navCtrl.push('ResultadosPage', { resultados: data });
        }
      );
    }
  }

  showFilterModal() {
    const filterModal = this.modalCtrl.create('FiltroModalPage', {terminos: this.terminos});
    filterModal.present();
  }
}
