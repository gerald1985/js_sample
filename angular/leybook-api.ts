import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LoadingController, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/toPromise';

import { ConfiguracionService } from '../configuracion/configuracion.service'
import handleDocumentWithURL from 'cordova-plugin-document-handler/www/DocumentHandler';

@Injectable()
export class LeybookApiProvider {
  constructor(public http: HttpClient,
      public configObj: ConfiguracionService,
      public loadingCtrl: LoadingController,
      private toastCtrl: ToastController) {
  }

  private getRequest(apiUrl, successCallback = undefined, errorCallback = undefined, parametros = undefined) {
    console.log(`Consuming ${this.configObj.API_ENDPOINT}`, parametros);
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    let toast = this.toastCtrl.create({
      message: 'Error: No se pudo cargar la información',
      duration: 3000,
      position: 'bottom'
    });
    loading.present();
    this.http.get(apiUrl, parametros)
    .toPromise()
    .then(data => {
      loading.dismiss();
      if (successCallback) {
        successCallback(data);
      }
    }).catch(error => {
      loading.dismiss();
      if (errorCallback) {
        errorCallback();
      }
      toast.present();
    });
  }

  getResultados(terminos: any, callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/search.json`, callback, errorCallback, { params: terminos });
  }

  getAyuda(callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/ayuda.json`, callback, errorCallback);
  }

  getFaq(callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/faq.json`, callback, errorCallback);
  }

  getActualizaciones(year: string = null, callback: Function = null, errorCallback: Function = null) {
    let parametros = {};
    if (year) {
      parametros = { year: year }
    }
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/actualizaciones.json`, callback, errorCallback, { params: parametros });
  }

  getUltimaActualizacion(): Observable<any> {
    return this.http.get(`${this.configObj.API_ENDPOINT}/actualizaciones/ultima.json`);
  }

  getLey(id, callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/doc/${id}.json`, callback, errorCallback);
  }

  getNovedades(callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/novedades.json`, callback, errorCallback);
  }

  getAcerca(callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/acerca.json`, callback, errorCallback);
  }

  getColaborar(callback: Function = null, errorCallback: Function = null) {
    this.getRequest(`${this.configObj.API_ENDPOINT}/servicios/colaborar.json`, callback, errorCallback);
  }

  getMaterias(): Observable<any> {
    return this.http.get(`${this.configObj.API_ENDPOINT}/materia.json`);
  }

  getRangos(): Observable<any> {
    return this.http.get(`${this.configObj.API_ENDPOINT}/rango.json`);
  }

  abrirPDF(leyId: Number): void {
    try {
      handleDocumentWithURL(
        () => console.log("El archivo fue descargado exitosamente"),
        (error) => console.log("ERROR! descargando documento: " + error.toString()),
        `${this.configObj.API_ENDPOINT}/doc/${leyId}.pdf`
      );
    } catch(e) {
      console.log("Error al invocar plugin DocumentHandler", e);
    }
  }
}
