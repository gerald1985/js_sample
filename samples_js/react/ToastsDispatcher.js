const { Map } = require('immutable')
const toastNamespace = 'toast-alert'
const { PropTypes } = require('prop-types')
const React = require('react')
const { Grid, Row } = require('react-bootstrap')
const { connect } = require('react-redux')
const { withRouter } = require('react-router')
const { ToastContainer, ToastMessage } = require('react-toastr')

const INITIALIZE_ALERTS = `${toastNamespace}.INITIALIZE_ALERTS`
const SHOW_ALERT = `${toastNamespace}.SHOW_ALERT`
const { hasLocationChanged } = require('utilities/utils')

// Use the animation mixin as implementation for transitions and animations
const ToastMessageFactory = React.createFactory(ToastMessage.animation)

const TOAST_INFO = 'info'
const TOAST_WARNING = 'warning'
const TOAST_SUCCESS = 'success'
const TOAST_ERROR = 'error'

/* This component only receives the last alert thrown
 * the queue of alerts is not accessible and is only handled internally by the ToastContainer component
 * */
class ToastsDispatcher extends React.Component {
  static propTypes =
  { currentAlert: PropTypes.shape(
    { alertId: PropTypes.number
      , alertType: PropTypes.string
      , title: PropTypes.string
      , message: PropTypes.string
    })
    , location: PropTypes.object
  }

  componentDidUpdate (prevProps, prevState) {
    const oldAlert = prevProps.currentAlert
    const currentAlert = this.props.currentAlert
    if (this.clearToastsFlag) {
      this.clearAlerts()
    }
    if (this.createAlertFlag) {
      this.tossAlert(currentAlert.alertType, currentAlert.title, currentAlert.message)
    }
    this.clearToastsFlag = false
    this.createAlertFlag = false
  }

  componentWillReceiveProps (nextProps) {
    const previousLocation = this.props.location
    const currentLocation = nextProps.location

    if (hasLocationChanged(previousLocation, currentLocation)) {
      this.props.initializeAlerts()
      this.clearToastsFlag = true
    }
    const oldAlert = this.props.currentAlert
    const currentAlert = nextProps.currentAlert

    if (typeof currentAlert !== 'undefined') {
      if (typeof oldAlert !== 'undefined' && (currentAlert.alertId > 0 && oldAlert.alertId !== currentAlert.alertId)) {
        this.createAlertFlag = true
      }
    }
  }

  getToastConfig = () => {
    return {
      timeOut: 86400000 // set one day since setting 0 (forever) makes clear() to not work
      , closeButton: true
      , showAnimation: 'animated fadeInDown'
      , hideAnimation: 'animated fadeOutUp'
    }
  }
  clearAlerts = () => {
    this.alertsContainer.clear()
  }
  tossWarningAlert = (title, message) => {
    this.alertsContainer.warning(title, message, this.getToastConfig())
  }
  tossInfoAlert = (title, message) => {
    this.alertsContainer.info(title, message, this.getToastConfig())
  }
  tossSuccessAlert = (title, message) => {
    this.alertsContainer.success(title, message, this.getToastConfig())
  }
  tossErrorAlert = (title, message) => {
    this.alertsContainer.error(title, message, this.getToastConfig())
  }
  tossAlert = (type, title, message) => {
    switch (type) {
      case TOAST_WARNING: {
        this.tossWarningAlert(title, message)
        break
      }
      case TOAST_INFO: {
        this.tossInfoAlert(title, message)
        break
      }
      case TOAST_SUCCESS: {
        this.tossSuccessAlert(title, message)
        break
      }
      case TOAST_ERROR: {
        this.tossErrorAlert(title, message)
      }
    }
  }

  render () {
    return <Grid>
      <Row className="row-toasts">
        <ToastContainer
          toastMessageFactory={ToastMessageFactory}
          ref={toastContainer => { this.alertsContainer = toastContainer }}
          className="toast-top-center"
          preventDuplicates={false}
        />
      </Row>
    </Grid>
  }
}

const initializeAlerts = () => {
  return { type: INITIALIZE_ALERTS }
}

const dispatchAlert = (alertType, title, message) => {
  const result =
    { type: SHOW_ALERT
    , payload:
      { alertType
        , title
        , message
      }
    }
  return result
}

const reducer = (state = Map(), action) => {
  switch (action.type) {
  case INITIALIZE_ALERTS:
    return state.set('currentAlert'
      , Map({ alertId: 0
         , alertType: null
         , title: null
         , message: null
       })
     )
  case SHOW_ALERT:
    let prevAlertId = state.getIn(['currentAlert', 'alertId'])
    return state.set('currentAlert', Map({alertId: prevAlertId + 1, ...action.payload}))
  default:
    return state
  }
}

const mapStateToProps = (state) => {
  return state.get(toastNamespace).toJS()
}

const combinedReducers =
{ [toastNamespace]: reducer
}

const ToastsDispatcherContainer = withRouter(connect(mapStateToProps, {initializeAlerts})(ToastsDispatcher))

module.exports =
{ 'actions': {
    INITIALIZE_ALERTS
    , SHOW_ALERT
  }
  , actionCreators: {
    dispatchAlert
    , initializeAlerts
  }
  , combinedReducers
  , ToastsDispatcher
  , ToastsDispatcherContainer
  , SHOW_ALERT
  , TOAST_INFO
  , TOAST_WARNING
  , TOAST_SUCCESS
  , TOAST_ERROR
}
