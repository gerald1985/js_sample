const { PropTypes } = require('prop-types')
const React = require('react')
const { CompareRow } = require('./CompareRow')

class CompareTable extends React.Component {
  static propTypes =
    { addToCart: PropTypes.func.isRequired
    , productsToCompare: PropTypes.array.isRequired
    , metrics: PropTypes.array.isRequired
    }

  constructor(props){
    super(props)

    this.generateMetricsTableRows = this.generateMetricsTableRows.bind(this)
    this.generateProductsTableRows = this.generateProductsTableRows.bind(this)
  }

  generateMetricsTableRows() {
    return this.props.metrics.map((metric, index) => {
      const metricKey = `metric-${index}`
      if(metric.attribute === 'introductoryPrice') {
        return <tr key={metricKey} className='price-row'><td>{metric.label}</td></tr>
      } else {
        return <tr key={metricKey} className='metric-rows'><td>{metric.label}</td></tr>
      }

    })
  }

  generateProductsTableRows() {
    if (this.props.productsToCompare.length) {
      return this.props.metrics.map((metric, index) => {
        const rowKey = `product-row-${index}`
        if(metric.attribute === 'introductoryPrice') {
          return <CompareRow key={rowKey} rowClass={'price-row'} cellClass={'test-gradient'} products={this.props.productsToCompare} rowLabel={metric} rowType='column' />
        } else {
          return <CompareRow key={rowKey} products={this.props.productsToCompare} rowLabel={metric} rowType='column' />
        }
      })
    }
  }

  render() {
    return (
      <div className='compare-grid-container row no-gutter'>
        <div className='compare-grid-metrics-table-container col3'>
          <table>
            <thead>
              <tr>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {
                this.generateMetricsTableRows()
              }
              <tr>
                <td></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className='compare-grid-products-table-container col-9'>
          <table>
            <thead>
              <CompareRow products={this.props.productsToCompare} rowLabel={{'attribute': 'name', 'label': 'Product Name'}} rowType='header'/>
            </thead>
            <tbody>
              {
                this.generateProductsTableRows()
              }
              <CompareRow products={this.props.productsToCompare} rowLabel={{'attribute': 'isAvailable', 'label': 'Add To Cart'}} rowType='button' buttonCallBack={this.props.addToCart} />
            </tbody>
          </table>
        </div>
      </div>
    )
  }
}

module.exports =
  { CompareTable
  }
