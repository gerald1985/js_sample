const { fromJS, Record } = require('immutable')
const { PropTypes } = require('prop-types')
const React = require('react')
const { connect } = require('react-redux')

const formActions = require('actions/BaseFormActions')
const baseForm = require('components/forms/BaseForm')
const { EFieldContainer, EFieldName } = require('components/fields/EField')
const { validationRuleTypes } = require('utilities/FormFieldValidation')
const selectors = require('selectors')
const inputFields = require('utilities/AvailableInputFields')

class AccountForm extends React.Component {
  static propTypes =
    { componentId: PropTypes.string.isRequired
    }

  formatDate(date) {
    if (!date || 'function' !== typeof date.slice) {
      return ''
    } else {
      const year = date.slice(0,4)
      const month = date.slice(4,6)
      const day = date.slice(6)
      if (!year || !month || !day) {
        return ''
      } else {
        const formatted = `${month}-${day}-${year}`
        return formatted
      }
    }
  }

  primaryAccount() {
    const containerId = this.props.componentId
    const dateOfBirth = this.formatDate(this.props.form.dateOfBirth)

    return (
      <div className='form-container' style={{'width': '100%'}}>
        <div className='row'>
          <div className='form-input-container col-xs-12 col-sm-3 col-md-3 col-lg-3'>
            <EFieldContainer
              requiredField={true}
              type={EFieldName.SelectField}
              placeholder="select"
              name={inputFields.SALUTATION}
              containerId={containerId}
              defaultValue={'default'}
              value={this.props.form.customerNamePrefix ? this.props.form.customerNamePrefix : 'default'}
            >
              <option value='default' disabled>Salutation</option>
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </EFieldContainer>
          </div>
          <div className='form-input-container col-xs-12 col-sm-4 col-md-4 col-lg-4'>
            <EFieldContainer
              limitTextCharacters={34}
              requiredField={true}
              type={EFieldName.TextField}
              placeholder="First Name"
              name={inputFields.FIRST_NAME}
              containerId={containerId}
              defaultValue={this.props.form.customerNameFirst}
              value={this.props.form.customerNameFirst}
            />
          </div>
          <div className='form-input-container col-xs-12 col-sm-5 col-md-5 col-lg-5'>
            <EFieldContainer
              limitTextCharacters={30}
              requiredField={true}
              type={EFieldName.TextField}
              placeholder="Last Name"
              name={inputFields.LAST_NAME}
              containerId={containerId}
              defaultValue={this.props.form.customerNameLast}
              value={this.props.form.customerNameLast}
            />
          </div>
        </div>
        <div className='row'>
          <div className='form-input-container col-xs-12 col-sm-4 col-md-4 col-lg-4'>
            <EFieldContainer
              type={EFieldName.PhoneNumberField}
              requiredField={true}
              placeholder="Phone Number"
              name={inputFields.PHONE_NUMBER}
              containerId={containerId}
              defaultValue={this.props.form.phoneNumber}
              value={this.props.form.phoneNumber}
            />
          </div>
          <div className='form-input-container col-xs-12 col-sm-4 col-md-4 col-lg-4'>
            <EFieldContainer
              type={EFieldName.DateOfBirthField}
              requiredField={true}
              placeholder="Date Of Birth (MM-DD-YYYY)"
              name={inputFields.DATE_OF_BIRTH}
              containerId={containerId}
              defaultValue={dateOfBirth}
              value={dateOfBirth}
            />
          </div>
          <div className='form-input-container col-xs-12 col-sm-4 col-md-4 col-lg-4'>
            <EFieldContainer
              requiredField={true}
              type={EFieldName.SocialSecurityField}
              placeholder="Social Security Number"
              name={inputFields.SOCIAL_SECURITY_NUMBER}
              containerId={containerId}
            />
          </div>
        </div>
        <div className='row'>
          <div className='form-input-container color col-xs-12 col-sm-12 col-md-12 col-lg-12'>
            <EFieldContainer
              requiredField={true}
              type={EFieldName.EmailField}
              placeholder="Email Address"
              name={inputFields.EMAIL}
              containerId={containerId}
              defaultValue={this.props.form.emailAddress}
              value={this.props.form.emailAddress}
            />
          </div>
        </div>
      </div>
    )
  }

  secondaryAccount() {
    const containerId = this.props.componentId

    return (
       <div className='form-container' style={{'width': '100%'}}>
        <div className='row'>
          <div className='form-input-container col-xs-12 col-sm-3 col-md-3 col-lg-3'>
            <EFieldContainer
              requiredField={true}
              type={EFieldName.SelectField}
              placeholder="select"
              name={inputFields.SALUTATION}
              containerId={containerId}
              defaultValue={'default'}
              value={this.props.form.otherAuthNamePrefix ? this.props.form.otherAuthNamePrefix : 'default'}
            >
              <option value='default' disabled>Salutation</option>
              <option value="Mr.">Mr.</option>
              <option value="Mrs.">Mrs.</option>
              <option value="Ms.">Ms.</option>
            </EFieldContainer>
          </div>
          <div className='form-input-container color col-xs-12 col-sm-4 col-md-4 col-lg-4'>
            <EFieldContainer
              limitTextCharacters={10}
              requiredField={true}
              type={EFieldName.TextField}
              placeholder="First Name"
              name={inputFields.FIRST_NAME}
              containerId={containerId}
              defaultValue={this.props.form.otherAuthNameFirst}
              value={this.props.form.otherAuthNameFirst}
            />
          </div>
          <div className='form-input-container color col-xs-12 col-sm-5 col-md-5 col-lg-5'>
            <EFieldContainer
              limitTextCharacters={9}
              requiredField={true}
              type={EFieldName.TextField}
              placeholder="Last Name"
              name={inputFields.LAST_NAME}
              containerId={containerId}
              defaultValue={this.props.form.otherAuthNameLast}
              value={this.props.form.otherAuthNameLast}
            />
          </div>
        </div>
        <div className='row'>
          <div className='form-input-container col-xs-12 col-sm-3 col-md-3 col-lg-3'>
            <EFieldContainer
              requiredField={true}
              type={EFieldName.SelectField}
              placeholder="select"
              name={inputFields.RELATIONSHIP}
              containerId={containerId}
              defaultValue={'default'}
              value={this.props.form.otherAuthType ? this.props.form.otherAuthType : 'default'}
            >
              <option value='default' disabled>Relationship to Primary</option>
              <option value="family-member">Family Member</option>
              <option value="friend">Friend</option>
            </EFieldContainer>
          </div>
        </div>
       </div>
    )
  }

  render () {
    const hints = [] // place tooltips { key: value } array
    if(this.props.form) {
      return (
        <baseForm.BaseFormContainer
          componentId={this.props.componentId}
          className=""
          hints={hints}
        >
          {
            this.props.isPrimaryAccountForm
            ? this.primaryAccount()
            : this.secondaryAccount()
          }
        </baseForm.BaseFormContainer>
      )
    } else {
      return null
    }

  }
}

//Selectors
const PRIMARY_ACCOUNT_FIELDS = fromJS(
  [ inputFields.SALUTATION
  , inputFields.PHONE_NUMBER
  , inputFields.DATE_OF_BIRTH
  , inputFields.EMAIL
  , inputFields.FIRST_NAME
  , inputFields.LAST_NAME
  , inputFields.SOCIAL_SECURITY_NUMBER
  ])

const ADDITIONAL_ACCOUNT_FIELDS = fromJS(
  [ inputFields.SALUTATION
  , inputFields.RELATIONSHIP
  , inputFields.FIRST_NAME
  , inputFields.LAST_NAME
  ])

const isAccountFormValid = (state, componentId, isPrimaryAccount) => {
  const form = baseForm.selectors.getFormFromState(state, componentId)

  if (form) {
    const fields = isPrimaryAccount ? PRIMARY_ACCOUNT_FIELDS : ADDITIONAL_ACCOUNT_FIELDS
    const formValid = fields.every(f => form.getIn([f, 'validation', 'isValid']))

    return formValid
  } else {
    return undefined
  }
}

const mapStateToProps = (state, ownProps) => {
  const cart = selectors.selectCart(state)

  return { form: cart }
}

const dispatchProps =
{ updateFormField: formActions.updateFormField
}

const AccountFormContainer = connect(mapStateToProps, dispatchProps)(AccountForm)

module.exports =
  { AccountForm
  , AccountFormContainer
  , mapStateToProps
  , selectors: { isAccountFormValid }
  }
