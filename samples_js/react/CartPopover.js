const { List } = require('immutable')
const { PropTypes } = require('prop-types')
const React = require('react')
const { Button } = require('react-bootstrap')
const { connect } = require('react-redux')

const application = require('application')
const selectors = require('selectors')
const { ActionButton } = require('components/ActionButton')
const { PriceModule } = require('components/price-module/PriceModule')

const getCustomizeButton = (customizeURL) => {
  return <ActionButton className="customize-btn" title="Customize Order" action={() => application.push(customizeURL)}/>
}

class CartPopover extends React.Component {
  static propTypes =
    { customizeURL: PropTypes.string.isRequired
    , cartProducts: PropTypes.object
    , width: PropTypes.number.isRequired
    , overrideOverlayPosition: PropTypes.bool
    }

  removeProductHandler(productId) {
    return event => {
      event.preventDefault()
      this.props.removeFromCart(productId, false)
    }
  }

  render () {
    const customizeButton = getCustomizeButton(this.props.customizeURL)
    const cartProductLines = 0 === this.props.cartProducts.size
      ?  <span className="empty-cart-msg">Your cart is empty</span>
      : this.props.cartProducts.map(cartProduct => (
          <div key={cartProduct.id} className="cart-line">
            <div className="description">
              <span className="headline">{cartProduct.productName}</span>
              <span className="product-name">{cartProduct.serviceLevelDescription}</span>
            </div>
            {!cartProduct.promotionId
              ? (
                  <span className="remove-product">
                    <Button className="remove-btn" onClick={this.removeProductHandler(cartProduct.id)}>
                      <i className="glyphicon glyphicon-remove-circle"></i>
                    </Button>
                  </span>
                )
              : null
            }
            <div className="clear"/>
          </div>
        ))

    /* style prop here is required so that the children can pick style values from the Overlay */
    const { overrideOverlayPosition, style, width, otherProps} = this.props
    let leftPosition = overrideOverlayPosition ? style.left : style.left - width/2
    return (
      <div style={{ width, left: leftPosition, top: style.top }} className="cart-drop-down">
        <div className="cart-tag">
          <span className="tag-border"></span>
          <span className="tag-mark"></span>
          <span className="tag-icon zmdi zmdi-shopping-cart"></span>
        </div>
        <div className="title">MY CART</div>
        <hr />
        <div className="product-details">
          {cartProductLines}
        </div>
        <hr />
        <div className="bottom-box">
          {customizeButton}
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  const cartProducts = selectors.selectCartProducts(state)

  return { cartProducts }
}

const mapDispatchToProps =
{ removeFromCart: application.removeFromCart
}

const CartPopoverContainer = connect(mapStateToProps, mapDispatchToProps)(CartPopover)

module.exports =
{ CartPopover
  , CartPopoverContainer
}
