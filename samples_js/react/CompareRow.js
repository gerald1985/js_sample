const { PropTypes } = require('prop-types')
const React = require('react')
const {Button} = require('react-bootstrap')

const { PriceModule } = require('../price-module/PriceModule')

const namespace = 'compare-row'

class CompareRow extends React.Component {
  static propTypes =
    { buttonCallBack: PropTypes.func
    , cellClass: PropTypes.string
    , products: PropTypes.array.isRequired
    , rowClass: PropTypes.string
    , rowLabel: PropTypes.object.isRequired
    , rowType: PropTypes.string.isRequired
    }

  constructor(props){
    super(props)

    this.generateGenericColumnCell = this.generateGenericColumnCell.bind(this)
    this.generateGenericHeaderCell = this.generateGenericHeaderCell.bind(this)
    this.generateButtonCell = this.generateButtonCell.bind(this)

    this.placeHolderCallback = this.placeHolderCallback.bind(this)
  }

  placeHolderCallback() {
    console.warn('a button callback was not defined for the compare row')
    return false
  }

  generateButtonCell(cssClass, nodeKey, productIndex) {
    const callback = this.props.buttonCallBack
                      ? () => this.props.buttonCallBack(productIndex)
                      : () => this.placeHolderCallback()

    return <td key={nodeKey}><Button onClick={callback}>Add To Cart</Button></td>
  }

  generateGenericColumnCell(cssClass, nodeKey, cellValue) {
    if(this.props.rowLabel.attribute === 'introductoryPrice') {
      return <td className={cssClass} key={nodeKey}><PriceModule splitPrice={true} /></td>
    } else {
      return <td className={cssClass} key={nodeKey}>{cellValue}</td>
    }
  }

  generateGenericHeaderCell(cssClass, nodeKey, cellValue) {
    return <th className={cssClass} key={nodeKey}>{cellValue}</th>
  }

  render() {
    const rowClass = this.props.rowClass ? this.props.rowClass : ''
    const cellClass = this.props.cellClass ? this.props.cellClass : ''

    return (
        <tr className={rowClass}>
          {
            this.props.products.map((product, index) => {
              const cellValue = product[this.props.rowLabel.attribute] ? product[this.props.rowLabel.attribute] : ''

              if(this.props.rowType === 'header') {
                return this.generateGenericHeaderCell(cellClass, this.props.rowLabel.attribute+index, cellValue)
              } else if (this.props.rowType === 'button') {
                return this.generateButtonCell(cellClass, this.props.rowLabel.attribute+index, index)
              } else {
                if(product.compareMatrix) {
                    return product.compareMatrix.map((productDetails, detailsIndex) => {
                      if (productDetails.label === this.props.rowLabel.attribute) {
                        const detailsCellValue = productDetails.value ? productDetails.value : 'N/A'
                        return this.generateGenericColumnCell(cellClass, this.props.rowLabel.attribute+index+detailsIndex, detailsCellValue)
                      }
                    })
                } else {
                  return <td key={`${this.props.rowLabel.attribute}${index}`} className={cellClass}></td>
                }
              }
            })
          }
        </tr>
    )
  }
}

module.exports =
{CompareRow
}
